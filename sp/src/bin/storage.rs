extern crate edsu_common;
#[macro_use]
extern crate edsu_server;
use edsu_server::ext::{self, Request};

fn main() {
    ext::handler(&|req: Request| {
        if let ext::Op::NameGet(name) = req.op {
            e_info!("Base sp ext got: {:?}", name);
            return Ok(Some(ext::ok_block_put()));
        } else {
            e_err!("unknown op: {:?}", req.op);
            return Ok(None);
        }
        /*

                    NAME_OMNI_NAME_DELETE => {
            let result_hash = omni_name_delete(db, hash)?;
            util::name_put_clobber(db, &result_name, &result_hash)?;
        },
        NAME_OMNI_DISK_USAGE_DETAILS => {
            let result_hash = omni_disk_usage_details(db)?;
            util::name_put_clobber(db, &result_name, &result_hash)?;
        },

        pub const NAME_PASSWORD_RESET_REQ: &'static str = "pub.srv.authentication.secret-reset-request";


        */
    })
}

/*


fn omni_name_delete(db: &mut Db, hash: &Multihash) -> Result<Multihash, Error> {
    let eson = Eson::from_bytes(db.block_get(hash)?.get_contents())?;
    let name_vals = eson.get_req("names")?;
    let mut not_deleted = Vec::new();
    for val in name_vals {
        if let Ok(name) = Name::from_value(val.clone()) {
            if !db.name_del_clobber(&name)? {
                not_deleted.push(val.clone());
            }
        } else {
            not_deleted.push(val.clone());
        }
    }
    let mut eson = name_block_eson_defaults(true, Some(DEFAULT_SRV_NAME_TTL));
    eson.insert(key!("not-deleted"), not_deleted);
    Ok(util::put_eson_block(db, &eson)?)
}


fn omni_disk_usage_details(db: &mut Db) -> Result<Multihash, Error> {
    let mut hash_to_info = HashMap::new();
    let mut cur_id_num = 0;

    // Make a more compact (less secret leaking) representation for the hashes
    let mut gen_new_id = || {
        cur_id_num += 1;
        let mut bytes = [0; 4];
        bytes[0] = cur_id_num as u8;
        bytes[1] = (cur_id_num >> 8) as u8;
        bytes[2] = (cur_id_num >> 16) as u8;
        bytes[3] = (cur_id_num >> 24) as u8;
        bs58::encode(&bytes).into_string()
    };

    // Grab every hash recursively for every name, convert to IDs
    let mut names_val_vec = Vec::new();
    for name in db.all_names()? {
        let name_hash = db.name_get(&name, false)?.0;
        let mut all_hashes = db.all_block_hashes_from_root(&name_hash)?;
        let mut line = name.into_string();
        for hash in all_hashes.drain() {
            let id = hash_to_info.entry(hash.clone()).or_insert_with(|| {
                let len = db.block_get_len(&hash);
                let id = gen_new_id();
                (id, len)
            });
            line += " ";
            line += &id.0;
        }
        names_val_vec.push(Value::from_bytes(line.as_bytes())?);
    }

    // Make a giant ESON with all of this info
    let mut details_val_vec = Vec::new();
    for (_, (id, len_maybe)) in hash_to_info.drain() {
        let len = len_maybe?;
        details_val_vec.push(Value::from_bytes(format!("{} len:{}", id, len).as_bytes())?);
    }

    let mut giant_eson = Eson::new();
    giant_eson.insert(key!("details"), details_val_vec);
    giant_eson.insert(key!("names"), names_val_vec);

    // Convert to blocks
    let mut bytes = Vec::new();
    giant_eson.write_into(&mut bytes).unwrap();
    let mut block_hashes = Vec::new();
    for chunk in bytes.chunks(62 * 1024) {
        let salt = util::gen_salt();
        let block = Block::from_parts(Vec::new(), chunk, salt.as_bytes())?;
        let hash = Multihash::from_source(block.as_bytes());
        db.block_put(hash.clone(), &block)?;
        block_hashes.push(hash);
    }

    // Create the name block
    let mut name_eson = {
        let mut bs = db::BlockStoreDb::new(db);
        bs.meta_eson(&block_hashes).map_err(|e| {
            err!("BlockStore error while getting disk usage details: {:?}", e);
            Error::Srv
        })?
    };
    name_eson.insert(key!(KEY_NB_ONCE), vec![value!("true")]);
    let expires = Value::from_str(&(util::now() + DEFAULT_SRV_NAME_TTL).to_string()).unwrap();
    name_eson.insert(key!(KEY_NB_EXPIRES), vec![expires]);

    Ok(util::put_eson_block(db, &name_eson)?)
}
*/
