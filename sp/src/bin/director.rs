extern crate bs58;
extern crate rand;
#[macro_use]
extern crate edsu_server;
extern crate edsu_client;
#[macro_use]
extern crate edsu_common;
use edsu_client::blocking_conn::{self, BlockStoreConn, Conn};
use edsu_common::block_store::{self};
use edsu_common::edsu::{self, Multihash, Name, OobCode, ServerMsg, Username, Value, ValueRead};
use edsu_common::eson::{self, Eson, Key};
use edsu_server::config::{EmailService, EmailTemplate};
use edsu_server::dynamic_config::{self, UserActive, UserConfig};
use edsu_server::ext::{self, Request};
use edsu_server::prelude::*;
use edsu_server::{email, util};
use rand::Rng;
use std::{
    collections::{HashMap, HashSet},
    convert, fs, io,
    path::PathBuf,
    str::FromStr,
};

const CONFIG_PATH_STR: &str = "/usr/local/edsu/etc/director.eson";
const USER: &str = "user";
const SECRET_RESET: &str = "secret-reset";
const SIGNUP_TOKEN_LIFETIME_S: u64 = 4 * 3600;
const SECRET_RESET_TOKEN_LIFETIME_S: u64 = 4 * 3600;
const DEFAULT_CONN_BUFFER_SIZE: usize = 4096;
const NUM_ATOMIC_ATTEMPTS: u32 = 3;

#[derive(Debug)]
enum Error {
    Eson(eson::Error),
    Edsu(edsu::Error),
    Io(io::Error),
    Email(email::Error),
    BlockStore(block_store::Error),
    DynamicConfig(dynamic_config::Error),
    BlockingConn(blocking_conn::Error),
    Parse(String),
    InvalidContent(String),
    ServerBusy,
    InvalidInput(String),
    UserAlreadyExists,
    TokenExpired,
    TokenMismatch,
}
impl convert::From<Error> for String {
    fn from(e: Error) -> Self {
        format!("{:?}", e)
    }
}
impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::Edsu(e)
    }
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}
impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}
impl convert::From<email::Error> for Error {
    fn from(e: email::Error) -> Self {
        Error::Email(e)
    }
}
impl convert::From<block_store::Error> for Error {
    fn from(e: block_store::Error) -> Self {
        Error::BlockStore(e)
    }
}
impl convert::From<dynamic_config::Error> for Error {
    fn from(e: dynamic_config::Error) -> Self {
        Error::DynamicConfig(e)
    }
}
impl convert::From<blocking_conn::Error> for Error {
    fn from(e: blocking_conn::Error) -> Self {
        Error::BlockingConn(e)
    }
}

const NAME_PREFIX: &str = "pub.srv.edclave.director.";

fn main() {
    ext::handler(&|req: Request| {
        let config = read_config()?;
        if let ext::Op::NameGet(name) = req.op {
            if !name.starts_with(NAME_PREFIX) {
                e_err!("unknown name: {:?}", name);
                return Ok(None);
            }
            let short_name = name.as_str().split_at(NAME_PREFIX.len()).1;
            let mut iter = short_name.split('.');
            match iter.next().unwrap() {
                "check-availability" => {
                    if let Some(un_str) = iter.next() {
                        let unescaped = unescape_username(un_str)?;
                        if let Ok(username) = edsu::from_bytes(unescaped.as_bytes()) {
                            let available = check_availability(&config, &username)?;
                            let available_str = if available { "true" } else { "false " };
                            let mut eson = Eson::new();
                            eson.insert(key!("ok"), vec![value!("true")]);
                            eson.insert(key!("available"), vec![value!(available_str)]);
                            return Ok(Some(ext::eson_block_put(&eson, Some(""))));
                        } else {
                            return Ok(Some(ext::err_block_put(
                                "invalid-input",
                                "unable to parse username",
                            )));
                        }
                    } else {
                        return Ok(Some(ext::err_block_put(
                            "invalid-input",
                            "missing username",
                        )));
                    }
                }
                "signup-token-gen" => {
                    // Break up and parse the name
                    // - username
                    let username = if let Some(x) = iter.next() {
                        if let Ok(y) = edsu::from_bytes(unescape_username(x)?.as_bytes()) {
                            y
                        } else {
                            return Ok(Some(ext::err_block_put(
                                "invalid-input",
                                "invalid username",
                            )));
                        }
                    } else {
                        return Ok(Some(ext::err_block_put(
                            "invalid-input",
                            "missing username",
                        )));
                    };
                    // - email
                    let email = if let Some(x) = iter.next() {
                        Value::from_bytes(unescape_username(x)?.as_bytes()).unwrap()
                    } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing email")));
                    };
                    // - region
                    let region = if let Some(x) = iter.next() {
                        Value::from_bytes(x.as_bytes()).unwrap()
                    } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing region")));
                    };
                    // - plan
                    let plan = if let Some(x) = iter.next() {
                        Value::from_bytes(x.as_bytes()).unwrap()
                    } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing plan")));
                    };

                    // Try to generate signup token
                    match signup_token_gen(&config, &username, &email, plan, region) {
                        Ok(_) => {
                            e_info!("generated signup token and emailed");
                            return Ok(Some(ext::ok_block_put()));
                        }
                        Err(e) => {
                            let err;
                            let msg;
                            match e {
                                Error::UserAlreadyExists => {
                                    err = "user-already-exists";
                                    msg = "";
                                }
                                Error::Email(ee) => {
                                    e_alert!("email error while signup token-gen: {:?}", ee);
                                    err = "email-send-error";
                                    msg = "";
                                }
                                ee => {
                                    e_alert!("server error while signup token-gen: {:?}", ee);
                                    err = "server-error";
                                    msg = "";
                                }
                            }
                            return Ok(Some(ext::err_block_put(err, msg)));
                        }
                    }
                }
                "signup-token-redeem" => {
                    // Break up and parse the name
                    // - username
                    let username = if let Some(x) = iter.next() {
                        if let Ok(y) = edsu::from_bytes(unescape_username(x)?.as_bytes()) {
                            y
                        } else {
                            return Ok(Some(ext::err_block_put(
                                "invalid-input",
                                "invalid username",
                            )));
                        }
                    } else {
                        return Ok(Some(ext::err_block_put(
                            "invalid-input",
                            "missing username",
                        )));
                    };
                    // - token
                    let token = if let Some(x) = iter.next() {
                        Value::from_bytes(x.as_bytes()).unwrap()
                    } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing token")));
                    };

                    // Redeem token
                    match signup_token_redeem(&config, &username, &token) {
                        Ok(pw_token) => {
                            let mut eson = Eson::new();
                            eson.insert(key!("ok"), vec![value!("true")]);
                            eson.insert(key!("pwtk"), vec![pw_token]);
                            return Ok(Some(ext::eson_block_put(&eson, Some(""))));
                        }
                        Err(e) => {
                            let formatted;
                            let err;
                            let msg;
                            match e {
                                Error::UserAlreadyExists => {
                                    err = "user-already-exists";
                                    msg = "";
                                }
                                Error::TokenExpired => {
                                    err = "token-expired";
                                    msg = "";
                                }
                                Error::TokenMismatch => {
                                    err = "token-mismatch";
                                    msg = "";
                                }
                                Error::InvalidInput(ee) => {
                                    err = "invalid-input";
                                    formatted = format!("{:?}", ee);
                                    msg = &formatted;
                                }
                                ee => {
                                    e_alert!("server error while signup token-redeem: {:?}", ee);
                                    err = "server-error";
                                    msg = "";
                                }
                            }
                            return Ok(Some(ext::err_block_put(err, msg)));
                        }
                    }
                }
                "secret-reset-request" => {
                    // Break up and parse the name
                    // - username
                    let username = if let Some(x) = iter.next() {
                        if let Ok(y) = edsu::from_bytes(unescape_username(x)?.as_bytes()) {
                            y
                        } else {
                            return Ok(Some(ext::err_block_put(
                                "invalid-input",
                                "invalid username",
                            )));
                        }
                    } else {
                        return Ok(Some(ext::err_block_put(
                            "invalid-input",
                            "missing username",
                        )));
                    };

                    // Send reset
                    match secret_reset_request(&config, &username) {
                        Ok(_) => {
                            return Ok(Some(ext::ok_block_put()));
                        }
                        Err(e) => {
                            let err;
                            let msg;
                            match e {
                                ee => {
                                    e_alert!("server error while secret-reset-request: {:?}", ee);
                                    err = "server-error";
                                    msg = "";
                                }
                            }
                            return Ok(Some(ext::err_block_put(err, msg)));
                        }
                    }
                }
                "secret-get-otp" => {
                    // - token
                    let token = if let Some(x) = iter.next() {
                        Value::from_bytes(x.as_bytes()).unwrap()
                    } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing token")));
                    };
                    // Grab from db
                    match SecretResetRecord::from_db(&config, &token) {
                        Ok(rec) => {
                            let mut eson = Eson::new();
                            eson.insert(key!("ok"), vec![value!("true")]);
                            eson.insert(key!("otp"), vec![rec.otp]);
                            return Ok(Some(ext::eson_block_put(&eson, Some(""))));
                        }
                        Err(e) => {
                            let formatted;
                            let err;
                            let msg;
                            match e {
                                Error::TokenExpired => {
                                    err = "token-expired";
                                    msg = "";
                                }
                                Error::TokenMismatch => {
                                    err = "token-mismatch";
                                    msg = "";
                                }
                                Error::InvalidInput(ee) => {
                                    err = "invalid-input";
                                    formatted = format!("{:?}", ee);
                                    msg = &formatted;
                                }
                                ee => {
                                    e_alert!("server error while secret-get-otp: {:?}", ee);
                                    err = "server-error";
                                    msg = "";
                                }
                            }
                            return Ok(Some(ext::err_block_put(err, msg)));
                        }
                    }
                }
                "secret-set-by-token" => {
                    // - token
                    let token_id = if let Some(x) = iter.next() {
                        Value::from_bytes(x.as_bytes()).unwrap()
                    } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing token")));
                    };
                    // Grab from db
                    let token_rec = match SecretResetRecord::from_db(&config, &token_id) {
                        Ok(x) => x,
                        Err(e) => {
                            let formatted;
                            let err;
                            let msg;
                            match e {
                                Error::TokenExpired => {
                                    err = "token-expired";
                                    msg = "";
                                }
                                Error::TokenMismatch => {
                                    err = "token-mismatch";
                                    msg = "";
                                }
                                Error::InvalidInput(ee) => {
                                    err = "invalid-input";
                                    formatted = format!("{:?}", ee);
                                    msg = &formatted;
                                }
                                ee => {
                                    e_alert!("server error while secret-set-by-token: {:?}", ee);
                                    err = "server-error";
                                    msg = "";
                                }
                            }
                            return Ok(Some(ext::err_block_put(err, msg)));
                        }
                    };
                    // - secret
                    let otp = if let Ok(x) = bs58::decode(token_rec.otp.as_bytes()).into_vec() {
                        x
                    } else {
                        e_alert!("invalid b58 encoding for OTP: {:?}", token_rec.username);
                        return Ok(Some(ext::err_block_put("server-error", "")));
                    };
                    let secret_maybe = iter
                        .next()
                        .and_then(|b58_encoded| {
                            bs58::decode(b58_encoded.as_bytes()).into_vec().ok()
                        })
                        .and_then(|mut cipher_text| {
                            if cipher_text.len() != otp.len() {
                                None
                            } else {
                                // XOR
                                for i in 0..cipher_text.len() {
                                    cipher_text[i] ^= otp[i];
                                }
                                // UTF8 decode
                                String::from_utf8(cipher_text).ok()
                            }
                        });
                    let secret_w_whitespace = if let Some(x) = secret_maybe {
                        x
                    } else {
                        return Ok(Some(ext::err_block_put(
                            "invalid-input",
                            "missing or invalid secret",
                        )));
                    };
                    // Set the secret
                    match secret_set(&config, &token_rec.username, secret_w_whitespace.trim()) {
                        Ok(_) => {
                            SecretResetRecord::remove(&config, &token_id)?;
                            return Ok(Some(ext::ok_block_put()));
                        }
                        Err(e) => {
                            let err;
                            let msg;
                            match e {
                                ee => {
                                    e_alert!("server error while secret-reset-request: {:?}", ee);
                                    err = "server-error";
                                    msg = "";
                                }
                            }
                            return Ok(Some(ext::err_block_put(err, msg)));
                        }
                    }
                }
                _ => {
                    e_err!("unknown sub-name: {:?}", short_name);
                    return Ok(None);
                }
            }
        } else {
            e_err!("unknown op: {:?}", req.op);
            return Ok(None);
        }
    });
}

fn unescape_username(text: &str) -> Result<String, Error> {
    let bytes = bs58::decode(text.as_bytes())
        .into_vec()
        .map_err(|_| Error::InvalidInput("invalid b58 encoding of username/email".to_owned()))?;
    Ok(String::from_utf8_lossy(&bytes).into_owned())
}

fn check_availability(config: &Config, username: &Username) -> Result<bool, Error> {
    if config.reserved_user_ids.contains(&username.id) {
        return Ok(false);
    }
    Ok(match db_get(config, USER, &username.to_fs_safe_string()) {
        Err(Error::Io(ref e)) if e.kind() == io::ErrorKind::NotFound => true,
        Ok(x) => match UserRecord::from_bytes(&x)? {
            UserRecord::User { .. } => false,
            UserRecord::SignUpToken { expires, .. } => {
                if util::now() > expires {
                    db_del(config, USER, &username.to_fs_safe_string())?;
                    true
                } else {
                    false
                }
            }
        },
        Err(e) => Err(e)?,
    })
}

fn signup_token_gen(
    config: &Config,
    username: &Username,
    email: &Value,
    plan: Value,
    region: Value,
) -> Result<(), Error> {
    // This creates a very unlikely race condition with a mild failure mode
    if !check_availability(&config, &username)? {
        return Err(Error::UserAlreadyExists);
    }

    // Generate token
    let token = rand_value();

    // Store the signup token
    let rec = UserRecord::SignUpToken {
        expires: util::now() + SIGNUP_TOKEN_LIFETIME_S,
        token: token.clone(),
        email: email.clone(),
        plan,
        region,
    };
    db_put(
        &config,
        USER,
        &username.to_fs_safe_string(),
        &rec.to_bytes(),
        true,
    )?;

    // Send the sign-up email
    let mut args = HashMap::new();
    let un_enc = bs58::encode(username.to_string().as_bytes()).into_string();
    let url = format!(
        "https://edclave.com/validate-email/?tk={}&un={}",
        token.as_str(),
        un_enc
    );
    args.insert("username".to_owned(), username.to_string());
    args.insert("url".to_owned(), url);

    email::send_email(
        &config.email_service,
        &config.signup_template,
        email.as_str(),
        &args,
    )?;
    Ok(())
}

fn signup_token_redeem(
    config: &Config,
    username: &Username,
    token: &Value,
) -> Result<Value, Error> {
    let rec = UserRecord::from_bytes(&db_get(&config, USER, &username.to_fs_safe_string())?)?;
    match rec {
        UserRecord::SignUpToken {
            expires,
            token: rec_token,
            email,
            plan,
            region,
        } => {
            // Make sure this token's still valid
            if expires < util::now() {
                return Err(Error::TokenExpired);
            }

            // Make sure it matches
            if *token != rec_token {
                return Err(Error::TokenMismatch);
            }

            // All good, create the user!
            if let Some(config_user) = config.region_routes.get(region.as_str()) {
                let cu_token = config.config_users[config_user].token.clone();
                user_create(&username, config_user, cu_token, email, plan)?;
                // User created, update our db
                let new_rec = UserRecord::User {
                    config_acct: config_user.clone(),
                };
                db_put(
                    &config,
                    USER,
                    &username.to_fs_safe_string(),
                    &new_rec.to_bytes(),
                    false,
                )?;

                // Return the success
                let (_, pw_token) = SecretResetRecord::create(&config, &username)?;
                Ok(pw_token)
            } else {
                return Err(Error::InvalidInput(format!(
                    "region does not exist: {:?}",
                    region
                )));
            }
        }
        _ => Err(Error::UserAlreadyExists),
    }
}

fn user_create(
    username: &Username,
    config_user: &Username,
    cu_token: Value,
    email: Value,
    plan: Value,
) -> Result<(), Error> {
    // Connect to the edsud
    let mut conn = Conn::create(
        config_user.clone(),
        Some(cu_token),
        None,
        DEFAULT_CONN_BUFFER_SIZE,
    )?;

    // Generate the user config value and put it
    let uc_block = UserConfig {
        username: username.clone(),
        active: UserActive::Active,
        plan,
        secret: None,
        email,
    }
    .into_block();
    let uc_hash = conn.block_put(&uc_block)?;
    user_config_put(NUM_ATOMIC_ATTEMPTS, &mut conn, &username, &Some(uc_hash))?;
    Ok(())
}

fn user_config_put(
    num_tries: u32,
    conn: &mut Conn,
    username: &Username,
    uc_hash_maybe: &Option<Multihash>,
) -> Result<(), Error> {
    // Try several times, in case it's busy and the atomic update fails
    let mut success = false;
    for _ in 0..num_tries {
        let dc_hash = conn.name_get_req(&name!(NAME_DYNAMIC_CONFIG))?;
        let new_dc_hash = {
            let mut bs = BlockStoreConn::new(conn);
            dynamic_config::user_config_put(
                &mut bs,
                &dc_hash,
                username.clone(),
                uc_hash_maybe.clone(),
            )?
        };
        // Try to update the dynamic config
        match conn.name_put(&name!(NAME_DYNAMIC_CONFIG), &new_dc_hash, Some(&dc_hash)) {
            Ok(_) => {
                success = true;
                break;
            }
            Err(blocking_conn::Error::Oob(ServerMsg::Oob { ref code, .. }))
                if code == &OobCode::HashMismatch =>
            {
                continue;
            }
            e => e?,
        }
    }

    if !success {
        return Err(Error::ServerBusy);
    }

    // Tell the server to refresh the dynamic config (since we just changed it)
    conn.name_get_req(&name!(NAME_REFRESH_DYNAMIC_CONFIG))?;

    Ok(())
}

fn user_config_get(conn: &mut Conn, username: &Username) -> Result<Option<UserConfig>, Error> {
    let dc_hash = conn.name_get_req(&name!(NAME_DYNAMIC_CONFIG))?;
    let mut bs = BlockStoreConn::new(conn);
    Ok(dynamic_config::user_config_get(
        &mut bs, &dc_hash, username,
    )?)
}

fn secret_reset_request(config: &Config, username: &Username) -> Result<(), Error> {
    // Get the current email
    let (uc, _) = live_uc_get(config, username)?;
    let email = uc.email;

    let (_, token) = SecretResetRecord::create(&config, username)?;

    // Send the sign-up email
    let mut args = HashMap::new();
    let url = format!(
        "https://{}.edsu.{}/edsu/auth/?op=reset&tk={}",
        username.id,
        username.domain,
        token.as_str()
    );
    args.insert("username".to_owned(), username.to_string());
    args.insert("url".to_owned(), url);

    email::send_email(
        &config.email_service,
        &config.pw_reset_template,
        email.as_str(),
        &args,
    )?;
    Ok(())
}

fn secret_set(config: &Config, username: &Username, secret: &str) -> Result<(), Error> {
    let (mut uc, mut conn) = live_uc_get(config, &username)?;

    // Update the password and update the remote storage
    uc.secret = Some(util::secret_hash(secret, &username));
    let uc_block = uc.into_block();
    let uc_hash = conn.block_put(&uc_block)?;
    user_config_put(NUM_ATOMIC_ATTEMPTS, &mut conn, &username, &Some(uc_hash))?;

    Ok(())
}

fn live_uc_get(config: &Config, username: &Username) -> Result<(UserConfig, Conn), Error> {
    // Look up the user to get which config account to connect to
    let user_rec = UserRecord::from_bytes(&db_get(config, USER, &username.to_fs_safe_string())?)?;
    let config_un = if let UserRecord::User { config_acct } = user_rec {
        config_acct
    } else {
        return Err(Error::InvalidContent(format!(
            "user not found in local db: {:?}",
            username
        )));
    };
    let config_rec = if let Some(x) = config.config_users.get(&config_un) {
        x
    } else {
        return Err(Error::InvalidContent(format!(
            "config user {:?} not found in local db for user: {:?}",
            config_un, username
        )));
    };

    // Open the conn and grab the existing user config
    let mut conn = Conn::create(
        config_un.clone(),
        Some(config_rec.token.clone()),
        None,
        DEFAULT_CONN_BUFFER_SIZE,
    )?;
    let uc = if let Some(x) = user_config_get(&mut conn, username)? {
        x
    } else {
        return Err(Error::InvalidContent(format!(
            "user {:?} not found in remote db accessed via: {:?}",
            username, config_un
        )));
    };
    Ok((uc, conn))
}

// ///////////////////////////////
// DB
// ///////////////////////////////

enum UserRecord {
    User {
        config_acct: Username,
    },
    SignUpToken {
        expires: u64,
        token: Value,
        email: Value,
        plan: Value,
        region: Value,
    },
}

impl UserRecord {
    fn from_bytes(bytes: &[u8]) -> Result<UserRecord, Error> {
        let eson = Eson::from_bytes(bytes)?;
        let type_ = eson.get_req_single("type")?.as_str();
        match type_ {
            "user" => {
                let config_acct_val = eson.get_req_single("config-acct")?.clone();
                let config_acct = Username::from_value(config_acct_val)?;
                Ok(UserRecord::User { config_acct })
            }
            "sign-up-token" => {
                let expires_val = eson.get_req_single("expires")?;
                let expires = expires_val
                    .as_str()
                    .parse::<u64>()
                    .map_err(|_| Error::InvalidContent("invalid number".to_owned()))?;
                let token = eson.get_req_single("token")?.clone();
                let email = eson.get_req_single("email")?.clone();
                let plan = eson.get_req_single("plan")?.clone();
                let region = eson.get_req_single("region")?.clone();
                Ok(UserRecord::SignUpToken {
                    expires,
                    token,
                    email,
                    plan,
                    region,
                })
            }
            _ => Err(Error::InvalidContent(format!(
                "unknown record type: {:?}",
                type_
            ))),
        }
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut eson = Eson::new();
        match self {
            UserRecord::User { ref config_acct } => {
                eson.insert(key!("type"), vec![Value::from_bytes(b"user").unwrap()]);
                eson.insert(key!("config-acct"), vec![config_acct.to_value()]);
            }
            UserRecord::SignUpToken {
                expires,
                token,
                email,
                plan,
                region,
            } => {
                eson.insert(
                    key!("type"),
                    vec![Value::from_bytes(b"sign-up-token").unwrap()],
                );
                eson.insert(key!("expires"), vec![util::int_to_val(*expires)]);
                eson.insert(key!("token"), vec![token.clone()]);
                eson.insert(key!("email"), vec![email.clone()]);
                eson.insert(key!("plan"), vec![plan.clone()]);
                eson.insert(key!("region"), vec![region.clone()]);
            }
        }
        let mut bytes = Vec::new();
        eson.write_into(&mut bytes).unwrap();
        bytes
    }
}

fn otp_gen() -> Value {
    let mut bytes = [0; 64];
    let mut rng = rand::thread_rng();
    rng.fill_bytes(&mut bytes);
    Value::from_bytes(bs58::encode(&bytes.to_vec()).into_string().as_bytes()).unwrap()
}

struct SecretResetRecord {
    username: Username,
    otp: Value,
}

impl SecretResetRecord {
    fn create(config: &Config, username: &Username) -> Result<(SecretResetRecord, Value), Error> {
        let token = rand_value();
        let expires = util::now() + SECRET_RESET_TOKEN_LIFETIME_S;
        let rec = SecretResetRecord {
            username: username.clone(),
            otp: otp_gen(),
        };

        // Convert to bytes
        let mut eson = Eson::new();
        eson.insert(key!("username"), vec![edsu::to_value(username)]);
        eson.insert(key!("expires"), vec![util::int_to_val(expires)]);
        eson.insert(key!("otp"), vec![rec.otp.clone()]);
        let mut bytes = Vec::new();
        eson.write_into(&mut bytes).unwrap();

        db_put(config, SECRET_RESET, token.as_str(), &bytes, true)?;
        Ok((rec, token))
    }
    fn from_db(config: &Config, token: &Value) -> Result<SecretResetRecord, Error> {
        let bytes = db_get(config, SECRET_RESET, token.as_str()).map_err(|e| match e {
            Error::Io(ref e) if e.kind() == io::ErrorKind::NotFound => Error::TokenMismatch,
            e => e,
        })?;
        let eson = Eson::from_bytes(&bytes)?;
        let expires = eson.get_req_single_parsed("expires", &|x| x.as_str().parse::<u64>())?;
        if expires < util::now() {
            return Err(Error::TokenExpired);
        }
        let username = eson
            .get_req_single_parsed("username", &|x| edsu::from_bytes::<Username>(x.as_bytes()))?;

        Ok(SecretResetRecord {
            username,
            otp: eson.get_req_single("otp")?.clone(),
        })
    }
    fn remove(config: &Config, token: &Value) -> Result<(), Error> {
        db_del(config, SECRET_RESET, token.as_str())
    }
}

// ///////////////////////////////
// Db
// ///////////////////////////////

fn db_path(config: &Config, kind: &str, id: &str) -> Result<PathBuf, Error> {
    let mut path = config.db_root.clone();
    path.push(kind);
    // Validate ID (needs to be filesystem safe)
    for ch in id.chars() {
        if !((ch >= 'A' && ch <= 'Z')
            || (ch >= 'a' && ch <= 'z')
            || (ch >= '0' && ch <= '9')
            || (ch == '-' || ch == '_' || ch == '.'))
        {
            return Err(Error::InvalidInput("invalid token format".to_owned()));
        }
    }
    path.push(id);
    Ok(path)
}

fn db_put(config: &Config, kind: &str, id: &str, bytes: &[u8], atomic: bool) -> Result<(), Error> {
    let path = db_path(config, kind, id)?;
    let parent = path.parent().unwrap();

    if !parent.is_dir() {
        fs::DirBuilder::new().recursive(true).create(&parent)?;
    }

    if atomic {
        util::bytes_to_new_file(&path, &bytes, None)?;
        Ok(())
    } else {
        util::bytes_to_file(&path, &bytes, None)?;
        Ok(())
    }
}

fn db_get(config: &Config, kind: &str, id: &str) -> Result<Vec<u8>, Error> {
    let path = db_path(config, kind, id)?;
    Ok(util::file_to_bytes(&path)?)
}

fn db_del(config: &Config, kind: &str, id: &str) -> Result<(), Error> {
    let path = db_path(config, kind, id)?;
    fs::remove_file(&path)?;
    Ok(())
}

// ///////////////////////////////
// Util
// ///////////////////////////////

fn rand_value() -> Value {
    let mut token_bytes = [0; 32];
    let mut rng = rand::thread_rng();
    rng.fill_bytes(&mut token_bytes);
    let bytes = Multihash::from_source(&token_bytes).as_bytes()[2..18].to_vec();
    Value::from_bytes(&bytes).unwrap()
}

// ///////////////////////////////
// Config file
// ///////////////////////////////

struct Config {
    pub open_domains: Vec<String>,
    pub config_users: HashMap<Username, ConfigUser>,
    pub region_routes: HashMap<String, Username>,
    pub reserved_user_ids: HashSet<String>,
    pub email_service: EmailService,
    pub signup_template: EmailTemplate,
    pub pw_reset_template: EmailTemplate,
    pub db_root: PathBuf,
}

struct ConfigUser {
    token: Value,
}

fn read_config() -> Result<Config, Error> {
    // Read in config file
    let config_bytes = util::file_to_bytes(&PathBuf::from(CONFIG_PATH_STR))?;

    // Get the ESON
    let mut eson_iter = config_bytes.iter().cloned();
    let eson = match eson::Parser::new().add_bytes(&mut eson_iter) {
        Ok(Some(x)) => Ok(x),
        Ok(None) => Err(eson::Error::InvalidEson("incomplete".to_owned())),
        Err(e) => Err(eson::Error::InvalidEson(format!("{:?}", e))),
    }?;

    // Useful functions
    fn trimmed(val: &Value) -> String {
        let (val, _) = val.clone().split_off_sep('"');
        val.as_str().trim().to_owned()
    }

    // Parse the server users
    let mut config_users: HashMap<Username, ConfigUser> = HashMap::new();
    for val in eson.get_req("config-users")? {
        let (sing, comp) = util::decode_flags(val.as_str());
        if sing.len() != 1 || !comp.contains_key("token") {
            return Err(Error::Parse(format!(
                "config-user line: {:?}",
                val.as_str()
            )));
        }
        config_users.insert(
            edsu::from_bytes(sing[0].as_bytes())?,
            ConfigUser {
                token: Value::from_bytes(comp["token"].as_bytes())?,
            },
        );
    }

    // The region routes
    let mut region_routes = HashMap::new();
    let (_, comps) = util::decode_flags(eson.get_req_single("region-routes")?.as_str());
    for (region, cu) in comps {
        let username = edsu::from_bytes(cu.as_bytes())?;
        if !config_users.contains_key(&username) {
            return Err(Error::Parse(format!(
                "region-route config-user not found: {:?}",
                username
            )));
        }
        region_routes.insert(region.to_owned(), username);
    }

    // Other non-trival parsings
    let db_root = util::expand_tilde(&PathBuf::from(trimmed(eson.get_req_single("db-root")?)));
    let reserved_user_ids = eson
        .get_req("reserved-user-ids")?
        .iter()
        .map(|x| trimmed(x))
        .collect();
    let email_service = EmailService::from_str(&trimmed(eson.get_req_single("email-service")?))
        .map_err(|e| Error::Parse(format!("{:?}", e)))?;
    let signup_template =
        EmailTemplate::from_str(&trimmed(eson.get_req_single("email-template-signup")?))
            .map_err(|e| Error::Parse(format!("{:?}", e)))?;
    let pw_reset_template =
        EmailTemplate::from_str(&trimmed(eson.get_req_single("email-template-pw-reset")?))
            .map_err(|e| Error::Parse(format!("{:?}", e)))?;

    Ok(Config {
        db_root,
        open_domains: eson
            .get_req("open-domains")?
            .iter()
            .map(|x| trimmed(x))
            .collect(),
        config_users,
        region_routes,
        reserved_user_ids,
        email_service,
        signup_template,
        pw_reset_template,
    })
}
