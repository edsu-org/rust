#![allow(unused_imports, dead_code)]
extern crate bs58;
extern crate edsu_client;
extern crate edsu_server;
extern crate edsu_utils;
extern crate openssl;
#[macro_use]
extern crate edsu_common;
use edsu_client::blocking_conn::{self, BlockStoreConn, Conn};
use edsu_common::block_store::{self, BlockStore};
use edsu_common::edsu::{self, Multihash, Name, UserId, Value, Token, ValueRead};
use edsu_common::eson::{self, Eson, Key};
use edsu_server::capabilities::{self, Capabilities, User, Role};
use edsu_server::config::{self, Config};
use edsu_server::db::{self, Db};
use edsu_server::dynamic_config::{self, DynamicConfig, SslCert, UserActive, UserConfig};
use edsu_server::prelude::*;
use edsu_server::util::{self};
use openssl::nid::Nid;
use openssl::pkey::{PKey, Private};
use openssl::x509::X509;
use std::{
    collections::HashSet,
    convert, env, fs,
    io::{self, BufRead, BufReader, Read, Write},
    os::unix::process::CommandExt,
    path::PathBuf,
    process::{self, Command, Stdio},
    sync::mpsc,
    thread, time,
    net::{SocketAddr, IpAddr, Ipv4Addr},
};

const DEFAULT_CONN_BUFFER_SIZE: usize = 4096;
const MONITOR_TICK_MS: u64 = 256;

#[derive(Debug)]
enum Error {
    ArgParse(String),
    InvalidInput(String),
    InvalidContent(String),
    DbAlreadyExists,
    InvalidDbContent(String),
    Db(db::Error),
    BlockStore(block_store::Error),
    DynamicConfig(dynamic_config::Error),
    Eson(eson::Error),
    Edsu(edsu::Error),
    Capabilities(capabilities::Error),
    BlockingConn(blocking_conn::Error),
    Io(io::Error),
}
impl convert::From<db::Error> for Error {
    fn from(e: db::Error) -> Self {
        Error::Db(e)
    }
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}
impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::Edsu(e)
    }
}
impl convert::From<capabilities::Error> for Error {
    fn from(e: capabilities::Error) -> Self {
        Error::Capabilities(e)
    }
}
impl convert::From<block_store::Error> for Error {
    fn from(e: block_store::Error) -> Self {
        Error::BlockStore(e)
    }
}
impl convert::From<dynamic_config::Error> for Error {
    fn from(e: dynamic_config::Error) -> Self {
        Error::DynamicConfig(e)
    }
}
impl convert::From<blocking_conn::Error> for Error {
    fn from(e: blocking_conn::Error) -> Self {
        Error::BlockingConn(e)
    }
}
impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

const HELP_TXT: &str = "\
\nControls the Edsu daemon
    Arguments are one of:
        start [config_file]
            Starts the Edsu daemon - defaults to trying
            /etc/edsu/config.toml, /usr/local/etc/edsu/config.toml,
            then /usr/local/edsu/etc/config.toml if config_file is omitted
            (as with all the [config_file] args below)

        stop [config_file]
            Stops the running Edsu daemon.

        status [config_file]
            Reports on whether the Edsu daemon is running.  Sets $? to
            0 if so, 1 if not

        list-users [config_file]
            Lists all the users

        bootstrap config_file key_file cert_chain_file [debug]
            Creates the minimal database if it doesn't exist already.  Prints
            out the config account omni token.  Note that config_file is
            required in this case.  Key_file is a .pem file of the initial
            domain's SSL private key, and cert_chain_file is the matching .pem
            chain (certificate first, concatenated with one or two intermediate
            certificates in order of leaf to root).  Debug is only for
            developers of edsud itself

        replace-cert [config_file] key_file cert_chain_file
            Replaces the key_file and cert_chain_file given in bootstrap

        user-put [config_file] user_id plan
            Creates a user, or updates an existing one.  If you're unsure which
            plan to use, put \"omni\", which is unlimited bandwidth and storage

        reload [config_file]
            Gets edsud to reload its dynamic config (i.e. from its database,
            not the config_file)
";

fn main() {
    let args = env::args().collect::<Vec<String>>();

    if args.len() < 2 {
        println!("{}", HELP_TXT);
        return;
    }

    let f = || {
        match args[1].as_str() {
            /*
            "start" => {
                if args.len() == 2 {
                    start(None);
                } else if args.len() == 3 {
                    start(Some(&args[2]));
                } else {
                    println!("{}", HELP_TXT);
                    return Ok(());
                }
                Ok(())
            }
            // This isn't meant to be called by users
            "_start-forked" => {
                if args.len() == 2 {
                    start_forked(None);
                } else if args.len() == 3 {
                    start_forked(Some(&args[2]));
                } else {
                    println!("{}", HELP_TXT);
                    return Ok(());
                }
                Ok(())
            }
            "stop" => {
                if args.len() == 2 {
                    stop(None);
                } else if args.len() == 3 {
                    stop(Some(&args[2]));
                } else {
                    println!("{}", HELP_TXT);
                    return Ok(());
                }
                Ok(())
            }
            "status" => panic!("unimplemented"),
            "reload" => panic!("unimplemented"),
            */
            "list-users" => {
                if args.len() == 2 {
                    list_users(None)?;
                } else if args.len() == 3 {
                    list_users(Some(&args[2]))?;
                } else {
                    println!("{}", HELP_TXT);
                    return Ok(());
                }
                Ok(())
            }
            "bootstrap" => {
                if args.len() == 5 {
                    bootstrap(
                        &args[2],
                        &PathBuf::from(&args[3]),
                        &PathBuf::from(&args[4]),
                        None,
                    )
                } else if args.len() == 6 {
                    bootstrap(
                        &args[2],
                        &PathBuf::from(&args[3]),
                        &PathBuf::from(&args[4]),
                        Some(&args[5]),
                    )
                } else {
                    println!("{}", HELP_TXT);
                    Ok(())
                }
            }
            /*
            "replace-cert" => {
                if args.len() == 5 {
                    replace_cert(
                        Some(&args[2]),
                        &PathBuf::from(&args[3]),
                        &PathBuf::from(&args[4]),
                    )
                } else if args.len() == 4 {
                    replace_cert(None, &PathBuf::from(&args[2]), &PathBuf::from(&args[3]))
                } else {
                    println!("{}", HELP_TXT);
                    Ok(())
                }
            }
            "user-put" => {
                if args.len() == 5 {
                    user_put(
                        Some(&args[2]),
                        &args[3],
                        &args[4],
                    )
                } else if args.len() == 4 {
                    user_put(
                        None,
                        &args[2],
                        &args[3],
                    )
                } else {
                    println!("{}", HELP_TXT);
                    Ok(())
                }
            }
            */
            _ => {
                println!("{}", HELP_TXT);
                Ok(())
            }
        }
    };
    match f() {
        Ok(_) => (),
        Err(e) => println!("Error: {:?}", e),
    }
}

/*
fn start(config_file_arg: Option<&String>) -> Result<(), Error> {
    // Grab the config
    let config = Config::from_maybe_path_str(config_file_arg.map(|x| x.as_str()))
        .map_err(|e| Error::InvalidInput(format!("while opening config file: {:?}", e)))?;

    // Find the edsud exec and error if not found
    let mut edsud_path = PathBuf::from(env::current_exe().unwrap().parent().unwrap());
    edsud_path.push("edsud");
    if !edsud_path.is_file() {
        return Err(Error::InvalidInput(
            "couldn't find edsud executable - it's \
             expected to be in the same directory as \
             edsuctrl"
                .to_owned(),
        ));
    }

    // Despite libraries that claim otherwise, Rust (2018-09) can't fork safely (the
    // runtime wasn't built with that in mind), so we do a weird dance to spawn
    // ourselves, then start up the edsud deamon from the "_start-forked" process
    let mut cmd_args = vec!["_start-forked"];
    if args.len() == 3 {
        cmd_args.push(&args[2]);
    }
    Command::new(env::current_exe()?)
        .uid(0)
        .args(cmd_args)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .stdin(Stdio::null())
        .spawn()
        .expect("could not fork as root - required for obtaining port 443");
    println!(
        "Config OK, kicking off...  See the log for progress:\n\n{:?}\n",
        config.log_file
    );
    Ok(())
}

fn stop(conf_user: UserId, token: Value) -> Result<(), Error> {
    // Connect to edsud TODO: make this sane
    let mut conn = Conn::create(conf_user, Some(token), None, DEFAULT_CONN_BUFFER_SIZE)?;
    println!("Sending stop request.  Watch the logs and then ctrl-c this command (I know this is a silly \
             way to do it - it will improve)");
    conn.name_get(&name!(NAME_SERVER_EXIT))?;
    Ok(())
}
*/

fn list_users(config_file_arg: Option<&str>) -> Result<(), Error> {
    let config = Config::from_maybe_path_str(config_file_arg)
        .map_err(|e| Error::InvalidInput(format!("while opening config file: {:?}", e)))?;

    // Connect to edsud
    let token = get_config_user_token(&config)?;
    let mut hostnames = get_hostnames(&config)?;
    let hostname = hostnames.pop().unwrap_or_else(|| value!("localhost"));
    let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), DEFAULT_EDSU_PORT);

    let mut conn = Conn::create(
        &addr,
        hostname.as_str(),
        &config.config_user,
        Some(&token),
        None,
        DEFAULT_CONN_BUFFER_SIZE
    )?;
    let dyn_config = get_dyn_config(&mut conn)?;
    for uc in dyn_config.user_configs {
        println!("{} {:?}", uc.user_id.to_string(), uc.active);
    }
    Ok(())
}

fn bootstrap(
    config_file_arg: &str,
    key_path: &PathBuf,
    chain_path: &PathBuf,
    maybe_debug: Option<&String>,
) -> Result<(), Error> {
    // Grab the config
    let config = Config::from_maybe_path_str(Some(config_file_arg))
        .map_err(|e| Error::InvalidInput(format!("while opening config file: {:?}", e)))?;

    // Safety guard
    if config.db_root.is_dir() && config.db_root.read_dir().unwrap().count() != 0 {
        println!("Database directory exists and contains data already: aborting");
        return Err(Error::DbAlreadyExists);
    }

    // User configs
    println!(
        "Creating user configs for {:?} and {:?}",
        config.server_user, config.config_user
    );
    let mut user_configs = vec![
        UserConfig {
            user_id: config.config_user.clone(),
            active: UserActive::Active,
            plan: value!("omni"),
        },
        UserConfig {
            user_id: config.server_user.clone(),
            active: UserActive::Active,
            plan: value!("omni"),
        },
    ];

    match maybe_debug {
        Some(ref x) if x.as_str() == "inttest" => {
            println!("Creating user configs for 000001 and 000002");
            println!(
                "\n!!!!!!! WARNING: don't use in production - they have well-known passwords\n"
            );
            for uid_str in &[b"000256", b"000257"] {
                let uid = edsu::from_bytes(*uid_str).unwrap();
                user_configs.push(UserConfig {
                    user_id: uid,
                    active: UserActive::Active,
                    plan: value!("omni"),
                });
            }
        }
        _ => (),
    }

    // SSL certs
    let key_bytes = util::file_to_bytes(&key_path).map_err(|e| {
        Error::InvalidInput(format!(
            "while reading private key '{:?}': {:?}",
            key_path, e
        ))
    })?;
    let chain_bytes = util::file_to_bytes(&chain_path).map_err(|e| {
        Error::InvalidInput(format!(
            "while reading cert chain '{:?}': {:?}",
            chain_path, e
        ))
    })?;
    let ssl_certs = vec![SslCert::from_pems(&key_bytes, &chain_bytes).map_err(|e| {
        Error::InvalidInput(format!(
            "while decoding ssl cert {:?} and {:?}: {:?}",
            key_path, chain_path, e
        ))
    })?];

    // Get a db
    let mut db = Db::new(&config.db_root, &config.config_user);

    // Use a BlockStore to create the config block in the db
    let config_hash = {
        let mut bs = db::BlockStoreDb::new(&mut db);
        DynamicConfig {
            user_configs,
            ssl_certs,
        }
        .into_block_store(&mut bs)
    }?;

    // Name the config block
    db.name_put(&name!(NAME_DYNAMIC_CONFIG), &config_hash, None)?;

    // Create a token for the config user and give it omni caps
    let mut caps_eson = Eson::new();
    caps_eson.insert(key!(KEY_CB_VERS), vec![value!(CAPS_BLOCK_VERS)]);
    caps_eson.insert(key!(KEY_CB_OMNI), vec![value!("*")]);
    let desc = value!("From bootstrap");
    let caps_hash = util::eson_block_put(&mut db, &caps_eson)?;
    util::token_put(&mut db, false, None, desc, None, &caps_hash, true).unwrap();

    // Put the db version in
    let mut version_path = config.db_root.clone();
    version_path.push("version");
    util::bytes_to_new_file(&version_path, b"0.1", None)?;

    println!("Successfully bootstrapped!");
    Ok(())
}




/*
fn replace_cert(
    config_file_arg: Option<&String>,
    key_path: &PathBuf,
    chain_path: &PathBuf,
) -> Result<(), Error> {
    // Grab the config
    let config = Config::from_maybe_path_str(config_file_arg.map(|x| x.as_str()))
        .map_err(|e| Error::InvalidInput(format!("while opening config file: {:?}", e)))?;

    // Encode SSL certs
    let key_bytes = util::file_to_bytes(key_path).map_err(|e| {
        Error::InvalidInput(format!(
            "while reading private key '{:?}': {:?}",
            key_path, e
        ))
    })?;
    let chain_bytes = util::file_to_bytes(chain_path).map_err(|e| {
        Error::InvalidInput(format!(
            "while reading cert chain '{:?}': {:?}",
            chain_path, e
        ))
    })?;
    let ssl_certs = vec![SslCert::from_pems(&key_bytes, &chain_bytes).map_err(|e| {
        Error::InvalidInput(format!(
            "while decoding ssl cert {:?} and {:?}: {:?}",
            key_path, chain_path, e
        ))
    })?];

    // Put it in the db
    let mut db = Db::new(&config.db_root, &config.config_user);
    let dc_name = name!(NAME_DYNAMIC_CONFIG);
    let (dc_hash, _, _) = db.name_get(&dc_name, false)?;

    let new_dc_hash = {
        let mut bs = db::BlockStoreDb::new(&mut db);
        let mut dc = DynamicConfig::from_block_store(&mut bs, &dc_hash)?;
        dc.ssl_certs = ssl_certs;
        dc.into_block_store(&mut bs)?
    };
    db.name_put(&dc_name, &new_dc_hash, Some(&dc_hash))?;

    println!("New SSL cert install.  Changes will take effect after restart or reload");

    Ok(())
}

fn user_put(
    config_file_arg: Option<&String>,
    user_id: &UserId,
    plan: &str,
) -> Result<(), Error> {
    // Grab the config
    let config = Config::from_maybe_path_str(config_file_arg.map(|x| x.as_str()))
        .map_err(|e| Error::InvalidInput(format!("while opening config file: {:?}", e)))?;

    if !config.plans.contains_key(plan) {
        Err(Error::ArgParse(format!(
            "plan does not exit: {:?}.  Plan must be one of {:?}",
            plan,
            config
                .plans
                .keys()
                .chain(["omni".to_owned()].iter())
                .collect::<Vec<&String>>()
        )))?;
    }

    // Create user config
    let uc = UserConfig {
        user_id: user_id.clone(),
        active: UserActive::Active,
        plan: Value::from_bytes(plan.as_bytes()).unwrap(),
        secret: Some(util::eson_utf8_encode(secret)),
        email,
    };

    // Put it in the db
    let mut db = Db::new(&config.db_root, &config.config_user);

    let dc_name = name!(NAME_DYNAMIC_CONFIG);
    let uc_hash = util::block_put(&mut db, &uc.into_block())?;
    let (dc_hash, _, _) = db.name_get(&dc_name, false)?;

    let new_dc_hash = {
        let mut bs = db::BlockStoreDb::new(&mut db);
        dynamic_config::user_config_put(&mut bs, &dc_hash, user_id.clone(), Some(uc_hash))?
    };
    db.name_put(&dc_name, &new_dc_hash, Some(&dc_hash))?;

    println!(
        "user {:?} created.  Changes will take effect after restart or reload",
        user_id
    );

    Ok(())
}

struct FileLogger(PathBuf);

impl FileLogger {
    fn new(path: PathBuf) -> FileLogger {
        FileLogger(path)
    }
    fn log(&self, text: &str) {
        let file_maybe = fs::OpenOptions::new()
            .append(true)
            .create(true)
            .open(&self.0);
        if let Ok(mut file) = file_maybe {
            file.write_all(format!("\nedsuctrl: {:?}\n", text).as_bytes())
                .ok();
        }
    }
}

fn start_forked(config_file_arg: Option<&String>) {
    // Grab the config
    let config = Config::from_maybe_path_str(config_file_arg.map(|x| x.as_str())).unwrap();
    let mut edsud_path = PathBuf::from(env::current_exe().unwrap().parent().unwrap());
    edsud_path.push("edsud");
    let logger = FileLogger::new(config.log_file.clone());

    loop {
        // Spawn edsud
        let mut cmd_args = vec![];
        if let Some(x) = config_file_arg {
            cmd_args.push(x);
        }
        let spawn_result = Command::new(&edsud_path)
            .args(cmd_args)
            .stdin(Stdio::null())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn();
        let mut child = match spawn_result {
            Ok(x) => x,
            Err(e) => {
                logger.log(&format!("error spawning edsud: {:?}", e));
                return;
            }
        };

        // Spawn threads to monitor the pipes
        fn spawn_w_logger<T: Read + Send + 'static>(
            thread_tx: mpsc::Sender<String>,
            name: String,
            pipe: T,
        ) {
            let reader = BufReader::new(pipe);
            thread::spawn(move || {
                for line_maybe in reader.lines() {
                    if let Ok(line) = line_maybe {
                        thread_tx.send(line).ok();
                    } else {
                        thread_tx.send(format!("{} pipe exiting", name)).ok();
                        break;
                    }
                }
            });
        }
        let (tx, rx) = mpsc::channel();
        spawn_w_logger(
            tx.clone(),
            "stdout".to_owned(),
            child.stdout.take().unwrap(),
        );
        spawn_w_logger(
            tx.clone(),
            "stderr".to_owned(),
            child.stderr.take().unwrap(),
        );

        // Log until server exits, loop around and start a new one unless it wanted to exit
        loop {
            match child.try_wait() {
                Ok(None) => (),
                Ok(Some(status)) => {
                    if status.code() == Some(REQUESTED_SERVER_EXIT_CODE) {
                        logger.log("edsud exited by request");
                        return;
                    } else {
                        logger.log(&format!(
                            "edsud exited unexpectedly with code: {:?}",
                            status.code()
                        ));
                    }
                    break;
                }
                Err(e) => {
                    logger.log(&format!("error on child try_wait: {:?}", e));
                    // Make sure the log buffer's empty
                    while let Ok(st) = rx.try_recv() {
                        logger.log(&st);
                    }
                    break;
                }
            }

            // Sleep for a while before looping around
            thread::sleep(time::Duration::from_millis(MONITOR_TICK_MS));

            // Check if there's anything to log
            while let Ok(st) = rx.try_recv() {
                logger.log(&st);
            }
        }
    }
}
*/

fn get_dyn_config(conn: &mut Conn) -> Result<DynamicConfig, Error> {
    if let Some(dyn_config_hash) = conn.name_get(&name!(NAME_DYNAMIC_CONFIG))? {
        let mut bs = BlockStoreConn::new(conn);
        Ok(DynamicConfig::from_block_store(&mut bs, &dyn_config_hash)?)
    } else {
        return Err(Error::InvalidDbContent(
            "missing dynamic config name - not bootstrapped?".to_owned(),
        ));
    }
}

fn get_config_user_token(config: &Config) -> Result<Token, Error> {
    // Get a db
    let mut db = Db::new(&config.db_root, &config.config_user);
    for name in db.all_names()? {
        if name.starts_with(NAME_TOKEN_OWNER_ROOT) {
            let (_, _, token_block_eson) = db.name_get(&name, false)?;
            let caps_hash_val = token_block_eson.get_req_single(KEY_TB_CAPS)?;
            let caps_hash = Multihash::from_value(caps_hash_val.clone())?;
            let caps_eson = Eson::from_bytes(&db.block_get(&caps_hash)?.get_contents())?;
            let caps = Capabilities::from_eson(Role::None, User::Anon, None, &caps_eson)?;
            if caps.is_omni() {
                let (_, text) = name.as_str().split_at(NAME_TOKEN_OWNER_ROOT.len());
                let token = Token::from_value(Value::from_bytes(text.as_bytes())?)?;
                return Ok(token);
            }
        }
    }
    Err(Error::InvalidContent("could not find the config user's omni token".to_owned()))
}

fn get_hostnames(config: &Config) -> Result<Vec<Value>, Error> {
    // Get a db
    let mut hostnames = Vec::new();
    let mut db = Db::new(&config.db_root, &config.config_user);

    // Get the dynamic config
    let dc_name = name!(NAME_DYNAMIC_CONFIG);
    let (dc_hash, _, _) = db.name_get(&dc_name, false)?;
    let mut bs = db::BlockStoreDb::new(&mut db);
    let dc = DynamicConfig::from_block_store(&mut bs, &dc_hash)?;
    for cert in dc.ssl_certs {
        hostnames.extend(cert.hostnames);
    }
    Ok(hostnames)
}
