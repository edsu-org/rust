import ssl, socket, io, base58, time, traceback, sys, websocket, json, textwrap, re
from collections import defaultdict

TestFns = defaultdict(lambda: [])
def T(timered=False, die=False):
    def _T(fn):
        if not fn.__name__.startswith('t_'):
            raise Exception('Test functions start with t_')
        section, name = fn.__name__[2:].split('_', 1)
        TestFns[section].append({
            'name': name,
            'timered': timered,
            'die': die,
            'fn': fn,
        })
        return fn
    return _T

'''
The Edsu Protocol, DRAFT
Version 0.1, Rev. B
2018-11-04


=ESON

The ESON format underlies much of Edsu.  It doesn't stand for anything: it's a pun on JSON, with Edsu's E substituted.  It was created to be simpler to emit and parse than JSON, but still be able to express the minimum amount of structure required by the higher-level protocols in Edsu that use it.


==Format

An ESON document is composed of zero or more item lines, followed by an empty line.  Each item line is composed of four parts:

┏━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━━━━━┓
┃ key ┃ space ┃ value ┃ line feed ┃
┗━━━━━┻━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┛

The key is one of either:

A) A caret: ^ (i.e. the ASCII value 0x5E), or
B) One or more characters from the set: abcdefghijklmnopqrstuvwxyz0123456789:-

In the case of B, keys cannot begin or end with a colon, nor can two or more colons be directly beside each other.

'''

ESON_KEY_VALID_CHARS = [ord(x) for x in 'abcdefghijklmnopqrstuvwxyz0123456789:-']

ESON_KEYS_VALID = [
    b'a',
    b'-',
    b'--',
    b'0',
    b'a:b',
    b'a:b:c',
    b'ab:bc:cd',
    b'-:-:-',
    b'abcdefghijklmnopqrstuvwxyz0123456789:-',
    bytes(''.join(['a'] * 4097), 'utf8'),
]

ESON_KEYS_VALID_EX = list(ESON_KEYS_VALID)

ESON_KEYS_INVALID = [
    b':a',
    b':a:',
    b'a:',
    b'a::b',
    b'a:::b',
    b'A',
    b'a.b',
    b'^b',
    b'b ',
    b' b',
    b'*',
]

ESON_KEYS_INVALID_EX = list(ESON_KEYS_VALID)
ESON_KEYS_INVALID_EX = [b'a' + bytes([x]) + b'b' for x in range(256) if x not in ESON_KEY_VALID_CHARS]
ESON_KEYS_INVALID_EX += [bytes([x]) + b'b' for x in range(256) if x not in ESON_KEY_VALID_CHARS]
ESON_KEYS_INVALID_EX += [b'a' + bytes([x]) for x in range(256) if x not in ESON_KEY_VALID_CHARS]


'''
The space is the ASCII value 0x20.

The value is zero or more characters from the set of 0x20 to 0x7e (inclusive) in ASCII, i.e. all non-control-code characters from 7-bit ASCII.
'''

ESON_VALUE_VALID_CHARS = list(range(0x20, 0x7e+1))
ESON_VALUES_VALID = [
    b'',
    bytes(''.join(chr(x) for x in ESON_VALUE_VALID_CHARS), 'utf8'),
    b'::',
    bytes(''.join(['a'] * 4097), 'utf8'),
]
ESON_VALUES_VALID_EX = list(ESON_VALUES_VALID)

ESON_VALUES_INVALID = [
    b'a\bb',
    bytes('大', 'utf8'),
    b'\r',
    b'hi\x7fm',
]

ESON_VALUES_INVALID_EX = list(ESON_VALUES_INVALID)
ESON_VALUES_INVALID_EX = [b'a' + bytes([x]) + b'b' for x in range(256) if x not in ESON_VALUE_VALID_CHARS]
ESON_VALUES_INVALID_EX += [bytes([x]) + b'b' for x in range(256) if x not in ESON_VALUE_VALID_CHARS]
ESON_VALUES_INVALID_EX += [b'a' + bytes([x]) for x in range(256) if x not in ESON_VALUE_VALID_CHARS]

'''
The line feed is the ASCII value 0x0A.

Whitespace is strict - the space and line feed characters cannot be substituted with any other character.  If a key is followed by two or more spaces, all but the first space becomes the initial part of the value.  Carriage return characters (ASCII 0x0D) are invalid anywhere in an ESON document.

There are no upper length restrictions on keys, values, or the number of lines.

Unless there is a specific reason not to, item lines should be sorted in ascending order, without regard to the meaning of the characters (i.e. a binary comparison, e.g. case insensitive).

The final, empty, line that signals the end of the ESON document is a single ASCII value 0x0A.  For example, here's a complete document with a single item:

┏━━━━━┳━━━━━━━┳━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━━┓
┃ key ┃ space ┃ value ┃ line feed ┃ line feed ┃
┗━━━━━┻━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━━┛

And here's an ESON document with no items:

┏━━━━━━━━━━━┓
┃ line feed ┃
┗━━━━━━━━━━━┛

'''

ESONS_VALID = [
    b'\n',
    b'a b\n\n',
    b'a b\nc d\n\n',
    b'a b\n^ d\n\n',
    b'a b\n^ d\na c\n\n',
]
ESONS_VALID_EX = list(ESONS_VALID)
for x in ESON_KEYS_VALID_EX:
    ESONS_VALID_EX.append(x + b' a\n\n')
for x in ESON_VALUES_VALID_EX:
    ESONS_VALID_EX.append(b'a ' + x + b'\n\n')


ESONS_INVALID_CORE = [
    b'a\nb c\n',
    b'^ c\n',
]
ESONS_INVALID_CORE_EX = list(ESONS_INVALID_CORE)
for x in ESON_KEYS_INVALID_EX:
    ESONS_INVALID_CORE_EX.append(x + b' a\n')
for x in ESON_VALUES_INVALID_EX:
    ESONS_INVALID_CORE_EX.append(b'a ' + x + b'\n')

ESONS_INVALID_PARSER_HANG = [
    b'',
    b'a',
    b'a b',
    b'a b\n',
]

ESONS_INVALID = ESONS_INVALID_CORE + ESONS_INVALID_PARSER_HANG
ESONS_INVALID_EX = ESONS_INVALID_CORE_EX + ESONS_INVALID_PARSER_HANG


@T()
def t_eson_valid():
    conn = Conn('test1@example.com', auth='omni')
    valids = ESONS_VALID_EX if is_exhaustive() else ESONS_VALID
    for valid_eson in valids:
        data = b'~\n' + valid_eson
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'
        hash = resp['hash']

        call_eson = {
            'edsu:blocks': hash,
        }
        resp = conn.srv_call('pub.srv.inttest.actions.eson-test', call_eson, except_on_err=False)
        assert resp.get('ok') == 'true'
        assert resp.get('eson-valid') == 'true'

@T()
def t_eson_invalid():
    conn = Conn('test1@example.com', auth='omni')
    valids = ESONS_INVALID_EX if is_exhaustive() else ESONS_INVALID
    for valid_eson in valids:
        data = b'~\n' + valid_eson
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'
        hash = resp['hash']

        call_eson = {
            'edsu:blocks': hash,
        }
        resp = conn.srv_call('pub.srv.inttest.actions.eson-test', call_eson, except_on_err=False)
        assert resp.get('ok') == 'true'
        assert resp.get('eson-valid') == 'false'



'''

==Interpretation

The abstract parsed representation ESON is a map of keys (which are strings), to lists of values (which are also strings).

An item line with a non-caret key represents a new entry in the map, with the key pointing to a list with a single item - the value.

An item line with a caret key represents an append of its value to the most-recent non-caret key's list.  For example, the following ESON document:

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
one two
^ three
^ four
five six
seven eight
^ nine

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Could be represented in JSON as:

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{
    "one": ["two", "three", "four"],
    "five": "six",
    "seven": ["eight", "nine"]
}
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Note that in the above example, the list with a single item ["six"] was represented as a simpler scalar value of "six".  This is a valid interpretation, but not mandatory - it's up to the language/library which is doing the parsing.

An ESON document starting with an item line with a caret key is invalid.  There is no upper limit to the number of items in a list.

An item line with a zero-length value is not semantically equivalent to the absence of that line - a zero-length value represents an empty string.

When a duplicate key is encountered during parsing, it is semantically equivalent to first discarding the complete list of values that the key previously mapped to.  For example:

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
a 1
^ 2
b 3
a 4

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Is equivalent to:

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{
    "a": "4",
    "b": "3"
}
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

'''

ESONS_TO_INTERPRET = [
    ('\n', {}),
    ('a b\n\n', {'a': 'b'}),
    ('one two\n^ three\n^ four\nfive six\nseven eight\n^ nine\n\n',
     { "one": ["two", "three", "four"], "five": "six", "seven": ["eight", "nine"] }),
    ('a 1\n^ 2\nb 3\na 4\n\n', {"a": "4", "b": "3"}),
]

@T()
def t_eson_interpretation():
    conn = Conn('test1@example.com', auth='omni')
    for (interpret_eson, goal_json) in ESONS_TO_INTERPRET:
        data = b'~\n' + bytes(interpret_eson, 'utf8')
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'
        hash = resp['hash']

        call_eson = {
            'edsu:blocks': hash,
        }
        resp = conn.srv_call('pub.srv.inttest.actions.eson-to-json', call_eson, except_on_err=False)
        assert resp.get('ok') == 'true'
        assert json.loads(resp.get('json')) == goal_json


'''

==ESON as used in Edsu

Any block which is interpreted as an ESON document in Edsu must have a payload which contains nothing except exactly one valid ESON document (i.e. no extraneous data before or after).

Extraneous ESON keys which are not defined by this specification will be ignored, unless otherwise stated.

All ESON keys starting with "edsu:" are reserved.


=Value Types Used in Edsu

Below are the requirements for converting ESON values to more meaningful types and back again.  These types will be referred to in the rest of this document.  Unless otherwise noted, whitespace is significant and is never discarded, and case is significant.

==bool

One of exactly two strings: "true" and "false".

'''

@T()
def t_types_bool():
    valids = [
        'true',
        'false',
    ]
    invalids = [
        '',
        'True',
        'T',
        't',
        'yes',
        'Yes',
        '1',
        'False',
        'F',
        'f',
        'no',
        'No',
        '0',
    ]
    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'bool',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==u16, u32, u64

Textual representations of non-negative integers, base 10.  Leading zero and characters outside of the set 0123456789 (e.g. decimals or commas) are not permitted.  The 16, 32, and 64 refer to the number of bits that the resulting number must fit into.

'''

@T()
def t_types_unsigneds():
    conn = Conn('test1@example.com', auth='omni')
    for bits in [16, 32, 64]:
        valids = [
            0,
            2**bits-1,
        ]
        invalids = [
            -1,
            2**bits,
        ]
        for expected, vals in [('true', valids), ('false', invalids)]:
            for val in vals:
                call_eson = {
                    'type': 'u{}'.format(bits),
                    'val': str(val),
                }
                resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
                assert resp.get('ok') == 'true'
                assert resp.get('valid') == expected

'''

==value

No conversion should be done.


==version

A u16 followed immediately by a period (ASCII 0x2E) followed immediately by another u16


==versions

One or more version values, separated by single spaces (ASCII 0x20)

'''

@T()
def t_types_versions():
    valids = [
        '0.1',
        '0.1 0.2',
        '0.1 0.2 0.3 65535.65535',
    ]
    invalids = [
        '0',
        '.1',
        '0.1b 3.14',
        '0.1  3.14',
        ' 0.1 3.14',
        '0.1 3.14 ',
        '0.1 3.65536',
    ]
    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'versions',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==encoding

An encoding ID.  As of this version there is only one:

deflate

Any future encodings will be composed entirely of the following characters:

abcdefghijklmnopqrstuvwxyz0123456789-

Note that unlike HTTP, deflate refers specifically to a raw RFC 1951 encoding, without any encapsulation by the zlib or gzip formats.  Servers must ignore any encodings that they do not recognize.  The encodings of gzip, zlib, and brotli are disallowed.


==encodings

One or more encoding IDs, separated by single spaces (ASCII 0x20)

'''

@T()
def t_types_encodings():
    valids = [
        'deflate',
        'wooter zooter',
        'hello-there deflate deflate 437',
    ]
    invalids = [
        '',
        'hi  there',
        ' hi there',
        'hi there ',
        'woot:zoot',
        '37&s',
    ]
    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'encodings',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==multihash

A multihash as defined by the IPFS project, encoded in the base58 format using the following alphabet:

123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz

As of this version of the protocol, servers must produce multihashes with the hash function sha2-256 (0x12) with a digest length of 32 bytes (0x20).  It is expected that other hash functions and digest lengths will be added to maintain cryptographic strength as technology improves.

'''

@T()
def t_types_multihash():
    valids = [
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY5',
        get_hash(b'a'),
        base58.b58encode(b'\x16\x20' + bytes([12] * 32)),     # Fake SHA3-256
        base58.b58encode(b'\x15\x30' + bytes([12] * 48)),     # Fake SHA3-384
        base58.b58encode(b'\xac\x02\x20' + bytes([12] * 32)), # Fake hash fn 300
    ]
    invalids = [
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY',
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY55',
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY0',
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqYO',
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqYI',
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqYl',
        'QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY+',
        base58.b58encode(b'\xac\x20' + bytes([12] * 32)), # Varint without the continuation byte
        base58.b58encode(b'\x12\xac' + bytes([12] * 32)), # Varint without the continuation byte
        base58.b58encode(b'\x16\x1f' + bytes([12] * 31)), # Too short
        base58.b58encode(b'\x15\x30' + bytes([12] * 32)), # Lying length
    ]
    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'multihash',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==username

An ID followed immediately by an @ (ASCII 0x40) followed immediately by a domain name.  The domain name must be apex: i.e. composed of exactly two labels joined with a period, the latter being the TLD; i.e. subdomains are not permitted.

The ID has identical restrictions to a domain name label: it must be between 1 and 63 characters from the set: 

ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-

and not start or end with a dash.

Usernames are case-insensitive on interpretation (i.e. a@B.net and A@b.NET are equivalent).  Clients and servers are required to accept mixed or uppercase versions of usernames, but are required to only emit lowercase versions.

'''

@T()
def t_types_username():
    valids = [
        'a@b.c',
        'A@B.CD',
        '{}@{}.{}'.format('a' * 63, 'b' * 63, 'c' * 63),
        'a-----B@c-----D.co-m',
        '{}@{}.{}'.format('xn--3ve8c9a', 'xn--hxa0amcg', 'xn--q9j2c295p'),
    ]
    invalids = [
        '@b.c',
        'a@.c',
        'a@c',
        'a@b.',
        '{}@{}.{}'.format('a' * 64, 'b' * 2, 'c' * 2),
        '{}@{}.{}'.format('a' * 2, 'b' * 64, 'c' * 2),
        '{}@{}.{}'.format('a' * 2, 'b' * 2, 'c' * 64),
        '-@b.c',
        'a-@b.c',
        '-a@b.c',
        'a@-.c',
        'a@-b.c',
        'a@b-.c',
        'a@b.-',
        'a@b.-c',
        'a@b.c-',
        'a.b@c.com',
        'a_b@c.com',
        'a@b.c.d.com',
        'a@b@c.d.com',
    ]
    if is_exhaustive():
        for x in range(0x20, 0x7e+1):
            c = chr(x)
            if c not in 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-':
                invalids.append('a{}b@c.com'.format(c))

    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'username',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==name

An Edsu name is nine or more characters from the set:

ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.

A name cannot start or end with a period, nor can two or more periods be directly beside each other.

The first 8 characters of a name must be one of the following:

pub.std.
pub.app.
pub.srv.
grp.std.
grp.app.
grp.srv.
prv.std.
prv.app.
prv.srv.

The maximum length of a name is 255 characters.

The groups of characters between periods in a name are referred to in this specification as segments.

Names must have 5 or more segments.  Names with the third segment of "edsu" are reserved.

By convention, names with a second segment of "app" have no restrictions, and those with "std" should be used in accordance with some published specification.  Names with the second segment of "srv" are used exclusively by the server, and in interactions with it.

By convention, in names with a second segment of "app", the third segment represents the user or organization which created the app, with the fourth segment representing the name of the app.  For example, the name starting with pub.app.Ada.BernoulliCalc would be read as an app named BernoulliCalc created by someone named "Ada".

By convention, in names with a second segment of "std", the third segment represents a category of standards, with the fourth naming the standard itself.  For example a name starting with pub.std.communications.email would be read as relating to a communications standard, specifically email.


'''

# TODO: 5 seg restriction
NAMES_VALID = [
    'pub.std.a.b.c',
    'pub.app.a.b.c',
    'pub.srv.a.b.c',
    'grp.std.a.b.c',
    'grp.app.a.b.c',
    'grp.srv.a.b.c',
    'prv.std.a.b.c',
    'prv.app.a.b.c',
    'prv.srv.a.b.c',
    'pub.app.a.b.' + 'a' * 243,
    'pub.app.a' + '.a' * 123,
    'pub.app.a.b.-',
    'pub.app.a.b._',
]

NAMES_INVALID = [
    '',
    'a',
    'pub',
    'pub.a',
    'pub.std.a',
    'pub.std.a.b',
    'peb.std.a.b.c',
    'pub.apl.a.b.c',
    'pub.std.',
    'pub.std.a.b.c.',
    '.pub.std.a.b.c',
    'pub.std.a.b.b..a',
    'pub.std.b.c.d.hi@there.a',
] 

@T()
def t_types_name():
    valids = list(NAMES_VALID)
    invalids = list(NAMES_INVALID)
    if is_exhaustive():
        for x in range(0x20, 0x7e+1):
            c = chr(x)
            if c not in 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.':
                invalids.append('pub.std.a.b.c{}b'.format(c))

    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'name',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==name-pattern

A name pattern is as follow:

┏━━━━━━┳━━━━┓
┃ name ┃ .* ┃
┗━━━━━━┻━━━━┛

Where name is exactly as defined above.  ".*" is the ASCII characters 0x2E followed by 0x2A - it may be omitted, in which case that name pattern and a name are identical.

'''

@T()
def t_types_name_pattern():
    valids = list(NAMES_VALID)
    valids += [x + '.*' for x in valids]
    invalids = list(NAMES_INVALID)
    invalids += [x + '.*' for x in invalids]
    invalids.append('pub.std.a.b.c.*.b')
    invalids.remove('pub.std.a.b.*')
    conn = Conn('test1@example.com', auth='omni')
    for expected, vals in [('true', valids), ('false', invalids)]:
        for val in vals:
            call_eson = {
                'type': 'name-pattern',
                'val': val,
            }
            resp = conn.srv_call('pub.srv.inttest.actions.type-test', call_eson, except_on_err=False)
            assert resp.get('ok') == 'true'
            assert resp.get('valid') == expected

'''

==ESON-encoded UTF-8

This encoding takes any UTF-8 string and encodes it so it may be stored in an ESON value.

Every byte which is valid in an ESON value, except for ~ (ASCII 0x7E), may be encoded as itself - all other bytes will be called "non-encodable" in this section.

Every occurrence of one or more contiguous non-encodable bytes is replaced by:

┏━━━━━━━━━━━━┳━━━┓
┃ ~ ┃ base58 ┃ ^ ┃
┗━━━━━━━━━━━━┻━━━┛

Where ~ and ^ are ASCII characters 0x7E and 0x5E respectively, and base58 is the non-encodable bytes base58-encoded as described elsewhere in this specification.

It is permitted to treat any or all encodable bytes as non-encodable (i.e. include them in the base58 encoding), but it is not recommended due to increased size and lack of consistency when being hashed.  Likewise it is recommended to group as many adjacent non-encodable bytes together as possible before base58 encoding (for the same reason as just mentioned), however the encoder is permitted to group in any way.

'''

UNICODE_TO_INTERPRET = [
    'hello',
    'hel~lo',
    '~lo',
    'hiya~',
    'hiya^',
    'Դ Ե Զ Է Ը',
    ' ԴԵԶԷ Ը',
    ' Դ Ե \nԶ \rԷ \tԸ',
    'Դ Ե  hi ԶԷԸ',
    'ԴԵԶԷ Ը ',
    '☹, ☺, ☻',
    '∄ ∅ ∆ =  ☎☏ and  癩羅蘿',
    ''.join(chr(x) for x in range(1, 128)),
]

def t_eson_utf8_interpretation():
    # TODO: test validation (i.e. non-strictly-conforming decode/encode)
    conn = Conn('test1@example.com', auth='omni')
    for utf8 in UNICODE_TO_INTERPRET:
        encoded = encode_eson_utf8(utf8)
        resp = conn.srv_call('pub.srv.inttest.actions.eson-encoded-utf8-to-b58', {
            'val': encoded,
        })
        assert resp.get('ok') == 'true'
        print(resp)
        assert resp.get('b58', False) != False
        assert str(base58.b58decode(resp['b58']), 'utf8') == utf8


'''

==address

An address is composed of usernames, names, and multihashes in one of three forms:

┏━━━━━━━━━━┳━━━┳━━━━━━┓
┃ username ┃ : ┃ name ┃
┗━━━━━━━━━━┻━━━┻━━━━━━┛

┏━━━━━━━━━━┳━━━┳━━━━━━━━━━━┓
┃ username ┃ # ┃ multihash ┃
┗━━━━━━━━━━┻━━━┻━━━━━━━━━━━┛

┏━━━━━━━━━━┳━━━┳━━━━━━┳━━━┳━━━━━━━━━━━┓
┃ username ┃ : ┃ name ┃ # ┃ multihash ┃
┗━━━━━━━━━━┻━━━┻━━━━━━┻━━━┻━━━━━━━━━━━┛

Where : is ASCII 0x3A, and # is 0x23.

'''

# NOTE: This is currently advisory - addresses are not used in the spec

'''

==length-or-stop

If this is correctly parses as a u16, it is done so, otherwise it is treated as a unmodified value.

'''

# NOTE: This is handled in the block-put section

'''

==oob-code

One of:

advisory
redirect
authentication-error
authentication-challenge
dropped-subs
not-found
hash-mismatch
timed-out
permission-denied
invalid-input
invalid-content
rate-limited
just-sent
server-error


==chain

The chain format is detailed in the section of this specification devoted to chaining.


==token

The tokens used in the hello message must be:

* 64 characters in length or less
* Meet the same requirements as a name segment


=Edsu Wire Protocol

Edsu is a client/server, connection-based protocol.  All connections are made, conceptually, between two Edsu users: the user-from and the user-to.  The user-from is the client, with the server acting on behalf of the user-to.  It's often the case that user-to and user-from are the same.

The first step to initiate a connection is to convert the user-to's username into a fully qualified domain name (FQDN): this is done by replacing the "@" with the characters ".edsu.", e.g. hello@example.com becomes hello.edsu.example.com.

Once the IP address is resolved from the FQDN, a TCP/IP connection is established by the client to one of port 3216 or 443 at that address.  Both ports only accept connections which are encapsulated with TLS.  The protocol spoken on ports 3216 and 443 are identical, except that on port 443 it is further encapsulated by the WebSocket protocol.

Edsu WebSocket connections must be encapsulated by SSL, and use the path /edsu/ws on connection.

'''

def connect_test_ok(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect(('localhost', port))
    except ConnectionRefusedError:
        return 'Connection refused: is the server running?'
    sock.settimeout(0.1)
    try:
        sock.recv(1)
    except socket.timeout:
        return
    return "Connection established, but non-functioning (couldn't read on socket)"

@T(die=True)
def t_conn_port3216():
    "Test if Edsu's port is listening"
    return connect_test_ok(3216)

@T(die=True)
def t_conn_port443():
    "Test if the WebSocket port is listening"
    return connect_test_ok(443)

'''
Once the connection is established, communication consists of messages sent from client to server and vice versa.  A message is composed of a header, which always a valid ESON document, followed by an optional payload.  A payload is zero or more unrestricted bytes (i.e. from 0-0xFF), followed by a line feed character (0x0A).  The maximum size of a payload (excluding the aforementioned terminating line feed character) is 64,512 bytes (i.e. 63kB).

Before or after messages, the client and server may send zero or more line feed characters - these must be discarded.

'''

@T()
def t_conn_extra_line_feeds():
    conn = Conn('test1@example.com', auth='none')
    conn.send_bytes(b'\n\n\n\n\n\nedsu hello\nversions 0.1\n\n\n\n\n\n\n\n\nedsu ping\n\n\n\n\n\n\n')
    resp = conn.read_response()
    assert resp['edsu'] == 'hello'
    resp = conn.read_response()
    assert resp['edsu'] == 'pong'


'''
The first item line of a header ESON document must contain the key "edsu" with the value stating the message type.  Both whitespace and letter case is always significant in both the keys and values of message headers.

If the header is found to be invalid (i.e. not a valid ESON document), if the values are not correctly formatted for the message type (e.g. a malformed multihash), or if the terminating payload character is not a line feed, the server response must be an oob with the code invalid-input and close-connection set to "true".

'''

@T()
def t_conn_messages_invalid():
    "Messages that the server should reject"
    malformeds = [
        b'a b\n\n',
        b'edsu woot\n\n',
        b'edsu block-get\n\n',
        b'edsu Ping\n\n',
        b'edsu  ping\n\n',
        b'edsu ping \n\n',
        b'edsu block-get\nhash Hello\n\n',
        b'edsu block-put\npayload-stop 4\n\n~\n123\n',
    ]
    if is_exhaustive():
        malformeds += ESONS_INVALID_CORE_EX
    for mal in malformeds:
        if mal.startswith(b'edsu block-put'):
            conn = Conn('test1@example.com', auth='omni')
        else:
            conn = Conn('test1@example.com')
        conn.send_bytes(mal)
        resp = conn.read_response()
        assert resp.get('code', None) == 'invalid-input'
        assert resp.get('close-connection', None) == 'true'
        

'''
Every key across all Edsu messages maps to a scalar value - i.e. the list functionality of ESON is not used.

The exhaustive list of messages that may be sent from the client:

hello
block-put
block-get
name-put
name-get
name-append
sub-put
sub-clear
ping
pong

And the similarly exhaustive list of messages from the server:

hello
oob
ok
block
name
sub-notify
authenticated
ping
pong

Though some of them share the same name, the client and server messages are distinct, and messages defined as being from the server must not be sent by the client, and vice versa.

Extraneous keys in the ESON header of messages are allowed, however they must contain at least one ":" character and cannot begin with "edsu:".

'''

@T()
def t_conn_messages_valid():
    "Messages that the server should accept"
    valids = [
        b'edsu ping\nhi:there woot\n\n',
    ]
    for msg in valids:
        conn = Conn('test1@example.com')
        conn.send_bytes(msg)
        resp = conn.read_response()
        assert resp.get('edsu', None) == 'pong'
        

'''

==Order of Actions

Unless otherwise specified, the actions taken by the Edsu server must be performed in the order that the instigating messages arrive on the connection.  

The exceptions are:

* Block-get messages and any resulting chains may be completed at any time 
  after the message arrives.

* Ping messages may be serviced at any time.

Multiple connections to the same hosts are independant of each other from an order of actions perspective.


==Channels

In every client-to-server message there is an optional key "channel", the value of which can be any valid ESON value.  Similarly, for every server-to-client message there is a required channel key of the same type.

If used in a client message, all server messages generated as a result must echo the channel unchanged.  If unused, or in the case of a server message that is unrelated to any specific client message, the channel returned in the server message must be the default value of "0".

'''

@T()
def t_conn_channel_default():
    "Testing the default channel"
    conn = Conn('test1@example.com')
    conn.send_bytes(b'edsu ping\n\n')
    resp = conn.read_response()
    assert resp.get('channel', None) == '0'
 
'''
In all message descriptions that follow, the channel key for both client and server messages is implied.

'''

@T()
def t_conn_channels():
    "Making sure that the channel is always sent back unchanged"
    msgs = [
        b'edsu block-put\nchannel 1\npayload-stop zoot\n\n~\nedsu:append-allow *\n\nzoot\n',
        b'edsu block-get\nchannel 0\nhash QmVkWmfXCRyiT4iNsu98hTCqsydy63x4F8ModznLbAunBu\n\n',
        b'edsu block-get\nchannel hello there\n' +
         b'hash Qmaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n\n',
        b'edsu name-put\nchannel    zo~!@#$%^&*()_+=ot    \nname pub.app.inttest.data.channel\n' +
         b'hash QmVkWmfXCRyiT4iNsu98hTCqsydy63x4F8ModznLbAunBu\n\n',
        b'edsu name-put\nchannel    zo~!@#$%^&*()_+=ot    \nname pub.app.inttest.data.channel2\n' +
         b'hash Qmaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n\n',
        b'edsu name-get\nchannel ' + bytes(x for x in range(0x20, 0x7e+1)) +
         b'\nname pub.app.inttest.data.channel\n\n',
        b'edsu name-get\nchannel ' + bytes(x for x in range(0x20, 0x7e+1)) +
         b'\nname pub.app.inttest.data.channel.non-existant\n\n',
        b'edsu name-append\nchannel \nname pub.app.inttest.data.channel\n' +
         b'hash QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY5\n\n',
        b'edsu name-append\nchannel \nname pub.app.inttest.data.channel.non-existant\n' +
         b'hash QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY5\n\n',
        b'edsu sub-clear\nchannel a\n\n',
        b'edsu ping\nchannel b\n\n',
    ]
    conn = Conn('test1@example.com', auth='omni')
    for msg in msgs:
        conn.send_bytes(msg)
        resp = conn.read_response()
        assert resp.get('channel', None) == eson_parse(msg)[0]['channel']
        
    conn.send_bytes(b'edsu sub-put\nname pub.app.inttest.data.channel\n' +
                    b'channel zoot\nonce true\n\n')
    for x in [0, 1]:
        resp = conn.read_response()
        assert resp.get('channel', None) == 'zoot'
                    
'''
==Encoding

The client message "block-put" and the server messages "block" and "oob" have a key called encoding.  This states the encoding (presumably but not necessarily a compression algorithm) which has been used on the payload of that message.

The decoding capabilities of both the client and server are exchanged during connection initiation.  It is at their descrection which encoding to use (or none) on a per-message basis, provided that the encoding is reported to be decodable by the receiver.


==Out Of Band Message

At any time the server may send an Out Of Band message (oob).  This has the format:

┏━━━━━━━━━━━━━━━━━━┓
┃ From server: oob ┃
┣━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━┳━━━━━━━━━━┓
┃ key              ┃ type     ┃ required ┃
┣━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━╋━━━━━━━━━━┫
┃ code             ┃ oob-code ┃ ✓        ┃
┃ close-connection ┃ bool     ┃          ┃
┃ retry-delay-ms   ┃ u32      ┃          ┃
┃ payload-length   ┃ u16      ┃          ┃
┃ encoding         ┃ encoding ┃          ┃
┗━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━┻━━━━━━━━━━┛

The oob-code key describes the reason for the message.

The close-connection key, if present and "true", means that the server has requested that the client close the connection immediately.  The server will stop processing further messages from the client and will close the connection on its side if the client does not do it quickly enough.

The retry-delay-ms key is a recommendation that the request that caused this OOB message to be retried, unchanged, after a wait.  That wait is specified in milliseconds.  This is an advisory and it can be ignored by retrying at any time or not at all.

The payload-length key specifies, in bytes, how long the payload of the message is (if there is one).  Likewise, the encoding key specifies the payload's encoding (if any).

The payload is dependant on the oob-code.  If not detailed explicitly in this specification, the server may send a payload that is an ESON document, and it may include debug information using the key "debug", with the value being a natural language description of what went wrong, and any futher details.


==Connection Initiation

The first messages sent on a newly established connection must be the client sending a hello.  Once that is sent (i.e. before the response is received) it is then free to send any other valid client-to-server message, though it is disallowed (OOB code invalid-input) to send another hello message during the lifetime of the connection.

'''

@T()
def t_hello_hello_first():
    "Anything other than exactly one hello at the very beginning of the message is an error"
    msgs = [
        b'edsu block-put\npayload-stop zoot\n\n~\nedsu:append-allow *\n\nzoot\n',
        b'edsu block-get\nhash QmVkWmfXCRyiT4iNsu98hTCqsydy63x4F8ModznLbAunBu\n\n',
        b'edsu name-put\nname pub.app.inttest.data.hello\n' +
         b'hash QmVkWmfXCRyiT4iNsu98hTCqsydy63x4F8ModznLbAunBu\n\n',
        b'edsu name-get\nname pub.app.inttest.data.hello\n\n',
        b'edsu name-append\nname pub.app.inttest.data.hello\n' +
         b'hash QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY5\n\n',
        b'edsu sub-put\nname pub.app.inttest.data.hello\n\n',
        b'edsu sub-clear\n\n',
        b'edsu ping\n\n',
        b'edsu not-an-op\n\n',
    ]
    for msg in msgs:
        conn = Conn('test1@example.com', auth='none')
        conn.send_bytes(msg)
        resp = conn.read_response()
        assert resp.get('edsu', None) == 'oob'
        assert resp.get('code', None) == 'invalid-input'

'''
The first message sent from the server to the client must be a hello in response to the client's.  Like the client, that hello can be sent only once per connection.

The client-to-server hello message is:

┏━━━━━━━━━━━━━━━━━━━━┓
┃ From client: hello ┃
┣━━━━━━━━━━━━━━━━━━┳━┻━━━━━━━━━┳━━━━━━━━━━┓
┃ key              ┃ type      ┃ required ┃
┣━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ versions         ┃ versions  ┃ ✓        ┃
┃ encodings        ┃ encodings ┃          ┃
┃ secret           ┃ value     ┃          ┃
┃ token            ┃ value     ┃          ┃
┃ visitor-username ┃ value     ┃          ┃
┗━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

The versions key states the versions of the Edsu protocol that the client is able to speak.  If the server does not speak any of them it must send an oob with the code server-error, with close-connection set to "true".

'''

@T()
def t_hello_version_mismatch():
    "Version mismatch on hello"
    msgs = [
        (b'edsu hello\nversions \n\n', 'invalid-input'),
        (b'edsu hello\nversions 3.14\n\n', 'server-error'),
    ]
    for msg, code in msgs:
        conn = Conn('test1@example.com', auth='none')
        conn.send_bytes(msg)
        resp = conn.read_response()
        assert resp.get('edsu', None) == 'oob'
        assert resp.get('code', None) == code
        assert resp.get('close-connection', None) == 'true'
 
'''
The encodings key states the payload encodings that the client is able to parse.

If none of secret, token, or visitor-username is sent, the client is requesting an anonymous connection.  Alternatively, they can request to authenticate themselves as A) the user-to via secret, B) the user-to via token, or C) a user-from via token and visitor-username.

No matter the method of authentication, the immediate response from the server is: 

┏━━━━━━━━━━━━━━━━━━━━┓
┃ From server: hello ┃
┣━━━━━━━━━━━┳━━━━━━━━┻━━┳━━━━━━━━━━┓
┃ key       ┃ type      ┃ required ┃
┣━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ version   ┃ version   ┃ ✓        ┃
┃ encodings ┃ encodings ┃          ┃
┗━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

The version key states the version of the Edsu protocol that the rest of the connection will be using.

The encodings key states the payload encodings that the server is able to parse.

'''

@T(die=True)
def t_hello_anon():
    "Anonymous hello"
    conn = Conn('test1@example.com', auth='none')
    conn.send_bytes(b'edsu hello\nzoot alors\nencodings hi-there\nversions 0.1 0.2 12.5\n\n')
    resp = conn.read_response()
    assert resp.get('edsu', None) == 'hello'
    assert resp.get('version', None) == '0.1'

'''

===Secret

The value of the secret key must be exactly 64 characters long.  In the likely event that the actual secret is shorter than this, it must extended on the right with spaces (ASCII 0x20).

To arrive at the secret to be compared, the server must first strip all trailing spaces, then converted to UTF-8 using the method described elsewhere in this specification, and then normalized using the NFKC form.

The secret key is exclusive of the token and visitor-username keys.  If the secret (which is established via some external mechanism) matches, the client is authenticated as the user-to; i.e. user-from and user-to are the same.  A message is sent:

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From server: authenticated ┃
┣━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━┻┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

This indicates to the client that the authentication is successful.

That capability block must at least grant the required permissions to use the functionality at pub.srv.edsu.authentication.tokens.generate.*.

'''

@T(die=True)
def t_hello_secret():
    conn = Conn('test1@example.com', auth='none')
    conn.send_bytes(b'edsu hello\nversions 0.1\nsecret inttest\n\n')
    assert conn.read_response()['edsu'] == 'hello'
    assert conn.read_response()['edsu'] == 'authenticated'
    # Try create a caps block
    caps_hash = conn.block_put(eson_to_block({'edsu:name-get': 'prv.srv.edsu.time.unix'}))
    resp = conn.srv_call('pub.srv.edsu.authentication.tokens.generate', {
        'edsu:description': 'inttest generated',
        'edsu:capabilities': caps_hash,
    }, except_on_err=False)
    assert resp.get('ok') == 'true'


'''
If the password doesn't match, an Out Of Band message (oob) is sent with the oob-code of "authentication-error", and close-connection set to "true".

'''
@T()
def t_hello_secret_mismatch():
    conn = Conn('test1@example.com', auth='none')
    conn.send_bytes(b'edsu hello\nversions 0.1\nsecret not-a-password-at-all\n\n')
    resp = conn.read_response()
    assert resp['edsu'] == 'oob'
    assert resp['close-connection'] == 'true'
    assert resp['code'] == 'authentication-error'

'''

====Authentication Challenge

On a successful match of the secret, instead of responding with an authenticated message, the server may send an OOB message with the code "authentication-challenge".

The payload of this message will be an ESON document with the key "edsu:type".  The value pointed to by this key must be: "question" (it is expected that in later versions of this specification there will be other possible values).

In the case of a "question" challenge, the ESON will also contain a key "edsu:question", with an ESON-encoded UTF-8 value which should be presented verbatim to the user.

The client must then collect an answer from the user, base58-encode it, then append that base-58 encoded value to the string "pub.srv.edsu.authentication.challenge.answer.", forming a name.

This name should then be sent to the server in a name-get message.  If the challenge has been successfully answered, the server will respond with a name message - the hash of which has no meaning and should be ignored - followed by an authenticated message identical to what is sent when there is no challenge and the secret match is successful.

Alternately, if the challenge has not been met, an OOB message with the code "not-found" will be sent, followed by an OOB message with the code "authentication-error".  This second OOB will have the key "close-connection" set to "true" and the connection will be closed shortly after.


===Token, Without Visitor-Username 

A token key without a visitor-username is interpreted as requesting authentication as the user-to based on that token.  The content of the token is left up to the server, as long as it meets the requirements of the "token" type described elsewhere in this specification.  The mechanism for validating the token is up to the server.  The response to the client is identical to that of a secret-based authentication which generates no challenge.

'''

@T(die=True)
def t_hello_token():
    # Connect using secret
    conn = Conn('test1@example.com', auth='none')
    conn.send_bytes(b'edsu hello\nversions 0.1\nsecret inttest\n\n')
    assert conn.read_response()['edsu'] == 'hello'
    assert conn.read_response()['edsu'] == 'authenticated'
    # Create a caps block
    caps_hash = conn.block_put(eson_to_block({'edsu:name-get': 'prv.srv.edsu.time.unix'}))
    resp = conn.srv_call('pub.srv.edsu.authentication.tokens.generate', {
        'edsu:description': 'inttest generated',
        'edsu:capabilities': caps_hash,
    }, except_on_err=False)
    assert resp.get('ok') == 'true'
    token_bytes = bytes(resp['edsu:token'], 'utf8')
    conn2 = Conn('test1@example.com', auth='none')
    conn2.send_bytes(b'edsu hello\nversions 0.1\ntoken ' + token_bytes + b'\n\n')
    assert conn2.read_response()['edsu'] == 'hello'
    assert conn2.read_response()['edsu'] == 'authenticated'

@T()
def t_hello_token_mismatch():
    conn = Conn('test1@example.com', auth='none')
    conn.send_bytes(b'edsu hello\nversions 0.1\ntoken not-a-token-at-all\n\n')
    resp = conn.read_response()
    assert resp['edsu'] == 'oob'
    assert resp['close-connection'] == 'true'
    assert resp['code'] == 'authentication-error'


'''

===Token, With Visitor-Username 

A hello message with both token key and visitor-username present is interpreted as requesting authentication as the user stated in visitor-username; if authentication is successful, the user-from of the connection is set to the contents of visitor-username.

To authenticate, the server which received the hello message (referred to in this section as the user-to server), opens a non-WebSocket, anonymous Edsu connection to the user-from's server, acting as a client.

On connection, a name-get message is sent, with the name being:

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━┓
┃ pub.srv.edsu.authentication.visitors.capabilities. ┃ token ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━┛

Where token is the value from the client's hello message.

Authentication is considered successful if the user-from server responds with a multihash which resolves to a valid visitor capabilities block.  These capabilities are then applied to the client's connection.

The user-to server may optionally cache the results of this authentication for up to 4096 seconds, after which it must repeat the transaction before performing any further actions for the client.  In the event of the capabilities being different from those obtained by a previous authentication, the connection must be amended to reflect this.

The response to the client hello message from the user-to server is identical to that of a token authentication without a username.

Before being used in the hello message, a visitor token must be specialized on the user-to.  First, the visitor token (as returned from the token generating service) is divided into two parts: the prefix and the postfix, with the former being the first 8 characters, and the latter being the remaining character.  An HMAC-SHA256 function is then applied, with its key being the postfix, and its message being the user-to username.  The result of this function is base58-encoded, and then appended to the prefix to produce the specialized visitor token which can then be used in the hello message.

Server implementations must take care that the postfix is still of sufficient length to provide adequate security properties.


'''

@T()
def t_hello_visitor():
    pass
    # TODO!

'''

==General OOB Messages

===At Any Time

The server may at any time send an oob with the codes "advisory", "server-error", "redirect", and "dropped-subs".

Advisory oobs can always be safely ignored.  As of this version of the specification, there are no advisories specified.

Server-error oobs can be sent at any time (using channel "0"), or in response to a particular client message (in which case it will use that message's channel value, or "0" if none was given).  It indicates that something has interfered with the normal operation on the server in such a way that the client should respond.  The client response is dictated by the presence and values of the close-connection and retry-delay-ms keys.

Redirect oobs can be sent at any time, and indicate that the client should drop the connection and reconnect to a different server.  The close-connection key must be "true".  The payload is a (poentially encoded, using the encoding specified in the message) ESON document with the following keys:

┏━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━┓
┃ key      ┃ type     ┃ required ┃
┣━━━━━━━━━━╋━━━━━━━━━━╋━━━━━━━━━━┫
┃ username ┃ username ┃  ✓       ┃
┃ alias    ┃ bool     ┃  ✓       ┃
┗━━━━━━━━━━┻━━━━━━━━━━┻━━━━━━━━━━┛

Username is what should be used as the basis for the reconnection - i.e. it will be used to determine the FQDN of the server to connect to.

A redirect with alias set to "false" indicates that the server has decided that another server on a different host is better equipped to handle the connection.  Semantically it is completely transparent: the user-to is, other than from the perspective of the network protocols that underly Edsu, the same as before the redirect.  This is used for load balancing.

A redirect with alias set to "true" indicates the user-to has aliased the username to another.  Semantically, the client is aware of the alias and can respond as it wants to this information.  It is used in the event of an Edsu user changing their username, but wants addresses featuring their old username to resolve to their current account.

Dropped-subs is addressed in the section of this specification which details subscriptions.


===In Response to Client Messages

Not-found indicates that the name or block which has been requested does not exist on the server, or in some cases it exists but the connection is lacking the capabilities to know of its existance.

Hash-mismatch is used to indicate when messages with existing-hash keys encounter a different multihash than what is specified.

Timed-out indicates that the request took too long to complete and the server has given up on it.  Note that it is possible that the request actually did complete, but the server was unaware of this at the time it generated the timed-out oob.  This message is the only response to the originating client message - i.e. the server must not send a timed-out oob and then afterwards send a message indicating success or failure of the original request.

Permission-denied indicates that the connection does not have sufficient capabilities to perform the action requested.

Invalid-input indicates that the contents of the requesting message are non-sensical in some way.

Invalid-content indicates that the data stored on the server does not make sense in the context of the request.  E.g. if a request presumes that a certain block stored on the server is an appendable block, and it isn't, it will generate an invalid-content oob.

Rate-limited indicates that either the connection, or all connections to a particular user-to have asked more of the server than it is willing to provide.  This oob must specify a retry-delay-ms to advise when a reasonable time to retry (unchanged) the client message will be.  However, this is not to be interpreted as a guarantee that waiting that amount of time will satisfy the rate limiter.

Just-sent is detailed in the section of this specification devoted to chaining.


=Blocks

Blocks are in one of two formats: text or binary.  In either case, the upper size limit for the final encoded form is 64,512 bytes (i.e. 63kB).


==Text Blocks

Text blocks consist of a header and a payload.  The header is in the form:

┏━━━┳━━━━━━┳━━━━━━━━━━━┓
┃ ~ ┃ salt ┃ line feed ┃
┗━━━┻━━━━━━┻━━━━━━━━━━━┛

Where ~ and line feed are ASCII 0x7E and 0x0A respectively, and salt is zero or more characters in any valid UTF-8 encoding.

The payload is zero or more characters in any valid UTF-8 encoding.  Any Unicode byte order mark is prohibited anywhere in the block.

'''

UTF8_INVALID = [
    b'\xc3\x28',
    b'\xa0\xa1',
    b'\xe2\x28\xa1',
    b'\xe2\x82\x28',
    b'\xf0\x28\x8c\xbc',
    b'\xf0\x90\x28\xbc',
    b'\xf0\x28\x8c\x28',
    b'\xf8\xa1\xa1\xa1\xa1',
    b'\xfc\xa1\xa1\xa1\xa1\xa1',
]

@T()
def t_blocks_text_invalid():
    bad_blocks = [
        b'',
        b'~',
        b'\n',
        b'a\nhi',
        b'~ab' + UTF8_INVALID[0] + b'c\nhi',
        b'~a\nb' + UTF8_INVALID[0] + b'c\nhi',
        b'\xef\xbb\xbf~hi\nthere', # BOM
        b'~\n\xef\xbb\xbf\nhi\nthere', # BOM
    ]
    if is_exhaustive():
        for x in UTF8_INVALID:
            bad_blocks.append(b'~' + x + b'a\na')
            bad_blocks.append(b'~a' + x + b'\na')
            bad_blocks.append(x + b'a\na')
            bad_blocks.append(b'~a' + x + b'\na')
            bad_blocks.append(b'~\n' + x + b'a')
            bad_blocks.append(b'~\na' + x)
            bad_blocks.append(b'~a\n' + x)
        for x in range(256):
            if chr(x) != '~':
                bad_text_blocks.append(bytes([x]) + b'\nhi')
    conn = Conn('test1@example.com', auth='omni')
    for data in bad_blocks:
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'oob'
        assert resp['code'] == 'invalid-input'


@T()
def t_blocks_text_valid():
    conn = Conn('test1@example.com', auth='omni')
    good_blocks = [
        b'~\n',
        b'~a\n',
        b'~\na',
        bytes('~ᚠᛇᚻ\nΆξιον\n常ならむ', 'utf8'),
    ]
    conn = Conn('test1@example.com', auth='omni')
    for data in good_blocks:
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'

'''

==Binary Blocks

Binary blocks consist of a header and a payload.  The header is in the form:

┏━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━┳━━━━━━━━┳━━━━━━━━━━┓
┃ vers ┃ cloc-msb ┃ cloc-lsb ┃ hloc-msb ┃ hloc-lsb ┃ salt ┃ hashes ┃ contents ┃
┗━━━━━━┻━━━━━━━━━━┻━━━━━━━━━━┻━━━━━━━━━━┻━━━━━━━━━━┻━━━━━━┻━━━━━━━━┻━━━━━━━━━━┛

Vers is a byte indicating the encoding format's version, which is 0.1.  The four most significant bits are the major version (0), and the four least significant bytes are the minor version (1).  So 0.1 is 0x01, 3.4 is 0x34, etc.  Servers must not reject blocks with a vers greater than what they recognize, however, the format is forwards-compatible, so any block must be parsable as if it were version 0.1

Cloc-msb and cloc-lsb are the most and least signficant bytes (respectively) of the 16-bit location of the start byte of the contents.

Hloc-msb and hloc-lsb are the most and least signficant bytes (respectively) of the 16-bit location of the start byte of the hashes.

Salt is zero or more bytes of salt, each byte of which can be any value from 0x00-0xFF.

Hashes is the concatenation of zero or more byte-encoded multihashes.  These are not the same multihash format as used elsewhere in this specification - the multihash format is the same, but it is not Base58-encode.  E.g. a SHA-256 multihash would always be represented as 34 bytes: two bytes of header followed by 32 bytes of digest.

Contents is zero or more bytes, each byte of which can be any value from 0x00-0xFF.

'''

@T()
def t_blocks_binary_invalid():
    bad_blocks = [
        b'',
        b'\x01',
        b'\x01\x00',
        b'\x01\x00\x05',
        b'\x01\x00\x05\x00',
        bin_block_emit(0x01, b'', [], b'hi', cloc_delta=-1),
        bin_block_emit(0x01, b'', [], b'', hloc_delta=-1),
        bin_block_emit(0x01, b'', [get_hash(b'hi')], b'zoot', hloc_delta=1),
        bin_block_emit(0x01, b'', [get_hash(b'hi')], b'a', cloc_delta=1),
        bin_block_emit(0x01, b'', [get_hash(b'hi')], b'zoot', hloc_delta=256),
        bin_block_emit(0x01, b'', [get_hash(b'hi')], b'a', cloc_delta=256),
        bin_block_emit(0x01, b'salt', [], b'hi', cloc_delta=-1),
        bin_block_emit(0x01, b'salt', [], b'', hloc_delta=-1),
        bin_block_emit(0x01, b'salt', [get_hash(b'hi')], b'zoot', hloc_delta=1),
        bin_block_emit(0x01, b'salt', [get_hash(b'hi')], b'a', cloc_delta=1),
        bin_block_emit(0x01, b'salt', [get_hash(b'hi')], b'zoot', hloc_delta=256),
        bin_block_emit(0x01, b'salt', [get_hash(b'hi')], b'a', cloc_delta=256),
        bin_block_emit(0x01, b'', [get_hash(b'hi'), base58.b58encode(b'not-a-multihash')], b'hiya'),
        bin_block_emit(0x31, b'', [get_hash(b'hi'), base58.b58encode(b'not-a-multihash')], b'hiya'),
        bin_block_emit(0x01, b'', [], bytes([ord('a')] * (63 * 1024 - 5 + 1))),
    ]
    conn = Conn('test1@example.com', auth='omni')
    for data in bad_blocks:
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'oob'
        assert resp['code'] == 'invalid-input'
 
def get_valid_blocks():
    return [
        bin_block_emit(0x01, b'', [], b''),
        bin_block_emit(0xf1, b'hi', [], b'there'),
        bin_block_emit(0xf1, bytes(range(0, 256)), [], b'there'),
        bin_block_emit(0xf1, b'salt', [], bytes(range(0, 256))),
        bin_block_emit(0xf1, b'salt', [get_hash(b'one'), get_hash(b'two')], b'contents'),
        bin_block_emit(0x01, b'', [], bytes([ord('a')] * (63 * 1024 - 5))),
    ]

@T()
def t_blocks_binary_valid():
    conn = Conn('test1@example.com', auth='omni')
    for data in get_valid_blocks():
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'
        assert resp['hash'] == get_hash(data)

'''

==Block Messages

===Block Put

The client stores blocks on the server using the block-put message:

┏━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: block-put ┃
┣━━━━━━━━━━━━━━┳━━━━━━━━━┻━━━━━━┳━━━━━━━━━━┓
┃ key          ┃ type           ┃ required ┃
┣━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━━╋━━━━━━━━━━┫
┃ payload-stop ┃ length-or-stop ┃ ✓        ┃
┃ encoding     ┃ encoding       ┃          ┃
┗━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━━━┻━━━━━━━━━━┛

Where payload-stop is either a u16 or an unparsed value.  If it is a u16, the server will read that many bytes of payload, then expect a line feed.  The line feed is discarded.

'''

@T()
def t_msg_block_put_u16():
    data = bin_block_emit(0x01, b'', [], b'')
    conn = Conn('test1@example.com', auth='omni')
    conn.send_bytes(b'edsu block-put\npayload-stop ')
    conn.send_bytes(bytes(str(len(data)), 'utf8'))
    conn.send_bytes(b'\n\n')
    conn.send_bytes(data)
    conn.send_bytes(b'\n')
    resp = conn.read_response()
    assert resp['edsu'] == 'ok'
    assert resp['hash'] == get_hash(data)

'''
If payload-stop is not a u16, the server will read payload bytes until that value is encountered, after which it will expect a line feed.  Both the payload-stop value and the line feed are discarded.

'''

@T()
def t_msg_block_put_stop():
    msgs = [
        b'edsu block-put\npayload-stop END\n\n~\nhello0END\n',
        b'edsu block-put\npayload-stop .\n\n~\nhello1.\n',
        b'edsu block-put\npayload-stop 65536\n\n~\nhello265536\n',
    ]
    conn = Conn('test1@example.com', auth='omni')
    for i, msg in enumerate(msgs):
        conn.send_bytes(msg)
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'
        assert resp['hash'] == get_hash(bytes('~\nhello{}'.format(i), 'utf8'))

@T()
def t_msg_block_put_stop_invalid():
    conn = Conn('test1@example.com', auth='omni')
    conn.send_bytes(b'edsu block-put\npayload-stop \n\n~\nhello\n')
    resp = conn.read_response()
    assert resp['edsu'] == 'oob'
    assert resp['code'] == 'invalid-input'


'''
If the connection does not have the capability to upload blocks, the server will respond with an oob with the code "permission-denied" and stop.

'''

@T()
def t_msg_block_put_invalid_perms():
    conn = Conn('test1@example.com', auth='anon')
    conn.send_bytes(b'edsu block-put\npayload-stop .\n\n~\nhello.\n')
    resp = conn.read_response()
    assert resp['edsu'] == 'oob'
    assert resp['code'] == 'permission-denied'

'''
If the encoding key is present, the server will then decode the payload using the specified method.  If the specified method is not supported, or the encoding is invalid, the server will respond with an oob using the code "invalid-input" and stop.  The decoded payload is referred to as "the payload" for the remainder of this section.

The payload must be a valid text or binary block.  If not the server will respond with an oob using the code "invalid-input" and stop.

'''

# NOTE: This is tested in the blocks section

'''
The payload is now stored on the server.  It may be associated with the user-to account, but that is not necessary (i.e. it is acceptable for the server to pool blocks between users).

If successful (i.e. none of the above-mentioned oobs are returned, nor any of the more generic oobs such as "server-error" or "timed-out"), the following message is sent to the client:

┏━━━━━━━━━━━━━━━━━┓
┃ From server: ok ┃
┣━━━━━━┳━━━━━━━━━━┻┳━━━━━━━━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash ┃ multihash ┃          ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Where hash key must be present, and is the multihash generated from the bytes of the payload (i.e. the block).


===Block Get

The client retrieves blocks from the server using the block-get message:

┏━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: block-get ┃
┣━━━━━━━┳━━━━━━━━━━━┳━━━━┻━━━━━┓
┃ key   ┃ type      ┃ required ┃
┣━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash  ┃ multihash ┃ ✓        ┃
┃ chain ┃ chain     ┃          ┃
┗━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Where hash is the multihash returned when the block was stored with block-put.  The chain key is addressed in the Chaining section of this specification - however no matter its value, the behaviour described below is invariant.

The server responds with one of: a generic oob, an oob with the code "not-found" (if the server does not have a block stored at that hash), or the following message:

┏━━━━━━━━━━━━━━━━━━━━┓
┃ From server: block ┃
┣━━━━━━━━━━━━━━━━┳━━━┻━━━━━━━┳━━━━━━━━━━┓
┃ key            ┃ type      ┃ required ┃
┣━━━━━━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash           ┃ multihash ┃ ✓        ┃
┃ payload-length ┃ u16       ┃ ✓        ┃
┃ encoding       ┃ encoding  ┃          ┃
┗━━━━━━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Hash is echoed unaltered from the same key in the client message.  Payload-length is the length of the payload in bytes, and encoding is the optional encoding of the payload.  The server is free to use any encoding that the client has reported it supports in its hello message, or none, at its discretion.

'''

@T()
def t_msg_block_get():
    conn = Conn('test1@example.com', auth='omni')
    for data in get_valid_blocks():
        hash = get_hash(data)
        conn.send_bytes(b'edsu block-put\npayload-stop ')
        conn.send_bytes(bytes(str(len(data)), 'utf8'))
        conn.send_bytes(b'\n\n')
        conn.send_bytes(data)
        conn.send_bytes(b'\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'ok'
        assert resp['hash'] == hash
        conn.send_bytes(b'edsu block-get\nhash ')
        conn.send_bytes(bytes(hash, 'utf8'))
        conn.send_bytes(b'\n\n')
        resp = conn.read_response()
        assert resp['edsu'] == 'block'
        assert resp['hash'] == hash
        assert resp['payload'] == data

'''

==Name Messages

===Name Put

In addition to blocks, Edsu stores name associations.  A name association is a one-to-one mapping between a name and a block, as referenced by its hash.  The block referenced by the name must exist on the server.

The client changes the association of a name with a block using the name-put message:

┏━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: name-put ┃
┣━━━━━━━━━━━━━━━┳━━━━━━━┻━━━┳━━━━━━━━━━┓
┃ key           ┃ type      ┃ required ┃
┣━━━━━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ name          ┃ name      ┃ ✓        ┃
┃ hash          ┃ multihash ┃          ┃
┃ existing-hash ┃ encoding  ┃          ┃
┗━━━━━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Name is the name association to be operated on.  Hash is optional: if it is not present the assocation is deleted.  If it exists the association is either created with the specified hash, or updated to it.

Existing-hash is required for every operation which isn't a creation.  If the association exists, it must match the hash that the association currently points to.  If it doesn't match, the server must send an oob with the code "hash-mismatch" and stop.  A non-existant hash compared to an existant one (e.g. existant-hash exists but the association doesn't) it is considered a mismatch.

As a special case, if the hash in name-put already matches that name's existing association, existing-hash is disregarded and the server must act as if the name-put operation had occured and was successful.

When hash is specified, if a corresponding block is found on the server the server will respond with an oob with the code "not-found" and stop.

The corresponding block must be a text block, containing exactly one valid ESON document, with no extraneous data before or after it.  If this requirement is not met, the server must respond with an oob with the code "invalid-content" and stop.

If the name-put results in an updated hash, and that block's ESON has the key "edsu:previous", the list pointed to by that key must contain a single item which matches the existing-hash key in the client message.  That is, if the name block has an "edsu:previous" key, it must be accurate.  If not, the server must send an oob with the code "invalid-content" and stop.

If the connection lacks the capabilities to perform the operation requested, the server must respond with an oob with the code "permission-denied" and stop.

If the operation is successful the following message is sent:

┏━━━━━━━━━━━━━━━━━┓
┃ From server: ok ┃
┣━━━━━━┳━━━━━━━━━━┻┳━━━━━━━━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash ┃ multihash ┃          ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Where the hash key is identical to the same key from the client message (including its absence if it is absent from the client message).

'''

# NAMES_INVALID


'''

===Name Get

To retrieve a name association from the server, the client sends a name-get message:

┏━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: name-get ┃
┣━━━━━━━┳━━━━━━━┳━━━━━━━┻━━┓
┃ key   ┃ type  ┃ required ┃
┣━━━━━━━╋━━━━━━━╋━━━━━━━━━━┫
┃ name  ┃ name  ┃ ✓        ┃
┃ chain ┃ chain ┃          ┃
┗━━━━━━━┻━━━━━━━┻━━━━━━━━━━┛

Where name is the association requested.  If the connection lacks the required capabilities to retrieve the association, the server will send an oob with the code "permission-denied" and stop.  If the association doesn't exist, or the connection lacks the required capabilities to know if it exists or not, the server will send an oob with the code "not-found" and stop.  Otherwise (and unless a more generic oob is sent), the server will respond with:

┏━━━━━━━━━━━━━━━━━━━┓
┃ From server: name ┃
┣━━━━━━┳━━━━━━━━━━━┳┻━━━━━━━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ name ┃ name      ┃ ✓        ┃
┃ hash ┃ multihash ┃ ✓        ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Name is an echo of the name from the client message.  Hash is the multihash which is associated with that name.


===Name Append

The message name-append appends a value to a named appendable block and changes the name association to point to the resultant block:

┏━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: name-append ┃
┣━━━━━━┳━━━━━━━━━━━┳━━━━━━━┻━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ name ┃ name      ┃ ✓        ┃
┃ hash ┃ multihash ┃ ✓        ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Name is an association which points to an appendable block.  If the block associated with the name isn't an appendable block, the server will send an oob with the code "invalid-content" and stop.  If the block does not exist, the server will send an oob with the code "not-found" and stop.  If the connection lacks the required capabilities to retrieve the association, or fails to meet the requirements stated in the appendable block, the server will send an oob with the code "permission-denied" and stop.  If the rate conditions stated in the appendable block would be exceeded by the append, the server will send an oob with the code "rate-limited", along with an advisory "retry-delay-ms" which will suggest when to try again, and stop.

If none of the above-mention disqualifiers exist, and barring other more generic oob responses, the server will ensure that the following append value is added to the appendable block:

┏━━━━━━┳━━━━━━━┳━━━━━━━━━━━┳━━━━━━━┳━━━━━━━┓
┃ time ┃ space ┃ user-from ┃ space ┃ hash  ┃ 
┗━━━━━━┻━━━━━━━┻━━━━━━━━━━━┻━━━━━━━┻━━━━━━━┛

Where time is a u64 representing when the message was received, encoded as the number of seconds elapsed since the Unix epoch.  Both spaces are ASCII 0x20.  User-from is that of the connection.  Hash is the hash contained in the message from the client - it does not need to reference a block which exists on the server.

If the key "edsu:appended" does not exist in the appendable block, the key will be added, pointing to a list with a single item, the append value.

If the key "edsu:appended" does exist, and the addition of the append value will not violate the size restriction specified in the appendable block, the append value is added to the top of the list (i.e. it will become its first item) of that key.

If the key "edsu:appended" does exist, and the addition of the append value     will violate the size restriction specified in the appendable block, the append value is added to the top of the list, and then the block is duplicated, with the top (most recent) half of the "edsu:appended" list being placed in one, and the bottom half being placed in the other.  If the number of items is odd, the larger of the two lists will be that of the least recent block.  The least recent block is stored in the user-to's storage, and its hash is placed in the most recent block under the key "edsu:append-previous" (replacing what came before if necessary).

The amended appendable block (or the most recent one in the case of a split) is then stored in the user-to's account, and the name stated in the client message will have its association changed to the amended block's hash.  Then the server sends the following message:

┏━━━━━━━━━━━━━━━━━┓
┃ From server: ok ┃
┣━━━━━━┳━━━━━━━━━━┻┳━━━━━━━━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash ┃ multihash ┃          ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Where hash is the multihash of the new name association.  However, if the connection lacks the capabilities to see the name association, the hash key will not be present in the message.

If more than one name-append message arrives at once or during the servicing of an already-begun name-append operation, the server is required to ensure that all of their resultant values are present in the appendable block, and that the times listed in the "edsu:appended" list monotonically decrease.

Further details on appendable blocks can be found in the section devoted to them.


==Subscription Messages

===Subscription Put

Subscriptions are added or amended via the following message:

┏━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: sub-put ┃
┣━━━━━━━━━━━━━━━┳━━━━━━┻━━━━━━━━┳━━━━━━━━━━┓
┃ key           ┃ type          ┃ required ┃
┣━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━╋━━━━━━━━━━┫
┃ name          ┃ name          ┃ ✓        ┃
┃ existing-hash ┃ multihash     ┃          ┃
┃ once          ┃ bool          ┃          ┃
┃ expires       ┃ u64           ┃          ┃
┃ chain         ┃ chain         ┃          ┃
┗━━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━━┻━━━━━━━━━━┛

Name is the name association to be monitored; it does not need to exist.

When the message is received, the value of existing-hash is compared against the hash currently associated with the name given, and if they differ a notification is sent - note that the absence of this key matches the absence of the name assocation, and if one exists without the other this is considered a mismatch.

Once being set to "true" causes the subscription to be deleted once it is responsible for a notification being sent to the client.

If expires exist, once the server clock (as measure in seconds since the Unix epoch) matches its value, the subscription is deleted. 

If chain exists, that chain will be executed every time a sub-notify message is generated as a result of this subscription.

Note that permissions checks are performed when notifications are generated, not when subscriptions are being created, so sub-put will never fail due to insufficient permissions.

Each connection has a set between 0 and not more than 256 subscriptions.  If a sub-put message is received which would result in the creation of a 257th subscription, the server will send an oob with the code "invalid-input" and stop.

If no generic oobs are sent, the server will send the following message:

┏━━━━━━━━━━━━━━━━━┓
┃ From server: ok ┃
┣━━━━━━┳━━━━━━━━━━┻┳━━━━━━━━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash ┃ multihash ┃          ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

The hash key will not be present.


===Subscription Clear

The client can delete all subscriptions attached to its connection by sending the following message:

┏━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: sub-clear ┃
┣━━━━━┳━━━━━━┳━━━━━━━━━━━┫
┃ key ┃ type ┃ required  ┃
┣━━━━━╋━━━━━━╋━━━━━━━━━━━┫
┗━━━━━┻━━━━━━┻━━━━━━━━━━━┛

The server will respond with:

┏━━━━━━━━━━━━━━━━━┓
┃ From server: ok ┃
┣━━━━━━┳━━━━━━━━━━┻┳━━━━━━━━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ hash ┃ multihash ┃          ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

The hash key will not be present.


===Subscription Notify

At any time the server may send a sub-notify to indicate the current state of a subscribed name:

┏━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ From client: sub-notify ┃
┣━━━━━━┳━━━━━━━━━━━┳━━━━━━┻━━━┓
┃ key  ┃ type      ┃ required ┃
┣━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ name ┃ name      ┃ ✓        ┃
┃ hash ┃ multihash ┃          ┃
┗━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Name is the subscribed name.  Hash is the associated hash - if the name association does not exist, the hash key will not be present.

The server must send a sub-notify message for every change in any subscribed name association - i.e. it is created, deleted, or becomes associated with a different hash.  However, there are no guarentees about not sending duplicate or spurious notifications, other than the hash key in each sub-notify message must always be accurate at the time of message being sent.

If there are too many subscription notifications to be sent (as determined by the server), the server may choose to not send them.  If this choice is made, the server must also delete all subscriptions on the connection (i.e. the same as if the client had sent a sub-clear message), and send an oob with the code "dropped-subs".


==Ping Messages

Both the client and server can send ping messages at any time:

┏━━━━━━━━━━━━━━━━━━━┓
┃ From server: ping ┃
┣━━━━━┳━━━━━━┳━━━━━━┻━━━┓
┃ key ┃ type ┃ required ┃
┣━━━━━╋━━━━━━╋━━━━━━━━━━┫
┗━━━━━┻━━━━━━┻━━━━━━━━━━┛

┏━━━━━━━━━━━━━━━━━━━┓
┃ From client: ping ┃
┣━━━━━┳━━━━━━┳━━━━━━┻━━━┓
┃ key ┃ type ┃ required ┃
┣━━━━━╋━━━━━━╋━━━━━━━━━━┫
┗━━━━━┻━━━━━━┻━━━━━━━━━━┛

The receiver is obligated to send a pong message as soon as possible:

┏━━━━━━━━━━━━━━━━━━━┓
┃ From client: pong ┃
┣━━━━━┳━━━━━━┳━━━━━━┻━━━┓
┃ key ┃ type ┃ required ┃
┣━━━━━╋━━━━━━╋━━━━━━━━━━┫
┗━━━━━┻━━━━━━┻━━━━━━━━━━┛

┏━━━━━━━━━━━━━━━━━━━┓
┃ From server: pong ┃
┣━━━━━┳━━━━━━┳━━━━━━┻━━━┓
┃ key ┃ type ┃ required ┃
┣━━━━━╋━━━━━━╋━━━━━━━━━━┫
┗━━━━━┻━━━━━━┻━━━━━━━━━━┛


=Special Blocks

Certain blocks will cause the behaviour of the Edsu server to change, or create an expectation on the part of those using its data.  These are called special blocks.

All of these blocks are required to be text blocks containing exactly one valid ESON document without any extraneous data outside of it document.  A block is interpreted as one or more types of special blocks dependent on the context in which it is used.  Any keys that are not recognized within the context that the block is being interpreted are ignored.


==Group Block

A group block contains the key "edsu:users", and with pointing to a list of one or more of the items, as follows.  A block that does not contain the key "edsu:users" is interpreted as an empty group.

Note that membership in a group is a set union function - if multiple items indicate that a user is part of the group, that is semantically equivalent to them being included once.

These are the possible items:


===Uername

A user whose username appears in the list is part of the group.


===Wildcard

An * (ASCII 0x2A) indicates that all Edsu users are part of this group


===Domain Wildcard

┏━━━┳━━━┳━━━━━━━━┓
┃ * ┃ @ ┃ domain ┃
┗━━━┻━━━┻━━━━━━━━┛

Where * and @ are ASCII 0x2A and 0x40 respectively and the domain is a valid domain name part of a username.  Any Edsu user whose username contains the same domain name is part of this group.


===Block Include

┏━━━┳━━━━━━┓
┃ # ┃ hash ┃
┗━━━┻━━━━━━┛

The "#" is ASCII 0x23; hash is a multihash which points to another group block which is stored on the server.  This group block is interpreted to be a union set with the referencing group block.


===Name Include

┏━━━┳━━━━━━┓
┃ : ┃ name ┃
┗━━━┻━━━━━━┛

The ":" is ASCII 0x3A; name is a name association in the user-to's account which is associated with a group block.  This group block is interpreted to be a union set with the referencing group block.  If the association is non-existant, or if it's associated block is not a valid group block, this item is interpreted as not existing.


==Name Block

When a name association is queried to find out its hash, the ESON document contained in the block which is referenced by that hash is interpreted.


===Previous

As mentioned in the Name Messages section, "edsu:previous" must be accurate in that its value must equal the hash of the association that immediately preceeded the present one.


===Once

If the name block contains the key "edsu:once" and it is set to "true", any action which sends any client the associated hash will then immediately delete the name association; e.g. name-get will trigger the deletion, as well as sub-notify.  The server must guarantee that the deletion is atomic - i.e. the hash is seen exactly at most once.


===Expires

If the name block contains the key "edsu:expires", and its value is a u64, the name association will be (to all outside appearences) deleted when the server time (number of seconds elapsed since the Unix epoch) equals the value of the key.


===Deny and Allow

The values of both "edsu:deny" and "edsu:allow" are interpreted identically to the "edsu:users" key in a group block.

If the connection's user-from belongs to the group specified by "edsu:deny", any attempt to determine the association of the name pointing to the name block will fail with either an "permission-denied" or "not-found" oob.

If the connection's user-from belongs to the group specified by "edsu:allow", and does not match the group specified by "edsu:deny", this is factored into the permissions required to determine the of the name pointing to the name block, depending on the context.  See the section on permissions for more details.


===Description

A name block may optionally include the key "edsu:description", which is an ESON-encoded UTF-8 string which describes what the block contains.  This value is meant to be read by humans.


==Metablock

A metablock contains keys that refer to other blocks.  The key "edsu:metablocks" should contain a list of one or more hashes of other blocks that are to be interpreted as metablocks.  The key "edsu:blocks" should contain a list of one or more hashes of blocks that are not metablocks.

By convention, when "resolving" a metablock into a single sequence of bytes, the steps are:

1. Start with an empty buffer for bytes
2. Concatenate onto the buffer all contents of blocks listed in "edsu:blocks",
   in the order in which they appear
3. For each block in "edsu:metablocks", in the order in which they appear, 
   repeat steps 2 and 3

I.e. metablock trees are resolved depth-first, with the contents of the nodes being resolved before their branches.

Any metablock may contain one or both of the keys "edsu:contents-size" and "edsu:size".  The former is the total, in bytes, of the block contents referred to in that block's "edsu:blocks" key, and recursively the same for all metablocks in its "edsu:metablocks" key.  I.e. the size of the resulting concatenation of block contents produced by the above procedure for resolving a metablock.

The latter ("edsu:size") is the same, except instead of the contents of the blocks, what is totalled is the entire block size of each (i.e. including the header).  Note that this total specifically does not include the size of the metablocks themselve, only the blocks which appear in "edsu:blocks" lists.

For both "edsu:contents-size" and "edsu:size", blocks which appear more than once contribute to the total as many times as they appear.


==Appendable Block


===Deny and Allow

The values of both "edsu:append-deny" and "edsu:append-allow" are interpreted identically to the "edsu:users" key in a group block.

If the connection's user-from is part of the "edsu:append-deny" group, an attempt to perform a name-append on that name block results in an oob with the code of "permission-denied".  If the connection's user-from is not part of the "edsu:append-allow", then the result is the same.

A block with does not contain the "edsu:append-allow" key is not considered an appendable block.  An attempt to perform a name-append on that name block results in an oob with the  code of "invalid-content".


===Appended

All items appended appear in the "edsu:appended" key.  If this key does not exist it will be created on the first successful append.


===Division

If the result of an append is a block whose size is larger than the u16 specified in "edsu:append-bytes-max", the server will divide the block in two, as described in the section detailing the name-append message.  The key "edsu:append-previous" will be set so that the divisions form a linked list from most to least recent.

If the "edsu:append-bytes-max" key is absent, the default of 63,488 (62kB) is used.


===Rate Limiting

If the key "edsu:append-rate-limit" exists, appends will be rate-limited.  The format of the value is a single space (ASCII 0x20) separated string of sub-items.  Sub-items may be in any order.  The format for a sub-item is:

┏━━━━━━━━━┳━━━┳━━━━━━━━━━━┓
┃ sub-key ┃ : ┃ sub-value ┃
┗━━━━━━━━━┻━━━┻━━━━━━━━━━━┛

In this case, the sub-values are all u64 values.  The sub-keys are "seconds", "all-users", and "per-user".  "Seconds" is required, as is at least one of "all-users", and "per-user".  "Seconds" must be greater than 0 and less than 86401 (one day).

Rate limiting is based on a sliding window which is "seconds" long.  "All-users", and "per-user" are the number of appends allowed within the time window.

For example, a value of: "seconds:3600 all-users:10 per-user:1" would be interpreted as: no more than 10 appends per hour, with no more than 1 append per hour by any one user".  Of the two rate limits (per-user and all-user), whichever one is the most restrictive for that append is applied.


===Time to Live

If the key "edsu:appended-ttl" is present in the head of the appendable block linked list (i.e. the one pointed to by a name), the server will periodically remove items from the "edsu:appended" list.  An item will be removed if it is older than time to live specified by the u64 value of "edsu:appended-ttl".  Note that if any item is removed, all the hashes up the linked list of appendable blocks will change, including an updated name association with the head.

There are no guarantees as to when or how often this will take place, but if the server is being run by a commercial entity that charges based on amount of storage used, garbage collection should be performed before those charges are evaluated.


==Capabilities Block

Capabilities blocks are detailed in the pemissions section of this specification.


=Chaining

When a block is sent to the client as result of a client message that contains a chain key, for the next one second the server may send additional blocks specified by the value of that key.

If during that one second the client sends a block-get requesting a block that was sent as a result of the chain that began the one second countdown, the server may, instead of a block message, send an oob with a code "just-sent" and "retry-delay-ms" set to the milliseconds remaining in the one second period.

A chain consists of zero or more chain items, separated by single spaces (ASCII 0x20), in the order of when the client would like them to be sent.  Each item is one of the following:

┏━━━━┳━━━┳━━━━━┓
┃ id ┃ * ┃ key ┃
┗━━━━┻━━━┻━━━━━┛
┏━━━━┳━━━┳━━━━━┓
┃ id ┃ $ ┃ key ┃
┗━━━━┻━━━┻━━━━━┛
┏━━━━┳━━━┳━━━━━┳━━━┳━━━━┓
┃ id ┃ $ ┃ key ┃ > ┃ id ┃
┗━━━━┻━━━┻━━━━━┻━━━┻━━━━┛
┏━━━━┳━━━┳━━━━━┓
┃ id ┃ @ ┃ key ┃
┗━━━━┻━━━┻━━━━━┛

Where key has the same definition as an ESON key, *, $, >, and @ are ASCII 0x2A, 0x24, 0x3E, and 0x40 respectively, key is identical to the ESON key format, and id is one or more characters of the set:

abcdefghijklmnopqrstuvwxyz0123456789-

An ID represents a block.  The ID "0" is implicit, and represents the block requested in a block-get message, or the block associated with the name in a name-get message or a subscription notification.

The characters *, $, and @ are selectors.  They cause the block identified by the ID to be parsed as ESON, and then have the selector operation performed on it, as described below.  If the block does not contain a valid ESON document, it is treated as an ESON document with zero items.

The * symbol indicates that any valid multihashes found in the list pointed to by the key in the chain item are requested blocks.

The $ symbol indicates that the first item found in the list pointed to by the key in the chain item represents a requested block, if it is a valid multihash.

If the item contains both a $ and > symbol, then the block requested by the $ operation thereafter becomes identifiable by the ID which comes after the > symbol, to be used in following chain items.

The @ symbol indicates that the value pointed to by the key in the chain item (provided that it is in a valid chain format) should be substituted inside current chain, replacing that chain item.

The sequence of ID -> selector -> ID, etc. represents a chain, each starting from the "0" ID.  The server may send zero or more blocks from any of these chains, but it must not "skip" sending blocks within those chains.  I.e. at any time during the one second, the client must have the data on hand to verify that the server has executed the chain faithfully.

In the case of a name-get or subscription notification any chain value, including one with zero items, implies the request for the name-associated block to be sent.



=Garbage Collection

Blocks may be deleted by the server when they are no longer referenced, ultimately, by a name association.  The process of identifying and deleting these unreferenced blocks is called garbage collection.  References are recursive; e.g. if block A is considered referenced by a name association and within it there is a multihash for block B (i.e. a reference to block B), block B is also considered referenced by a name association.  What is considered a reference within a block is detailed below.

The server may garbage collect when and as often as it chooses, however if that server is being run by a commercial entity that charges based on amount of storage used, garbage collection should be performed before those charges are evaluated.


==Binary Block References

The header of a binary block contains zero or more multihashes, as detailed elswhere in this specification; from the perspective of the garbage collector these are references to other blocks.  No other part of the block is examined for references.


==Text Block References

The payload of text blocks are examined for likely multihashes; any that it finds is considered a reference.

A likely multihash is in the format:

┏━━━━━━━━━━━━━━┳━━━━━━┳━━━━━━━━━━━━━━┓
┃ non-alphanum ┃ b58s ┃ non-alphanum ┃
┗━━━━━━━━━━━━━━┻━━━━━━┻━━━━━━━━━━━━━━┛

Where non-alphanum is any UTF-8 character which is not in the following set:

ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789

Alternately, the beginning or ending points of the block payload qualifies as a substitution for non-alphanum; e.g. a payload consisting entirely of a single multihash is interpreted as having one likely multihash.

B58s is defined as more than 31 and less than 191 contiguous characters which are part of the set of valid base58 characters as defined by the multihash format.


==Recently Put Blocks

Any block stored (or would have been stored had it not existed already) by any operation, including those done by the server (e.g. a name-append block split), is considered to be referenced by a name association for the 256 seconds following that storage event.


=Permissions

Edsu permissions are layered, though not all layers are involved for each action.  Before a requested action is performed, each relevant layer is evaluated, and the permissions are logically ANDed together; i.e. permission must be given at all levels before the action is permitted.

==Name Prefix Layer

The first four characters of any name must be one of:

pub.
grp.
prv.

Actions involving a name which starts with pub. will always receive permission from this layer.

Actions involving a name which starts with grp. will recieve permission from this layer if the connection has a user-from associated with it; i.e. some form of authentication has succeeded, and therefore it is not an anonymous connection.

Actions involving a name which starts with prv. will recieve permission from this layer only if the connection's user-to and user-from are equal.


==Block Layer

Special blocks can have keys which enact this layer when actions involving them are requested.


===Name Block

The keys "edsu:deny" and "edsu:allow", if present in a name block, are enacted for any action which would reveal that the hash of that name block is associated with any given name (e.g. name-get, sub-notify, and the name listing service).

If the key "edsu:deny" exists, and the connection's user-from is part of the group it defines, permission is denied at this layer.

If the key "edsu:allow" exists, and the connection's user-from is not part of the group it defines, permission is denied at this layer.

The above two permissions are logically ANDed together.


===Appendable Block

The keys "edsu:append-deny", "edsu:append-allow", and "edsu:append-allow-anonymous", when present, are enacted when the name-append operation is attempted.  They are orthogonal to the name block permission keys.

If the key "edsu:append-deny" exists, and the connection's user-from is part of the group it defines, permission is denied at this layer.

If the key "edsu:append-allow" exists, and the connection's user-from is not part of the group it defines, permission is denied at this layer.

If the key "edsu:append-allow-anonymous" exists, its value is "true", and the connection is anonymous (i.e. no authentication was attempted or has succeeded), permission is granted.

The permission at this layer is in the form of ((deny AND allow) OR allow-anonymous), where deny, allow, and allow-anonymous are the permission outcomes of the previous three paragraphs respectively.


==Capabilities Layer

When a connection is authenticated, a capabilities block, chosen by the server as part of its authentication scheme, is interpreted and its capabilities are assigned to the connection for its duration.

The capabilities block is a special block, with the following definition:

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━┓
┃ key                       ┃ type                               ┃ required ┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━┫
┃ edsu:version              ┃ version                            ┃ ✓        ┃
┃ edsu:name-get             ┃ list-of-name-patterns              ┃          ┃
┃ edsu:name-append          ┃ list-of-name-patterns              ┃          ┃
┃ edsu:name-put             ┃ list-of-name-patterns-and-put-opts ┃          ┃
┃ edsu:visiting-name-get    ┃ list-of-name-patterns              ┃          ┃
┃ edsu:visiting-name-append ┃ list-of-name-patterns              ┃          ┃
┃ edsu:omni                 ┃ bool                               ┃          ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━┛

Where "edsu:version" must be "0.1", list-of-name-patterns is an ESON list of name patterns, and name-pattern-and-put-opts consists of a list of values consisting of a name pattern followed by zero, one, or both of the following (if both, the order is undefined):

┏━━━━━━━┳━━━━━━━━━━━━━┓
┃ space ┃ destructive ┃
┗━━━━━━━┻━━━━━━━━━━━━━┛
┏━━━━━━━┳━━━━━━━━━━━┓
┃ space ┃ visitable ┃
┗━━━━━━━┻━━━━━━━━━━━┛

Where space is ASCII 0x20, and "destructive" and "visitable" are the ASCII representations of those words.  These are called options.

In the context of capabilities blocks, a name pattern is considered to match a name if the two become identical when the final segment (and only the final segment) of the name is replaced by a "*".

Each complete sequence of characters representing a name pattern (as distinct from its semantic meaning) must appear no more than once in each list, otherwise the capabilities block must be rejected as invalid.


===Edsu:name-get

Any action which would reveal the hash associated with a name (or the existance of that association) only receives permission from this layer if the name matches at least one of the name patterns found in this list.


===Edsu:name-append

A name-append message only receives permission from this layer if the name in the message matches at least one of the name patterns found in this list.


===Edsu:name-put

To receive permission from this layer, a name-put message must match one or more of the values in this list.

To match a value, the name in the message must match thats value's name pattern, as well its options.

If the name-put operation is destructive, the "destructive" option must not be present for the value to match.

A name-put operation is considered destructive if it will potentially expose blocks to deletion by the garbage collector which otherwise might not be (either because the hash key is omitted from the name-put message, or the introduction or amendment of "edsu:once", "edsu:expires", or "edsu:appended-ttl" keys), and if the proposed new name block does not contain a valid "edsu:previous" key.

If the name-put operation has an impact on the effective permissions of a visitor in relation to the name, the "visitable" option must be present.

Impact on the effective permissions means that the new name block must have identical non-existance or values for "edsu:deny", "edsu:allow", "edsu:append-deny", "edsu:append-allow", "edsu:append-rate-limit", and "edsu:append-bytes-max" as the name block the action would replace (or in the case of a new name, must not contain any of these keys).


===Edsu:visiting-name-get

This is identical in function to edsu:name-get, except that edsu:name-get applies only to connections authenticated as owner, and edsu:visiting-name-get applies only to connections authenticated as visitor.


===Edsu:visiting-name-append

This is identical in function to edsu:name-append, except that edsu:name-append applies only to connections authenticated as owner, and edsu:visiting-name-append applies only to connections authenticated as visitor.


===Edsu:omni

The "edsu:omni" key being "true" will cause all actions to be evaluated as permitted at this layer.


===Derived Permissions

If the lists for the keys "edsu:name-append" or "edsu:name-put" are not empty, or if "edsu:omni" is true, the connection is permitted to put blocks.


==Services layer

Services, denoted by a second segment of "srv" in names used to interact with them, may institute their own permissions layer, as detailed in their specifications.


=Block Gets

If the hash is possessed by the client, regardless of the authentication method or capabilities of their connection, they can request the matching block (e.g. via the block-get message or as the result of a chain).


=Services

Unless otherwise noted, services accessed using a name-put will generate a new ESON block containing the detailed keys, and create or update a name association.  This will be referred to as the "return block" and "return name" in this section.

The return block may contain the "edsu:once" key with a value of "true" and an "edsu:expires" key with a value of not less than 256 seconds after the block is created.

The return name will be the request name with an additional segment, that segment being an echo of the "hash" key in the initiating name-put message.


==prv.srv.edsu.time.unix

A name-get for this name will result in the hash which resolves to a name block as follows:

┏━━━━━━━━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━┓
┃ key             ┃ type     ┃ required ┃
┣━━━━━━━━━━━━━━━━━╋━━━━━━━━━━╋━━━━━━━━━━┫
┃ edsu:time       ┃ u64      ┃ ✓        ┃
┗━━━━━━━━━━━━━━━━━┻━━━━━━━━━━┻━━━━━━━━━━┛

Where edsu:time is the server time in number of seconds elapsed since the Unix epoch.


==pub.srv.edsu.authentication.visitors.capabilities.*

This service is described in the connection initiation section of this specification.


==pub.srv.edsu.listings.blocks

A name-put to this address will cause the server to interpret the block referenced in that message as a metablock.  The server generates a new metablock containing the hashes of all blocks referenced in the client's metablock which the server does not have stored - this is the return block.

This service is the semantic equivalent of the client sending a block-get for each hash and compiling a list of hashes for which the server responds with a "not-found" oob.


==pub.srv.edsu.listings.names

A name-put to this address will cause the server to interpret the block referenced in the message as follows:

┏━━━━━━━━━━━━━┳━━━━━━┳━━━━━━━━━━┓
┃ key         ┃ type ┃ required ┃
┣━━━━━━━━━━━━━╋━━━━━━╋━━━━━━━━━━┫
┃ edsu:after  ┃ name ┃          ┃
┃ edsu:prefix ┃ name ┃          ┃
┗━━━━━━━━━━━━━┻━━━━━━┻━━━━━━━━━━┛

The server will create a list of all name associations contained in the user-to's account and sort them in ascending order according to the numeric ASCII values of the characters in the name.

Further, for a name to appear in this listing, it must be named (either directly or using a wildcard) in the "edsu:name-get" value list of the capabilities block for the connection.  I.e. the permission rules for listing and name-getting are similar, but not identical; they differ in that the rule stating that anything in the pub.* namespace is always readable does not apply in the case of listing.  Otherwise they are the same.

If the key "edsu:after" is present, all names from the beginning of the list up to and including the name specified will be discarded.

If the key "edsu:prefix" is present, all names which do not have an identical initial segments as the specified name will be discarded.  A name matching the specified name exactly is interpreted as having the same initial segments.

A return block with the key "edsu:names" pointing to the remaining list of names will be created by the server.  Alternatively the server may (because it would exceed the maximum size of a valid Edsu block or for any other reason) include only an initial portion of that list, in which case it must also include a key "edsu:truncated" with the value "true". 


==pub.srv.edsu.host.contact

This retrieves the contact information of whoever is hosting the Edsu account.  There are currently two optional keys, each pointing to a single value: edsu:email, and edsu:www.  These point to the email address and web site of the host, respectively, and are formatted according to their respective requirements.


==prv.srv.edsu.authentication.tokens.generate.*

This service generates either an owner or a visitor token.  To use it, a name is first generated by appending either "owner" or "visitor" to "prv.srv.edsu.authentication.tokens.generate.".  This name is then used in a name-put operation, using the hash of an ESON block with the following structure:

┏━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━━┓
┃ key               ┃ type      ┃ required ┃
┣━━━━━━━━━━━━━━━━━━━╋━━━━━━━━━━━╋━━━━━━━━━━┫
┃ edsu:capabilities ┃ multihash ┃ ✓        ┃
┃ edsu:label        ┃ utf8      ┃ ✓        ┃
┃ edsu:ttl          ┃ u64       ┃          ┃
┃ edsu:once         ┃ bool      ┃          ┃
┃ edsu:request-id   ┃ value     ┃          ┃
┗━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━┻━━━━━━━━━━┛

Where "edsu:capabilities" is the hash of a previously-stored capabilities block; "edsu:label" is an ESON-encoded UTF-8, human-readable string which can help the client identify the purpose of the token later (e.g. for removal); "edsu:ttl" (Time To Live), when present, sets the token to expire a given number of seconds after it is created (or never, if omitted); "edsu:once", if set to "true", will cause the token to be single-use; and "edsu:request-id" is machine-readble ID which can be used to group related tokens together - this can be any value, but must be unique to the request (e.g. a large randomized value).

This will generate either a token, or produce an OOB error.  In the former case, the response can be read by appending the multihash of the above ESON block as a segment to the name used in the request, and then using that in a name-get.  This will reference a block in the form:

┏━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━┓
┃ key        ┃ type  ┃ required ┃
┣━━━━━━━━━━━━╋━━━━━━━╋━━━━━━━━━━┫
┃ edsu:ok    ┃ bool  ┃ ✓        ┃
┃ edsu:token ┃ token ┃ ✓        ┃
┗━━━━━━━━━━━━┻━━━━━━━┻━━━━━━━━━━┛

Where "edsu:ok" will be "true", and where the token value can be used in the hello operation as described in that section.


==prv.srv.edsu.authentication.tokens.delete.*

This service deletes either an owner or a visitor token.  To use it, a name is first generated by appending either "owner" or "visitor" to "prv.srv.edsu.authentication.tokens.delete.", and then further appending the token itself as another segment.  In the case of a visitor token, note that the token to be used is the original, i.e. what was returned from the token generate service.  This name is then used in a name-get operation, which will either produce an OOB error, or delete the token and return the hash of the following block:

┏━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━┓
┃ key        ┃ type  ┃ required ┃
┣━━━━━━━━━━━━╋━━━━━━━╋━━━━━━━━━━┫
┃ edsu:ok    ┃ bool  ┃ ✓        ┃
┗━━━━━━━━━━━━┻━━━━━━━┻━━━━━━━━━━┛

Where the value of "edsu:ok" is "true".  This result happens whether the token in question exists or not - semantically its meaning is that the token no longer exists.


==*.srv.ext.*

Servers may introduce services not listed in this specification, but all names must fit the pattern of one of:

*.srv.ext.app.*
*.srv.ext.std.*

Where in each pattern the initial asterisk is replaced by one of pub, grp, and prv, and the trailing asterisk is replaced by one or more segments of the server's choosing.

By convention, the first, "app", pattern's use is unrestricted, while the second, "std", pattern should be used according to some published specification.

'''


# Util


def wrap_ssl(sock, host):
    context = ssl.SSLContext()
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    return context.wrap_socket(sock, server_hostname=host)


def get_hash(data):
    return base58.b58encode(b'\x12\x20' + base58.sha256(data).digest())


class InvalidEson(Exception):
    pass

class EdsuException(Exception):
    pass

class AuthException(Exception):
    pass

class CallException(Exception):
    pass

class NotFound(Exception):
    pass


def eson_parse(data):
    bytes_used = 0
    eson = {}
    prev_key = None
    lines = data.split(b'\n')
    for line_num, line in enumerate(lines):
        last_line = line_num == len(lines)-1
        # For detecting the end of the document
        if line == b'':
            if last_line:
                return (None, 0)
            return (eson, bytes_used+1)
        # Split line
        try:
            key, val = line.split(b' ', 1)
        except:
            if last_line:
                return (None, 0)
            raise InvalidEson('line without value: {}'.format(line_num))
        # Validate key
        if key == b'^':
            caret = True
            if not prev_key:
                raise InvalidEson('orphaned ^ key: {}'.format(line_num))
            key = prev_key
        else:
            caret = False
            if set(key) - set(ESON_KEY_VALID_CHARS) != set():
                raise InvalidEson('invalid key: {} {}'.format(line_num, key))
            key = str(key, 'utf8')
            prev_key = key
        # Validate value
        if set(val) - set(ESON_VALUE_VALID_CHARS) != set():
            raise InvalidEson('invalid value: {} {}'.format(line_num, val))
        val = str(val, 'utf8')
        # Put in document
        if not caret or not key in eson:
            eson[key] = val
        elif isinstance(eson[key], list):
            eson[key].append(val)
        else:
            eson[key] = [eson[key], val]
        bytes_used += len(line) + 1
    return (None, 0)


def eson_emit(eson):
    lines = []
    for key in sorted(eson.keys()):
        val = eson[key]
        if isinstance(val, list):
            for i, v in enumerate(val):
                lines.append('{} {}\n'.format(key if i == 0 else '^', v))
        else:
            lines.append('{} {}\n'.format(key, val))
    lines.append('\n')
    return ''.join(lines)


def block_parse(data):
    if data[0] == 0x7e: # ~
        # Text block
        contexts_loc = data.index(b'\n') + 1
        return {
            'is_bin': False,
            'salt': data[1:contexts_loc-1],
            'contents': data[contexts_loc:]
        }
    else:
        raise Exception('unimplemented')


def eson_to_block(eson):
    contents = eson_emit(eson)
    return {
        'is_bin': False,
        'salt': b'',
        'contents': bytes(contents, 'utf8'),
    }


def text_block_emit(block):
    if block['is_bin']:
        raise Exception('not text block')
    buf = io.BytesIO()
    buf.write(b'~')
    buf.write(block['salt'])
    buf.write(b'\n')
    buf.write(block['contents'])
    return buf.getvalue()


def bin_block_emit(vers, salt, hashes, contents, cloc_delta=0, hloc_delta=0):
    buf0 = io.BytesIO()
    buf1 = io.BytesIO()
    buf1.write(salt)
    hloc = len(buf1.getvalue()) + 5 + hloc_delta
    for x in hashes:
        buf1.write(base58.b58decode(x))
    cloc = len(buf1.getvalue()) + 5 + cloc_delta
    buf0.write(bytes([vers]))
    buf0.write(bytes([(cloc >> 8) & 0xff]))
    buf0.write(bytes([(cloc >> 0) & 0xff]))
    buf0.write(bytes([(hloc >> 8) & 0xff]))
    buf0.write(bytes([(hloc >> 0) & 0xff]))
    buf0.write(buf1.getvalue())
    buf0.write(contents)
    return buf0.getvalue()


def encode_eson_utf8(x):
    out = io.BytesIO()
    b58buf = None
    for ch in bytes(x, 'utf8'):
        if ch >= 0x20 and ch < 0x7e:
            if b58buf:
                out.write(b'~')
                out.write(bytes(base58.b58encode(b58buf.getvalue()), 'utf8'))
                out.write(b'^')
                b58buf = None
            out.write(bytes([ch]))
        else:
            if not b58buf:
                b58buf = io.BytesIO()
            b58buf.write(bytes([ch]))
    if b58buf:
        out.write(b'~')
        out.write(bytes(base58.b58encode(b58buf.getvalue()), 'utf8'))
    return str(out.getvalue(), 'utf8')



def decode_eson_utf8(x):
    out = io.StringIO()
    b58buf = None
    for ch in x:
        if b58buf:
            if ch == '^':
                out.write(str(base58.b58decode(b58buf.getvalue()), 'utf8'))
                b58buf = None
            else:
                b58buf.write(ch)
        else:
            if ch == '~':
                b58buf = io.StringIO()
            else:
                out.write(ch)
    if b58buf:
        out.write(str(base58.b58decode(b58buf.getvalue()), 'utf8'))
    return out.getvalue()


ConnState = {
    'ws': False,
    'omni_tokens': {},
    'cache': {},
}

class Conn():
    def __init__(self, username, ws=False, auth='anon'):
        host = username.replace('@', '.edsu.')
        if ws:
            self.sock = websocket.WebSocket()
            self.sock.connect('wss://{}/edsu/ws'.format(host))
        else:
            port = 3216
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(('localhost', port))
            self.sock = wrap_ssl(sock, host)
        self.read_buf = io.BytesIO()
        self.username = username
        # Auth
        if auth == 'anon':
            self.send_bytes(b'edsu hello\nversions 0.1\n\n')
            resp = self.read_response()
            if resp['edsu'] != 'hello':
                raise EdsuException('failed at hello: {}'.format(resp))
        elif auth == 'omni':
            if username not in ConnState['omni_tokens']:
                sub_conn = Conn(username, auth='none')
                sub_conn.send_bytes(b'edsu hello\nversions 0.1\nsecret inttest\n\n')
                sub_conn.read_response() # Grab the server hello
                if sub_conn.read_response()['edsu'] != 'authenticated':
                    raise EdsuException('failed trying to PW authenticate')
                # Create an omni caps block
                caps_hash = sub_conn.block_put(eson_to_block({'edsu:omni': '*'}))
                token = sub_conn.srv_call('pub.srv.edsu.authentication.tokens.generate', {
                    'edsu:description': 'inttest generated',
                    'edsu:capabilities': caps_hash,
                })['edsu:token']
                ConnState['omni_tokens'][username] = token
            else:
                token = ConnState['omni_tokens'][username]
            self.send_bytes(b'edsu hello\nversions 0.1\ntoken ')
            self.send_bytes(bytes(token, 'utf8'))
            self.send_bytes(b'\n\n')
            resp = self.read_response()
            if resp['edsu'] != 'hello':
                raise EdsuException('failed at hello: {}'.format(resp))
            resp = self.read_response()
            if resp['edsu'] != 'authenticated':
                raise EdsuException('failed at auth: {}'.format(resp))
        elif auth == 'none':
            pass
        else:
            # Presumably a token
            self.send_bytes(b'edsu hello\nversions 0.1\ntoken {}\n\n'.format(auth))
            resp = self.read_response()
            if resp['edsu'] != 'hello':
                raise EdsuException('failed at hello: {}'.format(resp))
            resp = self.read_response()
            if resp['edsu'] != 'authenticated':
                raise EdsuException('failed at auth: {}'.format(resp))
    def send_bytes(self, data):
        sent = 0
        while sent < len(data):
            sent += self.sock.send(data[sent:])
    def read_response(self, timeout_s=16):
        remaining_payload_bytes = 0
        payload_buf = io.BytesIO()
        eson = None
        do_network_read = len(self.read_buf.getvalue()) == 0
        while 1:
            # We might not need new bytes, but if we do, see if the network as some
            if do_network_read:
                self.sock.settimeout(timeout_s)
                self.read_buf.write(self.sock.recv())
                network_read_done = True
            if remaining_payload_bytes == 0:
                eson, bytes_consumed = eson_parse(self.read_buf.getvalue())
                self.read_buf = io.BytesIO(self.read_buf.getvalue()[bytes_consumed:])
                if eson == {}:
                    # Eat newlines
                    continue
                if eson == None:
                    # Partial ESON, loop around
                    do_network_read = True
                    continue
                # We have a complete ESON doc, figure out if we need to keep going for the payload
                if 'payload-length' in eson:
                    remaining_payload_bytes = int(eson['payload-length']) + 1
                    eson.pop('payload-length')
                else:
                    return eson
            # Grab payload (if there is one)
            to_add = self.read_buf.getvalue()[:remaining_payload_bytes]
            payload_buf.write(to_add)
            self.read_buf = io.BytesIO(self.read_buf.getvalue()[len(to_add):])
            remaining_payload_bytes -= len(to_add)
            if remaining_payload_bytes == 0:
                payload = payload_buf.getvalue()
                if payload[-1] != 0x0a: # \n
                    raise Exception('invalid message: payload not terminated by new line')
                eson['payload'] = payload[:-1]
                return eson
            else:
                do_network_read = True
    def block_get(self, hash, not_found=Exception):
        if hash in ConnState['cache']:
            return ConnState['cache'][hash]
        self.send_bytes(b'edsu block-get\nhash ' + bytes(hash, 'utf8') + b'\n\n')
        resp = self.read_response()
        if resp['edsu'] != 'block':
            if resp['edsu'] == 'oob' and resp['code'] == 'not-found':
                if not_found == Exception:
                    raise NotFound('block {}'.format(hash))
                return not_found
            raise EdsuException('failed at block-get {}: {}'.format(hash, resp))
        block = block_parse(resp['payload'])
        ConnState['cache'][hash] = block
        return block
    def name_get(self, name, not_found=Exception):
        self.send_bytes(b'edsu name-get\nname ' + bytes(name, 'utf8') + b'\n\n')
        resp = self.read_response()
        if resp['edsu'] != 'name':
            if resp['edsu'] == 'oob' and resp['code'] == 'not-found':
                if not_found == Exception:
                    raise NotFound('name {}'.format(name))
                return not_found
            raise EdsuException('failed at name-get {}: {}'.format(name, resp))
        return resp['hash']
    def name_block_get(self, name, not_found=Exception):
        hash = self.name_get(name, not_found)
        return eson_parse(self.block_get(hash)['contents'])[0]
    def name_put(self, name, hash):
        self.send_bytes(b'edsu name-put\nname ')
        self.send_bytes(bytes(name, 'utf8'))
        self.send_bytes(b'\nhash ')
        self.send_bytes(bytes(hash, 'utf8'))
        self.send_bytes(b'\n\n')
        resp = self.read_response()
        if resp['edsu'] != 'ok':
            raise EdsuException('failed at name-put {} {}: {}'.format(name, hash, resp))
    def block_put(self, block):
        data = text_block_emit(block)
        self.send_bytes(b'edsu block-put\npayload-stop ')
        self.send_bytes(bytes(str(len(data)), 'utf8'))
        self.send_bytes(b'\n\n')
        self.send_bytes(data)
        self.send_bytes(b'\n')
        resp = self.read_response()
        if resp['edsu'] != 'ok':
            raise EdsuException('failed at block-put: {} {}'.format(block, resp))
        return resp['hash']
    def srv_call(self, name, eson, except_on_err=True):
        block = eson_to_block(eson)
        hash = self.block_put(block)
        self.name_put(name, hash)
        resp = self.name_block_get('{}.{}'.format(name, hash))
        if except_on_err:
            if resp.get('ok', None) != 'true':
                raise CallException('{}: {} {}'.format(name, eson, resp))
        return resp



TestState = {
    'exhaustive': False,
}
def is_exhaustive():
    return TestState['exhaustive']




def run_sections(section_order):
    if set(TestFns.keys()) != set(section_order):
        raise Exception('missing or unexpected sections')
    fails = 0
    successes = 0
    print('')
    for section in section_order:
        print('Section: {}'.format(section))
        for test in TestFns[section]:
            print('Test: {}'.format(test['name']))
            msg = None
            try:
                msg = test['fn']()
                if msg != None:
                    assert False # Read the MESSAGE below :)
                successes += 1
            except AssertionError:
                _, _, tb = sys.exc_info()
                tb_info = traceback.extract_tb(tb)
                filename, line, func, text = tb_info[-1]
                fails += 1
                print('ERROR: Line {}, {}:{}, "{}"'.format(
                        line, section, test['name'], text))
                if test['fn'].__doc__:
                    print('DOC: {}'.format(test['fn'].__doc__))
                if msg:
                    print('MESSAGE: {}'.format(msg))
                if test['die']:
                    print('\n\u001b[1m\u001b[31mFATAL ERROR\u001b[0m: exiting early')
                    return
            except Exception as e:
                raise e
    if fails == 0:
        print('\u001b[1m\u001b[32m')
    else:
        print('\u001b[1m\u001b[31m')
    print('COMPLETE\u001b[0m: Success {}, Fail {}'.format(successes, fails))


def main(exhaustive=False):
    TestState['exhaustive'] = exhaustive
    section_order = [
        'conn',
        'types',
        'hello',
        'eson',
        'blocks',
        'msg',
    ]
    run_sections(section_order)


def spec_to_text(html=False, out_file_name=None):
    with open(__file__) as fp:
        lines = fp.read().splitlines()
    in_code = True
    out_lines = []
    heir = defaultdict(lambda: 0)
    for line in lines:
        # Skip the code
        if line.strip() == "'''":
            in_code = not in_code
            continue
        if in_code:
            continue

        # Replace titles with numbering
        title_match = re.match('^(=+)', line)
        if title_match:
            title_depth = len(title_match.group(0))
            title_parts = []
            for i in range(title_depth):
                if i == title_depth-1:
                    heir[i] += 1
                    j = 1
                    while heir[i+j] != 0:
                        heir[i+j] = 0
                        j += 1
                title_parts.append(str(heir[i]))
            line = '.'.join(title_parts) + ' ' + line[title_depth:].strip()
            if html:
                line = '<h{}>{}</h{}>'.format(title_depth+1, line, title_depth+1)

        # Line wrap
        if line.strip() == '':
            out_lines.append('')
            continue
        
        out_lines += textwrap.wrap(line, width=80, drop_whitespace=False,
                                   replace_whitespace=False)
    out_lines = [x.strip() for x in out_lines]
    if out_file_name:
        with open(out_file_name, 'w') as fp:
            fp.write('\n'.join(out_lines) + '\n')
    else:
        for line in out_lines:
            if html:
                if '┏' in line:
                    line = '<div class="dia">' + line
                elif '┛' in line:
                    line = line + '</div>'
                line = line.replace('✓', '&#9745;')
            print(line)


try: __IPYTHON__
except:
    if 'html' in sys.argv:
        spec_to_text(html=True)
    else:
        spec_to_text()
