use std::collections::{BTreeMap, HashMap};
use std::fmt::Write;
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader};
use std::path::PathBuf;
use std::{env, fmt, io, mem};

fn main() {
    // Generate code
    let out_dir = env::var("OUT_DIR").unwrap();
    let root_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    let mut in_path = PathBuf::from(&root_dir);
    in_path.push("spec");
    in_path.push("msg");
    let mut out_path = PathBuf::from(&out_dir);
    out_path.push("edsu_types.rs");
    gen_edsu_types(&in_path, &out_path).unwrap();
}

#[derive(Debug, Clone)]
struct Field {
    name: String,
    type_: String,
    required: bool,
}

#[derive(PartialEq, Eq)]
enum Presence {
    None,
    Required,
    Optional,
}

fn gen_edsu_types(in_path: &PathBuf, out_path: &PathBuf) -> Result<(), fmt::Error> {
    // Parse from text format
    let in_fp = File::open(in_path).unwrap();
    let mut parsed = HashMap::new();
    let mut partial_context = HashMap::new();
    let mut partial_msg = vec![];
    let mut lines = BufReader::new(in_fp)
        .lines()
        .collect::<Vec<io::Result<String>>>();
    for line in lines.drain(..).rev() {
        let line = line.unwrap();
        if line.trim() == "" {
            continue;
        } else if line.starts_with("#!") {
            let context = line.split_at(2).1.trim();
            let mut pc = HashMap::new();
            mem::swap(&mut pc, &mut partial_context);
            parsed.insert(context.to_owned(), pc);
        } else if line.starts_with(" ") || line.starts_with("\t") {
            let mut field_bits = line.trim().split_whitespace();
            let name = field_bits.next().unwrap().to_owned();
            let type_ = field_bits.next().unwrap().to_owned();
            let rest = field_bits.collect::<Vec<&str>>();
            partial_msg.push(Field {
                name,
                type_,
                required: rest.contains(&"required"),
            });
        } else {
            let msg = line.trim();
            let mut pm = vec![];
            mem::swap(&mut pm, &mut partial_msg);
            partial_context.insert(msg.to_owned(), pm);
        }
    }

    // Merge common into all the messages
    for msgs in parsed.values_mut() {
        let common = msgs.remove("common").unwrap();
        for m in msgs.values_mut() {
            m.extend(common.to_vec());
        }
    }

    // Write out
    let mut out_fp = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(out_path)
        .unwrap();
    for (context, mut msgs) in parsed.drain() {
        // Restructure the fields (want them to be ordered)
        let mut ordered = vec![];
        let mut channel_presence = Presence::None;
        for (msg_type, mut fields) in msgs.drain() {
            let mut ord_fields = BTreeMap::new();
            for Field {
                name,
                type_,
                required,
            } in fields.drain(..)
            {
                if &name == "channel" {
                    if required {
                        channel_presence = Presence::Required
                    } else {
                        channel_presence = Presence::Optional
                    }
                }
                ord_fields.insert(name, (type_, required));
            }
            ordered.push((msg_type, ord_fields));
        }

        // Structs
        let mut structs = String::new();
        for &(ref msg_type, ref fields) in ordered.iter() {
            write!(structs, "    {} {{\n", to_happy_camel(&msg_type))?;
            for (ref name, &(ref type_, ref required)) in fields.iter() {
                if name == &"payload-length" || name == &"payload-stop" {
                    if *required {
                        write!(structs, "        payload: Vec<u8>,\n")?;
                    } else {
                        write!(structs, "        payload: Option<Vec<u8>>,\n")?;
                    }
                } else {
                    if *required {
                        write!(
                            structs,
                            "        {}: {},\n",
                            to_snake(&name),
                            translate_type(&type_)
                        )?;
                    } else {
                        write!(
                            structs,
                            "        {}: Option<{}>,\n",
                            to_snake(&name),
                            translate_type(&type_)
                        )?;
                    }
                }
            }
            write!(structs, "    }},\n")?;
        }

        // Froms
        let mut froms = String::new();
        for &(ref msg_type, ref fields) in ordered.iter() {
            write!(froms, "            \"{}\" => {{\n", msg_type)?;
            write!(
                froms,
                "                Ok({}::{} {{\n",
                context,
                to_happy_camel(&msg_type)
            )?;
            for (ref name, &(_, ref required)) in fields.iter() {
                if name == &"payload-length" || name == &"payload-stop" {
                    if *required {
                        write!(froms, "                    payload: _payload.ok_or_else(|| Error::InvalidMessage(\
                                       \"missing required payload\".to_owned()))?,\n")?;
                    } else {
                        write!(froms, "                    payload: _payload,\n")?;
                    }
                } else {
                    if *required {
                        write!(
                            froms,
                            "                    {}: into_required_single(&mut es, \"{}\")?,\n",
                            to_snake(&name),
                            name
                        )?;
                    } else {
                        write!(
                            froms,
                            "                    {}: into_optional_single(&mut es, \"{}\")?,\n",
                            to_snake(&name),
                            name
                        )?;
                    }
                }
            }
            write!(froms, "                }})\n")?;
            write!(froms, "            }},\n")?;
        }

        // Tos
        let mut tos = String::new();
        for &(ref msg_type, ref fields) in ordered.iter() {
            let mut payload_type = Presence::None;
            write!(
                tos,
                "            {}::{} {{ ",
                context,
                to_happy_camel(&msg_type)
            )?;
            for (ref name, _) in fields.iter() {
                if name == &"payload-length" || name == &"payload-stop" {
                    write!(tos, "ref payload,")?;
                } else {
                    write!(tos, "ref {},", to_snake(&name))?
                }
            }
            write!(tos, "}} => {{\n")?;
            write!(
                tos,
                "                ret = b\"edsu {}\\n\".to_vec();\n",
                msg_type
            )?;

            for (ref name, &(_, ref required)) in fields.iter() {
                if name == &"payload-length" || name == &"payload-stop" {
                    if !*required {
                        write!(
                            tos,
                            "                if let Some(payload) = payload.as_ref() {{\n"
                        )?;
                    }
                    write!(
                        tos,
                        "                ret.write_all(b\"{} \").unwrap();\n",
                        name
                    )?;
                    write!(tos, "                ret.write_all(format!(\"{{}}\", payload.len()).as_bytes()).unwrap();\n")?;
                    write!(tos, "                ret.write_all(b\"\\n\").unwrap();\n")?;
                    if !*required {
                        write!(tos, "                }}\n")?;
                        payload_type = Presence::Optional;
                    } else {
                        payload_type = Presence::Required;
                    }
                } else {
                    if *required {
                        write!(
                            tos,
                            "                write_required_single(&mut ret, b\"{}\", {});\n",
                            name,
                            to_snake(&name)
                        )?;
                    } else {
                        write!(
                            tos,
                            "                write_optional_single(&mut ret, b\"{}\", {});\n",
                            name,
                            to_snake(&name)
                        )?;
                    }
                }
            }
            write!(tos, "                ret.push(b'\\n');\n")?;
            match payload_type {
                Presence::Optional => write!(
                    tos,
                    "                if let Some(p) = payload.as_ref() {{ ret.extend(p); ret.push(b'\\n'); }};\n"
                )?,
                Presence::Required => {
                    write!(tos, "                ret.extend(payload);\n")?;
                    write!(tos, "                ret.push(b'\\n');\n")?;
                }
                Presence::None => (),
            }
            write!(tos, "            }},\n")?;
        }

        // Channel
        let mut channel = String::new();
        for &(ref msg_type, _) in ordered.iter() {
            write!(
                channel,
                "            {}::{} {{ ref channel, .. }} => {{\n",
                context,
                to_happy_camel(&msg_type)
            )?;
            match channel_presence {
                Presence::Required => write!(channel, "                channel.clone()\n")?,
                Presence::Optional => {
                    write!(channel, concat!("                match channel {{\n",
                                            "                    Some(ref c) => c.clone(),\n",
                                            "                    None => eson::Value::from_bytes(b\"0\").unwrap()\n",
                                            "                }}\n"))?;
                }
                _ => panic!(),
            }
            write!(channel, "            }},\n")?;
        }

        // Write type
        let mut out = String::new();
        write!(
            out,
            r#"
#[derive(Debug)]
pub enum {context} {{
{structs}
}}

impl FromEson<{context}> for {context} {{
    fn from_eson(mut es: Eson, _payload: Option<Vec<u8>>) -> Result<Self, Error> {{
        let type_:Value = into_required_single(&mut es, "edsu")?;
        match type_.as_str() {{
{froms}
            _ => Err(Error::InvalidMessage(format!("unknown message type: {{}}", String::from_utf8_lossy(&type_.as_bytes())))),
        }}
    }}
}}
impl {context} {{
    pub fn to_eson_bytes(&self) -> Vec<u8> {{
        let mut ret;
        match self {{
{tos}
        }}
        ret
    }}
    pub fn get_channel(&self) -> Value {{
        match self {{
{channel}
        }}
    }}
}} "#,
            context = context,
            structs = structs,
            froms = froms,
            tos = tos,
            channel = channel
        )?;
        {
            use std::io::Write;
            out_fp.write_all(out.as_bytes()).unwrap();
        }
    }
    Ok(())
}

fn to_happy_camel(x: &str) -> String {
    // Special case: Ok is conflicts with the prelude
    if x == "ok" {
        return "Ok_".to_owned();
    }

    let mut ret = String::new();
    let mut cap = true;
    for ch in x.chars() {
        if ch == '-' {
            cap = true;
        } else if cap {
            cap = false;
            ret.push(ch.to_ascii_uppercase());
        } else {
            ret.push(ch);
        }
    }
    ret
}

fn to_snake(x: &str) -> String {
    x.to_owned().replace('-', "_")
}

fn translate_type(x: &str) -> String {
    match x {
        "value" => "Value",
        "multihash" => "Multihash",
        "version" => "Version",
        "versions" => "Versions",
        "encodings" => "Encodings",
        "bool" => "bool",
        "u32" => "u32",
        "u16" => "u16",
        "u64" => "u64",
        "user-id" => "UserId",
        "name" => "Name",
        "address" => "Address",
        "chain" => "Chain",
        "oob-code" => "OobCode",
        "length-or-stop" => "LengthOrStop",
        "token" => "Token",
        _ => panic!("unknown type: {}", x),
    }
    .to_owned()
}
