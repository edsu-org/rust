use std::borrow::Borrow;
use std::collections::{self, hash_map, HashMap};
use std::hash::Hash;
use std::io::{self, Write};
use std::{fmt, mem, str};

#[derive(Debug)]
pub enum Error {
    InvalidKey(String),
    InvalidValue(String),
    InvalidEson(String),
    NotFound,
    UnexpectedList(String),
    RequiredKeyNotFound(String),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Eson(HashMap<Key, Vec<Value>>);

impl Eson {
    pub fn from_bytes(bytes: &[u8]) -> Result<Self, Error> {
        let mut iter = bytes.iter().cloned();
        let eson = match Parser::new().add_bytes(&mut iter) {
            Ok(Some(n)) => Ok(n),
            Ok(None) => Err(Error::InvalidEson("incomplete".to_owned())),
            Err(e) => Err(Error::InvalidEson(format!("{:?}", e))),
        }?;

        // Disallow anything following the ESON
        if iter.count() != 0 {
            Err(Error::InvalidEson("trailing data".to_owned()))
        } else {
            Ok(eson)
        }
    }
    pub fn new() -> Self {
        Eson(HashMap::new())
    }
    pub fn write_into<T: Write>(&self, writable: &mut T) -> io::Result<()> {
        // Spin through sorted keys, converting each key and val to bytes
        let mut keys = self.0.keys().cloned().collect::<Vec<Key>>();
        keys.sort();
        for key in keys {
            let val_vec = &self.0[&key];
            for (i, v) in val_vec.iter().enumerate() {
                // Key
                if i == 0 {
                    writable.write_all(key.as_bytes())?;
                } else {
                    writable.write_all(b"^")?;
                }

                // Space, value, newline
                writable.write_all(b" ")?;
                writable.write_all(v.as_bytes())?;
                writable.write_all(b"\n")?;
            }
        }
        // Final newline
        writable.write_all(b"\n")?;
        Ok(())
    }
    pub fn get_single<Q: ?Sized>(&self, k: &Q) -> Result<Option<&Value>, Error>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
        Q: fmt::Display,
    {
        if let Some(v) = self.get(k) {
            if v.len() != 1 {
                return Err(Error::UnexpectedList(format!("{}", k)));
            }
            Ok(Some(&v[0]))
        } else {
            Ok(None)
        }
    }
    pub fn get_req_single<Q: ?Sized>(&self, k: &Q) -> Result<&Value, Error>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
        Q: fmt::Display,
    {
        if let Some(v) = self.get_single(k)? {
            Ok(v)
        } else {
            Err(Error::RequiredKeyNotFound(format!("{}", k)))
        }
    }
    pub fn get_req<Q: ?Sized>(&self, k: &Q) -> Result<&Vec<Value>, Error>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
        Q: fmt::Display,
    {
        if let Some(v) = self.get(k) {
            Ok(v)
        } else {
            Err(Error::RequiredKeyNotFound(format!("{}", k)))
        }
    }
    pub fn get_single_parsed<Q: ?Sized, T, E>(
        &self,
        k: &Q,
        f: &dyn Fn(&Value) -> Result<T, E>,
    ) -> Result<Option<T>, Error>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
        Q: fmt::Display,
        E: fmt::Debug,
    {
        if let Some(x) = self.get_single(k)? {
            Ok(Some(
                f(x).map_err(|e| Error::InvalidValue(format!("{:?}", e)))?,
            ))
        } else {
            Ok(None)
        }
    }
    pub fn get_req_single_parsed<Q: ?Sized, T, E>(
        &self,
        k: &Q,
        f: &dyn Fn(&Value) -> Result<T, E>,
    ) -> Result<T, Error>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
        Q: fmt::Display,
        E: fmt::Debug,
    {
        let x = self.get_req_single(k)?;
        Ok(f(x).map_err(|e| Error::InvalidValue(format!("{:?}", e)))?)
    }
    // Passing through the HashMap fns
    pub fn get_mut<Q: ?Sized>(&mut self, k: &Q) -> Option<&mut Vec<Value>>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.0.get_mut(k)
    }
    pub fn get<Q: ?Sized>(&self, k: &Q) -> Option<&Vec<Value>>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.0.get(k)
    }
    pub fn contains_key<Q: ?Sized>(&self, k: &Q) -> bool
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.0.contains_key(k)
    }
    pub fn insert(&mut self, k: Key, v: Vec<Value>) -> Option<Vec<Value>> {
        self.0.insert(k, v)
    }
    pub fn remove<Q: ?Sized>(&mut self, k: &Q) -> Option<Vec<Value>>
    where
        Key: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.0.remove(k)
    }
    pub fn entry(&mut self, key: Key) -> collections::hash_map::Entry<Key, Vec<Value>> {
        self.0.entry(key)
    }
    pub fn keys(&self) -> hash_map::Keys<Key, Vec<Value>> {
        self.0.keys()
    }
}

impl Default for Eson {
    fn default() -> Self {
        Eson::new()
    }
}

#[derive(Clone, PartialEq, Eq, Hash, Debug, PartialOrd, Ord)]
pub struct Key(String);

enum KeyRepr {
    Key(Key),
    Caret,
}

impl Key {
    pub fn from_bytes(bytes: &[u8]) -> Result<Key, Error> {
        match Self::repr_from_bytes(bytes)? {
            KeyRepr::Key(k) => Ok(k),
            KeyRepr::Caret => Err(Error::InvalidKey("not expecting ^".to_owned())),
        }
    }
    pub fn from_static(x: &'static str) -> Self {
        Self::from_bytes(x.as_bytes()).unwrap()
    }
    fn repr_from_bytes(bytes: &[u8]) -> Result<KeyRepr, Error> {
        // Validate (no invalid chars or adjacent segment separators) while copying to an owed
        if bytes.is_empty() {
            return Err(Error::InvalidKey("zero-length segment".to_owned()));
        }

        if bytes[0] == b'^' {
            Ok(KeyRepr::Caret)
        } else {
            // Name
            let mut preceeding_char = ':';
            let mut key = String::with_capacity(bytes.len());
            for (i, x) in bytes.iter().enumerate() {
                let x = *x as char;
                if x == ':' {
                    if preceeding_char == ':' || i == bytes.len() - 1 {
                        return Err(Error::InvalidKey("zero-length segment".to_owned()));
                    }
                } else if !((x >= 'a' && x <= 'z') || (x >= '0' && x <= '9') || (x == '-')) {
                    return Err(Error::InvalidKey(format!("invalid character {:?}", x)));
                }
                key.push(x);
                preceeding_char = x;
            }
            Ok(KeyRepr::Key(Key(key)))
        }
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn into_bytes(self) -> Vec<u8> {
        self.0.into_bytes()
    }
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
    pub fn into_string(self) -> String {
        self.0
    }
    pub fn as_str(&self) -> &str {
        &self.0
    }
}

impl Borrow<str> for Key {
    fn borrow(&self) -> &str {
        &self.0[..]
    }
}
impl fmt::Display for Key {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[macro_export]
macro_rules! key {
    ($k:expr) => {
        Key::from_static($k)
    };
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Value(String);

impl Value {
    pub fn from_bytes(bytes: &[u8]) -> Result<Self, Error> {
        let mut value = String::with_capacity(bytes.len());
        // Validate (no invalid chars) while copying to an owned
        for x in bytes.iter() {
            let x = *x as char;
            value.push(x);
            if !(x >= ' ' && x <= '~') {
                return Err(Error::InvalidValue(format!("invalid character {:?}", x)));
            }
        }
        Ok(Value(value))
    }
    pub fn empty() -> Self {
        Value("".to_owned())
    }
    pub fn from_static(s: &'static str) -> Self {
        Value::from_bytes(s.as_bytes()).unwrap()
    }
    pub fn into_bytes(self) -> Vec<u8> {
        self.0.into_bytes()
    }
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
    pub fn into_string(self) -> String {
        self.0
    }
    pub fn as_str(&self) -> &str {
        &self.0
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn split_off_sep(self, ch: char) -> (Value, Option<Value>) {
        let Value(mut st) = self;
        let ci = st.find(ch);
        if let Some(i) = ci {
            let mut rhs = st.split_off(i);
            if !rhs.is_empty() {
                rhs.remove(0);
            }
            (Value(st), Some(Value(rhs)))
        } else {
            (Value(st), None)
        }
    }
    pub fn truncate(&mut self, len: usize) {
        self.0.truncate(len);
    }
}

impl str::FromStr for Value {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Error> {
        Value::from_bytes(s.as_bytes())
    }
}

#[macro_export]
macro_rules! value {
    ($k:expr) => {
        Value::from_static($k)
    };
}

pub struct Parser {
    current_key: Option<(Key, usize)>,
    prev_key: Option<(Key, usize)>,
    new_key: bool,
    incomplete: Vec<u8>,
    eson: Eson,
}

impl Parser {
    pub fn new() -> Self {
        Parser {
            current_key: None,
            prev_key: None,
            new_key: true,
            incomplete: Vec::new(),
            eson: Eson::new(),
        }
    }
    pub fn add_bytes<I>(&mut self, iter: &mut I) -> Result<Option<Eson>, Error>
    where
        I: Iterator<Item = u8>,
    {
        // Using this construct instead of a for loop so that I don't consume the entire iter
        for byte in iter {
            if self.current_key.is_none() && byte as char == ' ' {
                // Key is finished
                match Key::repr_from_bytes(&self.incomplete)? {
                    KeyRepr::Key(key) => {
                        self.current_key = Some((key, 0));
                        self.new_key = true;
                    }
                    KeyRepr::Caret => {
                        if let Some((cur_key, cur_index)) = self.prev_key.take() {
                            self.current_key = Some((cur_key, cur_index + 1));
                            self.new_key = false;
                        } else {
                            return Err(Error::InvalidKey("orphaned ^".to_owned()));
                        }
                    }
                }
                self.incomplete.clear();
            } else if self.current_key.is_some() && byte as char == '\n' {
                // Value is finished
                let (key, index) = self.current_key.take().unwrap();
                let val = Value::from_bytes(&self.incomplete)?;
                // Make sure we've got a Vec
                if self.new_key {
                    self.eson.insert(key.clone(), Vec::new());
                }
                let v = self.eson.get_mut(&key).unwrap();
                // Put the value into the Vec
                v.push(val);
                self.incomplete.clear();
                self.prev_key = Some((key, index));
            } else if self.current_key.is_none()
                && byte as char == '\n'
                && self.incomplete.is_empty()
            {
                // Eson is finished
                let mut eson = Eson::new();
                mem::swap(&mut eson, &mut self.eson);
                return Ok(Some(eson));
            } else {
                self.incomplete.push(byte);
            }
        }
        Ok(None)
    }
}

impl Default for Parser {
    fn default() -> Self {
        Parser::new()
    }
}
