use crate::eson::{self, Eson};
use crate::prelude::Either::Left;
use crate::prelude::*;
use crate::util;
use bs58;
use ring::digest;
use std::borrow::Borrow;
use std::collections::{self, HashMap, VecDeque};
use std::hash::Hash;
use std::io::{self, Write};
use std::str::{self, FromStr};
use std::{convert, fmt};

include!(concat!(env!("OUT_DIR"), "/edsu_types.rs"));

const BLOCK_LOCS_LEN: usize = 5;

#[derive(Debug)]
pub enum Error {
    InvalidMessage(String),
    InvalidHeader(String),
    InvalidValue(String),
    InvalidBlockLength,
    InvalidBlock(String),
    Io(io::Error),
    Eson(eson::Error),
    PermissionDenied,
    RateLimited(u32),
}

impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}
impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

pub trait ValueRead<T> {
    fn from_value(val: eson::Value) -> Result<T, Error>;
}

pub trait ValueWrite {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error>;
}

pub fn from_bytes<T>(bytes: &[u8]) -> Result<T, Error>
where
    T: ValueRead<T>,
{
    let val = Value::from_bytes(bytes).map_err(|e| Error::InvalidValue(format!("{:?}", e)))?;
    T::from_value(val)
}

pub fn to_value<T>(val: &T) -> Value
where
    T: ValueWrite,
{
    let mut bytes = Vec::new();
    val.write_to_bytes(&mut bytes).unwrap();
    Value::from_bytes(&bytes).unwrap()
}

pub fn from_eson<T, Q: ?Sized>(eson: &Eson, k: &Q) -> Result<Option<T>, Error>
where
    T: ValueRead<T>,
    eson::Key: Borrow<Q>,
    Q: Hash + Eq,
    Q: fmt::Display,
{
    Ok(if let Some(v) = eson.get_single(k)? {
        Some(T::from_value(v.clone())?)
    } else {
        None
    })
}

pub fn from_eson_req<T, Q: ?Sized>(eson: &Eson, k: &Q) -> Result<T, Error>
where
    T: ValueRead<T>,
    eson::Key: Borrow<Q>,
    Q: Hash + Eq,
    Q: fmt::Display,
{
    Ok(T::from_value(eson.get_req_single(k)?.clone())?)
}

// //////////////////////////
// Types
// //////////////////////////

// Block

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct Block {
    is_bin: bool,
    bytes: Vec<u8>,
    data_loc: usize,
    salt_locs: (usize, usize),
    hashes: Vec<Multihash>,
}

impl Block {
    pub fn from_bytes(bytes: Vec<u8>) -> Result<Self, Error> {
        if bytes.is_empty() || bytes.len() > MAX_BLOCK_LEN {
            return Err(Error::InvalidBlockLength);
        }

        // Text or binary block?
        if bytes[0] == b'~' {
            // Text block

            let mut hashes = Vec::new();
            // Skip the salt and convert what's after that to string
            let nl_loc = if let Some(nl_loc) = bytes.iter().position(|x| *x == b'\n') {
                // Explicitly disallow a byte order mark
                if bytes.len() >= nl_loc + 3
                    && bytes[nl_loc + 1] == 0xef
                    && bytes[nl_loc + 2] == 0xbb
                    && bytes[nl_loc + 3] == 0xbf
                {
                    return Err(Error::InvalidBlock(
                        "byte order marks are illegal in text blocks".to_owned(),
                    ));
                }

                // Make sure the salt is valid UTF-8
                str::from_utf8(&bytes[1..nl_loc]).map_err(|_| {
                    Error::InvalidBlock("text block salt is not valid UTF-8".to_owned())
                })?;

                // Make sure the contents are valid UTF-8, and convert to String
                let text = str::from_utf8(&bytes[nl_loc + 1..])
                    .map_err(|_| Error::InvalidBlock("text block is not valid UTF-8".to_owned()))?;

                // Locate likely hashes
                if text.len() > MIN_TEXT_MULTIHASH_LEN {
                    let mut maybe_hash_start = None;
                    let mut last_ch = '\n';
                    // Appending a pretend '\n' so that it triggers the non-b58 char code for
                    // hashes at the very end of the block
                    for (i, ch) in text.as_bytes().iter().chain(b"\n".iter()).enumerate() {
                        let ch = *ch as char;
                        if let Some(hash_start) = maybe_hash_start.take() {
                            let slice = &text.as_bytes()[hash_start..i];
                            if slice.len() > MAX_TEXT_MULTIHASH_LEN {
                                // Too long to be a hash
                            } else if !util::is_b58(ch) || i == text.len() {
                                // We've got 1 or more b58 chars bracketed non-b58 chars
                                if slice.len() < MIN_TEXT_MULTIHASH_LEN {
                                    // Too short to be a hash
                                } else {
                                    // Try to parse as a Multihash
                                    let val = Value::from_bytes(slice).unwrap();
                                    if let Ok(hash) = Multihash::from_value(val) {
                                        // Good - it's likely intended to be a multihash
                                        hashes.push(hash);
                                    }
                                }
                            } else {
                                // Still in the b58s - Keep going with the same start point
                                maybe_hash_start = Some(hash_start);
                            }
                        } else if !util::is_ascii_alphanum(last_ch) && util::is_b58(ch) {
                            // A non-alphanumeric followed by a b58 char might be the start
                            // of a multihash.  Not using b58 to start in order have a
                            // slightly better shot at not interpreting a bunch of ascii chars
                            // as a hash (e.g.  an0Qm... is not the start of a hash at Qm)
                            maybe_hash_start = Some(i);
                        }
                        last_ch = ch;
                    }
                }
                nl_loc
            } else {
                return Err(Error::InvalidBlock(
                    "text block missing header newline".to_owned(),
                ));
            };

            // Create and return Block
            Ok(Block {
                is_bin: false,
                bytes,
                data_loc: nl_loc + 1,
                salt_locs: (1, nl_loc),
                hashes,
            })
        } else {
            // Binary block

            if bytes.len() < BLOCK_LOCS_LEN {
                return Err(Error::InvalidBlock("missing header".to_owned()));
            }

            // Header
            // Note: the block format is guaranteed to be backwards compatible, so versions above what
            //       we understand shouldn't make a difference, and there are no lower versions right now
            //       so we don't look a the version number
            let data_loc = (bytes[1] as usize) << 8 | bytes[2] as usize; // Big endian
            let hashes_loc = (bytes[3] as usize) << 8 | bytes[4] as usize; // Big endian

            // However, the locations can be bad
            if data_loc > bytes.len() {
                return Err(Error::InvalidBlock("bad data index".to_owned()));
            }
            if hashes_loc > bytes.len() {
                return Err(Error::InvalidBlock("bad hashes index".to_owned()));
            }
            if hashes_loc > data_loc {
                return Err(Error::InvalidBlock("bad hashes index".to_owned()));
            }

            // Locate hashes
            let mut cur_hash_loc = hashes_loc;
            let mut hashes = Vec::new();
            while cur_hash_loc != data_loc {
                if let Some((hash_len, _)) = util::get_multihash_length(&bytes, cur_hash_loc) {
                    let next_hash_loc = cur_hash_loc + hash_len;
                    if next_hash_loc > data_loc {
                        return Err(Error::InvalidBlock("bad hash length(s)".to_owned()));
                    }
                    let enc = bs58::encode(&bytes[cur_hash_loc..next_hash_loc]).into_string();
                    let val = Value::from_bytes(enc.as_bytes()).unwrap();
                    hashes.push(Multihash::from_value(val).unwrap());
                    cur_hash_loc = next_hash_loc;
                } else {
                    return Err(Error::InvalidBlock("bad hash length(s)".to_owned()));
                }
            }

            Ok(Block {
                is_bin: true,
                bytes,
                data_loc,
                salt_locs: (BLOCK_LOCS_LEN, hashes_loc),
                hashes,
            })
        }
    }
    pub fn from_parts(hashes: Vec<Multihash>, data: &[u8], salt: &[u8]) -> Result<Self, Error> {
        // Convert the hashes to bytes from b58
        let mut hash_bytes: Vec<u8> = Vec::new();
        for h in hashes.iter() {
            hash_bytes.append(&mut bs58::decode(h.as_bytes()).into_vec().unwrap());
        }

        // Figure out where everything is going to go
        let hashes_loc = BLOCK_LOCS_LEN + salt.len();
        let data_loc = hashes_loc + hash_bytes.len();
        let bytes_len = data_loc + data.len();
        if bytes_len > MAX_BLOCK_LEN {
            return Err(Error::InvalidBlockLength);
        }

        // All good - allocate the buffer and fill it
        let mut bytes = Vec::with_capacity(bytes_len);
        bytes.push(CURRENT_BLOCK_VERSION);
        bytes.push((data_loc >> 8 & 0xff) as u8); // Big endian
        bytes.push((data_loc & 0xff) as u8);
        bytes.push((hashes_loc >> 8 & 0xff) as u8); // Big endian
        bytes.push((hashes_loc & 0xff) as u8);
        bytes.extend(salt);
        bytes.extend(hash_bytes);
        bytes.extend(data);

        Ok(Block {
            is_bin: true,
            bytes,
            data_loc,
            salt_locs: (BLOCK_LOCS_LEN, BLOCK_LOCS_LEN + salt.len()),
            hashes,
        })
    }
    pub fn from_text_parts(
        hashes: Vec<Multihash>,
        data_loc: usize,
        bytes: Vec<u8>,
    ) -> Result<Self, Error> {
        if bytes.len() > MAX_BLOCK_LEN {
            return Err(Error::InvalidBlockLength);
        }
        Ok(Block {
            is_bin: false,
            bytes,
            data_loc,
            salt_locs: (1, data_loc - 1),
            hashes,
        })
    }
    pub fn from_eson(eson: &Eson, salt: Option<&str>) -> Result<Self, Error> {
        let mut bytes = Vec::new();
        bytes.push(b'~');
        if let Some(x) = salt {
            bytes.extend(x.as_bytes().iter());
        } else {
            bytes.extend(util::gen_salt().as_bytes().iter());
        }
        bytes.push(b'\n');
        eson.write_into(&mut bytes).unwrap();
        Self::from_bytes(bytes)
    }
    pub fn into_parts(self) -> (Vec<Multihash>, Vec<u8>, usize) {
        (self.hashes, self.bytes, self.data_loc)
    }
    pub fn get_salt(&self) -> &[u8] {
        &self.bytes[self.salt_locs.0..self.salt_locs.1]
    }
    pub fn get_hashes(&self) -> &Vec<Multihash> {
        &self.hashes
    }
    pub fn get_contents(&self) -> &[u8] {
        &self.bytes[self.data_loc..]
    }
    pub fn get_header(&self) -> &[u8] {
        &self.bytes[..self.data_loc]
    }
    pub fn is_bin(&self) -> bool {
        self.is_bin
    }
    pub fn into_bytes(self) -> Vec<u8> {
        self.bytes
    }
    pub fn as_bytes(&self) -> &Vec<u8> {
        &self.bytes
    }
}

// //////////////////////////
// Header Types
// //////////////////////////

// Value

pub type Value = eson::Value;

impl ValueRead<Value> for Value {
    fn from_value(val: eson::Value) -> Result<Value, Error> {
        Ok(val)
    }
}

impl ValueWrite for Value {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(self.as_bytes())?;
        Ok(())
    }
}

// Multihash

#[derive(PartialEq, Eq, Hash, Clone)]
pub struct Multihash(String);

impl ValueRead<Multihash> for Multihash {
    fn from_value(val: eson::Value) -> Result<Multihash, Error> {
        let bytes = val.into_bytes();
        // Validate hash structure and encoding
        let decoded = match bs58::decode(&bytes).into_vec() {
            Err(_) => {
                return Err(Error::InvalidValue(
                    "invalid hash: base58 encoding".to_owned(),
                ));
            }
            Ok(val) => val,
        };
        let hash_length = util::get_multihash_length(&decoded, 0);
        if hash_length.is_some() && hash_length.unwrap().0 == decoded.len() {
            if hash_length.unwrap().0 < 34 {
                // This doesn't catch a multi-byte hash fn encoding with a 31 byte hash, but
                // that's just to weird to worry about
                return Err(Error::InvalidValue(
                    "invalid hash: multihash must be 256 bits or more".to_owned(),
                ));
            }
            // All good
            Ok(Multihash(String::from_utf8(bytes).unwrap()))
        } else {
            Err(Error::InvalidValue(
                "invalid hash: multihash encoding".to_owned(),
            ))
        }
    }
}

impl ValueWrite for Multihash {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(self.0.as_bytes())?;
        Ok(())
    }
}

impl fmt::Debug for Multihash {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if cfg!(debug_assertions) {
            write!(f, "Multihash({})", self.0)
        } else {
            write!(f, "Multihash([redacted])")
        }
    }
}

impl Multihash {
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
    pub fn as_str(&self) -> &str {
        &self.0
    }
    pub fn into_string(self) -> String {
        self.0
    }
    pub fn from_source(bytes: &[u8]) -> Self {
        // Currently we use SHA-256
        let digest = digest::digest(&digest::SHA256, bytes).as_ref().to_owned();
        let mut hash = vec![0x12, 0x20];
        hash.extend(digest);
        Multihash(bs58::encode(hash).into_string())
    }
    pub fn from_multihash_bytes(bytes: &[u8]) -> Self {
        Multihash(bs58::encode(bytes).into_string())
    }
    pub fn null() -> Self {
        Multihash(NULL_HASH.to_owned())
    }
}

// Version
#[derive(PartialEq, Eq, Hash, Debug)]
pub struct Version {
    pub major: u16,
    pub minor: u16,
}

impl ValueRead<Version> for Version {
    fn from_value(val: eson::Value) -> Result<Version, Error> {
        let val = val.into_string();
        if val.len() < 3 {
            return Err(Error::InvalidValue("protocol version format".to_owned()));
        }
        let dot = val.find('.').unwrap_or(0);
        let maybe_major = val[..dot].parse();
        let maybe_minor = val[dot + 1..].parse();
        if let (Ok(major), Ok(minor)) = (maybe_major, maybe_minor) {
            Ok(Version { major, minor })
        } else {
            return Err(Error::InvalidValue("protocol version format".to_owned()));
        }
    }
}

impl ValueWrite for Version {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        write!(writer, "{}.{}", self.major, self.minor)?;
        Ok(())
    }
}

impl Version {
    pub fn new(major: u16, minor: u16) -> Self {
        Version { major, minor }
    }
    pub fn as_string(&self) -> String {
        format!("{}.{}", self.major, self.minor)
    }
}

// Versions
pub type Versions = Vec<Version>;

impl ValueRead<Versions> for Versions {
    fn from_value(val: eson::Value) -> Result<Versions, Error> {
        let mut vec = Vec::new();
        for item in val.as_str().split(' ') {
            vec.push(Version::from_value(
                Value::from_bytes(item.as_bytes()).unwrap(),
            )?);
        }
        Ok(vec)
    }
}

impl ValueWrite for Versions {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        for (i, item) in self.iter().enumerate() {
            if i == 0 {
                writer.write_all(b" ")?;
            }
            writer.write_all(item.as_string().as_bytes())?;
        }
        Ok(())
    }
}

// Encodings (NOP for now)

pub type Encodings = Vec<eson::Value>;

impl ValueRead<Encodings> for Encodings {
    fn from_value(val: eson::Value) -> Result<Encodings, Error> {
        for item in val.as_str().split(' ') {
            if item.is_empty()
                || !item
                    .chars()
                    .all(|x| "abcdefghijklmnopqrstuvwxyz0123456789-".contains(x))
            {
                return Err(Error::InvalidValue("encodings".to_owned()));
            }
        }
        Ok(Vec::new())
    }
}

impl ValueWrite for Encodings {
    fn write_to_bytes(&self, _writer: &mut dyn Write) -> Result<(), Error> {
        Ok(())
    }
}

// bool

impl ValueRead<bool> for bool {
    fn from_value(val: eson::Value) -> Result<bool, Error> {
        let val = val.into_bytes();
        if val == b"false" {
            Ok(false)
        } else if val == b"true" {
            Ok(true)
        } else {
            Err(Error::InvalidValue("boolean".to_owned()))
        }
    }
}

impl ValueWrite for bool {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(if *self { b"true" } else { b"false" })?;
        Ok(())
    }
}

// u16

impl ValueRead<u16> for u16 {
    fn from_value(val: eson::Value) -> Result<u16, Error> {
        Ok(val
            .into_string()
            .parse()
            .map_err(|_| Error::InvalidValue("u16".to_owned()))?)
    }
}

impl ValueWrite for u16 {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        write!(writer, "{}", self)?;
        Ok(())
    }
}

// u32

impl ValueRead<u32> for u32 {
    fn from_value(val: eson::Value) -> Result<u32, Error> {
        Ok(val
            .into_string()
            .parse()
            .map_err(|_| Error::InvalidValue("u32".to_owned()))?)
    }
}

impl ValueWrite for u32 {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        write!(writer, "{}", self)?;
        Ok(())
    }
}

// u64

impl ValueRead<u64> for u64 {
    fn from_value(val: eson::Value) -> Result<u64, Error> {
        Ok(val
            .into_string()
            .parse()
            .map_err(|_| Error::InvalidValue("u64".to_owned()))?)
    }
}

impl ValueWrite for u64 {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        write!(writer, "{}", self)?;
        Ok(())
    }
}

// User ID

#[derive(PartialEq, Eq, Hash, Clone)]
pub struct UserId(String);

impl ValueRead<UserId> for UserId {
    fn from_value(val: eson::Value) -> Result<UserId, Error> {
        Self::from_string(val.into_string())
    }
}

impl ValueWrite for UserId {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(self.0.as_bytes())?;
        Ok(())
    }
}

impl fmt::Debug for UserId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Display for UserId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl UserId {
//    pub fn as_str(&self) -> &str {
//        self.0.as_str()
//    }
    pub fn from_string(mut text: String) -> Result<UserId, Error> {
        text.make_ascii_uppercase();

        // Length
        if text.len() < MIN_USER_ID_LEN || text.len() > MAX_USER_ID_LEN {
            return Err(Error::InvalidValue(format!(
                "user ID must be {}-{} characters",
                MIN_USER_ID_LEN, MAX_USER_ID_LEN
            )));
        }

        // Valid chars
        if !text.chars().all(|ch| "CHJKNPRTVWXY0123456789".contains(ch)) {
            return Err(Error::InvalidValue(
                "user ID has invalid characters".to_owned(),
            ));
        }

        Ok(UserId(text))
    }
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
    pub fn into_string(self) -> String {
        self.0
    }
    pub fn to_string(&self) -> String {
        self.0.to_owned()
    }
    pub fn to_fs_safe_string(&self) -> String {
        self.0.to_owned()
    }
}

// Name

#[derive(PartialEq, Eq, Clone, Hash, PartialOrd, Ord)]
pub struct Name(String);

impl ValueRead<Name> for Name {
    fn from_value(val: eson::Value) -> Result<Name, Error> {
        // All good, just wrap in the type
        let string = val.into_string();
        Name::validate_str(&string)?;
        Ok(Name(string))
    }
}

impl ValueWrite for Name {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(self.0.as_bytes())?;
        Ok(())
    }
}

impl fmt::Debug for Name {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Name {
    pub fn validate_str(st: &str) -> Result<(), Error> {
        // Validate length
        if st.is_empty() {
            return Err(Error::InvalidValue("names cannot be empty".to_owned()));
        }
        if st.len() > MAX_NAME_LEN {
            return Err(Error::InvalidValue("name too long".to_owned()));
        }

        // Make sure the segments are valid
        let mut num_segs = 0;
        for seg in st.split('.') {
            NameSegment::validate_str(seg)?;
            num_segs += 1;
        }

        // Enforce the 5 segment minimum
        if num_segs < 5 {
            return Err(Error::InvalidValue(
                "names must have at least 5 segments".to_owned(),
            ));
        }

        // Make sure the first two segments check out
        {
            if !(st.starts_with("pub.srv.")
                || st.starts_with("pub.app.")
                || st.starts_with("pub.std.")
                || st.starts_with("grp.srv.")
                || st.starts_with("grp.app.")
                || st.starts_with("grp.std.")
                || st.starts_with("prv.srv.")
                || st.starts_with("prv.app.")
                || st.starts_with("prv.std."))
            {
                return Err(Error::InvalidValue(
                    "names must start with pub, grp, or prv, and then \
                     srv, app, or std"
                        .to_owned(),
                ));
            }
        }

        Ok(())
    }
    pub fn from_static(x: &'static str) -> Self {
        Self::from_value(Value::from_bytes(x.as_bytes()).unwrap()).unwrap()
    }
    pub fn as_fs_safe_str(&self) -> &str {
        &self.0
    }
    pub fn as_str(&self) -> &str {
        &self.0
    }
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
    pub fn into_string(self) -> String {
        self.0
    }
    pub fn starts_with(&self, x: &str) -> bool {
        self.0.starts_with(x)
    }
    pub fn parent(&self) -> Option<Name> {
        self.0.rfind('.').and_then(|last_dot_loc| {
            let mut new_str = self.0.clone();
            new_str.truncate(last_dot_loc);
            Some(Name(new_str))
        })
    }
    pub fn push(&mut self, segment: &NameSegment) {
        self.0.push('.');
        self.0.push_str(segment.as_str());
    }
}

#[macro_export]
macro_rules! name {
    ($k:expr) => {
        Name::from_static($k)
    };
}

// Name Segment

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct NameSegment(String);

impl NameSegment {
    pub fn validate_str(x: &str) -> Result<(), Error> {
        if x.is_empty() {
            return Err(Error::InvalidValue(
                "name segments cannot be empty".to_owned(),
            ));
        }
        for ch in x.chars() {
            if !((ch >= 'A' && ch <= 'Z')
                || (ch >= 'a' && ch <= 'z')
                || (ch >= '0' && ch <= '9')
                || (ch == '-' || ch == '_'))
            {
                return Err(Error::InvalidValue(
                    "name segment has invalid character(s)".to_owned(),
                ));
            }
        }
        Ok(())
    }
    pub fn create(string: String) -> Result<NameSegment, Error> {
        Self::validate_str(&string)?;
        Ok(NameSegment(string))
    }
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

// Name Pattern

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct NamePattern {
    pub segs: Vec<NameSegment>,
    pub wild: bool,
}

impl NamePattern {
    pub fn from_value(val: &eson::Value) -> Result<Self, Error> {
        let mut wild = false;
        let mut segs = Vec::new();
        for seg_str in val.as_str().split('.') {
            if wild {
                return Err(Error::InvalidValue(
                    "name pattern: * can only be last segment".to_owned(),
                ));
            } else if seg_str == "*" {
                wild = true;
                continue;
            }
            segs.push(NameSegment::create(seg_str.to_owned())?);
        }
        Self::from_parts(segs, wild)
    }
    pub fn from_name(name: &Name, wild: bool) -> Self {
        let segs = name
            .as_str()
            .split('.')
            .map(|x| NameSegment::create(x.to_owned()).unwrap())
            .collect::<Vec<NameSegment>>();
        Self::from_parts(segs, wild).unwrap()
    }
    pub fn from_parts(segs: Vec<NameSegment>, wild: bool) -> Result<Self, Error> {
        if (segs.len() < 5 && !wild) || segs.len() < 4 {
            return Err(Error::InvalidValue(
                "name pattern: not enough segments".to_owned(),
            ));
        }
        if !["pub", "prv", "grp"].contains(&segs[0].as_str()) {
            return Err(Error::InvalidValue(
                "name pattern: first segment must be one of pub, prv, or grp".to_owned(),
            ));
        }
        if !["app", "std", "srv"].contains(&segs[1].as_str()) {
            return Err(Error::InvalidValue(
                "name pattern: second segment must be one of std, app, or srv".to_owned(),
            ));
        }
        Ok(NamePattern { segs, wild })
    }
    pub fn into_name_segments(self) -> Vec<NameSegment> {
        self.segs
    }
    pub fn is_wild(&self) -> bool {
        self.wild
    }
    pub fn match_name(&self, name: &Name) -> bool {
        for (i, name_seg_str) in name.as_str().split('.').enumerate() {
            if i == self.segs.len() && self.wild {
                continue;
            } else if i >= self.segs.len() {
                return false;
            }
            if self.segs[i].as_str() != name_seg_str {
                return false;
            }
        }
        true
    }
    pub fn map_match<T: Clone>(map: &HashMap<NamePattern, T>, name: &Name) -> Option<T> {
        // Short circuit this if there's no map contents
        if map.is_empty() {
            return None;
        }

        // Straight match
        let mut name_segs = Self::from_name(&name, false).into_name_segments();
        let name_pattern = NamePattern::from_parts(name_segs.clone(), false).unwrap();
        if let Some(opt) = map.get(&name_pattern) {
            return Some((*opt).clone());
        }

        // Wildcard match
        name_segs.pop();
        let name_pattern_wild = NamePattern::from_parts(name_segs, true).unwrap();
        if let Some(opt) = map.get(&name_pattern_wild) {
            return Some((*opt).clone());
        }

        None
    }
    pub fn map_match_deep<T: Clone>(map: &HashMap<NamePattern, T>, name: &Name) -> Option<T> {
        // Short circuit this if there's no map contents
        if map.is_empty() {
            return None;
        }

        let mut name_segs = Self::from_name(&name, false).into_name_segments();

        while name_segs.len() >= 5 {
            // Straight match
            let name_pattern = NamePattern::from_parts(name_segs.clone(), false).unwrap();
            if let Some(opt) = map.get(&name_pattern) {
                return Some((*opt).clone());
            }

            // Wildcard match
            name_segs.pop();
            let name_pattern_wild = NamePattern::from_parts(name_segs.clone(), true).unwrap();
            if let Some(opt) = map.get(&name_pattern_wild) {
                return Some((*opt).clone());
            }
        }

        None
    }
}

// Address

#[derive(PartialEq, Eq, Hash, Debug)]
pub struct Address {
    pub user_id: Option<UserId>,
    pub name: Option<Name>,
    pub hash: Option<Multihash>,
}

impl ValueRead<Address> for Address {
    fn from_value(val: eson::Value) -> Result<Address, Error> {
        let user_id;
        let name;
        let hash;

        // Split at ':'
        let (lhs1, rhs1) = val.split_off_sep(':');
        let without_user_id = if let Some(r) = rhs1 {
            user_id = Some(UserId::from_value(lhs1)?);
            r
        } else {
            user_id = None;
            lhs1
        };

        // Split at '#'
        let (lhs2, rhs2) = without_user_id.split_off_sep('#');
        name = if lhs2.is_empty() {
            None
        } else {
            Some(Name::from_value(lhs2)?)
        };
        hash = if let Some(r) = rhs2 {
            Some(Multihash::from_value(r)?)
        } else {
            None
        };

        Self::from_parts(user_id, name, hash)
    }
}

impl ValueWrite for Address {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        if let Some(x) = self.user_id.as_ref() {
            write!(writer, "{:?}:", x)?
        }
        if let Some(x) = self.name.as_ref() {
            write!(writer, "{:?}", x)?
        }
        if let Some(x) = self.hash.as_ref() {
            write!(writer, "#{:?}", x)?
        }
        Ok(())
    }
}

impl Address {
    pub fn from_parts(
        user_id: Option<UserId>,
        name: Option<Name>,
        hash: Option<Multihash>,
    ) -> Result<Address, Error> {
        // Sanity
        if name.is_none() && hash.is_none() {
            return Err(Error::InvalidValue(
                "address must have a name or a hash".to_owned(),
            ));
        }

        Ok(Address {
            user_id,
            name,
            hash,
        })
    }
}

// Chain

#[derive(Clone, Debug)]
pub struct Chain(VecDeque<ChainItem>);

impl ValueRead<Chain> for Chain {
    fn from_value(val: eson::Value) -> Result<Chain, Error> {
        if val.is_empty() {
            return Ok(Chain(VecDeque::new()));
        }
        let mut chain = VecDeque::new();
        fn is_sep(x: char) -> bool {
            x == '*' || x == '$' || x == '@'
        }
        for item_str in val.as_str().split(' ') {
            if item_str.is_empty() {
                return Err(Error::InvalidValue(
                    "chain: items must be separated by a single space, with none at ends"
                        .to_owned(),
                ));
            }
            let sel_loc = if let Some(x) = item_str.find(is_sep) {
                x
            } else {
                return Err(Error::InvalidValue(
                    "chain: missing selector on item".to_owned(),
                ));
            };

            let block = Left(ChainAlias::from_value(Value::from_str(
                &item_str[..sel_loc],
            )?)?);
            let (key, alias) = if let Some(alias_loc) = item_str.find('>') {
                (
                    eson::Key::from_bytes(&item_str[sel_loc + 1..alias_loc].as_bytes())?,
                    Some(ChainAlias::from_value(Value::from_str(
                        &item_str[alias_loc + 1..],
                    )?)?),
                )
            } else {
                (
                    eson::Key::from_bytes(item_str[sel_loc + 1..].as_bytes())?,
                    None,
                )
            };
            let selector = match (&item_str[sel_loc..=sel_loc], alias) {
                ("*", None) => ChainSelector::Wildcard(key),
                ("@", None) => ChainSelector::Include(key),
                ("$", None) => ChainSelector::Key(key),
                ("$", Some(a)) => ChainSelector::KeyWithAlias(key, a),
                _ => {
                    return Err(Error::InvalidValue(
                        "chain: * and @ selectors cannot have a >".to_owned(),
                    ));
                }
            };
            chain.push_back(ChainItem { block, selector });
        }
        Ok(Chain(chain))
    }
}

impl ValueWrite for Chain {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        let mut f = || {
            for (i_item, item) in self.0.iter().enumerate() {
                if i_item != 0 {
                    writer.write_all(b" ")?
                }
                let block_alias = if let Left(ref x) = item.block {
                    x
                } else {
                    panic!("tried to write partially-resolved chain");
                };
                writer.write_all(block_alias.0.as_bytes())?;
                match item.selector {
                    ChainSelector::Wildcard(ref k) => write!(writer, "*{}", k)?,
                    ChainSelector::Include(ref k) => write!(writer, "@{}", k)?,
                    ChainSelector::Key(ref k) => write!(writer, "${}", k)?,
                    ChainSelector::KeyWithAlias(ref k, ref a) => {
                        write!(writer, "${}>{}", k, a.as_str())?;
                    }
                    ChainSelector::Resolved => {
                        panic!("tried to write partially-resolved chain");
                    }
                }
            }
            Ok(())
        };
        f().map_err(Error::Io)
    }
}

impl Chain {
    pub fn with_capacity(x: usize) -> Self {
        Chain(VecDeque::with_capacity(x))
    }
    pub fn iter(&mut self) -> collections::vec_deque::Iter<ChainItem> {
        self.0.iter()
    }
    pub fn iter_mut(&mut self) -> collections::vec_deque::IterMut<ChainItem> {
        self.0.iter_mut()
    }
    pub fn extend_from_vec(&mut self, mut v: Vec<ChainItem>) {
        self.0.extend(v.drain(..))
    }
    pub fn drain_all(&mut self) -> collections::vec_deque::Drain<ChainItem> {
        self.0.drain(..)
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn pop_front(&mut self) -> Option<ChainItem> {
        self.0.pop_front()
    }
    pub fn push_front(&mut self, x: ChainItem) {
        self.0.push_front(x)
    }
    pub fn pop_back(&mut self) -> Option<ChainItem> {
        self.0.pop_back()
    }
    pub fn push_back(&mut self, x: ChainItem) {
        self.0.push_back(x)
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct ChainAlias(String);

impl ValueRead<ChainAlias> for ChainAlias {
    fn from_value(val: eson::Value) -> Result<ChainAlias, Error> {
        for ch in val.as_str().chars() {
            if (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9') || ch == '-' {
                // NOP
            } else {
                return Err(Error::InvalidValue(
                    "chain alias must only be composed of a-z, 0-9, and -".to_owned(),
                ));
            }
        }
        Ok(ChainAlias(val.into_string()))
    }
}

impl ValueWrite for ChainAlias {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(self.as_bytes())?;
        Ok(())
    }
}

impl ChainAlias {
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
    pub fn as_str(&self) -> &str {
        &self.0
    }
    pub fn root() -> Self {
        ChainAlias("0".to_owned())
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct ChainItem {
    pub block: Either<ChainAlias, Multihash>,
    pub selector: ChainSelector,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum ChainSelector {
    Wildcard(eson::Key),
    Key(eson::Key),
    KeyWithAlias(eson::Key, ChainAlias),
    Include(eson::Key),
    Resolved,
}

// LengthOrStop

#[derive(PartialEq, Eq, Hash, Debug)]
pub enum LengthOrStop {
    Length(u16),
    Stop(Vec<u8>),
}

impl ValueRead<LengthOrStop> for LengthOrStop {
    fn from_value(val: eson::Value) -> Result<LengthOrStop, Error> {
        if val.is_empty() {
            return Err(Error::InvalidValue("invalid length/stop".to_owned()));
        }
        if let Ok(x) = val.as_str().parse::<u16>() {
            Ok(LengthOrStop::Length(x))
        } else {
            Ok(LengthOrStop::Stop(val.into_bytes()))
        }
    }
}

impl ValueWrite for LengthOrStop {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        match self {
            LengthOrStop::Length(ref x) => {
                write!(writer, "{}", x)?;
                Ok(())
            }
            LengthOrStop::Stop(ref x) => {
                writer.write_all(&x)?;
                Ok(())
            }
        }
    }
}

// OobCode

#[derive(PartialEq, Eq, Hash, Debug)]
pub enum OobCode {
    // Conn
    Advisory,
    Redirect,
    AuthenticationError,
    AuthenticationChallenge,
    DroppedSubs,

    // Channel
    NotFound,
    HashMismatch,
    TimedOut,
    PermissionDenied,
    InvalidInput,
    InvalidContent,
    RateLimited,
    JustSent,

    // Both
    ServerError,
}

impl ValueRead<OobCode> for OobCode {
    fn from_value(val: eson::Value) -> Result<OobCode, Error> {
        // TODO: Can this be replaced by a macro?
        Ok(match val.as_str() {
            "advisory" => OobCode::Advisory,
            "redirect" => OobCode::Redirect,
            "authentication-error" => OobCode::AuthenticationError,
            "authentication-challenge" => OobCode::AuthenticationChallenge,
            "dropped-subs" => OobCode::DroppedSubs,

            "not-found" => OobCode::NotFound,
            "hash-mismatch" => OobCode::HashMismatch,
            "timed-out" => OobCode::TimedOut,
            "permission-denied" => OobCode::PermissionDenied,
            "invalid-input" => OobCode::InvalidInput,
            "invalid-content" => OobCode::InvalidContent,
            "rate-limited" => OobCode::RateLimited,
            "just-sent" => OobCode::JustSent,

            "server-error" => OobCode::ServerError,
            _ => return Err(Error::InvalidValue(format!("invalid oob-code: {:?}", val))),
        })
    }
}

impl ValueWrite for OobCode {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        let happy_camel = format!("{:?}", self);
        let mut kebab = String::new();
        for (i, ch) in happy_camel.chars().enumerate() {
            if ch.is_uppercase() {
                if i != 0 {
                    kebab.push('-')
                }
                kebab.extend(ch.to_lowercase());
            } else {
                kebab.push(ch);
            }
        }
        writer.write_all(kebab.as_bytes())?;
        Ok(())
    }
}

// Token

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub struct Token(String);

impl Token {
    pub fn validate(&self) -> Result<(), Error> {
        // This type only does the bare minimum validation on creation - additional checks
        let st = &self.0;

        // Basic sanity and the spec's upper length restriction
        if st.len() < 10 || st.len() > MAX_TOKEN_LEN {
            return Err(Error::InvalidValue("invalid token length".to_owned()));
        }
        if !st.starts_with("0_") {
            return Err(Error::InvalidValue("invalid token".to_owned()));
        }
        Ok(())
    }
    pub fn as_str(&self) -> &str {
        &self.0
    }
    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
}

impl ValueRead<Token> for Token {
    fn from_value(val: eson::Value) -> Result<Token, Error> {
        let st = val.into_string();
        NameSegment::validate_str(&st)?;
        Ok(Token(st))
    }
}

impl ValueWrite for Token {
    fn write_to_bytes(&self, writer: &mut dyn Write) -> Result<(), Error> {
        writer.write_all(self.0.as_bytes())?;
        Ok(())
    }
}

// //////////////////////////

fn into_optional_single<T>(eson: &mut Eson, key: &str) -> Result<Option<T>, Error>
where
    T: ValueRead<T>,
{
    match eson.get_mut(key) {
        Some(vec) => match vec.len() {
            0 => Ok(None),
            1 => Ok(Some(T::from_value(vec.pop().unwrap())?)),
            _ => Err(Error::InvalidHeader(format!("key not a list: {}", key))),
        },
        None => Ok(None),
    }
}

fn into_required_single<T>(eson: &mut Eson, key: &str) -> Result<T, Error>
where
    T: ValueRead<T>,
{
    if let Some(vec) = eson.get_mut(key) {
        if vec.len() != 1 {
            Err(Error::InvalidHeader(format!("missing key: {}", key)))
        } else {
            Ok(T::from_value(vec.pop().unwrap())?)
        }
    } else {
        Err(Error::InvalidHeader(format!("missing key: {}", key)))
    }
}

fn write_required_single<T>(vec: &mut Vec<u8>, key: &[u8], val: &T)
where
    T: ValueWrite,
{
    vec.write_all(key).unwrap();
    vec.push(b' ');
    val.write_to_bytes(vec).unwrap();
    vec.push(b'\n');
}

fn write_optional_single<T>(vec: &mut Vec<u8>, key: &[u8], val: &Option<T>)
where
    T: ValueWrite,
{
    if let Some(v) = val.as_ref() {
        vec.write_all(key).unwrap();
        vec.push(b' ');
        v.write_to_bytes(vec).unwrap();
        vec.push(b'\n');
    }
}

// //////////////////////////
// Parser
// //////////////////////////

/*

TODO: If I want to avoid duplicating memory while waiting for bits to come in,
      I can (conceptually) peek() instead of next() during the payload phase,
      and only copy once there's enough bytes

TODO: This would be better as a proper type-specified state machine

*/

pub trait FromEson<T> {
    fn from_eson(es: Eson, payload: Option<Vec<u8>>) -> Result<T, Error>;
}

pub struct Parser {
    header_start_bytes_count: usize,
    eson_parser: eson::Parser,
    partial_payload: Option<(Vec<u8>, LengthOrStop)>,
    header_eson: Option<Eson>,
}

impl Parser {
    pub fn new() -> Self {
        Parser {
            header_start_bytes_count: 0,
            eson_parser: eson::Parser::new(),
            header_eson: None,
            partial_payload: None,
        }
    }
    pub fn add_bytes<T, I>(&mut self, iter: &mut I) -> Result<Option<T>, Error>
    where
        I: Iterator<Item = u8>,
        T: FromEson<T>,
    {
        let mut msg = None;

        if self.header_eson.is_none() {
            // Eat newlines until the header starts, and verify the start bytes
            while self.header_start_bytes_count < 5 {
                if let Some(ch) = iter.next() {
                    let ch = ch as char;
                    match (ch, self.header_start_bytes_count) {
                        ('\n', 0) => (),
                        ('e', 0) => self.header_start_bytes_count += 1,
                        ('d', 1) => self.header_start_bytes_count += 1,
                        ('s', 2) => self.header_start_bytes_count += 1,
                        ('u', 3) => self.header_start_bytes_count += 1,
                        (' ', 4) => self.header_start_bytes_count += 1,
                        _ => {
                            return Err(Error::InvalidMessage("invalid start bytes".to_owned()));
                        }
                    }
                } else {
                    break;
                }
                if self.header_start_bytes_count == 5 {
                    self.eson_parser.add_bytes(&mut b"edsu ".iter().cloned())?;
                }
            }

            // Getting header
            match self.eson_parser.add_bytes(iter) {
                Err(e) => return Err(Error::Eson(e)),
                Ok(None) => return Ok(None),
                Ok(Some(mut header_eson)) => {
                    match header_eson
                        .remove("payload-length")
                        .or_else(|| header_eson.remove("payload-stop"))
                    {
                        None => msg = Some(T::from_eson(header_eson, None)?),
                        Some(mut x) => {
                            if x.is_empty() {
                                msg = Some(T::from_eson(header_eson, None)?);
                            } else if x.len() != 1 {
                                return Err(Error::InvalidHeader(
                                    "payload-length/-stop not a list".to_owned(),
                                ));
                            } else {
                                let val = x.pop().unwrap();
                                self.partial_payload =
                                    Some((Vec::new(), LengthOrStop::from_value(val)?));
                                self.header_eson = Some(header_eson);
                            }
                        }
                    }
                }
            }
        }

        if let Some((mut payload, stop)) = self.partial_payload.take() {
            // Getting payload
            let mut satisfied = false;
            match stop {
                LengthOrStop::Length(ref x) => {
                    // Read bytes into payload until its length matches the target
                    if *x > 0 {
                        while let Some(byte) = iter.next() {
                            payload.push(byte);
                            if payload.len() == *x as usize {
                                satisfied = true;
                                break;
                            }
                        }
                    } else {
                        // Special case: if there's no payload length, we don't want to iter.next()
                        satisfied = true;
                    }
                }
                LengthOrStop::Stop(ref stop) => {
                    // Read bytes until we hit the stop string
                    for byte in iter {
                        payload.push(byte);
                        if payload.len() >= stop.len()
                            && &payload[payload.len() - stop.len()..payload.len()]
                                == stop.as_slice()
                        {
                            let len_without_stop = payload.len() - stop.len();
                            payload.truncate(len_without_stop);
                            satisfied = true;
                            break;
                        }
                    }
                }
            }

            // Limit payload size
            if payload.len() > MAX_PAYLOAD_LEN {
                return Err(Error::InvalidMessage("payload too large".to_owned()));
            }

            if satisfied {
                let header_eson = self.header_eson.take().unwrap();
                msg = Some(T::from_eson(header_eson, Some(payload))?);
            } else {
                self.partial_payload = Some((payload, stop));
            }
        }

        if msg.is_some() {
            self.header_start_bytes_count = 0;
            return Ok(Some(msg.take().unwrap()));
        }

        Ok(None)
    }
}

impl Default for Parser {
    fn default() -> Self {
        Parser::new()
    }
}
