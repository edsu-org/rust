use crate::edsu::{self, Block, Multihash};
use crate::eson::{self, Eson, Key};
use crate::prelude::*;
use std::borrow::Cow;
use std::collections::VecDeque;
use std::convert;

const MAX_HASHES_PER_META: usize = 1024;

#[derive(Debug)]
pub enum Error {
    NotFound,
    MissingReference(Multihash),
    PermissionDenied,
    InvalidInput(String),
    InvalidContent(String),
    Server(String),
    Transport(String),
}

impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::InvalidContent(format!("{:?}", e))
    }
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::InvalidContent(format!("{:?}", e))
    }
}

pub trait BlockStore {
    fn block_get(&mut self, hash: &Multihash) -> Result<Block, Error>;
    fn block_put(&mut self, block: &Block) -> Result<Multihash, Error>;
    fn meta_get_hashes(&mut self, hash: &Multihash) -> Result<Vec<Multihash>, Error> {
        let mut hashes = Vec::new();
        let mut mb_hashes = VecDeque::new();
        mb_hashes.push_back(hash.clone());
        while let Some(mb_hash) = mb_hashes.pop_front() {
            let mb_eson = Eson::from_bytes(self.block_get(&mb_hash)?.get_contents())?;

            // Start with the blocks
            if let Some(vec_val) = mb_eson.get(KEY_MB_BLOCKS) {
                for val in vec_val {
                    hashes.push(edsu::from_bytes(val.as_bytes())?);
                }
            }

            // Then queue the metas
            if let Some(vec_val) = mb_eson.get(KEY_MB_METAS) {
                // Pushing to front to match a recursive algo (i.e. depth first)
                for val in vec_val.iter().rev() {
                    mb_hashes.push_front(edsu::from_bytes(val.as_bytes())?);
                }
            }
        }
        Ok(hashes)
    }
    fn meta_put(&mut self, hashes: &[Multihash]) -> Result<Multihash, Error> {
        let block = Block::from_eson(&self.meta_eson(hashes)?, None)?;
        self.block_put(&block)
    }
    fn meta_eson(&mut self, hashes: &[Multihash]) -> Result<Eson, Error> {
        let mut recursed = false;
        let mut to_encode = Cow::from(hashes);

        loop {
            let mut hashes_acc = Vec::new();
            let last_one = to_encode.len() <= MAX_HASHES_PER_META;
            for chunk in to_encode.chunks(MAX_HASHES_PER_META) {
                // Convert each chunk of hashes into a meta and put it (or return the ESON
                // instead if it will fit into one block)
                let mut eson = Eson::new();
                let key = key!(if recursed {
                    KEY_MB_METAS
                } else {
                    KEY_MB_BLOCKS
                });
                eson.insert(key, chunk.iter().map(|x| edsu::to_value(x)).collect());
                if last_one {
                    return Ok(eson);
                }
                hashes_acc.push(self.block_put(&Block::from_eson(&eson, None)?)?);
            }
            recursed = true;
            to_encode = Cow::from(hashes_acc);
        }
    }
    fn meta_append(&mut self, mb_hash: &Multihash, hash: &Multihash) -> Result<Multihash, Error> {
        // Build a path to the blocks list in the last meta
        let mut path = Vec::new();
        let mut cur_mb_hash = mb_hash.clone();
        loop {
            // Grab meta, convert to eson, then append to path, then get a ref to it
            path.push(Eson::from_bytes(
                self.block_get(&cur_mb_hash)?.get_contents(),
            )?);
            let eson = path.last().unwrap();

            // If there's metas, re-set our root and loop around
            if let Some(vec_val) = eson.get(KEY_MB_METAS) {
                if let Some(val) = vec_val.last() {
                    let hash = edsu::from_bytes(val.as_bytes())?;
                    cur_mb_hash = hash;
                    continue;
                }
            }

            // Otherwise, break if we've hit the leaf
            if eson.get(KEY_MB_BLOCKS).is_some() {
                break;
            }

            // It's not a meta if there's no outgoing references
            return Err(Error::InvalidContent(
                "meta reference pointing to a non-meta".to_owned(),
            ));
        }

        // Helper fn for determining the total hashes in the block (that we'd expect in a MB)
        fn total_hashes(eson: &Eson) -> usize {
            vec![KEY_MB_BLOCKS, KEY_MB_METAS]
                .into_iter()
                .map(|k| eson.get(k).and_then(|v| Some(v.len())).unwrap_or(0))
                .sum()
        }

        enum Action {
            Amend(Multihash),
            Append(Multihash),
            Leaf,
        }

        // Go back up the path, amending/appending the blocks
        let mut action = Action::Leaf;
        for mut eson in path.into_iter().rev() {
            let next_action;
            match action {
                Action::Leaf => {
                    if total_hashes(&eson) >= MAX_HASHES_PER_META {
                        // Leaf meta's maxed out, create a new one and ask that it be appended
                        let mut new_eson = Eson::new();
                        new_eson.insert(key!(KEY_MB_BLOCKS), vec![edsu::to_value(hash)]);
                        let next_action_block = Block::from_eson(&new_eson, None)?;
                        next_action = Action::Append(self.block_put(&next_action_block)?);
                    } else {
                        // Otherwise, add it to the end of the list
                        let mut b_hashes = eson.remove(KEY_MB_BLOCKS).unwrap();
                        b_hashes.push(edsu::to_value(hash));
                        eson.insert(key!(KEY_MB_BLOCKS), b_hashes);
                        let amend_hash = self.block_put(&Block::from_eson(&eson, None)?)?;
                        next_action = Action::Amend(amend_hash);
                    }
                }
                Action::Amend(ref hh) => {
                    let mut mb_hashes = eson.remove(KEY_MB_METAS).unwrap();
                    mb_hashes.pop();
                    mb_hashes.push(edsu::to_value(hh));
                    eson.insert(key!(KEY_MB_METAS), mb_hashes);
                    next_action = Action::Amend(self.block_put(&Block::from_eson(&eson, None)?)?);
                }
                Action::Append(ref hh) => {
                    if total_hashes(&eson) >= MAX_HASHES_PER_META {
                        // This meta's maxed out, create a new one and ask that it be appended
                        let mut new_eson = Eson::new();
                        new_eson.insert(key!(KEY_MB_METAS), vec![edsu::to_value(hh)]);
                        let next_action_block = Block::from_eson(&new_eson, None)?;
                        next_action = Action::Append(self.block_put(&next_action_block)?);
                    } else {
                        // Otherwise, add it to the end of the list
                        let mut mb_hashes = eson.remove(KEY_MB_METAS).unwrap();
                        mb_hashes.push(edsu::to_value(hh));
                        eson.insert(key!(KEY_MB_METAS), mb_hashes);
                        let amend_hash = self.block_put(&Block::from_eson(&eson, None)?)?;
                        next_action = Action::Amend(amend_hash);
                    }
                }
            }
            action = next_action;
        }

        // Make a new block at the root if need be, otherwise, return the hash of the amended one
        match action {
            Action::Amend(hh) => Ok(hh),
            Action::Append(hh) => {
                let mut eson = Eson::new();
                eson.insert(key!(KEY_MB_METAS), vec![edsu::to_value(&hh)]);
                self.block_put(&Block::from_eson(&eson, None)?)
            }
            Action::Leaf => unreachable!(),
        }
    }
}
