use crate::eson::Value;
use crate::prelude::*;
use bs58;
use rand::{self, Rng};
use std::collections::HashMap;
use std::time;

/// If a character is in the Base58 alphabet (bs58's default)
pub fn is_b58(x: char) -> bool {
    !(x == 'I' || x == 'O' || x == 'l')
        && ((x >= '1' && x <= '9') || (x >= 'A' && x <= 'Z') || (x >= 'a' && x <= 'z'))
}

/// If a character is in a-zA-Z0-9 (the stdlib version is gated)
pub fn is_ascii_alphanum(x: char) -> bool {
    (x >= '0' && x <= '9') || (x >= 'A' && x <= 'Z') || (x >= 'a' && x <= 'z')
}

/// Given an un-b58-encoded buffer and an offset to the multihash, returns
/// (length, start_of_digest).  Only handles digests up to 127 bytes in length,
/// and hash functions up to ~1M
pub fn get_multihash_length(bytes: &[u8], start: usize) -> Option<(usize, usize)> {
    if bytes.len() - start < 8 {
        return None;
    }
    let mut i = start;
    // Skip the varint hash function bytes.  Only considering varints of up to
    // three bytes valid (enough to encode ~1M hash functions)
    if bytes[i] & 0b1000_0000 != 0 {
        i += 1;
        if bytes[i] & 0b1100_0000 != 0 {
            i += 1
        }
        if bytes[i] & 0b1000_0000 != 0 {
            return None;
        }
    }
    i += 1;

    // Grab the length (under 128 bytes)
    if bytes[i] & 0b1000_0000 != 0 {
        return None;
    }
    let digest_length = bytes[i] as usize;
    let digest_start = i + 1;
    Some((digest_start - start + digest_length, digest_start))
}

/// Unix epoch time in seconds
pub fn now() -> u64 {
    time::SystemTime::now()
        .duration_since(time::UNIX_EPOCH)
        .unwrap()
        .as_secs()
}

/// Unix epoch time to SystemTime
pub fn unix_to_system_time(x: u64) -> time::SystemTime {
    time::UNIX_EPOCH + time::Duration::new(x, 0)
}

/// A SystemTime x seconds from now
pub fn seconds_from_now(x: u64) -> time::SystemTime {
    time::SystemTime::now() + time::Duration::new(x, 0)
}

/*
- Use the ring version instead
/// Calculate the HMAC-SHA256
pub fn hmac_sha256(key: &[u8], text: &[u8]) -> Vec<u8> {
    const PADDED_KEY_LEN: usize = 64;
    const INNER_PAD: u8 = 0x36;
    const OUTER_PAD: u8 = 0x5c;
    const KEY_PAD: u8 = 0;

    // Proper HMAC handles keys longer than the padded length, which we don't need
    assert!(key.len() <= PADDED_KEY_LEN);

    // Do the padding
    let mut key_padded: [u8; PADDED_KEY_LEN] = [KEY_PAD; PADDED_KEY_LEN];
    key_padded[..key.len()].copy_from_slice(key);
    let mut inner_padded: [u8; PADDED_KEY_LEN] = [INNER_PAD; PADDED_KEY_LEN];
    let mut outer_padded: [u8; PADDED_KEY_LEN] = [OUTER_PAD; PADDED_KEY_LEN];
    for i in 0..PADDED_KEY_LEN {
        inner_padded[i] ^= key_padded[i];
        outer_padded[i] ^= key_padded[i];
    }

    // Inner hash
    let mut inner_hasher = sha2::Sha256::default();
    inner_hasher.input(&inner_padded);
    inner_hasher.input(text);
    let inner_hash = inner_hasher.result();

    // Outer hash
    let mut outer_hasher = sha2::Sha256::default();
    outer_hasher.input(&outer_padded);
    outer_hasher.input(&inner_hash);
    outer_hasher.result().into_iter().collect()
}
*/

/// Decodes a line in the format: "one two:three" -> (["one"], {"two": "three"})
pub fn decode_flags(line: &str) -> (Vec<String>, HashMap<String, String>) {
    let mut singles = Vec::new();
    let mut compound = HashMap::new();
    for part in line.split_whitespace() {
        let mut bits = part.splitn(2, ':');
        match (bits.next(), bits.next()) {
            (Some(k), Some(v)) => {
                compound.insert(k.to_owned(), v.to_owned());
            }
            (Some(x), None) => {
                singles.push(x.to_owned());
            }
            _ => (),
        }
    }
    (singles, compound)
}

/// Generate salt for blocks
pub fn gen_salt() -> String {
    let mut rng = rand::thread_rng();
    let mut bytes = Vec::with_capacity(DEFAULT_SALT_LEN);
    let alpha = bs58::alphabet::DEFAULT;
    for _ in 0..DEFAULT_SALT_LEN {
        bytes.push(alpha[rng.gen_range(0, alpha.len())]);
    }
    String::from_utf8(bytes).unwrap()
}

pub fn eson_utf8_encode(x: &str) -> Value {
    let mut out = String::with_capacity(x.len());
    let mut b58_buf = String::new();
    let mut b58_encoded = String::new();
    let mut in_b58 = false;

    fn encode_into(x: &str, into: &mut String, mut b58_encoded: &mut String) {
        bs58::encode(x.as_bytes())
            .into(&mut b58_encoded)
            .unwrap();
        into.push_str(b58_encoded);
    }

    for ch in x.chars() {
        if ch >= ' ' && ch < '~' {
            if in_b58 {
                out.push('~');
                encode_into(&b58_buf, &mut out, &mut b58_encoded);
                out.push('^');
                b58_buf.clear();
                in_b58 = false;
            }
            out.push(ch);
        } else {
            in_b58 = true;
            b58_buf.push(ch);
        }
    }
    if in_b58 {
        out.push('~');
        encode_into(&b58_buf, &mut out, &mut b58_encoded);
        out.push('^');
    }
    Value::from_bytes(out.as_bytes()).unwrap()
}

pub fn eson_utf8_decode(x: &str) -> Result<String, String> {
    let mut out = String::with_capacity(x.len());
    let mut b58_buf = String::new();
    let mut in_b58 = false;

    fn decode_into(x: &mut String, into: &mut String) -> Result<(), String> {
        let b58_decoded = bs58::decode(x)
            .into_vec()
            .map_err(|_| "invalid base58 encoding".to_owned())?;
        let str_decoded =
            String::from_utf8(b58_decoded).map_err(|_| "invalid UTF-8 encoding".to_owned())?;
        into.push_str(&str_decoded);
        Ok(())
    }

    for ch in x.chars() {
        if in_b58 {
            if ch == '^' {
                decode_into(&mut b58_buf, &mut out)?;
                b58_buf.clear();
                in_b58 = false;
            } else {
                b58_buf.push(ch);
            }
        } else if ch == '~' {
            in_b58 = true;
        } else {
            out.push(ch);
        }
    }
    if in_b58 {
        Err("base58 encoding section missing terminating ^".to_owned())
    } else {
        Ok(out)
    }
}
