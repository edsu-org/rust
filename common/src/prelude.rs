// Configuration
pub const MAX_PAYLOAD_LEN: usize = 63 * 1024;
pub const MAX_BLOCK_LEN: usize = MAX_PAYLOAD_LEN;
pub const MAX_TEXT_MULTIHASH_LEN: usize = 190;
pub const MIN_TEXT_MULTIHASH_LEN: usize = 32;
pub const MAX_NAME_LEN: usize = 255;
pub const MIN_USER_ID_LEN: usize = 6;
pub const MAX_USER_ID_LEN: usize = 9;
pub const MAX_TOKEN_LEN: usize = 64;
pub const DEFAULT_SALT_LEN: usize = 8;
pub const SECRET_VALUE_LEN: usize = 64;
pub const TOKEN_UNENCODED_BYTE_LEN: usize = 24;

// Useful data
pub const CURRENT_BLOCK_VERSION: u8 = 0b0000_0001;
pub const NULL_HASH: &str = "QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY5";
pub const MULTIHASH_BYTE_LEN: usize = 34;
pub const DEFAULT_EDSU_PORT: u16 = 3216;

// Reified ESON keys
// - Name Blocks
pub const KEY_NB_DENY: &str = "edsu:deny";
pub const KEY_NB_ALLOW: &str = "edsu:allow";
pub const KEY_NB_PREVIOUS: &str = "edsu:previous";
pub const KEY_NB_ONCE: &str = "edsu:once";
pub const KEY_NB_EXPIRES: &str = "edsu:expires";

// - Appendable Blocks
pub const KEY_AB_APPENDED: &str = "edsu:appended";
pub const KEY_AB_DENY: &str = "edsu:append-deny";
pub const KEY_AB_ALLOW: &str = "edsu:append-allow";
pub const KEY_AB_BYTES_MAX: &str = "edsu:append-bytes-max";
pub const KEY_AB_PREVIOUS: &str = "edsu:append-previous";
pub const KEY_AB_APPENDED_TTL: &str = "edsu:appended-ttl";
pub const KEY_AB_RATE_LIMIT: &str = "edsu:append-rate-limit";
pub const FLAG_AB_RATE_SECONDS: &str = "seconds";
pub const FLAG_AB_RATE_ALL_USERS: &str = "all-users";
pub const FLAG_AB_RATE_PER_USER: &str = "per-user";

// - Meta Blocks
pub const KEY_MB_BLOCKS: &str = "edsu:blocks";
pub const KEY_MB_METAS: &str = "edsu:metablocks";

// - Group Blocks
pub const KEY_GB_USERS: &str = "edsu:users";

// - Capabilities Blocks
pub const KEY_CB_VERS: &str = "edsu:version";
pub const KEY_CB_NAME_GET: &str = "edsu:name-get";
pub const KEY_CB_NAME_PUT: &str = "edsu:name-put";
pub const KEY_CB_NAME_APPEND: &str = "edsu:name-append";
pub const KEY_CB_OMNI: &str = "edsu:omni";
pub const FLAG_CB_DESTRUCTIVE: &str = "destructive";
pub const FLAG_CB_VISITOR_GETTABLE: &str = "visitor-gettable";
pub const FLAG_CB_VISITOR_APPENDABLE: &str = "visitor-appendable";

// Reified Names
pub const NAME_SERVER_TIME: &str = "prv.srv.edsu.time.unix";
pub const NAME_BLOCK_EXISTENCE_TEST: &str = "prv.srv.edsu.listings.blocks";
pub const NAME_NAME_LIST: &str = "prv.srv.edsu.listings.names";
pub const NAME_TOKEN_OWNER_GEN: &str = "prv.srv.edsu.authentication.tokens.generate.owner";
pub const NAME_TOKEN_VISITOR_GEN: &str = "prv.srv.edsu.authentication.tokens.generate.visitor";
pub const NAME_TOKEN_OWNER_DEL_ROOT: &str = "pub.srv.edsu.authentication.tokens.delete.owner.";
pub const NAME_TOKEN_VISITOR_DEL_ROOT: &str = "pub.srv.edsu.authentication.tokens.delete.visitor.";
pub const NAME_VISITOR_CAPS_ROOT: &str = "pub.srv.edsu.authentication.visitors.capabilities.";
pub const NAME_AUTH_CHALLENGE_ANSWER_ROOT: &str = "pub.srv.edsu.authentication.challenge.answer.";

// Srv keys
pub const KEY_SRV_NAME_LIST_AFTER: &str = "edsu:after";
pub const KEY_SRV_NAME_LIST_PREFIX: &str = "edsu:prefix";
pub const KEY_SRV_NAME_LIST_NAMES: &str = "edsu:names";
pub const KEY_SRV_NAME_LIST_TRUNCATED: &str = "edsu:truncated";
pub const KEY_SRV_GEN_LABEL: &str = "edsu:label";
pub const KEY_SRV_GEN_REQ_ID: &str = "edsu:request-id";
pub const KEY_SRV_GEN_CAPS: &str = "edsu:capabilities";
pub const KEY_SRV_GEN_TOKEN_TTL: &str = "edsu:ttl";
pub const KEY_SRV_GEN_TOKEN_TOKEN: &str = "edsu:token";

// Oob payloads
pub const OOB_KEY_DEBUG: &str = "edsu:debug";
pub const OOB_KEY_ERR: &str = "edsu:error";
pub const OOB_ERR_VISITOR_CONN: &str = "visitor-connection";

#[derive(PartialEq, Eq, Clone, Hash, Debug)]
pub enum Either<T, U> {
    Left(T),
    Right(U),
}
