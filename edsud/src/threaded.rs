use crate::capabilities::{self, Capabilities, Role, User};
use crate::config::Config;
use crate::db::{self, Db};
use crate::hub::{ConnId, SubId, UserStateArc};
use crate::logging;
use crate::prelude::*;
use crate::srv;
use crate::util;
use edsu_client::blocking_conn;
use edsu_common::edsu::{
    self, Block, Chain, Multihash, Name, OobCode, ServerMsg, Token, UserId, Value, ValueRead,
};
use edsu_common::eson::{self, Eson, Key};
use std::collections::HashSet;
use std::iter::FromIterator;
use std::sync::{Arc, RwLock};
use std::time::{Duration, Instant};
use std::{convert, mem};

#[derive(Debug)]
pub enum Error {
    Db(db::Error),
    Eson(eson::Error),
    Edsu(edsu::Error),
    Capabilities(capabilities::Error),
    Poisoned,
    TimedOut,
    InvalidContent(String),
    InvalidInput(String),
    Srv,
    Server,
    VisitorConn(blocking_conn::Error),
}

impl convert::From<db::Error> for Error {
    fn from(e: db::Error) -> Self {
        Error::Db(e)
    }
}
impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::Edsu(e)
    }
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}
impl convert::From<capabilities::Error> for Error {
    fn from(e: capabilities::Error) -> Self {
        Error::Capabilities(e)
    }
}
impl convert::From<blocking_conn::Error> for Error {
    fn from(e: blocking_conn::Error) -> Self {
        Error::VisitorConn(e)
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct OpId(u64);

impl OpId {
    pub fn new(x: u64) -> Self {
        OpId(x)
    }
}

pub struct Op {
    pub db_arc: Arc<RwLock<Db>>,
    pub us_arc: Arc<UserStateArc>,
    pub op_id: OpId,
    pub conn_id: ConnId,
    pub payload: OpPayload,
}

pub enum OpPayload {
    BlockPut {
        hash: Multihash,
        block: Block,
    },
    BlockGet {
        hash: Multihash,
        chain: Option<Chain>,
        from_chain: bool,
    },
    NameDel {
        caps: Arc<Capabilities>,
        name: Name,
        existing_hash: Multihash,
    },
    NamePut {
        caps: Arc<Capabilities>,
        name: Name,
        existing_hash: Option<Multihash>,
        hash: Multihash,
    },
    NameGet {
        caps: Arc<Capabilities>,
        name: Name,
        chain: Option<Chain>,
    },
    NameAppend {
        user_from: UserId,
        append_name: Name,
        hash: Multihash,
    },
    AuthOwner {
        token: Token,
        role: Role,
    },
    AuthVisitor {
        token: Token,
        user_from: UserId,
    },
    SubNameGet(Arc<Capabilities>, Name),
    SubPermissionsCheck {
        caps: Arc<Capabilities>,
        name: Name,
        hash: Option<Multihash>,
        chain: Option<Chain>,
        previously_permitted: bool,
        sub_id: SubId,
    },
    Gc {
        user_id: UserId,
    },
}

pub struct OpResult {
    pub conn_id: ConnId,
    pub op_id: OpId,
    pub result: Result<OpResultPayload, Error>,
}

// TODO: Box the caps to make Clippy happy
#[allow(clippy::large_enum_variant)]
pub enum OpResultPayload {
    BlockPut {
        hash: Multihash,
        bytes_written: usize,
    },
    BlockGet {
        hash: Multihash,
        block: Block,
        chain: Option<Chain>,
        from_chain: bool,
    },
    NameDel(Name),
    NamePut(Name, Multihash),
    NameGet {
        name: Name,
        hash: Multihash,
        chain: Option<Chain>,
        block: Option<Block>,
    },
    NameAppend(Name, Multihash),
    AuthOwner(Option<Capabilities>),
    AuthVisitor(Option<Capabilities>),
    SubNameGet(Name, Option<Multihash>),
    SubPermissionsCheck {
        name: Name,
        hash: Option<Multihash>,
        chain: Option<Chain>,
        sub_id: SubId,
        permitted: bool,
        name_block: Option<Block>,
    },
    Gc {
        user_id: UserId,
        bytes_used: u64,
        bytes_collected: u64,
        run_time: Duration,
    },
    GcFailed {
        user_id: UserId,
        error: Error,
    },
}

pub fn handle_op(config: &Arc<Config>, op: Op) -> OpResult {
    let Op {
        db_arc,
        us_arc,
        conn_id,
        op_id,
        payload,
    } = op;

    // Special case: want to be able to trigger GC directly for inttest
    if cfg!(feature = "inttest") {
        if let OpPayload::NameGet { ref name, .. } = &payload {
            if name.as_str() == INTTEST_NAME_GC && us_arc.user_to.to_string() == INTTEST_USER_ID {
                handle_op(
                    config,
                    Op {
                        db_arc: db_arc.clone(),
                        us_arc: us_arc.clone(),
                        conn_id,
                        op_id,
                        payload: OpPayload::Gc {
                            user_id: edsu::from_bytes(INTTEST_USER_ID.as_bytes()).unwrap(),
                        },
                    },
                );
            }
        }
    }

    match payload {
        // BlockPut
        OpPayload::BlockPut { hash, block } => {
            let r = || {
                // Get the db
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // Put the block
                let bytes_written = db.block_put(hash.clone(), &block)?;

                // Return OK
                Ok(OpResultPayload::BlockPut {
                    hash,
                    bytes_written,
                })
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // BlockGet
        OpPayload::BlockGet {
            hash,
            chain,
            from_chain,
        } => {
            let r = || {
                // Get the db
                let db = db_arc.read().map_err(|_| Error::Poisoned)?;

                // Get the block
                let block = db.block_get(&hash)?;

                Ok(OpResultPayload::BlockGet {
                    hash,
                    block,
                    chain,
                    from_chain,
                })
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // NameDel
        OpPayload::NameDel {
            name,
            existing_hash,
            ..
        } => {
            let r = || {
                // Get the db
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // Delete the name
                db.name_del(&name, &existing_hash)?;

                // Return OK
                Ok(OpResultPayload::NameDel(name))
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // NamePut
        OpPayload::NamePut {
            caps,
            name,
            existing_hash,
            hash,
        } => {
            let r = || -> Result<OpResultPayload, Error> {
                // Get the db
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // If it's in a *.srv.* namespace, it doesn't actually get created, and
                // has its own validation and permissions
                if &name.as_bytes()[3..8] == b".srv." {
                    srv::name_put(
                        &config,
                        &us_arc,
                        &caps,
                        &mut db,
                        &name,
                        existing_hash,
                        &hash,
                    )?;
                    return Ok(OpResultPayload::NamePut(name, hash));
                }

                // The new block must exist
                let name_block_new = db.block_get(&hash)?;

                // And we require that name blocks are text blocks
                if name_block_new.is_bin() {
                    Err(Error::InvalidContent(
                        "name blocks must be text blocks".to_owned(),
                    ))?;
                }

                let eson_new = Eson::from_bytes(name_block_new.get_contents()).map_err(|e| {
                    Error::InvalidContent(format!("name blocks must be valid ESON: {:?}", e))
                })?;

                // Validate the previous key if it exists
                if let Some(val) = eson_new.get_single(KEY_NB_PREVIOUS)? {
                    if existing_hash.is_none()
                        || val.as_bytes() != existing_hash.as_ref().unwrap().as_bytes()
                    {
                        Err(Error::InvalidContent(format!(
                            "{} does not match existing-hash",
                            KEY_NB_PREVIOUS
                        )))?;
                    }
                }

                let eson_existing_maybe: Result<Option<Eson>, db::Error> = if let Some(eh) =
                    existing_hash.as_ref()
                {
                    let name_block_existing = match db.block_get(eh) {
                        Ok(x) => Ok(x),
                        Err(db::Error::NotFound) => Err(db::Error::HashMismatch(Some(eh.clone()))),
                        e => e,
                    }?;
                    Ok(Some(
                        Eson::from_bytes(name_block_existing.get_contents()).map_err(|_| {
                            db::Error::CorruptedStorage("invalid existing name".to_owned())
                        })?,
                    ))
                } else {
                    Ok(None)
                };
                let eson_existing = eson_existing_maybe?;
                // TODO: untangle why the compiler needs the eson_existing_maybe annotation

                // Check permissions
                if !caps.name_put_eson(&name, &eson_new, &eson_existing) {
                    return Err(Error::Edsu(edsu::Error::PermissionDenied));
                }

                // Put the name and return OK
                db.name_put(&name, &hash, existing_hash.as_ref())?;
                Ok(OpResultPayload::NamePut(name, hash))
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // NameGet
        OpPayload::NameGet { caps, name, chain } => {
            let r = || {
                // Get the db
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // Give srv a chance "pre-create" the name block
                if &name.as_bytes()[3..8] == b".srv." {
                    match srv::name_get(&config, &us_arc, &caps, &mut db, &name) {
                        Ok(hash) => {
                            return Ok(OpResultPayload::NameGet {
                                name,
                                hash,
                                block: None,
                                chain,
                            });
                        }
                        Err(Error::Db(db::Error::NotFound)) => (),
                        Err(e) => Err(e)?,
                    }
                }

                let (hash, block, eson) = db.name_get(&name, false)?;

                // Check the permissions again (first time was in hub.rs) now that we have the
                // name ESON
                if !caps.name_get_eson(&db, &eson) {
                    return Err(Error::Edsu(edsu::Error::PermissionDenied));
                }

                // We shouldn't trigger a once for the permissions check, but we should now
                db.name_del_if_once(&name, &eson)?;

                // Return OK
                Ok(OpResultPayload::NameGet {
                    name,
                    hash,
                    block: if chain.is_some() { Some(block) } else { None },
                    chain,
                })
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // NameAppend
        OpPayload::NameAppend {
            append_name,
            user_from,
            hash,
        } => {
            let r = || {
                // Get the db (we'll use the write() db throughout to make this atomic)
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // Get the name data
                let (existing_hash, name_block, mut name_eson) =
                    db.name_get(&append_name, false)?;

                // Must be a text block
                if name_block.is_bin() {
                    return Err(Error::InvalidInput(
                        "name block specified must be a text block".to_owned(),
                    ));
                }

                // Rate limit
                if let Some(ref val_vec) = name_eson.get(KEY_AB_RATE_LIMIT) {
                    let now = util::now();
                    for val in val_vec.iter() {
                        let lim =
                            append_rate_limit(&db, &user_from, existing_hash.clone(), val, now)?;
                        if let Some(retry_ms) = lim {
                            return Err(Error::Edsu(edsu::Error::RateLimited(retry_ms)));
                        }
                    }
                }

                // Check permissions
                let rd = MAX_USER_BLOCK_RECURSION;
                // - First deny,
                if let Some(ref val_vec) = name_eson.get(KEY_AB_DENY) {
                    if util::group_membership(&db, &user_from, val_vec, rd)? {
                        return Err(Error::Edsu(edsu::Error::PermissionDenied));
                    }
                }
                // - Then allow
                if let Some(ref val_vec) = name_eson.get(KEY_AB_ALLOW) {
                    if !util::group_membership(&db, &user_from, val_vec, rd)? {
                        return Err(Error::Edsu(edsu::Error::PermissionDenied));
                    }
                } else {
                    return Err(Error::Edsu(edsu::Error::PermissionDenied));
                }

                // Permissions are good - create the value that we're going to append
                let val_str = format!("{} {}#{}", util::now(), user_from.as_str(), hash.as_str());
                let val = Value::from_bytes(val_str.as_bytes()).unwrap();
                let appended_key = key!(KEY_AB_APPENDED);

                // Split the name block if we need to
                let bytes_max = name_eson
                    .get_single(KEY_AB_BYTES_MAX)
                    .unwrap_or(None)
                    .and_then(|v| v.as_str().parse::<usize>().ok())
                    .unwrap_or(DEFAULT_APPEND_BYTES_MAX);
                if name_block.get_contents().len() > bytes_max {
                    return Err(Error::InvalidContent(
                        "name block to append already larger than its own limit".to_owned(),
                    ));
                }
                if name_block.get_contents().len() + val.as_bytes().len() > bytes_max {
                    // Remove the appended and split it
                    let mut appended = name_eson.remove(&appended_key).ok_or_else(|| {
                        Error::InvalidContent("append byte limit too low to allow split".to_owned())
                    })?;
                    let split_idx = appended.len() >> 1;
                    let prev_appended = appended.split_off(split_idx);

                    // Create and put the "previous" block
                    let mut prev_eson = name_eson.clone();
                    prev_eson.insert(appended_key.clone(), prev_appended);
                    let mut prev_block_bytes = name_block.get_header().to_vec();
                    prev_eson.write_into(&mut prev_block_bytes).unwrap();
                    let prev_hash = Multihash::from_source(&prev_block_bytes);
                    let prev_block = Block::from_bytes(prev_block_bytes).unwrap();
                    db.block_put(prev_hash.clone(), &prev_block)?;

                    // Put the appended items that didn't go into previous back in the ESON
                    name_eson.insert(appended_key.clone(), appended);

                    // And the reference to the "previous" block we just created
                    let prev_key = key!(KEY_AB_PREVIOUS);
                    let prev_val = vec![Value::from_bytes(prev_hash.as_bytes()).unwrap()];
                    name_eson.insert(prev_key, prev_val);
                }

                // Add the appended item to the top of the list
                let appended_vec = name_eson.entry(appended_key).or_insert_with(Vec::new);
                appended_vec.insert(0, val);

                // Create new name block
                let mut new_block_bytes = name_block.get_header().to_vec();
                name_eson.write_into(&mut new_block_bytes).unwrap();
                let new_hash = Multihash::from_source(&new_block_bytes);
                let new_name_block = Block::from_bytes(new_block_bytes).unwrap();

                // One last check for size, then put it
                if new_name_block.as_bytes().len() > bytes_max {
                    return Err(Error::InvalidContent(
                        "append byte limit too low to allow split".to_owned(),
                    ));
                }
                db.block_put(new_hash.clone(), &new_name_block)?;

                // Put the name
                db.name_put(&append_name, &new_hash, Some(&existing_hash))?;

                // Return OK
                Ok(OpResultPayload::NameAppend(append_name, new_hash))
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        OpPayload::Gc { user_id } => {
            info!("starting GC for {:?}", user_id);
            let started = Instant::now();
            let r = || {
                // Grab the db as write so nothing changes while we're doing this
                // TODO: Make GCing not block every access while it's happening
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // Get blocks uploaded in the last 256s - they're considered roots
                let window = Instant::now() - Duration::from_secs(BLOCK_UPLOAD_GC_WINDOW_S);
                let mut roots = db
                    .recent_blocks
                    .iter()
                    .filter_map(|(when, hash)| {
                        if *when > window {
                            Some(hash.clone())
                        } else {
                            None
                        }
                    })
                    .collect::<HashSet<Multihash>>();

                // Gather more roots from the user's names (this also expires them if need be)
                for name in db.all_names()? {
                    let (mut hash, block, eson) = db.name_get(&name, false)?;
                    let mut refs = block.get_hashes().clone();
                    // If it's got an appended-expiry, take care of that
                    let ttl_maybe = eson
                        .get_single(KEY_AB_APPENDED_TTL)
                        .ok()
                        .and_then(|val_mbe| {
                            val_mbe.and_then(|val| val.as_str().parse::<u64>().ok())
                        });
                    if let Some(ttl) = ttl_maybe {
                        match appended_ttl_expire(&mut db, ttl, eson, hash, refs) {
                            Err(e) => {
                                info!("invalid name append entry for {:?}: {:?}", user_id, e);
                                continue;
                            }
                            Ok(x) => {
                                hash = x.0;
                                refs = x.1;
                            }
                        }
                    }
                    // The named block
                    roots.insert(hash);
                    // Every block referenced in the name block
                    roots.extend(refs);
                }

                // Start with wanting to delete all the blocks
                let (abh, total_bytes) = db.all_block_hashes()?;
                let mut to_delete: HashSet<Multihash> = HashSet::from_iter(abh.into_iter());

                // Then loop, deleting all our roots from the list
                let mut new_roots = HashSet::new();
                loop {
                    for hash in roots.drain() {
                        // If we can remove a root from the delete list, first read that block
                        // and gather new roots from it - in doing so we make sure we don't ever
                        // visit the same block twice
                        if to_delete.contains(&hash) {
                            if let Ok(block) = db.block_get(&hash) {
                                for h in block.get_hashes() {
                                    new_roots.insert(h.clone());
                                }
                            }
                            to_delete.remove(&hash);
                        }
                    }
                    // Either we're done (no more roots to explore), or swap and loop around
                    if new_roots.is_empty() {
                        break;
                    } else {
                        mem::swap(&mut roots, &mut new_roots);
                    }
                }

                // Delete the blocks that are unreferenced
                let mut total_bytes_deleted = 0;
                for hash in to_delete {
                    debug!("deleting block: {:?}", hash);
                    match db.block_del(&hash) {
                        Err(e) => alarm!("error deleting block: {:?}", e),
                        Ok(x) => total_bytes_deleted += x,
                    }
                }

                let bytes_used = total_bytes - total_bytes_deleted;

                // Record the total usage for the user
                let mut record_eson = Eson::new();
                record_eson.insert(key!("bytes-used"), vec![util::int_to_val(bytes_used)]);
                record_eson.insert(key!("when"), vec![util::int_to_val(util::now())]);
                let record_hash = util::eson_block_put(&mut db, &record_eson)?;
                util::name_put_clobber(&mut db, &name!(NAME_DISK_USAGE), &record_hash)?;

                // Done!
                Ok(OpResultPayload::Gc {
                    user_id: user_id.clone(),
                    bytes_used,
                    bytes_collected: total_bytes_deleted,
                    run_time: started.elapsed(),
                })
            };
            // We want the GC to keep on trucking, even if there's an error on one of the accounts
            let result = r().unwrap_or_else(|e| OpResultPayload::GcFailed { user_id, error: e });
            OpResult {
                conn_id,
                op_id,
                result: Ok(result),
            }
        }

        // AuthOwner
        OpPayload::AuthOwner { token, role } => {
            let r = || {
                // Get the db
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // See if we can get a matching caps
                let tk_name_text = format!("{}{}", NAME_TOKEN_OWNER_ROOT, token.as_str());
                let tk_name = edsu::from_bytes(tk_name_text.as_bytes()).unwrap();
                match db.name_get(&tk_name, true) {
                    Err(db::Error::NotFound) => Ok(OpResultPayload::AuthOwner(None)),
                    Err(e) => Err(e)?,
                    Ok((_, _, tk_eson)) => {
                        // Token exists, grab the matching capabilities
                        let caps_hash_val = tk_eson.get_req_single(KEY_TB_CAPS)?;
                        let caps_hash = Multihash::from_value(caps_hash_val.clone())?;
                        let caps_block = db.block_get(&caps_hash)?;
                        let caps_eson = Eson::from_bytes(caps_block.get_contents())?;
                        let caps = Capabilities::from_eson(
                            role,
                            User::Owner,
                            Some(caps_hash),
                            &caps_eson,
                        )?;
                        Ok(OpResultPayload::AuthOwner(Some(caps)))
                    }
                }
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // AuthVisitor
        OpPayload::AuthVisitor { .. } => { // token, user_from } => {
            /*
            use bs58;

            let r = || {
                info!("performing visitor verification for: {:?}", user_from);
                // Prep the name
                let b58_un = bs58::encode(us_arc.user_to.to_string().as_bytes()).into_string();
                let name_st = format!("{}{}.{}", NAME_VISITOR_CAPS_ROOT, b58_un, token.as_str());
                let name = edsu::from_bytes(name_st.as_bytes())?;

                // Connect to the visitor's account and get the caps eson
                let mut conn = blocking_conn::Conn::create(
                    user_from.clone(),
                    None,
                    None,
                    VISITOR_VALIDATE_CONN_BUF_SIZE,
                )?;
                if let Some(caps_eson) = conn.name_block_get(&name)? {
                    // Parse the caps
                    let caps = Capabilities::from_eson(
                        Role::None,
                        User::Visitor(user_from),
                        None,
                        &caps_eson,
                    )?;
                    Ok(OpResultPayload::AuthVisitor(Some(caps)))
                } else {
                    Ok(OpResultPayload::AuthVisitor(None))
                }
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
            */
            unimplemented!();
        }

        // SubNameGet
        OpPayload::SubNameGet(caps, name) => {
            let r = || {
                if !caps.name_get(&name) {
                    return Ok(OpResultPayload::SubNameGet(name, None));
                }

                // Get the db
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;

                // Find the hash
                let maybe_hash = match db.name_get(&name, false) {
                    Ok((h, _, eson)) => {
                        if !caps.name_get_eson(&db, &eson) {
                            Ok(None)
                        } else {
                            db.name_del_if_once(&name, &eson)?;
                            Ok(Some(h))
                        }
                    }
                    Err(db::Error::NotFound) => Ok(None),
                    Err(e) => Err(Error::Db(e)),
                }?;

                // Return OK
                Ok(OpResultPayload::SubNameGet(name, maybe_hash))
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }

        // SubPermissionsCheck
        OpPayload::SubPermissionsCheck {
            caps,
            name,
            hash,
            chain,
            previously_permitted,
            sub_id,
        } => {
            let r = || {
                let mut permitted = false;
                let mut name_block = None;
                // The name might have changed since this Op was queued - we only want to
                // address the current hash because that's the one we can check permissions on
                let mut current_hash = hash;

                // Same permissions as name-get
                let mut db = db_arc.write().map_err(|_| Error::Poisoned)?;
                if caps.name_get(&name) {
                    match db.name_get(&name, false) {
                        Err(db::Error::NotFound) => (),
                        Err(e) => return Err(Error::Db(e)),
                        Ok((actual_hash, block, eson)) => {
                            current_hash = Some(actual_hash);
                            permitted = caps.name_get_eson(&db, &eson);
                            name_block = Some(block);
                            if permitted {
                                db.name_del_if_once(&name, &eson)?;
                            }
                        }
                    }
                }

                // Special case - if there used to be permissions, but not any more, we can
                // notify that (it looks exactly like the name disappearing)
                if !permitted && previously_permitted {
                    permitted = true;
                    current_hash = None;
                }

                Ok(OpResultPayload::SubPermissionsCheck {
                    name,
                    hash: current_hash,
                    chain,
                    sub_id,
                    permitted,
                    name_block,
                })
            };
            OpResult {
                conn_id,
                op_id,
                result: r(),
            }
        }
    }
}

// Returns: Some(limit_ms_for_x_ms) or None if no limit
fn append_rate_limit(
    db: &Db,
    user_from: &UserId,
    start_hash: Multihash,
    val: &Value,
    now: u64,
) -> Result<Option<u32>, Error> {
    // Split the rate limit value
    let args = util::decode_flags(val.as_str()).1;

    let seconds_str = if let Some(x) = args.get(FLAG_AB_RATE_SECONDS) {
        x
    } else {
        return Err(Error::InvalidContent(format!(
            "{} arg missing in append block rate limit",
            FLAG_AB_RATE_SECONDS
        )));
    };
    let all_users_str = args.get(FLAG_AB_RATE_ALL_USERS);
    let per_user_str = args.get(FLAG_AB_RATE_PER_USER);

    // Parse
    let seconds = seconds_str.parse::<u64>().map_err(|_| {
        Error::InvalidContent(format!("{} arg must be an integer", FLAG_AB_RATE_SECONDS))
    })?;
    let all_users = if let Some(x) = all_users_str {
        Some(x.parse::<u64>().map_err(|_| {
            Error::InvalidContent(format!("{} arg must be an integer", FLAG_AB_RATE_ALL_USERS))
        })?)
    } else {
        None
    };
    let per_user = if let Some(x) = per_user_str {
        Some(x.parse::<u64>().map_err(|_| {
            Error::InvalidContent(format!("{} arg must be an integer", FLAG_AB_RATE_PER_USER))
        })?)
    } else {
        None
    };

    // Sanity
    if all_users.or(per_user).is_none() {
        return Err(Error::InvalidContent(format!(
            "append block rate limit must have at least one of {} or {}",
            FLAG_AB_RATE_ALL_USERS, FLAG_AB_RATE_PER_USER
        )));
    }
    if seconds > MAX_APPEND_RATE_LIMIT_WINDOW {
        return Err(Error::InvalidContent(format!(
            "maximum append rate limit window is {} seconds",
            MAX_APPEND_RATE_LIMIT_WINDOW
        )));
    }

    // Spin through the indexes until we've got our window, or the limit is exceeded
    let mut num_appends = 0;
    let mut num_user_from_appends = 0;
    let window_end = now - seconds;
    for eson in util::EsonIterator::new(db, key!(KEY_AB_PREVIOUS), start_hash) {
        let eson = eson?;
        let appended = if let Some(x) = eson.get(KEY_AB_APPENDED) {
            x
        } else {
            continue;
        };
        for val in appended {
            // Parse
            let val_str = val.as_str();
            let space_loc = if let Some(x) = val_str.find(' ') {
                x
            } else {
                return Err(Error::InvalidContent(
                    "append block in chain contains invalid append".to_owned(),
                ));
            };
            let hash_loc = if let Some(x) = val_str.find('#') {
                x
            } else {
                return Err(Error::InvalidContent(
                    "append block in chain contains invalid append".to_owned(),
                ));
            };
            if hash_loc <= space_loc {
                return Err(Error::InvalidContent(
                    "append block in chain contains invalid append".to_owned(),
                ));
            }
            let time = val_str[..space_loc].parse::<u64>().map_err(|_| {
                Error::InvalidContent("append block in chain contains invalid append".to_owned())
            })?;
            let uv = Value::from_bytes(val_str[space_loc + 1..hash_loc].as_bytes()).ok();
            let user_id = if let Some(x) = uv.and_then(|y| UserId::from_value(y).ok()) {
                x
            } else {
                return Err(Error::InvalidContent(
                    "append block in chain contains invalid append".to_owned(),
                ));
            };

            // Interpret
            // - If the window's closed and we haven't run into any limit, we're good
            if time < window_end {
                return Ok(None);
            }
            // - Increment our two counts
            num_appends += 1;
            if user_id == *user_from {
                num_user_from_appends += 1;
            }

            // - If the limits have been exceeded, return how long it will be before the append
            //   that did it falls out of the window
            if let Some(limit) = all_users {
                if num_appends >= limit {
                    return Ok(Some(((time - window_end + 1) * 1000) as u32));
                }
            }
            if let Some(limit) = per_user {
                if num_user_from_appends >= limit {
                    return Ok(Some(((time - window_end + 1) * 1000) as u32));
                }
            }
        }
    }

    Ok(None)
}

fn appended_ttl_expire(
    db: &mut Db,
    ttl: u64,
    name_eson: Eson,
    hash: Multihash,
    refs: Vec<Multihash>,
) -> Result<(Multihash, Vec<Multihash>), Error> {
    fn parse_time(x: &str) -> Result<u64, Error> {
        if let Some(s) = x.split(' ').next() {
            Ok(s.parse::<u64>()
                .map_err(|_| Error::InvalidContent("invalid name append entry".to_owned()))?)
        } else {
            Err(Error::InvalidContent(
                "invalid name append entry".to_owned(),
            ))
        }
    }

    // Collect the ESONs on the way down
    let expires = util::now() + ttl;
    let mut esons = Vec::new();
    let mut first = 0;
    let mut last = 0;
    {
        let eson_iter = util::EsonIterator::new(db, key!(KEY_AB_PREVIOUS), hash.clone());
        for eson in eson_iter {
            let eson = eson?;
            // The first block may not have any appends
            {
                let appends = if let Some(x) = eson.get(KEY_AB_APPENDED) {
                    x
                } else {
                    break;
                };
                if appends.is_empty() {
                    break;
                }
                first = parse_time(appends.first().unwrap().as_str())?;
                last = parse_time(appends.last().unwrap().as_str())?;
            };

            // If the first entry is expired, we can forget about this and any subsequent blocks
            if first > expires {
                break;
            }

            esons.push(eson);

            // If the last entry is expired, we're done here
            if first > expires {
                break;
            }
            // Otherwise, loop around and continue
        }
    }

    // No first and last and esons is empty = the name block has no appends
    if esons.is_empty() && first == 0 && last == 0 {
        return Ok((hash, refs));
    }

    // Grab the last block with non-expired entries (or the root if there is none)
    let mut last_eson = if let Some(x) = esons.pop() {
        x
    } else {
        name_eson
    };

    // Filter the appends
    let mut filtered = Vec::new();
    if let Some(to_filter) = last_eson.get(KEY_AB_APPENDED) {
        filtered = Vec::with_capacity(to_filter.len());
        for val in to_filter.iter().cloned() {
            if parse_time(val.as_str())? <= expires {
                filtered.push(val);
            }
        }
    }
    last_eson.remove(KEY_AB_PREVIOUS);
    last_eson.insert(key!(KEY_AB_APPENDED), filtered);

    // Put the new block
    let last_block = Block::from_eson(&last_eson, None).unwrap();
    let last_hash = Multihash::from_source(last_block.as_bytes());
    db.block_put(last_hash.clone(), &last_block)?;

    // If there's no chain to work back up, we're done
    if esons.is_empty() {
        return Ok((last_hash, last_block.into_parts().0));
    }

    // Otherwise, work back up the chain, amending the prev's
    let mut prev_block = last_block;
    let mut prev_hash = last_hash;
    while let Some(mut eson) = esons.pop() {
        eson.insert(key!(KEY_AB_PREVIOUS), vec![edsu::to_value(&prev_hash)]);
        prev_block = Block::from_eson(&eson, None).unwrap();
        prev_hash = Multihash::from_source(prev_block.as_bytes());
        db.block_put(prev_hash.clone(), &prev_block)?;
    }

    Ok((prev_hash, prev_block.into_parts().0))
}

pub fn err_to_oob(err: Error, channel: Value) -> ServerMsg {
    match err {
        Error::Db(e) => db::err_to_oob(e, Some(channel)),
        Error::Poisoned => util::oob_server_err(),
        Error::Capabilities(e) => {
            alert!("bad capabilities: {:?}", e);
            util::oob_server_err()
        }
        Error::TimedOut => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::TimedOut,
            retry_delay_ms: Some(0),
            payload: None,
            encoding: None,
        },
        Error::Eson(e) => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::InvalidInput,
            retry_delay_ms: None,
            payload: util::oob_msg(&format!("{:?}", e)),
            encoding: None,
        },
        Error::Edsu(e) => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::InvalidInput,
            retry_delay_ms: None,
            payload: util::oob_msg(&format!("{:?}", e)),
            encoding: None,
        },
        Error::InvalidInput(e) => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::InvalidInput,
            retry_delay_ms: None,
            payload: util::oob_msg(&format!("{:?}", e)),
            encoding: None,
        },
        Error::InvalidContent(e) => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::InvalidContent,
            retry_delay_ms: None,
            payload: util::oob_msg(&format!("{:?}", e)),
            encoding: None,
        },
        Error::Srv => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::ServerError,
            retry_delay_ms: None,
            payload: None,
            encoding: None,
        },
        Error::Server => ServerMsg::Oob {
            channel,
            close_connection: None,
            code: OobCode::ServerError,
            retry_delay_ms: None,
            payload: None,
            encoding: None,
        },
        Error::VisitorConn(_) => {
            let mut eson = Eson::new();
            eson.insert(
                key!(OOB_ERR_VISITOR_CONN),
                vec![value!(OOB_ERR_VISITOR_CONN)],
            );
            let mut payload = Vec::new();
            eson.write_into(&mut payload).ok();
            ServerMsg::Oob {
                channel,
                close_connection: None,
                code: OobCode::AuthenticationError,
                retry_delay_ms: None,
                payload: Some(payload),
                encoding: None,
            }
        }
    }
}
