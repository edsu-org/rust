use crate::db::{self, Db};
use crate::logging;
use crate::prelude::*;
use bs58;
use edsu_common::edsu::{
    self, Block, Multihash, Name, OobCode, ServerMsg, Token, UserId, Value, ValueRead, Version,
};
use edsu_common::eson::{Eson, Key};
pub use edsu_common::util::*;
use rand::{self, Rng};
use std::{
    io::{self, Read, Write},
    os::unix::fs::PermissionsExt,
    path::{self, Path, PathBuf},
    str::FromStr,
    {env, fs},
};

pub fn default_channel() -> Value {
    Value::from_bytes(b"0").unwrap()
}

pub fn protocol_version() -> Version {
    Version::new(0, 2)
}

/// Walk through ESON blocks stored in the DB by a "next" key
pub struct EsonIterator<'a> {
    db: &'a Db,
    key: Key,
    next_hash: Option<Multihash>,
}

impl<'a> EsonIterator<'a> {
    pub fn new(db: &'a Db, key: Key, start_hash: Multihash) -> Self {
        EsonIterator {
            db,
            key,
            next_hash: Some(start_hash),
        }
    }
}

impl<'a> Iterator for EsonIterator<'a> {
    type Item = Result<Eson, db::Error>;
    fn next(&mut self) -> Option<Result<Eson, db::Error>> {
        if let Some(next_hash) = self.next_hash.take() {
            match self.db.block_get(&next_hash) {
                Ok(block) => {
                    if let Ok(eson) = Eson::from_bytes(block.get_contents()) {
                        self.next_hash = eson
                            .get_single(&self.key)
                            .unwrap_or(None)
                            .and_then(|e| Multihash::from_value(e.clone()).ok());
                        Some(Ok(eson))
                    } else {
                        None
                    }
                }
                // It's a content error to be missing a block like this e.g. if the tail of a
                // blacklist is gone, that's an issue
                Err(db::Error::NotFound) => Some(Err(db::Error::InvalidContent(
                    "missing referenced block in chain".to_owned(),
                ))),
                Err(e) => Some(Err(e)),
            }
        } else {
            None
        }
    }
}

/// Searches through group ESON blocks for a name
pub fn group_membership(
    db: &Db,
    user_id: &UserId,
    vals: &[Value],
    depth_left: u32,
) -> Result<bool, db::Error> {
    if depth_left == 0 {
        return Err(db::Error::InvalidInput(
            "too much recursion in group blocks".to_owned(),
        ));
    }

    for val in vals {
        if val.as_str() == "*" {
            // Wildcard
            return Ok(true);
        } else if let Ok(ref uid) = UserId::from_value(val.clone()) {
            // Username
            if uid == user_id {
                return Ok(true);
            }
        } else if val.as_str().starts_with('#') {
            if let Ok(hash) = Multihash::from_value(val.clone()) {
                // Hash of a group block
                if let Ok(block) = db.block_get(&hash) {
                    match Eson::from_bytes(block.get_contents()) {
                        Ok(eson) => {
                            if let Some(block_vals) = eson.get(KEY_GB_USERS) {
                                if group_membership(db, user_id, block_vals, depth_left - 1)? {
                                    return Ok(true);
                                }
                            }
                        }
                        Err(e) => {
                            return Err(db::Error::InvalidInput(format!(
                                "group references block with invalid ESON: {:?}",
                                e
                            )));
                        }
                    }
                } else {
                    return Err(db::Error::InvalidInput(
                        "group contains hash of missing block".to_owned(),
                    ));
                }
            }
        }
    }
    Ok(false)
}

pub enum Retry {
    Default,
    Ms(u32),
    None,
}

impl Retry {
    pub fn as_msg_ms(&self) -> Option<u32> {
        match *self {
            Retry::Default => Some(DEFAULT_RETRY_DELAY_MS),
            Retry::Ms(x) => Some(x),
            Retry::None => None,
        }
    }
}

pub fn oob_msg(text: &str) -> Option<Vec<u8>> {
    // Make sure there's no illegal ESON chars
    let key_and_sep = format!("{} ", OOB_KEY_DEBUG);
    let mut bytes = Vec::with_capacity(key_and_sep.len() + text.len() + 2);
    for ch in key_and_sep.chars().chain(text.chars()) {
        if ch >= ' ' && ch <= '~' {
            bytes.push((ch as u32) as u8);
        } else {
            bytes.push(b'_');
        }
    }
    bytes.push(b'\n');
    bytes.push(b'\n');
    Some(bytes)
}

pub fn edsu_err_to_oob(err: edsu::Error, channel: Option<Value>, close: bool) -> ServerMsg {
    let code;
    let payload;
    let retry;
    match err {
        edsu::Error::InvalidMessage(s) => {
            code = OobCode::InvalidInput;
            payload = oob_msg(&format!("invalid message: {}", s));
            retry = Retry::None;
        }
        edsu::Error::InvalidHeader(s) => {
            code = OobCode::InvalidInput;
            payload = oob_msg(&format!("invalid message header: {}", s));
            retry = Retry::None;
        }
        edsu::Error::InvalidValue(s) => {
            code = OobCode::InvalidInput;
            payload = oob_msg(&format!("invalid message value: {}", s));
            retry = Retry::None;
        }
        edsu::Error::InvalidBlockLength => {
            code = OobCode::InvalidInput;
            payload = oob_msg(&"message invalid: block length".to_owned());
            retry = Retry::None;
        }
        edsu::Error::InvalidBlock(s) => {
            code = OobCode::InvalidInput;
            payload = oob_msg(&format!("invalid block: {}", s));
            retry = Retry::None;
        }
        edsu::Error::Io(e) => {
            alarm!("i/o error: {:?}", e);
            code = OobCode::ServerError;
            payload = None;
            retry = Retry::Default;
        }
        edsu::Error::Eson(e) => {
            code = OobCode::InvalidInput;
            payload = oob_msg(&format!("invalid ESON: {:?}", e));
            retry = Retry::None;
        }
        edsu::Error::PermissionDenied => {
            code = OobCode::PermissionDenied;
            payload = None;
            retry = Retry::None;
        }
        edsu::Error::RateLimited(retry_ms) => {
            code = OobCode::RateLimited;
            payload = None;
            retry = Retry::Ms(retry_ms);
        }
    }
    ServerMsg::Oob {
        channel: channel.unwrap_or_else(default_channel),
        close_connection: if close { Some(true) } else { None },
        code,
        retry_delay_ms: retry.as_msg_ms(),
        payload,
        encoding: None,
    }
}

pub fn oob_server_err() -> ServerMsg {
    ServerMsg::Oob {
        channel: default_channel(),
        close_connection: Some(true),
        code: OobCode::ServerError,
        retry_delay_ms: Some(0),
        payload: None,
        encoding: None,
    }
}

pub fn oob_perm_denied(channel: Value) -> ServerMsg {
    ServerMsg::Oob {
        channel,
        close_connection: None,
        code: OobCode::PermissionDenied,
        retry_delay_ms: None,
        payload: None,
        encoding: None,
    }
}

pub fn eson_block_put(db: &mut Db, eson: &Eson) -> Result<Multihash, db::Error> {
    let block = Block::from_eson(eson, None).unwrap();
    let hash = Multihash::from_source(block.as_bytes());
    db.block_put(hash.clone(), &block)?;
    Ok(hash)
}

pub fn block_put(db: &mut Db, block: &Block) -> Result<Multihash, db::Error> {
    let hash = Multihash::from_source(block.as_bytes());
    db.block_put(hash.clone(), &block)?;
    Ok(hash)
}

pub fn token_gen() -> Token {
    let mut rng = rand::thread_rng();
    let mut bytes = [0; TOKEN_UNENCODED_BYTE_LEN];
    rng.fill(&mut bytes);
    edsu::from_bytes(format!("0_{}", bs58::encode(bytes).into_string()).as_bytes()).unwrap()
}

pub fn token_put(
    db: &mut Db,
    once: bool,
    ttl: Option<u64>,
    label: Value,
    req_id: Option<Value>,
    caps_hash: &Multihash,
    owner_not_visitor: bool,
) -> Result<Token, db::Error> {
    // Put the token block
    let mut tk_eson = Eson::new();
    let created = Value::from_bytes(format!("{}", now()).as_bytes()).unwrap();
    tk_eson.insert(key!(KEY_TB_LABEL), vec![label]);
    tk_eson.insert(key!(KEY_TB_CAPS), vec![edsu::to_value(caps_hash)]);
    tk_eson.insert(key!(KEY_TB_CREATED), vec![created]);
    if once {
        tk_eson.insert(key!(KEY_NB_ONCE), vec![Value::from_bytes(b"true").unwrap()]);
    }
    if let Some(val) = req_id {
        tk_eson.insert(key!(KEY_TB_REQ_ID), vec![val]);
    }
    if let Some(t) = ttl {
        let expires = now() + t;
        tk_eson.insert(
            key!(KEY_NB_EXPIRES),
            vec![Value::from_str(&format!("{}", expires)).unwrap()],
        );
    }
    let caps_hash = eson_block_put(db, &tk_eson)?;

    // Finally, name the token block
    if owner_not_visitor {
        let tk = token_gen();
        let tk_name_string = format!("{}{}", NAME_TOKEN_OWNER_ROOT, tk.as_str());
        let tk_name = edsu::from_bytes(tk_name_string.as_bytes()).unwrap();
        db.name_put(&tk_name, &caps_hash, None)?;
        Ok(tk)
    } else {
        for _ in 0..MAX_VISITOR_TOKEN_ATTEMPTS {
            // Get a free token or loop around
            let tk = token_gen();
            let tk_name_string = format!(
                "{}{}",
                NAME_TOKEN_VISITOR_ROOT,
                &tk.as_str()[0..VISITOR_TOKEN_PUB_LEN]
            );
            let tk_name = edsu::from_bytes(tk_name_string.as_bytes()).unwrap();
            match db.name_get(&tk_name, false) {
                Ok(_) => continue,
                Err(db::Error::NotFound) => (),
                Err(e) => return Err(e),
            }

            // Got one, create a name block which contains the full token (the name only has the
            // prefix)
            let tk_val = Value::from_bytes(tk.as_bytes()).unwrap();
            let mut tk_token_eson = Eson::new();
            tk_token_eson.insert(key!(KEY_TB_VISITOR_TOKEN), vec![tk_val.clone()]);
            tk_token_eson.insert(key!(KEY_TB_VISITOR_CAPS), vec![edsu::to_value(&caps_hash)]);
            let tk_token_hash = eson_block_put(db, &tk_token_eson)?;

            // Then put the name based on the prefix
            let tk_name = edsu::from_bytes(tk_name_string.as_bytes()).unwrap();
            db.name_put(&tk_name, &tk_token_hash, None)?;

            return Ok(tk);
        }
        Err(db::Error::CorruptedStorage(
            "couldn't find a free visitor token prefix".to_owned(),
        ))
    }
}

pub fn name_block_put(
    db: &mut Db,
    clobber: bool,
    name: &Name,
    block: &Block,
) -> Result<Multihash, db::Error> {
    let hash = Multihash::from_source(block.as_bytes());
    db.block_put(hash.clone(), block)?;
    if clobber {
        name_put_clobber(db, name, &hash)?;
    } else {
        db.name_put(name, &hash, None)?;
    }
    Ok(hash)
}

pub fn name_put_clobber(db: &mut Db, name: &Name, hash: &Multihash) -> Result<(), db::Error> {
    match db.name_put(name, hash, None) {
        Ok(_) => Ok(()),
        Err(db::Error::HashMismatch(ref existing_hash)) => {
            db.name_put(name, hash, existing_hash.as_ref())?;
            Ok(())
        }
        Err(e) => Err(e),
    }
}

/// Atomically create a new file (errors if it exists) and puts the given bytes into it
pub fn bytes_to_new_file(path: &path::Path, bytes: &[u8], mode: Option<u32>) -> io::Result<()> {
    let mut file = fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(path)?;
    if let Some(m) = mode {
        let metadata = file.metadata()?;
        let mut perms = metadata.permissions();
        perms.set_mode(m);
    }
    file.write_all(bytes)
}

/// Create a new file (silently overwrites if it exists) and puts the given bytes into it
pub fn bytes_to_file(path: &path::Path, bytes: &[u8], mode: Option<u32>) -> io::Result<()> {
    fs::remove_file(&path).ok();
    bytes_to_new_file(path, bytes, mode)
}

/// Get the contents of a file
pub fn file_to_bytes(path: &path::Path) -> io::Result<Vec<u8>> {
    let mut bytes = Vec::new();
    fs::File::open(path)?.read_to_end(&mut bytes)?;
    Ok(bytes)
}

/// Attempts to expand ~ into $HOME
pub fn expand_tilde(path: &Path) -> PathBuf {
    if let Ok(suffix) = path.strip_prefix("~") {
        if let Ok(home) = env::var("HOME") {
            let mut path_build = PathBuf::new();
            path_build.set_file_name(home);
            path_build.push(suffix);
            path_build
        } else {
            path.to_path_buf()
        }
    } else {
        path.to_path_buf()
    }
}

/// Convert an int to a Value
pub fn int_to_val(x: u64) -> Value {
    Value::from_bytes(x.to_string().as_bytes()).unwrap()
}
