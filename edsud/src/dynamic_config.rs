use crate::prelude::*;
use crate::util;
use bs58;
use edsu_common::block_store::{self, BlockStore};
use edsu_common::edsu::{self, Block, Multihash, UserId, Value, ValueRead};
use edsu_common::eson::{self, Eson, Key};
use openssl::nid::Nid;
use openssl::pkey::PKey;
use openssl::x509::X509;
use std::{
    collections::{HashMap, HashSet},
    convert,
};

const KEY_SSL_CERT_HOSTNAMES: &str = "hostnames";
const KEY_SSL_CERT_KEY: &str = "key";
const KEY_SSL_CERT_CERTS: &str = "certs";
const KEY_USER_CONF_USER_ID: &str = "user-id";
const KEY_USER_CONF_ACTIVE: &str = "state";
const KEY_USER_CONF_PLAN: &str = "plan";
const MAX_CONFIG_HASHES_BLOCK_SIZE: usize = 63488;
pub const KEY_USER_CONFS: &str = "user-configs";
pub const KEY_SSL_CERTS: &str = "ssl-certs";

#[derive(Debug)]
pub enum Error {
    InvalidInput(String),
    InvalidContent(String),
    Eson(eson::Error),
    Edsu(edsu::Error),
    BlockStore(block_store::Error),
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}
impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::Edsu(e)
    }
}
impl convert::From<block_store::Error> for Error {
    fn from(e: block_store::Error) -> Self {
        Error::BlockStore(e)
    }
}

pub struct DynamicConfig {
    pub user_configs: Vec<UserConfig>,
    pub ssl_certs: Vec<SslCert>,
}

impl DynamicConfig {
    pub fn from_block_store<B: BlockStore>(bs: &mut B, root: &Multihash) -> Result<Self, Error> {
        // Get the name block and the root hashes of our sub-values
        let name_block = bs.block_get(root)?;
        let eson = Eson::from_bytes(name_block.get_contents())?;
        let ucs_hash = edsu::from_eson_req(&eson, KEY_USER_CONFS)?;
        let sc_mb_hash = edsu::from_eson_req(&eson, KEY_SSL_CERTS)?;

        // Read in all the blocks
        // - User configs
        let uc_hashes = user_config_hashes_get(bs, ucs_hash)?;
        let user_configs = uc_hashes
            .values()
            .map(|x| UserConfig::from_block(&bs.block_get(x)?))
            .collect::<Result<Vec<UserConfig>, Error>>()?;

        // - SSL certs
        let sc_hashes = bs.meta_get_hashes(&sc_mb_hash)?;
        let ssl_certs = sc_hashes
            .iter()
            .map(|x| SslCert::from_block(&bs.block_get(x)?))
            .collect::<Result<Vec<SslCert>, Error>>()?;

        Ok(DynamicConfig {
            user_configs,
            ssl_certs,
        })
    }
    pub fn into_block_store<B: BlockStore>(self, bs: &mut B) -> Result<Multihash, Error> {
        // User configs go into a format that's quicker to update piecemeal
        let mut uc_hashes = HashMap::new();
        for uc in self.user_configs.into_iter() {
            let un = uc.user_id.clone();
            uc_hashes.insert(un, bs.block_put(&uc.into_block())?);
        }
        let ucs_hash = user_config_hashes_put(bs, uc_hashes)?;

        // Put the SSL certs into a metablock
        let sc_hashes = self
            .ssl_certs
            .into_iter()
            .map(|x| Ok(bs.block_put(&x.into_block())?))
            .collect::<Result<Vec<Multihash>, Error>>()?;
        let sc_mb_hash = bs.meta_put(&sc_hashes)?;

        // Make the name block and put it
        let mut eson = Eson::new();
        eson.insert(key!(KEY_USER_CONFS), vec![edsu::to_value(&ucs_hash)]);
        eson.insert(key!(KEY_SSL_CERTS), vec![edsu::to_value(&sc_mb_hash)]);
        Ok(bs.block_put(&Block::from_eson(&eson, None).unwrap())?)
    }
}

fn user_config_hashes_put<B: BlockStore>(
    bs: &mut B,
    uc_hashes: HashMap<UserId, Multihash>,
) -> Result<Multihash, Error> {
    let mut encoded_vals = Vec::with_capacity(uc_hashes.len());
    let mut total_len = 0;
    let mut last_hash = None;
    let num_ucs = uc_hashes.len();
    for (i, (user_id, hash)) in uc_hashes.into_iter().enumerate() {
        let string = format!("{} {}", user_id.to_string(), hash.as_str());
        encoded_vals.push(Value::from_bytes(string.as_bytes()).unwrap());
        total_len += string.len();
        if total_len > MAX_CONFIG_HASHES_BLOCK_SIZE || i == num_ucs - 1 {
            let mut eson = Eson::new();
            let mut vec_val = Vec::with_capacity(encoded_vals.len());
            vec_val.extend(encoded_vals.drain(..));
            eson.insert(key!(KEY_USER_CONFS), vec_val);
            if let Some(h) = last_hash {
                eson.insert(key!(KEY_NB_PREVIOUS), vec![edsu::to_value(&h)]);
            }
            last_hash = Some(bs.block_put(&Block::from_eson(&eson, None).unwrap())?);
            total_len = 0;
        }
    }
    if let Some(hash) = last_hash {
        Ok(hash)
    } else {
        Err(Error::InvalidInput(
            "tried to put empty user_configs".to_owned(),
        ))
    }
}

fn user_config_hashes_get<B: BlockStore>(
    bs: &mut B,
    root_hash: Multihash,
) -> Result<HashMap<UserId, Multihash>, Error> {
    let mut eson_hash_maybe = Some(root_hash);
    let mut ret = HashMap::new();
    while let Some(eson_hash) = eson_hash_maybe {
        let eson_block = bs.block_get(&eson_hash)?;
        let eson = Eson::from_bytes(eson_block.get_contents())?;
        let vec_val = eson.get(KEY_USER_CONFS).ok_or_else(|| {
            Error::InvalidContent("user config reference block missing key".to_owned())
        })?;
        for val in vec_val {
            let mut split = val.as_str().split(' ');
            if let (Some(uid_s), Some(hash_s)) = (split.next(), split.next()) {
                let un = edsu::from_bytes(uid_s.as_bytes())?;
                let hash = edsu::from_bytes(hash_s.as_bytes())?;
                ret.insert(un, hash);
            } else {
                return Err(Error::InvalidContent(
                    "user config reference block had unparsable line".to_owned(),
                ));
            }
        }
        eson_hash_maybe = edsu::from_eson(&eson, KEY_NB_PREVIOUS)?;
    }
    Ok(ret)
}

pub fn user_config_put<B: BlockStore>(
    bs: &mut B,
    dc_hash: &Multihash,
    user_id: UserId,
    uc_hash_maybe: Option<Multihash>,
) -> Result<Multihash, Error> {
    let mut dc_eson = Eson::from_bytes(bs.block_get(dc_hash)?.get_contents())?;
    let ucs_hash = edsu::from_eson_req(&dc_eson, KEY_USER_CONFS)?;
    let mut uc_hashes = user_config_hashes_get(bs, ucs_hash)?;
    if let Some(new_hash) = uc_hash_maybe {
        uc_hashes.insert(user_id, new_hash);
    } else {
        uc_hashes.remove(&user_id);
    }
    let new_ucs_hash = user_config_hashes_put(bs, uc_hashes)?;
    dc_eson.insert(key!(KEY_USER_CONFS), vec![edsu::to_value(&new_ucs_hash)]);
    Ok(bs.block_put(&Block::from_eson(&dc_eson, None).unwrap())?)
}

pub fn user_config_get<B: BlockStore>(
    bs: &mut B,
    dc_hash: &Multihash,
    user_id: &UserId,
) -> Result<Option<UserConfig>, Error> {
    let dc_eson = Eson::from_bytes(bs.block_get(dc_hash)?.get_contents())?;
    let ucs_hash = edsu::from_eson_req(&dc_eson, KEY_USER_CONFS)?;
    let uc_hashes = user_config_hashes_get(bs, ucs_hash)?;
    Ok(if let Some(hash) = uc_hashes.get(user_id) {
        Some(UserConfig::from_block(&bs.block_get(hash)?)?)
    } else {
        None
    })
}

// User Config

#[derive(Debug)]
pub enum UserActive {
    Active,
    Deactivated,
}

impl UserActive {
    pub fn from_value(val: &Value) -> Result<Self, Error> {
        let (state_vec, _state_map) = util::decode_flags(val.as_str());
        Ok(match state_vec.first().and_then(|x| Some(x.as_str())) {
            Some("active") => UserActive::Active,
            Some("deactivated") => UserActive::Deactivated,
            _ => Err(Error::InvalidInput(format!(
                "bad state line in user config: {:?}",
                val.as_str()
            )))?,
        })
    }
    pub fn to_value(&self) -> Value {
        match self {
            UserActive::Active => Value::from_bytes(b"active").unwrap(),
            UserActive::Deactivated => Value::from_bytes(b"deactivated").unwrap(),
        }
    }
}

pub struct UserConfig {
    pub user_id: UserId,
    pub active: UserActive,
    pub plan: Value,
}

impl UserConfig {
    pub fn from_block(block: &Block) -> Result<Self, Error> {
        let eson = Eson::from_bytes(block.get_contents())?;

        // Decode the state
        let active = UserActive::from_value(eson.get_req_single(KEY_USER_CONF_ACTIVE)?)?;
        let uid_val = eson.get_req_single(KEY_USER_CONF_USER_ID)?.clone();

        Ok(UserConfig {
            user_id: UserId::from_value(uid_val)?,
            active,
            plan: eson.get_req_single(KEY_USER_CONF_PLAN)?.clone(),
        })
    }
    pub fn into_block(self) -> Block {
        let mut eson = Eson::new();
        eson.insert(
            key!(KEY_USER_CONF_USER_ID),
            vec![edsu::to_value(&self.user_id)],
        );
        eson.insert(key!(KEY_USER_CONF_ACTIVE), vec![self.active.to_value()]);
        eson.insert(key!(KEY_USER_CONF_PLAN), vec![self.plan.clone()]);
        Block::from_eson(&eson, None).unwrap()
    }
}

// SSL Cert

pub struct SslCert {
    pub hostnames: Vec<Value>,
    pub key: Value,
    pub certs: Vec<Value>,
}

impl SslCert {
    pub fn from_block(block: &Block) -> Result<Self, Error> {
        let eson = Eson::from_bytes(block.get_contents())?;
        Ok(SslCert {
            hostnames: eson.get_req(KEY_SSL_CERT_HOSTNAMES)?.to_vec(),
            key: eson.get_req_single(KEY_SSL_CERT_KEY)?.clone(),
            certs: eson.get_req(KEY_SSL_CERT_CERTS)?.to_vec(),
        })
    }
    pub fn from_pems(key_bytes: &[u8], chain_bytes: &[u8]) -> Result<Self, Error> {
        // Convert to DER
        // - Private key
        let key = PKey::private_key_from_pem(&key_bytes)
            .map_err(|e| Error::InvalidInput(format!("while reading PEM private key: {:?}", e)))?;
        let key_der = key
            .private_key_to_der()
            .map_err(|_| Error::InvalidInput("DER serializing PEM private key".to_owned()))?;

        // - Cert chain
        let chain = X509::stack_from_pem(&chain_bytes)
            .map_err(|e| Error::InvalidInput(format!("while reading PEM cert chain: {:?}", e)))?;
        let chain_ders = chain
            .iter()
            .filter_map(|x| x.to_der().ok())
            .collect::<Vec<Vec<u8>>>();
        if chain_ders.len() != chain.len() {
            return Err(Error::InvalidInput(
                "DER serializing one or more PEM certs".to_owned(),
            ));
        }

        // Construct from DER
        Self::from_ders(&key_der, &chain_ders)
    }
    pub fn from_ders(key_der: &[u8], chain_ders: &[Vec<u8>]) -> Result<Self, Error> {
        // Extract hostnames from first cert in chain
        if chain_ders.is_empty() {
            return Err(Error::InvalidInput("empty cert chain".to_owned()));
        }
        let cert0 = X509::from_der(&chain_ders[0])
            .map_err(|e| Error::InvalidInput(format!("while converting DER to cert: {:?}", e)))?;
        let mut hostnames = HashSet::new();
        let subject = cert0
            .subject_name()
            .entries_by_nid(Nid::COMMONNAME)
            .next()
            .and_then(|x| x.data().as_utf8().ok());
        if let Some(x) = subject {
            hostnames.insert((&x).to_lowercase());
        }
        if let Some(names) = cert0.subject_alt_names() {
            for alt in names {
                if let Some(x) = alt.dnsname() {
                    hostnames.insert(x.to_lowercase());
                }
            }
        }

        // Encode everything
        let mut hostname_vals = Vec::with_capacity(hostnames.len());
        for hostname in hostnames {
            if let Ok(x) = Value::from_bytes(hostname.as_bytes()) {
                hostname_vals.push(x);
            } else {
                return Err(Error::InvalidInput(format!(
                    "hostname has illegal chars: {:?}",
                    hostname
                )));
            }
        }
        let key_b58 = Value::from_bytes(bs58::encode(key_der).into_string().as_bytes()).unwrap();
        let chain_b58 = chain_ders
            .iter()
            .map(|x| Value::from_bytes(bs58::encode(x).into_string().as_bytes()).unwrap())
            .collect();

        Ok(SslCert {
            hostnames: hostname_vals,
            key: key_b58,
            certs: chain_b58,
        })
    }
    pub fn into_block(self) -> Block {
        let mut eson = Eson::new();
        eson.insert(key!(KEY_SSL_CERT_HOSTNAMES), self.hostnames);
        eson.insert(key!(KEY_SSL_CERT_KEY), vec![self.key]);
        eson.insert(key!(KEY_SSL_CERT_CERTS), self.certs);
        Block::from_eson(&eson, None).unwrap()
    }
}

impl fmt::Debug for SslCert {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
         write!(f, "SslCert(")?;
         for hn in self.hostnames.iter() {
            write!(f, "{}", hn.as_str())?;
         }
         write!(f, ")")?;
         Ok(())
    }
}
