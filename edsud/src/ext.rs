use crate::capabilities::{Role, User};
use crate::dynamic_config;
use edsu_common::edsu::{self, Block, Multihash, Name, UserId};
use edsu_common::eson::{self, Eson};
use std::io::{self, Read, Write};
use std::{convert, process};

pub struct StdinIter();

impl Iterator for StdinIter {
    type Item = u8;
    fn next(&mut self) -> Option<u8> {
        loop {
            let mut byte = [0; 1];
            match io::stdin().read(&mut byte) {
                Ok(0) => return None,
                Ok(_) => return Some(byte[0]),
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => continue,
                Err(_) => {
                    log("error", "error reading stdin", file!(), line!());
                    die();
                }
            }
        }
    }
}

#[derive(Debug)]
pub enum Op {
    NameGet(Name),
    NamePut(Name),
}

pub struct Request {
    pub op: Op,
    pub user_to: UserId,
    pub caps_user: User,
    pub caps_role: Role,
    pub caps_hash: Option<Multihash>,
}

struct ParseError(String);
impl convert::From<eson::Error> for ParseError {
    fn from(e: eson::Error) -> Self {
        ParseError(format!("parse error: {:?}", e))
    }
}
impl convert::From<edsu::Error> for ParseError {
    fn from(e: edsu::Error) -> Self {
        ParseError(format!("parse error: {:?}", e))
    }
}
impl convert::From<dynamic_config::Error> for ParseError {
    fn from(e: dynamic_config::Error) -> Self {
        ParseError(format!("parse error: {:?}", e))
    }
}

impl Request {
    fn from_eson(eson: &Eson) -> Result<Self, ParseError> {
        // Pull from ESON
        let op_val = eson.get_req_single("op")?;
        let name_val = eson.get_req_single("name")?;
        let user_to_val = eson.get_req_single("user-to")?;
        let caps_user_val = eson.get_req_single("caps-user")?;
        let caps_role_val = eson.get_req_single("caps-role")?;
        let caps_hash_val = eson.get_single("caps-hash")?;

        // Parse (TODO: this is gross - come up with a better way)
        let name = edsu::from_bytes(name_val.as_bytes())?;
        let op = match op_val.as_str() {
            "name-put" => Ok(Op::NamePut(name)),
            "name-get" => Ok(Op::NameGet(name)),
            _ => Err(ParseError("unrecognized op".to_owned())),
        }?;
        let user_to = edsu::from_bytes(user_to_val.as_bytes())?;
        let caps_user = match caps_user_val.as_str() {
            "anon" => Ok(User::Anon),
            "owner" => Ok(User::Owner),
            s if s.starts_with("visitor ") => {
                let un = edsu::from_bytes(&s.as_bytes()[b"visitor ".len()..])?;
                Ok(User::Visitor(un))
            }
            _ => Err(ParseError("unrecognized caps-user".to_owned())),
        }?;
        let caps_role = match caps_role_val.as_str() {
            "config" => Ok(Role::Config),
            "server" => Ok(Role::Server),
            "none" => Ok(Role::None),
            _ => Err(ParseError("unrecognized caps-role".to_owned())),
        }?;
        let caps_hash = if let Some(x) = caps_hash_val {
            Some(edsu::from_bytes(x.as_bytes())?)
        } else {
            None
        };

        Ok(Request {
            op,
            user_to,
            caps_user,
            caps_role,
            caps_hash,
        })
    }
}

pub fn log(level: &str, text: &str, file: &str, line: u32) {
    println!("type log");
    println!("level {}", level);
    println!("text {}", val_proof(text));
    println!("file {}", val_proof(file));
    println!("line {}\n", line);
}

#[macro_export]
macro_rules! e_debug {
    ($txt:expr) => (ext::log("debug", $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (ext::log("debug", &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! e_info {
    ($txt:expr) => (ext::log("info", $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (ext::log("info", &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! e_err {
    ($txt:expr) => (ext::log("error", $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (ext::log("error", &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! e_alert {
    ($txt:expr) => (ext::log("alert", $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (ext::log("alert", &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! e_alarm {
    ($txt:expr) => (ext::log("alarm", $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (ext::log("alarm", &format!($fmt, $($arg)*),
                                              file!(), line!()));
}

pub fn name_put(name: &Name, hash: &Multihash) {
    println!("type name");
    println!("name {}", edsu::to_value(name).as_str());
    println!("hash {}\n", edsu::to_value(hash).as_str());
}

pub fn block_put(block: &Block) {
    println!("type block");
    println!("payload-length {}\n", block.as_bytes().len());
    if io::stdout().write_all(block.as_bytes()).is_err() {
        process::exit(1);
    }
    if io::stdout().write_all(b"\n").is_err() {
        process::exit(1);
    }
}

pub fn ok_block_put() -> Multihash {
    let bytes = b"~\nok true\n\n".to_vec();
    let hash = Multihash::from_source(&bytes);
    block_put(&Block::from_bytes(bytes).unwrap());
    hash
}

pub fn err_block_put(err: &str, msg: &str) -> Multihash {
    let bytes = format!("~\nerr {}\nmsg {}\n\n", val_proof(err), val_proof(msg))
        .as_bytes()
        .to_vec();
    let hash = Multihash::from_source(&bytes);
    block_put(&Block::from_bytes(bytes).unwrap());
    hash
}

pub fn eson_block_put(eson: &Eson, salt: Option<&str>) -> Multihash {
    let block = Block::from_eson(eson, salt).unwrap();
    let hash = Multihash::from_source(block.as_bytes());
    block_put(&block);
    hash
}

fn die() {
    println!("err true");
    println!("type result\n");
}

fn val_proof(x: &str) -> String {
    x.chars()
        .clone()
        .filter(|&y| y >= ' ' && y <= '~')
        .collect()
}

pub fn handler(f: &dyn Fn(Request) -> Result<Option<Multihash>, String>) {
    let mut parser = eson::Parser::new();
    let mut iter = StdinIter();

    let req;
    loop {
        match parser.add_bytes(&mut iter) {
            Err(e) => {
                let val = val_proof(&format!("{:?}", e));
                log(
                    "alert",
                    &format!("returning with error: {:?}", val),
                    file!(),
                    line!(),
                );
                return die();
            }
            Ok(None) => continue,
            Ok(Some(eson)) => {
                req = if let Ok(x) = Request::from_eson(&eson) {
                    x
                } else {
                    log("alert", "error parsing request", file!(), line!());
                    return die();
                };
                break;
            }
        }
    }

    match f(req) {
        Ok(hash_maybe) => {
            if let Some(hash) = hash_maybe {
                println!("hash {}", edsu::to_value(&hash).as_str());
            }
            println!("ok true");
            println!("type result\n");
        }
        Err(e) => {
            let val = val_proof(&format!("{:?}", e));
            log("alert", &format!("returning with error: {}", val), "n/a", 0);
            die();
        }
    }
    io::stdout().flush().ok();
    io::stderr().flush().ok();
}
