use edsu_server::*;
use std::path::PathBuf;
use std::sync::Arc;
use std::{env, fs};

// ////////////////////////

// Synced with aux.c

//const AUX_OK: i32 = 0;
//const AUX_ERR: i32 = -1;
const AUX_FATAL: i32 = -2;

extern "C" {
    fn lock_file(path: *const u8) -> i32;
}

// ////////////////////////

fn acquire_lock(path: &PathBuf) {
    // It's easier to make sure the file exists from Rust
    let parent_path = path.parent().unwrap();
    if !parent_path.is_dir() {
        fs::DirBuilder::new()
            .recursive(true)
            .create(&parent_path)
            .unwrap();
    }
    unsafe {
        if lock_file(path.to_str().unwrap().as_bytes().as_ptr()) == AUX_FATAL {
            panic!("failed to acquire file lock");
        }
    }
}

fn main() {
    // Load config
    let arg = env::args().nth(1);
    let config =
        Arc::new(config::Config::from_maybe_path_str(arg.as_ref().map(String::as_ref)).unwrap());

    // Start logging
    logging::init_logging(config.log_file.clone(), config.email_alarm.clone())
        .expect("failed while initing logging!");

    // DB version sanity check
    let mut version_path = config.db_root.clone();
    version_path.push("version");
    match util::file_to_bytes(&version_path) {
        Err(e) => {
            alert!("fatal: could not open database version file: {:?}", e);
            return;
        }
        Ok(ref x) if x == b"0.1" => (),
        _ => {
            alert!("fatal: incompatible database version on-disk");
            return;
        }
    }

    // Grab the lock
    acquire_lock(&config.lock_file);

    // Go!
    hub::run(&config);
}
