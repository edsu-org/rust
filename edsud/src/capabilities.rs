use crate::db::{self, Db};
use crate::prelude::*;
use crate::util;
use edsu_common::edsu::{self, Multihash, Name, NamePattern, UserId};
use edsu_common::eson::{self, Eson, Value};
use std::collections::HashMap;
use std::convert;

#[derive(Debug)]
pub enum Error {
    InvalidCapabilitiesBlock(String),
    Db(db::Error),
    Edsu(edsu::Error),
    Eson(eson::Error),
}

impl convert::From<db::Error> for Error {
    fn from(e: db::Error) -> Self {
        Error::Db(e)
    }
}
impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::Edsu(e)
    }
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum Role {
    Config,
    Server,
    None,
}

pub struct Capabilities {
    hash: Option<Multihash>,
    user: User,
    role: Role,
    block_put: bool,
    omni: bool,
    name_get: HashMap<NamePattern, NameGetOptions>,
    name_put: HashMap<NamePattern, NamePutOptions>,
    name_append: HashMap<NamePattern, NameAppendOptions>,
}

trait OptionsParse<T> {
    fn parse(iter: &mut dyn Iterator<Item = &str>) -> T;
}

#[derive(Clone)]
struct NameGetOptions {}

impl OptionsParse<NameGetOptions> for NameGetOptions {
    fn parse(_iter: &mut dyn Iterator<Item = &str>) -> NameGetOptions {
        NameGetOptions {}
    }
}

#[derive(Clone, Debug)]
struct NamePutOptions {
    destructive: bool,
    visitor_gettable: bool,
    visitor_appendable: bool,
}

impl OptionsParse<NamePutOptions> for NamePutOptions {
    fn parse(iter: &mut dyn Iterator<Item = &str>) -> NamePutOptions {
        let mut destructive = false;
        let mut visitor_gettable = false;
        let mut visitor_appendable = false;
        for opt_str in iter {
            if opt_str == FLAG_CB_DESTRUCTIVE {
                destructive = true
            }
            if opt_str == FLAG_CB_VISITOR_GETTABLE {
                visitor_gettable = true
            }
            if opt_str == FLAG_CB_VISITOR_APPENDABLE {
                visitor_appendable = true
            }
        }
        NamePutOptions {
            destructive,
            visitor_gettable,
            visitor_appendable,
        }
    }
}

#[derive(Clone)]
struct NameAppendOptions {}

impl OptionsParse<NameAppendOptions> for NameAppendOptions {
    fn parse(_iter: &mut dyn Iterator<Item = &str>) -> NameAppendOptions {
        NameAppendOptions {}
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub enum User {
    Anon,
    Owner,
    Visitor(UserId),
}

impl Capabilities {
    pub fn from_eson(
        role: Role,
        user: User,
        hash: Option<Multihash>,
        eson: &Eson,
    ) -> Result<Self, Error> {
        // Useful functions
        fn parse_key<T: OptionsParse<T>>(
            key: &'static str,
            es: &Eson,
        ) -> Result<HashMap<NamePattern, T>, Error> {
            let mut map = HashMap::new();
            if let Some(val_vec) = es.get(key) {
                for val in val_vec {
                    let mut iter = val.as_str().split(' ');
                    let np = if let Some(np_str) = iter.next() {
                        NamePattern::from_value(&Value::from_bytes(np_str.as_bytes()).unwrap())?
                    } else {
                        return Err(Error::InvalidCapabilitiesBlock(
                            "line without name pattern".to_owned(),
                        ));
                    };
                    if map.contains_key(&np) {
                        return Err(Error::InvalidCapabilitiesBlock(format!(
                            "duplicated name pattern: {:?}",
                            np
                        )));
                    }
                    map.insert(np, T::parse(&mut iter));
                }
            }
            Ok(map)
        }
        let is_asterisk = |k| {
            if let Ok(Some(val)) = eson.get_single(k) {
                val.as_str() == "*"
            } else {
                false
            }
        };

        // Parse
        let name_get = parse_key(KEY_CB_NAME_GET, eson)?;
        let name_put = parse_key(KEY_CB_NAME_PUT, eson)?;
        let name_append = parse_key(KEY_CB_NAME_APPEND, eson)?;
        let omni = is_asterisk(KEY_CB_OMNI);
        let version = eson.get_req_single(KEY_CB_VERS)?;
        let block_put = if let User::Owner = user {
            !name_put.is_empty() || !name_append.is_empty() || omni
        } else {
            false
        };

        // Sanity
        if version.as_str() != CAPS_BLOCK_VERS {
            return Err(Error::InvalidCapabilitiesBlock(
                "unrecognized version on caps block".to_owned(),
            ));
        }
        if let User::Visitor(_) = user {
            if omni || !name_put.is_empty() {
                return Err(Error::InvalidCapabilitiesBlock(
                    "invalid values for a visitor caps block".to_owned(),
                ));
            }
        }

        Ok(Capabilities {
            hash,
            role,
            user,
            block_put,
            omni,
            name_get,
            name_put,
            name_append,
        })
    }
    pub fn for_anon() -> Self {
        Capabilities {
            hash: None,
            role: Role::None,
            user: User::Anon,
            block_put: false,
            omni: false,
            name_get: HashMap::new(),
            name_put: HashMap::new(),
            name_append: HashMap::new(),
        }
    }
    pub fn block_put(&self) -> bool {
        self.block_put
    }
    pub fn name_get(&self, name: &Name) -> bool {
        if self.omni {
            return true;
        }
        if name.as_str().starts_with("pub.") {
            return true;
        }
        match self.user {
            User::Anon => false,
            User::Visitor(..) => name.as_str().starts_with("grp."),
            User::Owner => NamePattern::map_match(&self.name_get, name).is_some(),
        }
    }
    pub fn name_get_eson(&self, db: &Db, eson: &Eson) -> bool {
        if self.omni {
            return true;
        }
        // Anon and Owner have been checked before the Op was sent - only need
        // to check the Visitor, now that we've got the ESON for it
        match self.user {
            User::Anon => true,
            User::Owner => true,
            User::Visitor(ref user) => {
                let rd = MAX_USER_BLOCK_RECURSION;
                // First deny
                if let Some(ref val_vec) = eson.get(KEY_NB_DENY) {
                    if util::group_membership(&db, user, val_vec, rd).unwrap_or(false) {
                        return false;
                    }
                }

                // Then allow
                if let Some(ref val_vec) = eson.get(KEY_NB_ALLOW) {
                    if util::group_membership(&db, user, val_vec, rd).unwrap_or(false) {
                        return true;
                    }
                }
                false
            }
        }
    }
    pub fn name_del(&self, name: &Name) -> bool {
        if self.omni {
            return true;
        }
        if let Some(options) = NamePattern::map_match(&self.name_put, name) {
            return options.destructive;
        }
        false
    }
    pub fn name_put(&self, name: &Name) -> bool {
        if self.omni {
            return true;
        }
        NamePattern::map_match(&self.name_put, name).is_some()
    }
    pub fn name_put_eson(&self, name: &Name, eson: &Eson, eson_existing: &Option<Eson>) -> bool {
        if self.omni {
            return true;
        }

        // Grab the flags, and boot if there isn't that name pattern at all
        let flags = if let Some(x) = NamePattern::map_match(&self.name_put, name) {
            x
        } else {
            return false;
        };

        // Useful closure
        let changed = |key: &str| {
            if let Some(ee) = eson_existing {
                // If there's an existing name block, make sure it matches
                ee.get(key) != eson.get(key)
            } else {
                // If there isn't an existing name block, make sure isn't being added
                eson.get_req(key).is_ok()
            }
        };

        // Destructive
        if !flags.destructive {
            // Anything non-destructive requires a previous key (its contents are validated
            // at an earlier stage)
            if eson.get_req_single(KEY_NB_PREVIOUS).is_err() {
                return false;
            }

            for key in [KEY_NB_ONCE, KEY_NB_EXPIRES, KEY_AB_APPENDED_TTL].iter() {
                if changed(key) {
                    return false;
                }
            }
        }

        // Visitor gettable
        if !flags.visitor_gettable {
            for key in [KEY_NB_DENY, KEY_NB_ALLOW].into_iter() {
                if changed(key) {
                    return false;
                }
            }
        }

        // Visitor visitor_appendable
        if !flags.visitor_appendable {
            for key in [
                KEY_AB_APPENDED,
                KEY_AB_DENY,
                KEY_AB_ALLOW,
                KEY_AB_BYTES_MAX,
                KEY_AB_PREVIOUS,
                KEY_AB_APPENDED_TTL,
                KEY_AB_RATE_LIMIT,
            ]
            .iter()
            {
                if changed(key) {
                    return false;
                }
            }
        }

        true
    }
    pub fn name_append(&self, name: &Name) -> bool {
        if self.omni {
            return true;
        }
        if name.as_str().starts_with("pub.") {
            return true;
        }
        match self.user {
            User::Anon => false,
            User::Visitor(..) => name.as_str().starts_with("grp."),
            User::Owner => NamePattern::map_match(&self.name_append, name).is_some(),
        }
    }
    pub fn get_role(&self) -> Role {
        self.role
    }
    pub fn get_user(&self) -> &User {
        &self.user
    }
    pub fn get_hash(&self) -> &Option<Multihash> {
        &self.hash
    }
    pub fn is_omni(&self) -> bool {
        self.omni
    }
}
