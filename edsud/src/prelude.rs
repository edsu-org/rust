pub use edsu_common::prelude::*;

pub const DEFAULT_OP_TIMEOUT_S: u64 = 32;
pub const DEFAULT_SEND_TIMEOUT_S: u64 = 64;
pub const DEFAULT_RETRY_DELAY_MS: u32 = 512;
pub const DEFAULT_LOCK_EXPIRY_S: u64 = 8;
pub const SOFT_CLOSE_TIMEOUT_S: u64 = 16;
pub const BLOCK_UPLOAD_GC_WINDOW_S: u64 = 256;
pub const GC_PERIODIC_S: u64 = 3600 * 24;
pub const SUB_OP_TIMEOUT_S: u64 = 16;
pub const DEFAULT_APPEND_BYTES_MAX: usize = 1024 * 62;
pub const MAX_USER_BLOCK_RECURSION: u32 = 64;
pub const DEFAULT_SRV_NAME_TTL: u64 = 64;
pub const MAX_APPEND_RATE_LIMIT_WINDOW: u64 = 3600 * 24;
pub const MAX_CHAINS: usize = 16;
pub const EMAIL_SEND_TIMEOUT_S: u64 = 30;
pub const CONN_TICK_RATIO: u64 = 10;
pub const MAX_SUBS: usize = 256;
pub const MAX_SUB_NOTIFICATIONS_QUEUED: usize = 256;
pub const CHAIN_DURATION_S: u64 = 1;
pub const SRV_EXT_TIMEOUT_S: u64 = 30;
pub const SRV_EXT_CHECK_INTERVAL_MS: u64 = 100;
pub const DB_FILE_MODE: u32 = 0o640;
pub const LIMITER_BW_EXPONENT: i32 = 5; // 5 = 1/2 speed at 15% over, 1/100 speed at 150% over
pub const LIMITER_DISK_EXPONENT: i32 = 5; // 5 = 1/2 speed at 15% over, 1/100 speed at 150% over
pub const LIMITER_EMAIL_PERIOD_S: u64 = 3600 * 24;
pub const LIMITER_DISK_RATIO_WARNING: f64 = 0.9;
pub const OMNI_PLAN_NAME: &str = "omni";
pub const PBKDF2_ROUNDS: u32 = 16384;
pub const CAPS_BLOCK_VERS: &str = "0.1";
pub const MAX_VISITOR_TOKEN_ATTEMPTS: usize = 16;
pub const VISITOR_TOKEN_PUB_LEN: usize = 8;
pub const VISITOR_VALIDATE_CONN_BUF_SIZE: usize = 4096;
pub const REQUESTED_SERVER_EXIT_CODE: i32 = 194;

// Names
pub const NAME_REFRESH_DYNAMIC_CONFIG: &str = "prv.srv.edsu-org.edsud.dynamic-config-refresh";
pub const NAME_SERVER_EXIT: &str = "prv.srv.edsu-org.edsud.server-exit";
pub const NAME_DYNAMIC_CONFIG: &str = "prv.app.edsu-org.edsud.dynamic-config";
pub const NAME_TOKEN_OWNER_ROOT: &str = "prv.app.edsu-org.edsud.secrets.tokens.owner.";
pub const NAME_TOKEN_VISITOR_ROOT: &str = "prv.app.edsu-org.edsud.secrets.tokens.visitor.";
pub const NAME_DISK_USAGE: &str = "prv.app.edsu-org.edsud.usage.storage";

// Keys
pub const KEY_TB_VISITOR_TOKEN: &str = "edsud:token";
pub const KEY_TB_VISITOR_CAPS: &str = "edsud:capabilities";
pub const KEY_TB_CREATED: &str = "edsud:created";
pub const KEY_TB_LABEL: &str = "edsud:label";
pub const KEY_TB_CAPS: &str = "edsud:capabilities";
pub const KEY_TB_REQ_ID: &str = "edsud:request-id";

// Inttest
pub const INTTEST_USER_ID: &str = "abcdef";
pub const INTTEST_NAME_GC: &str = "pub.srv.inttest.actions.gc";
