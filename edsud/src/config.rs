use crate::email::EmailAlarm;
use crate::util;
use edsu_common::edsu::{self, NamePattern, UserId};
use edsu_common::eson::{self, Eson};
use std::{
    collections::HashMap,
    io::{self, Read},
    path::PathBuf,
    str::FromStr,
    //time::{Duration, Instant},
    {convert, fs},
};

// TODO: This is super ugly - needs to be switched to TOML, and abstracted in lots of places

const DEFAULT_PATHS: &[&str] = &[
    "/etc/edsu/edsud.eson",
    "/usr/local/etc/edsu/edsud.eson",
    "/usr/local/edsu/etc/edsud.eson",
];

#[derive(Debug)]
pub enum Error {
    FileOpen(io::Error),
    FileRead(io::Error),
    Io(io::Error),
    Edsu(edsu::Error),
    Eson(eson::Error),
    Parse(String),
    MissingKey(String),
}

impl convert::From<edsu::Error> for Error {
    fn from(e: edsu::Error) -> Self {
        Error::Edsu(e)
    }
}
impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::Eson(e)
    }
}
impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

pub struct Config {
    pub num_socks: u32,
    pub num_threads: u32,
    pub db_root: PathBuf,
    pub tick_us: u32,
    pub gc_tick_ratio: u32,
    pub limiter_tick_ratio: u32,
    pub log_file: PathBuf,
    pub lock_file: PathBuf,
    pub config_user: UserId,
    pub server_user: UserId,
    pub email_service: Option<EmailService>,
    pub email_alarm: Option<EmailAlarm>,
    pub unix_user: String,
    pub unix_group: String,
    pub srv_ext_map: HashMap<NamePattern, PathBuf>,
    pub plans: HashMap<String, Plan>,
}

#[derive(Clone)]
pub struct Plan {
    pub disk_bytes: u64,
    pub bw_bytes: u64,
    pub bw_window_s: u64,
}

#[derive(Clone)]
pub enum EmailService {
    Postmark { secret: String },
}

impl FromStr for EmailService {
    type Err = Error;
    fn from_str(x: &str) -> Result<Self, Self::Err> {
        let (single, args) = util::decode_flags(x);
        let pf = |k| {
            args.get(k)
                .ok_or_else(|| Error::Parse(format!("invalid 'email-service' line: {:?}", x)))
        };
        match single.first().and_then(|x| Some(x.as_str())) {
            Some("postmark") => Ok(EmailService::Postmark {
                secret: pf("secret")?.to_owned(),
            }),
            _ => Err(Error::Parse(
                "email service should be one of: [postmark]".to_owned(),
            )),
        }
    }
}

#[derive(Clone)]
pub struct EmailTemplate {
    pub id: String,
    pub from_addr: String,
    pub from_name: String,
    pub subject: String,
}

impl FromStr for EmailTemplate {
    type Err = Error;
    fn from_str(x: &str) -> Result<Self, Self::Err> {
        let (single, args) = util::decode_flags(x);
        if single.len() != 1 {
            return Err(Error::Parse(format!(
                "invalid email template line: {:?}",
                x
            )));
        }
        let pf = |k| {
            args.get(k)
                .ok_or_else(|| Error::Parse(format!("invalid email template line: {:?}", x)))
        };
        Ok(EmailTemplate {
            id: single[0].clone(),
            from_addr: pf("from-addr")?.to_owned(),
            from_name: args
                .get("from-name")
                .map(|x| x.as_str())
                .unwrap_or("")
                .replace("_", " "),
            subject: args
                .get("subject")
                .map(|x| x.as_str())
                .unwrap_or("")
                .replace("_", " "),
        })
    }
}

impl Config {
    pub fn from_maybe_path_str(maybe_path_str: Option<&str>) -> Result<Config, Error> {
        // Open the config file
        let mut file = if let Some(path_str) = maybe_path_str {
            // A specific config file path was given
            match fs::File::open(util::expand_tilde(&PathBuf::from(path_str))) {
                Ok(x) => x,
                Err(e) => return Err(Error::FileOpen(e)),
            }
        } else {
            // Try opening the default config file locations
            let mut fp = None;
            for path in DEFAULT_PATHS.iter() {
                match fs::File::open(PathBuf::from(path)) {
                    Ok(x) => {
                        fp = Some(x);
                        break;
                    }
                    Err(ref e) if e.kind() == io::ErrorKind::NotFound => continue,
                    Err(e) => return Err(Error::FileOpen(e)),
                }
            }
            if let Some(x) = fp {
                x
            } else {
                return Err(Error::FileOpen(io::Error::new(
                    io::ErrorKind::NotFound,
                    "config file not found in any default location",
                )));
            }
        };
        // Read and parse
        let mut text = String::new();
        file.read_to_string(&mut text).map_err(Error::FileRead)?;

        // Pad with some newlines to be friendly
        text.push('\n');
        text.push('\n');

        // Get the ESON
        let mut eson_iter = text.as_bytes().iter().cloned();
        let eson = match eson::Parser::new().add_bytes(&mut eson_iter) {
            Ok(Some(x)) => Ok(x),
            Ok(None) => Err(eson::Error::InvalidEson("incomplete".to_owned())),
            Err(e) => Err(eson::Error::InvalidEson(format!("{:?}", e))),
        }?;

        // Parse in a way that's bit more forgiving (trims whitespace) and allows comments
        fn get(es: &Eson, k: &'static str) -> Result<String, Error> {
            let cmt_val = es
                .get_single(k)?
                .ok_or_else(|| Error::MissingKey(k.to_owned()))?;
            let (val, _) = cmt_val.clone().split_off_sep('"');
            Ok(val.as_str().trim().to_owned())
        }

        // Useful functions
        fn maybe_get(es: &Eson, k: &'static str) -> Result<Option<String>, Error> {
            if es.contains_key(k) {
                Ok(Some(get(es, k)?))
            } else {
                Ok(None)
            }
        }

        let me = |e, k| Error::Parse(format!("error parsing '{}': {:?}", k, e));

        // Complex requireds
        let mut srv_ext_map = HashMap::new();
        if let Some(vec_val) = eson.get("srv-ext-map") {
            for val in vec_val {
                let mut sp = val.as_str().split_whitespace();
                match (sp.next(), sp.next(), sp.next()) {
                    (Some(np_str), Some(path_str), None) => {
                        let np = NamePattern::from_value(&eson::Value::from_str(np_str).unwrap())?;
                        let path = util::expand_tilde(&PathBuf::from(path_str));
                        srv_ext_map.insert(np, path);
                    }
                    _ => {
                        return Err(Error::Parse(format!(
                            "invalid 'srv-ext-map' line: {:?}",
                            val.as_str()
                        )))
                    }
                }
            }
        }
        let mut plans = HashMap::new();
        if let Some(vec_val) = eson.get("plans") {
            for val in vec_val {
                let (mut singles, args) = util::decode_flags(val.as_str());
                let pf = |k| {
                    let a = args.get(k).ok_or_else(|| {
                        Error::Parse(format!("invalid 'plan' line: {:?}", val.as_str()))
                    })?;
                    a.parse::<u64>().map_err(|_| {
                        Error::Parse(format!("invalid 'plan' line: {:?}", val.as_str()))
                    })
                };
                if singles.len() != 1 {
                    return Err(Error::Parse(format!(
                        "invalid 'plan' line: {:?}",
                        val.as_str()
                    )));
                }
                let plan = Plan {
                    disk_bytes: pf("disk-mb")? * 1_048_576,
                    bw_bytes: pf("bw-mb")? * 1_048_576,
                    bw_window_s: pf("bw-window-s")?,
                };
                if plan.disk_bytes == 0 || plan.bw_bytes == 0 || plan.bw_window_s == 0 {
                    return Err(Error::Parse(format!(
                        "zeros invalid in 'plan' line: {:?}",
                        val.as_str()
                    )));
                }
                plans.insert(singles.pop().unwrap(), plan);
            }
        }

        // Optionals
        let email_service = if let Some(x) = maybe_get(&eson, "email-service")? {
            Some(EmailService::from_str(&x)?)
        } else {
            None
        };
        /*
        let email_alarm = if let Some(val) = eson.get_single("email-alarm")? {
            let service = if let Some(ref x) = email_service { x.clone() } else {
                return Err(Error::Parse("email-alarm without email-service".to_owned()));
            };
            let (_, args) = util::decode_flags(val.as_str());
            let pf = |k| {
                args.get(k).ok_or_else(|| {
                    Error::Parse(format!("invalid 'email-alarm' line: {:?}", val.as_str()))
                })
            };
            let template: EmailTemplate = pf("template")?.to_owned().from_str();
            let addr = pf("addr")?.to_owned();
            let limit_s = pf("limit-s")?.parse::<u64>().map_err(|e| me(e, "email-alarm"))?;
            Some(EmailAlarm {
                service,
                template,
                addr,
                limit_s,
                last_sent: Instant::now() - Duration::new(limit_s * 2, 0),
            })
        } else {
            None
        };
        */
        let email_alarm = None;

        Ok(Config {
            num_socks: get(&eson, "num-socks")?
                .parse::<u32>()
                .map_err(|e| me(e, "num-socks"))?,
            num_threads: get(&eson, "num-threads")?
                .parse::<u32>()
                .map_err(|e| me(e, "num-threads"))?,
            log_file: util::expand_tilde(&PathBuf::from(&get(&eson, "log-file")?)),
            lock_file: util::expand_tilde(&PathBuf::from(&get(&eson, "lock-file")?)),
            db_root: util::expand_tilde(&PathBuf::from(&get(&eson, "db-root")?)),
            tick_us: get(&eson, "tick-us")?
                .parse::<u32>()
                .map_err(|e| me(e, "tick-us"))?,
            gc_tick_ratio: get(&eson, "gc-tick-ratio")?
                .parse::<u32>()
                .map_err(|e| me(e, "gc-tick-ratio"))?,
            limiter_tick_ratio: get(&eson, "limiter-tick-ratio")?
                .parse::<u32>()
                .map_err(|e| me(e, "limiter-tick-ratio"))?,
            config_user: edsu::from_bytes(get(&eson, "config-user")?.as_bytes())
                .map_err(|e| Error::Parse(format!("bad 'config-user': {:?}", e)))?,
            server_user: edsu::from_bytes(get(&eson, "server-user")?.as_bytes())
                .map_err(|e| Error::Parse(format!("bad 'server-user': {:?}", e)))?,
            unix_user: get(&eson, "unix-user")?,
            unix_group: get(&eson, "unix-group")?,
            email_service,
            email_alarm,
            srv_ext_map,
            plans,
        })
    }
}
