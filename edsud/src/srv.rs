use crate::capabilities::{Capabilities, Role, User};
use crate::config::Config;
use crate::db::{self, Db};
use crate::hub::UserStateArc;
use crate::logging::{self, LogLevel};
use crate::prelude::*;
use crate::threaded::{self, Error};
use crate::util;
use bs58;
use edsu_common::edsu::{
    self, Block, Encodings, Multihash, Name, NamePattern, NameSegment, UserId, Value, ValueRead,
    Versions,
};
use edsu_common::eson::{self, Eson, Key};
use ring;
use std::{
    collections::VecDeque,
    io::{self, Read},
    process::{Command, Stdio},
    str::FromStr,
    sync::Arc,
    time::{Duration, Instant},
    {convert, thread},
};

pub fn name_get(
    config: &Arc<Config>,
    us_arc: &UserStateArc,
    caps: &Capabilities,
    db: &mut Db,
    name: &Name,
) -> Result<Multihash, Error> {
    // Handle inttest
    if cfg!(feature = "inttest")
        && us_arc.user_to.to_string() == INTTEST_USER_ID
        && name.as_bytes().len() > 16
        && &name.as_bytes()[3..16] == b".srv.inttest."
    {
        let maybe_hash = handle_inttest_gets(db, name)?;
        if let Some(hash) = maybe_hash {
            return Ok(hash);
        } else {
            Err(db::Error::NotFound)?;
        }
    }

    // Handle extensions
    if &name.as_bytes()[3..13] != b".srv.edsu." {
        let maybe_hash = handle_ext("name-get", config, us_arc, caps, db, name, None, None)?;
        if let Some(hash) = maybe_hash {
            return Ok(hash);
        } else {
            Err(db::Error::NotFound)?;
        }
    }

    // In the Edsu spec
    let name_str = name.as_str();
    match name_str {
        NAME_SERVER_TIME => {
            let now = util::now();
            let text = format!("~\ntime {}\n\n", now);
            let block = Block::from_bytes(text.into_bytes()).unwrap();
            let hash = Multihash::from_source(block.as_bytes());
            db.block_put(hash.clone(), &block)?;
            Ok(hash)
        }
        x if x.starts_with(NAME_VISITOR_CAPS_ROOT) => {
            let mut si = x[NAME_VISITOR_CAPS_ROOT.len()..].split('.');
            match (si.next(), si.next(), si.next()) {
                (Some(v_b58), Some(encoded), None) => {
                    let v_bytes = bs58::decode(v_b58.as_bytes())
                        .into_vec()
                        .map_err(|_| Error::Db(db::Error::NotFound))?;
                    let visitee =
                        edsu::from_bytes(&v_bytes).map_err(|_| Error::Db(db::Error::NotFound))?;
                    visitor_caps(db, &visitee, encoded).map_err(|_| Error::Db(db::Error::NotFound))
                }
                _ => Err(Error::Db(db::Error::NotFound)),
            }
        }
        x if x.starts_with(NAME_TOKEN_OWNER_DEL_ROOT) => {
            let token = &x[NAME_TOKEN_OWNER_DEL_ROOT.len()..];
            token_del(db, token, true)
        }
        x if x.starts_with(NAME_TOKEN_VISITOR_DEL_ROOT) => {
            let token = &x[NAME_TOKEN_VISITOR_DEL_ROOT.len()..];
            token_del(db, token, false)
        }
        _ => Err(Error::Db(db::Error::NotFound)),
    }
}

pub fn name_put(
    config: &Arc<Config>,
    us_arc: &UserStateArc,
    caps: &Capabilities,
    db: &mut Db,
    name: &Name,
    existing_hash: Option<Multihash>,
    hash: &Multihash,
) -> Result<(), Error> {
    let mut result_name = name.clone();
    result_name.push(&NameSegment::create(hash.as_str().to_owned()).unwrap());

    // Handle inttest
    if cfg!(feature = "inttest")
        && us_arc.user_to.to_string() == INTTEST_USER_ID
        && &name.as_bytes()[3..16] == b".srv.inttest."
    {
        let result_hash = handle_inttest_puts(db, name, &existing_hash, hash)?;
        util::name_put_clobber(db, &result_name, &result_hash)?;
        return Ok(());
    }

    // Handle extensions
    if &name.as_bytes()[3..13] != b".srv.edsu." {
        handle_ext(
            "name-put",
            config,
            us_arc,
            caps,
            db,
            name,
            existing_hash,
            Some(hash.clone()),
        )?;
        return Ok(());
    }

    let name_str = name.as_str();
    match name_str {
        NAME_BLOCK_EXISTENCE_TEST => {
            let result_hash = block_existence_test(db, hash)?;
            util::name_put_clobber(db, &result_name, &result_hash)?;
        }
        NAME_NAME_LIST => {
            let result_hash = name_list(db, caps, hash)?;
            util::name_put_clobber(db, &result_name, &result_hash)?;
        }
        NAME_TOKEN_OWNER_GEN => {
            let result_hash = token_gen(db, caps, hash, User::Owner)?;
            util::name_put_clobber(db, &result_name, &result_hash)?;
        }
        NAME_TOKEN_VISITOR_GEN => {
            let result_hash = token_gen(db, caps, hash, User::Visitor(us_arc.user_to.clone()))?;
            util::name_put_clobber(db, &result_name, &result_hash)?;
        }
        _ => Err(db::Error::NotFound)?,
    }

    Ok(())
}

// /////////////////////////////////
// Srv ext
// /////////////////////////////////

// Hide implementation details for srv exts, but log
struct SrvExtError(Option<String>);
impl convert::From<SrvExtError> for Error {
    fn from(e: SrvExtError) -> Self {
        if let Some(x) = e.0 {
            alert!("srv ext: {:?}", x)
        };
        Error::Srv
    }
}
impl convert::From<io::Error> for SrvExtError {
    fn from(e: io::Error) -> Self {
        alert!("srv ext err: {:?}", e);
        SrvExtError(None)
    }
}
impl convert::From<db::Error> for SrvExtError {
    fn from(e: db::Error) -> Self {
        alert!("srv ext err: {:?}", e);
        SrvExtError(None)
    }
}
impl convert::From<edsu::Error> for SrvExtError {
    fn from(e: edsu::Error) -> Self {
        alert!("srv ext err: {:?}", e);
        SrvExtError(None)
    }
}
impl convert::From<threaded::Error> for SrvExtError {
    fn from(e: threaded::Error) -> Self {
        alert!("srv ext err: {:?}", e);
        SrvExtError(None)
    }
}
impl convert::From<eson::Error> for SrvExtError {
    fn from(e: eson::Error) -> Self {
        alert!("srv ext err: {:?}", e);
        SrvExtError(None)
    }
}

// TODO: Make Clippy happy
#[allow(clippy::too_many_arguments)]
fn handle_ext(
    op: &'static str,
    config: &Arc<Config>,
    us_arc: &UserStateArc,
    caps: &Capabilities,
    db: &mut Db,
    name: &Name,
    existing_hash: Option<Multihash>,
    hash: Option<Multihash>,
) -> Result<Option<Multihash>, SrvExtError> {
    // Look up the exec to call
    let path = if let Some(x) = NamePattern::map_match_deep(&config.srv_ext_map, name) {
        x
    } else {
        return Err(SrvExtError(Some("service not found".to_owned())));
    };

    // Start it running
    let mut child = Command::new(path.clone())
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;

    // Send it the data
    {
        let role = match caps.get_role() {
            Role::Config => value!("config"),
            Role::Server => value!("server"),
            Role::None => value!("none"),
        };

        let user = match caps.get_user() {
            User::Anon => value!("anon"),
            User::Owner => value!("owner"),
            User::Visitor(ref uid) => {
                Value::from_bytes(format!("visitor {}", uid.to_string()).as_bytes()).unwrap()
            }
        };

        let stdin = child.stdin.as_mut().unwrap();
        let mut eson = Eson::new();
        eson.insert(key!("op"), vec![value!(op)]);
        eson.insert(key!("name"), vec![edsu::to_value(name)]);
        eson.insert(key!("user-to"), vec![edsu::to_value(&us_arc.user_to)]);
        eson.insert(key!("caps-user"), vec![user]);
        eson.insert(key!("caps-role"), vec![role]);
        if let Some(hash) = caps.get_hash() {
            eson.insert(key!("caps-hash"), vec![edsu::to_value(hash)]);
        }
        if op == "name-put" {
            eson.insert(key!("hash"), vec![edsu::to_value(&hash.unwrap())]);
            if let Some(eh) = existing_hash {
                eson.insert(key!("existing-hash"), vec![edsu::to_value(&eh)]);
            }
        }

        eson.write_into(stdin).ok(); // Ignoring any error so we can read the result
    }

    // Read the output bytes back in
    let mut out_bytes = Vec::new();
    let mut err_bytes = Vec::new();
    let mut buf = [0; 4096];
    let timeout = Instant::now() + Duration::new(SRV_EXT_TIMEOUT_S, 0);
    loop {
        // Read stdout
        loop {
            let bytes_read = match child.stdout.as_mut().unwrap().read(&mut buf) {
                Ok(0) => break,
                Ok(x) => Ok(x),
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => Ok(0),
                Err(e) => Err(e),
            }?;
            out_bytes.extend(buf[..bytes_read].iter());
        }

        // Read stderr
        loop {
            let err_bytes_read = match child.stderr.as_mut().unwrap().read(&mut buf) {
                Ok(0) => break,
                Ok(x) => Ok(x),
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => Ok(0),
                Err(e) => Err(e),
            }?;
            err_bytes.extend(buf[..err_bytes_read].iter());
        }

        //use std::path;
        //util::bytes_to_file(&path::PathBuf::from("/home/heath/scratch/out_bytes"), &out_bytes);
        //util::bytes_to_file(&path::PathBuf::from("/home/heath/scratch/err_bytes"), &err_bytes);

        // See if the child has died
        if let Some(status) = child.try_wait()? {
            if !status.success() {
                if !err_bytes.is_empty() {
                    alert!("srv ext stderr: {:?}", String::from_utf8_lossy(&err_bytes));
                }

                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "exec exit code non-zero",
                ))?;
            }
            break;
        }

        // Check for timeout, otherwise sleep for a bit and loop around
        if Instant::now() > timeout {
            Err(SrvExtError(Some("exec timed out".to_owned())))?;
        }
        thread::sleep(Duration::from_millis(SRV_EXT_CHECK_INTERVAL_MS));
    }

    // Parse the response ESONs
    let mut parser = eson::Parser::new();
    let mut byte_iter = out_bytes.drain(..);
    while let Some(eson) = parser.add_bytes(&mut byte_iter)? {
        match eson.get_req_single("type")?.as_str() {
            "log" => {
                let level_val = eson.get_req_single("level")?;
                let level = match level_val.as_str() {
                    "debug" => LogLevel::Debug,
                    "info" => LogLevel::Info,
                    "error" => LogLevel::Error,
                    "alert" => LogLevel::Alert,
                    "alarm" => LogLevel::Alarm,
                    _ => {
                        return Err(SrvExtError(Some(format!(
                            "unknown srv ext log level: {:?}",
                            level_val.as_str()
                        ))));
                    }
                };
                let text = eson.get_req_single("text")?.as_str();
                let file = eson.get_req_single("file")?.as_str();
                let line = eson
                    .get_req_single("line")?
                    .as_str()
                    .parse::<u32>()
                    .map_err(|_| {
                        SrvExtError(Some("unable to parse log message from srv ext".to_owned()))
                    })?;
                logging::log(&level, &format!("srv ext: {}", text), file, line);
            }
            "result" => {
                if eson.get_single("ok")?.is_some() {
                    return if let Some(val) = eson.get_single("hash")? {
                        Ok(Some(Multihash::from_value(val.clone())?))
                    } else {
                        Ok(None)
                    };
                } else {
                    Err(SrvExtError(Some("exited with an error".to_owned())))?
                }
            }
            "name" => {
                let ext_name = Name::from_value(eson.get_req_single("name")?.clone())?;
                let ext_hash = Multihash::from_value(eson.get_req_single("hash")?.clone())?;
                if let Ok((existing_hash, _, _)) = db.name_get(&ext_name, false) {
                    db.name_put(&ext_name, &ext_hash, Some(&existing_hash))?;
                } else {
                    db.name_put(&ext_name, &ext_hash, None)?;
                }
            }
            "block" => {
                let len_val = eson.get_req_single("payload-length")?;
                let len = len_val.as_str().parse::<u16>().map_err(|_| {
                    alert!("bad payload-length value from srv ext");
                    Error::Srv
                })?;
                let mut payload = Vec::with_capacity(len as usize);
                for _ in 0..len {
                    payload.push(byte_iter.next().unwrap());
                }
                byte_iter.next(); // Skip newline
                let payload_hash = Multihash::from_source(&payload);
                let block = Block::from_bytes(payload)?;
                db.block_put(payload_hash, &block)?;
            }
            x => {
                err!("unrecognized srv ext ESON: {:?}", x);
            }
        }
    }
    Err(SrvExtError(Some("exited without a result".to_owned())))?
}

// /////////////////////////////////
// Util functions
// /////////////////////////////////

fn name_block_eson_defaults(once: bool, ttl: Option<u64>) -> Eson {
    let mut eson = Eson::new();
    if once {
        eson.insert(key!(KEY_NB_ONCE), vec![value!("true")]);
    }
    if let Some(t) = ttl {
        let expires = Value::from_str(&(util::now() + t).to_string()).unwrap();
        eson.insert(key!(KEY_NB_EXPIRES), vec![expires]);
    }
    eson
}

// ////////////////////////////////////////////
// inttest
// ////////////////////////////////////////////

fn handle_inttest_gets(db: &mut Db, get_name: &Name) -> Result<Option<Multihash>, SrvExtError> {
    debug!("inttest_gets: {:?}", get_name.as_str());
    Ok(match get_name.as_str() {
        "pub.srv.inttest.info.server" => None,
        "pub.srv.inttest.actions.wipe" => {
            let all_names = db.all_names()?;
            for name in all_names {
                db.name_del_clobber(&name)?;
            }
            let mut eson = Eson::new();
            eson.insert(key!("ok"), vec![value!("true")]);
            Some(util::eson_block_put(db, &eson)?)
        }
        "pub.srv.inttest.actions.gc" => {
            // The GC has already been run in handle_op in threaded.rs, so just report success
            let mut eson = Eson::new();
            eson.insert(key!("ok"), vec![value!("true")]);
            Some(util::eson_block_put(db, &eson)?)
        }
        _ => None,
    })
}

fn handle_inttest_puts(
    db: &mut Db,
    put_name: &Name,
    _put_existing_hash: &Option<Multihash>,
    put_hash: &Multihash,
) -> Result<Multihash, SrvExtError> {
    debug!("inttest_puts: {:?}", put_name.as_str());
    let mut eson = name_block_eson_defaults(true, Some(DEFAULT_SRV_NAME_TTL));
    match put_name.as_str() {
        "pub.srv.inttest.actions.eson-test" => {
            let name_block = db.block_get(&put_hash)?;
            let name_eson = Eson::from_bytes(name_block.get_contents())?;
            let val_to_test = name_eson.get_req_single(KEY_MB_BLOCKS)?.clone();
            let hash_to_test = Multihash::from_value(val_to_test)?;
            let block_to_test = db.block_get(&hash_to_test)?;
            let valid = Eson::from_bytes(block_to_test.get_contents()).is_ok();
            eson.insert(key!("ok"), vec![value!("true")]);
            eson.insert(
                key!("eson-valid"),
                vec![value!(if valid { "true" } else { "false" })],
            );
        }
        "pub.srv.inttest.actions.eson-to-json" => {
            let name_block = db.block_get(&put_hash)?;
            let name_eson = Eson::from_bytes(name_block.get_contents())?;
            let val_to_test = name_eson.get_req_single(KEY_MB_BLOCKS)?.clone();
            let hash_to_test = Multihash::from_value(val_to_test)?;
            let block_to_test = db.block_get(&hash_to_test)?;
            let to_encode = Eson::from_bytes(block_to_test.get_contents())?;

            // Convert to JSON
            let mut json = String::new();
            json.push_str("{");
            let mut first = true;
            for key in to_encode.keys() {
                // Key
                if first {
                    first = false;
                } else {
                    json.push(',');
                }
                json.push('"');
                json.push_str(key.as_str());
                json.push_str("\":");

                // Val
                let vec_val = to_encode.get(key).unwrap();
                if vec_val.len() == 1 {
                    json.push('"');
                    json.push_str(vec_val[0].as_str());
                    json.push('"');
                } else {
                    let mut first_val = true;
                    json.push('[');
                    for val in vec_val {
                        if first_val {
                            first_val = false;
                        } else {
                            json.push(',');
                        }
                        json.push('"');
                        json.push_str(val.as_str());
                        json.push('"');
                    }
                    json.push(']');
                }
            }
            json.push_str("}");
            eson.insert(key!("ok"), vec![value!("true")]);
            eson.insert(
                key!("json"),
                vec![Value::from_bytes(json.as_bytes()).unwrap()],
            );
        }
        "pub.srv.inttest.actions.eson-encoded-utf8-to-b58" => {
            let name_block = db.block_get(&put_hash)?;
            let name_eson = Eson::from_bytes(name_block.get_contents())?;
            let val_to_test = name_eson.get_req_single("val")?.clone();
            if let Ok(decoded) = util::eson_utf8_decode(val_to_test.as_str()) {
                // Re-encode and compare
                if util::eson_utf8_encode(&decoded) != val_to_test {
                    eson.insert(key!("err"), vec![value!("re-encode-mismatch")]);
                } else {
                    let b58 = bs58::encode(decoded).into_string();
                    eson.insert(key!("ok"), vec![value!("true")]);
                    eson.insert(
                        key!("b58"),
                        vec![Value::from_bytes(b58.as_bytes()).unwrap()],
                    );
                }
            } else {
                eson.insert(key!("ok"), vec![value!("true")]);
                eson.insert(key!("valid"), vec![value!("false")]);
            }
        }
        "pub.srv.inttest.actions.type-test" => {
            let name_block = db.block_get(&put_hash)?;
            let name_eson = Eson::from_bytes(name_block.get_contents())?;
            let val_to_test = name_eson.get_req_single("val")?.clone();
            let type_to_test = name_eson.get_req_single("type")?;
            let valid = match type_to_test.as_str() {
                "bool" => bool::from_value(val_to_test).is_ok(),
                "u16" => u16::from_value(val_to_test).is_ok(),
                "u32" => u32::from_value(val_to_test).is_ok(),
                "u64" => u64::from_value(val_to_test).is_ok(),
                "versions" => Versions::from_value(val_to_test).is_ok(),
                "encodings" => Encodings::from_value(val_to_test).is_ok(),
                "multihash" => Multihash::from_value(val_to_test).is_ok(),
                "user-id" => UserId::from_value(val_to_test).is_ok(),
                "name" => Name::from_value(val_to_test).is_ok(),
                "name-pattern" => NamePattern::from_value(&val_to_test).is_ok(),
                _ => true,
            };
            eson.insert(key!("ok"), vec![value!("true")]);
            eson.insert(
                key!("valid"),
                vec![value!(if valid { "true" } else { "false" })],
            );
        }
        _ => {
            err!("inttest name not found: {:?}", put_name.as_str());
            eson.insert(key!("err"), vec![value!("not-found")]);
        }
    }
    Ok(util::eson_block_put(db, &eson)?)
}

// /////////////////////////////////
// Srv functions
// /////////////////////////////////

fn block_existence_test(db: &mut Db, root: &Multihash) -> Result<Multihash, Error> {
    let mut mb_hashes = VecDeque::new();
    let mut missing = Vec::new();
    let mut missing_size = 0;
    let mut next = None;

    mb_hashes.push_back(root.clone());
    while let Some(mb_hash) = mb_hashes.pop_front() {
        let mb_eson = Eson::from_bytes(db.block_get(&mb_hash)?.get_contents())?;

        // Start with the blocks
        if let Some(vec_val) = mb_eson.get(KEY_MB_BLOCKS) {
            for val in vec_val {
                // Special case for null, which we always have
                if val.as_str() == NULL_HASH {
                    continue;
                }
                let hash = edsu::from_bytes(val.as_bytes())?;
                if !db.block_exists(&hash) {
                    missing_size += hash.as_bytes().len() + 3;
                    missing.push(hash);
                    if missing_size > 62 * 1024 {
                        let mut eson = Eson::new();
                        let val_vec = missing.drain(..).map(|x| edsu::to_value(&x)).collect();
                        eson.insert(key!(KEY_MB_BLOCKS), val_vec);
                        if let Some(n) = next.take() {
                            eson.insert(key!(KEY_MB_METAS), vec![edsu::to_value(&n)]);
                        }
                        next = Some(util::eson_block_put(db, &eson)?);
                        missing_size = 0;
                    }
                } else {
                    db.block_mark_as_recent(hash);
                }
            }
        }

        // Then queue the metas for the next loop
        if let Some(vec_val) = mb_eson.get(KEY_MB_METAS) {
            for val in vec_val.iter() {
                mb_hashes.push_back(edsu::from_bytes(val.as_bytes())?);
            }
        }
    }

    // Make name block/final metablock
    let mut eson = name_block_eson_defaults(true, Some(DEFAULT_SRV_NAME_TTL));
    if !missing.is_empty() {
        let val_vec = missing.drain(..).map(|x| edsu::to_value(&x)).collect();
        eson.insert(key!(KEY_MB_BLOCKS), val_vec);
    }
    if let Some(n) = next {
        eson.insert(key!(KEY_MB_METAS), vec![edsu::to_value(&n)]);
    }
    Ok(util::eson_block_put(db, &eson)?)
}

fn name_list(db: &mut Db, caps: &Capabilities, hash: &Multihash) -> Result<Multihash, Error> {
    let query = Eson::from_bytes(db.block_get(hash)?.get_contents())?;
    let after =
        query.get_single_parsed(KEY_SRV_NAME_LIST_AFTER, &|x| Name::from_value(x.clone()))?;
    let prefix = query.get_single_parsed(KEY_SRV_NAME_LIST_PREFIX, &|x| {
        let nm = match Name::from_value(x.clone()) {
            Ok(x) => x,
            Err(e) => return Err(e),
        };
        let mut st = x.as_str().to_owned();
        st.push('.');
        Ok((nm, st))
    })?;

    let mut all_names = db.all_names()?;
    all_names.sort();

    // Filter by both the query and permissions
    let mut started = after.is_none();
    let mut err = None; // Until try_for_each is stable
    let name_vals = all_names
        .drain(..)
        .filter_map(|name| {
            // Filter on started
            if !started {
                if let Some(a) = after.as_ref() {
                    started = *a == name;
                }
                return None;
            }

            // Filter on prefix
            if let Some((ref nm, ref st)) = prefix.as_ref() {
                if !name.as_str().starts_with(st) && *nm != name {
                    return None;
                }
            }

            // Permissions based on the name
            if !caps.name_get(&name) {
                return None;
            }

            // Permissions based on the name block
            match db.name_get(&name, false) {
                Err(db::Error::NotFound) => None,
                Err(e) => {
                    err = Some(e);
                    None
                }
                Ok((_, _, eson)) => {
                    if !caps.name_get_eson(&db, &eson) {
                        None
                    } else {
                        // All good - add to list
                        Some(edsu::to_value(&name))
                    }
                }
            }
        })
        .collect::<Vec<Value>>();
    if let Some(e) = err {
        Err(e)?
    }

    let truncated = false;

    let mut eson = name_block_eson_defaults(true, Some(DEFAULT_SRV_NAME_TTL));
    if !name_vals.is_empty() {
        eson.insert(key!(KEY_SRV_NAME_LIST_NAMES), name_vals);
        if truncated {
            eson.insert(key!(KEY_SRV_NAME_LIST_TRUNCATED), vec![value!("true")]);
        }
    }
    Ok(util::eson_block_put(db, &eson)?)
}

fn token_gen(
    db: &mut Db,
    _: &Capabilities,
    hash: &Multihash,
    user: User,
) -> Result<Multihash, Error> {
    let owner_not_visitor = if let User::Owner = user { true } else { false };

    // Get args
    let call = Eson::from_bytes(db.block_get(hash)?.get_contents())?;
    let label = call.get_req_single(KEY_SRV_GEN_LABEL)?.clone();
    let req_id = call.get_single(KEY_SRV_GEN_REQ_ID)?.cloned();
    let caps_hash = call
        .get_req_single_parsed(KEY_SRV_GEN_CAPS, &|x| Multihash::from_value(x.clone()))?
        .clone();
    let ttl = call.get_single_parsed(KEY_SRV_GEN_TOKEN_TTL, &|x: &Value| x.as_str().parse())?;
    let once_val = call.get_single(KEY_NB_ONCE)?;
    let once = if let Some(x) = once_val {
        x.as_str() == "true"
    } else {
        false
    };

    // Validate
    let caps_eson = Eson::from_bytes(db.block_get(&caps_hash)?.get_contents())?;
    Capabilities::from_eson(Role::None, user, Some(caps_hash.clone()), &caps_eson)?;
    util::eson_utf8_decode(label.as_str()).map_err(|e| {
        Error::InvalidInput(format!(
            "{} must be valid ESON-encoded UTF-8: {}",
            KEY_SRV_GEN_LABEL, e
        ))
    })?;

    // Create the token block
    let token = util::token_put(db, once, ttl, label, req_id, &caps_hash, owner_not_visitor)?;

    // Response
    let mut eson = name_block_eson_defaults(true, Some(DEFAULT_SRV_NAME_TTL));
    eson.insert(key!(KEY_SRV_GEN_TOKEN_TOKEN), vec![edsu::to_value(&token)]);
    eson.insert(key!("edsu:ok"), vec![value!("true")]);
    Ok(util::eson_block_put(db, &eson)?)
}

fn edsu_ok(db: &mut Db) -> Result<Multihash, Error> {
    let mut eson = Eson::new();
    eson.insert(key!("edsu:ok"), vec![value!("true")]);
    Ok(util::eson_block_put(db, &eson)?)
}

fn token_del(db: &mut Db, token_st: &str, owner_not_visitor: bool) -> Result<Multihash, Error> {
    // Construct name from token
    let name;
    if owner_not_visitor {
        let name_st = format!("{}{}", NAME_TOKEN_OWNER_ROOT, token_st);
        name = edsu::from_bytes(name_st.as_bytes())?;
    } else {
        // Verify the whole visitor token first - need the oringinal token to authorize deletion
        let tk_pub = &token_st[0..VISITOR_TOKEN_PUB_LEN];
        let name_st = format!("{}{}", NAME_TOKEN_VISITOR_ROOT, tk_pub);
        name = edsu::from_bytes(name_st.as_bytes())?;
        match db.name_get(&name, false) {
            Err(db::Error::NotFound) => {
                return edsu_ok(db);
            }
            Err(e) => Err(e)?,
            Ok((_, _, nb_eson)) => {
                let actual_token_st = nb_eson.get_req_single(KEY_TB_VISITOR_TOKEN)?.as_str();
                if token_st != actual_token_st {
                    return Err(Error::InvalidInput("token mismatch".to_owned()));
                }
            }
        }
    }

    // Delete (clobber doesn't error on non-existance - only returns a false, which we ignore)
    db.name_del_clobber(&name)?;

    // Thumbs up
    edsu_ok(db)
}

fn visitor_caps(db: &mut Db, visitee: &UserId, encoded: &str) -> Result<Multihash, Error> {
    let tk_pub = &encoded[0..VISITOR_TOKEN_PUB_LEN];
    let encoded_prv = &encoded[VISITOR_TOKEN_PUB_LEN..];
    let prv = bs58::decode(encoded_prv)
        .into_vec()
        .map_err(|_| Error::Db(db::Error::NotFound))?;

    // Grab the token and the caps hash
    let name_string = format!("{}{}", NAME_TOKEN_VISITOR_ROOT, tk_pub);
    let name = edsu::from_bytes(name_string.as_bytes())?;
    let (_, _, nb_eson) = db.name_get(&name, true)?;
    let token_st = nb_eson.get_req_single(KEY_TB_VISITOR_TOKEN)?.as_str();
    let caps_desc_hash = nb_eson
        .get_req_single_parsed(KEY_TB_VISITOR_CAPS, &|x| Multihash::from_value(x.clone()))?;

    // Verify the secret
    let tk_prv = &token_st[VISITOR_TOKEN_PUB_LEN..];
    let hmac_key = ring::hmac::Key::new(ring::hmac::HMAC_SHA256, tk_prv.as_bytes());
    let visitee_st = visitee.to_string();
    ring::hmac::verify(&hmac_key, visitee_st.as_bytes(), &prv)
        .map_err(|_| Error::Db(db::Error::NotFound))?;

    // All good, return the hash of the caps
    let caps_desc_eson = Eson::from_bytes(db.block_get(&caps_desc_hash)?.get_contents())?;
    let caps_hash = caps_desc_eson
        .get_req_single_parsed(KEY_TB_VISITOR_CAPS, &|x| Multihash::from_value(x.clone()))?;
    Ok(caps_hash)
}
