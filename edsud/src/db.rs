use crate::logging;
use crate::prelude::*;
use crate::util;
use bs58;
use edsu_common::block_store::{self, BlockStore};
use edsu_common::edsu::{self, Block, Multihash, Name, OobCode, ServerMsg, UserId, Value};
use edsu_common::eson::{self, Eson};
use std::{
    collections::HashSet,
    os::unix::ffi::OsStrExt,
    path::{Path, PathBuf},
    time::{Duration, Instant},
    {convert, fs, io},
};

const DIR_NAMES: &str = "names";
const DIR_BLOCKS: &str = "blocks";

#[derive(Debug)]
pub enum Error {
    NotFound,
    HashMismatch(Option<Multihash>),
    Io(io::Error),
    CorruptedStorage(String),
    InvalidInput(String),
    InvalidContent(String),
}

impl convert::From<eson::Error> for Error {
    fn from(e: eson::Error) -> Self {
        Error::InvalidInput(format!("{:?}", e))
    }
}
impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

pub struct Db {
    root: PathBuf,
    pub recent_blocks: Vec<(Instant, Multihash)>,
}

impl Db {
    /// Note: this is not thread-safe for a single user - needs a RwLock further up
    pub fn new(db_root: &Path, user: &UserId) -> Self {
        let mut root = PathBuf::from(db_root);
        root.push(&PathBuf::from(&user.to_fs_safe_string()));
        Db {
            root,
            recent_blocks: Vec::new(),
        }
    }

    // Path creation
    fn block_path(&self, hash: &Multihash) -> PathBuf {
        let mut path = self.root.clone();
        path.push(&PathBuf::from(DIR_BLOCKS));
        path.push(&PathBuf::from(hash.as_str()));
        path
    }
    fn name_path(&self, name: &Name) -> PathBuf {
        let mut path = self.root.clone();
        path.push(&PathBuf::from(DIR_NAMES));
        path.push(&PathBuf::from(name.as_fs_safe_str()));
        path
    }

    // Block ops
    pub fn block_put(&mut self, hash: Multihash, block: &Block) -> Result<usize, Error> {
        // Get the path
        let path = self.block_path(&hash);

        // Record it for the GC
        let now = Instant::now();
        let window = now - Duration::from_secs(BLOCK_UPLOAD_GC_WINDOW_S);
        self.recent_blocks.retain(|&(when, _)| when >= window);
        self.recent_blocks.push((now, hash));

        // If it exists already, we're done
        if path.exists() {
            return Ok(0);
        }

        let mut bytes_vec;
        let bytes = if block.is_bin() {
            block.as_bytes()
        } else {
            // If it's a text block, we want to write store its parsed hashes, it'd be nice to
            // just wrap it in a bin block, but that could violate the size restriction - just
            // using a simple encoding instead
            let hashes = block.get_hashes();
            let len = 3 + MULTIHASH_BYTE_LEN * hashes.len() + block.as_bytes().len();
            bytes_vec = Vec::with_capacity(len);
            bytes_vec.push(b'~');
            bytes_vec.push((hashes.len() >> 8) as u8);
            bytes_vec.push(hashes.len() as u8);
            for h in hashes {
                bytes_vec.append(&mut bs58::decode(h.as_bytes()).into_vec().unwrap());
            }
            bytes_vec.extend(block.as_bytes());
            &bytes_vec
        };
        let bytes_written = bytes.len();

        // Atomically write the file
        atomic_create(&path, &bytes)?;
        Ok(bytes_written)
    }
    pub fn block_get(&self, hash: &Multihash) -> Result<Block, Error> {
        let path = self.block_path(hash);
        let file_bytes = util::file_to_bytes(&path).map_err(|e| {
            if e.kind() == io::ErrorKind::NotFound {
                Error::NotFound
            } else {
                Error::Io(e)
            }
        })?;

        if file_bytes[0] == b'~' {
            // Text block
            let mut num_hashes = file_bytes[2] as usize;
            num_hashes |= (file_bytes[1] as usize) << 8;
            let mut hashes = Vec::with_capacity(num_hashes);
            let mut hash_start = 3;
            for _ in 0..num_hashes {
                let end = hash_start + MULTIHASH_BYTE_LEN;
                hashes.push(Multihash::from_multihash_bytes(
                    &file_bytes[hash_start..end],
                ));
                hash_start = end;
            }
            let bytes = &file_bytes[hash_start..];

            // Then re-package as a text block
            if let Some(nl_loc) = bytes.iter().position(|x| *x == b'\n') {
                let b = Block::from_text_parts(hashes, nl_loc + 1, bytes.to_vec())
                    .map_err(|e| Error::CorruptedStorage(format!("{:?}", e)))?;
                Ok(b)
            } else {
                Err(Error::CorruptedStorage(format!(
                    "invalid text block: {:?}",
                    hash
                )))
            }
        } else {
            // Bin block
            Ok(Block::from_bytes(file_bytes)
                .map_err(|e| Error::CorruptedStorage(format!("{:?}", e)))?)
        }
    }
    pub fn block_exists(&self, hash: &Multihash) -> bool {
        self.block_path(hash).exists()
    }
    pub fn block_del(&self, hash: &Multihash) -> Result<u64, Error> {
        let path = self.block_path(hash);
        let len = fs::metadata(&path)?.len();
        match fs::remove_file(&path) {
            Ok(_) => Ok(len),
            Err(ref e) if e.kind() == io::ErrorKind::NotFound => Err(Error::NotFound),
            Err(e) => Err(Error::Io(e)),
        }
    }
    pub fn block_mark_as_recent(&mut self, hash: Multihash) {
        self.recent_blocks.push((Instant::now(), hash));
    }

    // Name ops
    pub fn name_put(
        &mut self,
        name: &Name,
        hash: &Multihash,
        existing_hash: Option<&Multihash>,
    ) -> Result<(), Error> {
        let path = self.name_path(name);

        // Grab the potentially existing hash
        let actual_existing_hash = match self.name_get(name, true) {
            Ok(x) => Ok(Some(x.0)),
            Err(Error::NotFound) => Ok(None),
            Err(e) => Err(e),
        }?;

        if actual_existing_hash.as_ref() == existing_hash {
            // Make sure it's a text block containing valid ESON
            let block = self.block_get(&hash)?;
            if block.is_bin() {
                Err(Error::InvalidContent(
                    "names must point to text blocks, not binary".to_owned(),
                ))?
            }
            if let Err(e) = Eson::from_bytes(block.get_contents()) {
                Err(Error::InvalidContent(format!(
                    "names must point to valid ESON blocks: {:?}",
                    e
                )))?
            }
            atomic_create(&path, hash.as_bytes())
        } else if existing_hash == Some(hash) {
            // If the existing hash doesn't match, *but* the name is already what the call wanted
            // it to be, then return success - this is an intentional policy to enable NamePut
            // retries
            Ok(())
        } else {
            Err(Error::HashMismatch(actual_existing_hash))
        }
    }
    pub fn name_get(
        &mut self,
        name: &Name,
        trigger_once: bool,
    ) -> Result<(Multihash, Block, Eson), Error> {
        let path = self.name_path(name);

        // Grab the file bytes
        let file_bytes = util::file_to_bytes(&path).map_err(|e| {
            if e.kind() == io::ErrorKind::NotFound {
                Error::NotFound
            } else {
                Error::Io(e)
            }
        })?;

        // Parse the hash
        let hash = edsu::from_bytes(&file_bytes)
            .map_err(|e| Error::CorruptedStorage(format!("bad hash for {:?}: {:?}", path, e)))?;

        // Get the name block
        let name_block_maybe = self.block_get(&hash);

        // A name pointing to nothing, or a bin block, is forbidden - if this happens, log it,
        // delete the name, and pretend that the name never existed (as should have been the
        // case)
        let name_block = if name_block_maybe.is_err() || name_block_maybe.as_ref().unwrap().is_bin()
        {
            alert!(
                "found bad name (pointing to nothing, or bin block), removing: {:?}",
                name
            );
            if let Err(e) = fs::remove_file(&path) {
                alert!("error removing bad name: {:?}", e);
            }
            return Err(Error::NotFound);
        } else {
            name_block_maybe.unwrap()
        };

        // Parse the ESON
        let name_eson = Eson::from_bytes(name_block.get_contents())
            .map_err(|e| Error::CorruptedStorage(format!("bad ESON for {:?}: {:?}", path, e)))?;

        // Check for expiry
        if let Some(expires) = name_eson.get_single(KEY_NB_EXPIRES)? {
            if let Ok(seconds) = expires.as_str().parse::<u64>() {
                if util::now() > seconds {
                    // Expired - delete it and act like it never existed
                    fs::remove_file(&path).map_err(Error::Io)?;
                    return Err(Error::NotFound);
                }
            }
        }

        // Check for once
        if trigger_once {
            if let Some(once) = name_eson.get_single(KEY_NB_ONCE)? {
                if once.as_bytes() == b"true" {
                    // We've read it once - delete it
                    fs::remove_file(&path).map_err(Error::Io)?;
                }
            }
        }

        // All good, return the data we gathered
        Ok((hash, name_block, name_eson))
    }

    pub fn name_del(&mut self, name: &Name, existing_hash: &Multihash) -> Result<(), Error> {
        let actual_existing_hash = match self.name_get(name, false) {
            Ok(x) => Ok(Some(x.0)),
            Err(Error::NotFound) => Ok(None),
            Err(e) => Err(e),
        }?;

        if Some(existing_hash) == actual_existing_hash.as_ref() {
            // Hashes match - remove file
            let path = self.name_path(name);
            fs::remove_file(&path).map_err(Error::Io)
        } else {
            Err(Error::HashMismatch(actual_existing_hash))
        }
    }

    pub fn name_del_clobber(&mut self, name: &Name) -> Result<bool, Error> {
        let path = self.name_path(name);
        match fs::remove_file(&path) {
            Ok(()) => Ok(true),
            Err(ref e) if e.kind() == io::ErrorKind::NotFound => Ok(false),
            Err(e) => Err(Error::Io(e)),
        }
    }

    pub fn name_del_if_once(&mut self, name: &Name, eson: &Eson) -> Result<(), Error> {
        if let Ok(once) = eson.get_req_single(KEY_NB_ONCE) {
            if once.as_bytes() == b"true" {
                let path = self.name_path(name);
                fs::remove_file(&path).map_err(Error::Io)?;
            }
        }
        Ok(())
    }

    // Utils

    pub fn all_names(&mut self) -> Result<Vec<Name>, Error> {
        let mut names = Vec::new();
        let mut name_dir = self.root.clone();
        name_dir.push(&PathBuf::from(DIR_NAMES));
        for entry in name_dir.read_dir().map_err(Error::Io)? {
            match entry {
                Ok(ent) => {
                    if let Ok(name) = edsu::from_bytes(ent.file_name().as_os_str().as_bytes()) {
                        names.push(name);
                    }
                }
                Err(e) => alarm!("error while reading {:?}: {:?}", name_dir, e),
            }
        }
        Ok(names)
    }

    pub fn block_get_refs(&self, hash: &Multihash) -> Result<Vec<Multihash>, Error> {
        // TODO: this can be a lot more efficient by just reading the header
        let block = self.block_get(hash)?;
        Ok(block.into_parts().0)
    }

    pub fn block_get_len(&self, hash: &Multihash) -> Result<usize, Error> {
        // TODO: this can be more efficient by reading the header & extrapolating from file size
        let block = self.block_get(hash)?;
        Ok(block.as_bytes().len())
    }

    pub fn all_block_hashes(&mut self) -> Result<(Vec<Multihash>, u64), Error> {
        let mut hashes = Vec::new();
        let mut block_dir = self.root.clone();
        let mut total_len = 0;
        block_dir.push(&PathBuf::from(DIR_BLOCKS));
        for entry in block_dir.read_dir().map_err(Error::Io)? {
            match entry {
                Ok(ent) => {
                    total_len += fs::metadata(ent.path())?.len();
                    if let Ok(hash) = edsu::from_bytes(ent.file_name().as_os_str().as_bytes()) {
                        hashes.push(hash);
                    }
                }
                Err(e) => alarm!("error while reading {:?}: {:?}", block_dir, e),
            }
        }
        Ok((hashes, total_len))
    }

    pub fn all_block_hashes_from_root(
        &self,
        hash: &Multihash,
    ) -> Result<HashSet<Multihash>, Error> {
        let mut ret = HashSet::new();
        let mut to_walk = self.block_get_refs(hash)?;
        while let Some(walk_hash) = to_walk.pop() {
            to_walk.extend(self.block_get_refs(&walk_hash)?);
            ret.insert(walk_hash);
        }
        Ok(ret)
    }
}

// Name iterator

/*
pub struct NameIterator {
    prefix_len: usize,
    iter: ReadDir,
}

impl NameIterator {
    fn new(dir: &PathBuf) -> Result<Self, Error> {
        let prefix_len = dir.to_str().unwrap().len() + 1;
        Ok(NameIterator {
            prefix_len,
            iter: fs::read_dir(dir)?,
        })
    }
}

impl Iterator for NameIterator {
    type Item = Result<Name, Error>;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.iter.next() {
                None => return None,
                Some(Err(e)) => return Some(Err(Error::Io(e))),
                Some(Ok(entry)) => {
                    let string = entry.path().to_str().unwrap().to_owned();
                    let name_str = &string[self.prefix_len..];
                    if let Ok(name) = edsu::from_bytes(name_str.as_bytes()) {
                        return Some(Ok(name));
                    }
                }
            }
        }
    }
}
*/

// BlockStore implementation
pub struct BlockStoreDb<'a> {
    db: &'a mut Db,
}

impl<'a> BlockStoreDb<'a> {
    pub fn new(db: &'a mut Db) -> Self {
        BlockStoreDb { db }
    }
}

impl<'a> BlockStore for BlockStoreDb<'a> {
    fn block_get(&mut self, hash: &Multihash) -> Result<Block, block_store::Error> {
        Ok(self.db.block_get(hash)?)
    }
    fn block_put(&mut self, block: &Block) -> Result<Multihash, block_store::Error> {
        let hash = Multihash::from_source(block.as_bytes());
        self.db.block_put(hash.clone(), &block)?;
        Ok(hash)
    }
}

impl convert::From<Error> for block_store::Error {
    fn from(e: Error) -> Self {
        match e {
            Error::NotFound => block_store::Error::NotFound,
            Error::HashMismatch(_) => panic!(),
            Error::Io(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::CorruptedStorage(_) => block_store::Error::Server(format!("{:?}", e)),
            Error::InvalidInput(_) => block_store::Error::InvalidInput(format!("{:?}", e)),
            Error::InvalidContent(_) => block_store::Error::InvalidContent(format!("{:?}", e)),
        }
    }
}

// Util fns

fn atomic_create(path: &PathBuf, bytes: &[u8]) -> Result<(), Error> {
    let mut num = 0u32;
    loop {
        num += 1;
        // Sanity backstop
        if num > 0xffff {
            return Err(Error::CorruptedStorage("ran out of temp files".to_owned()));
        }

        // Make temp file name
        let mut tmp_path = PathBuf::from(path.parent().unwrap());
        tmp_path.push(PathBuf::from("#tmp"));
        tmp_path.push(PathBuf::from(num.to_string()));

        // Atomically create the temp file
        match util::bytes_to_new_file(&tmp_path, bytes, Some(DB_FILE_MODE)) {
            Ok(_) => {
                // Temp file written, rename it
                match fs::rename(&tmp_path, &path) {
                    Ok(_) => return Ok(()),
                    Err(e) => {
                        // Error, try to remove temp file (if it still exists)
                        fs::remove_file(&tmp_path).ok();
                        return Err(Error::Io(e));
                    }
                }
            }
            Err(ref e) if e.kind() == io::ErrorKind::AlreadyExists => {
                // That name is in use already, try the next one
                continue;
            }
            Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
                // Missing directory structure - try to create, and try again
                fs::DirBuilder::new()
                    .recursive(true)
                    .create(&tmp_path.parent().unwrap())
                    .map_err(Error::Io)?;
                continue;
            }
            Err(e) => return Err(Error::Io(e)),
        }
    }
}

pub fn err_to_oob(err: Error, channel: Option<Value>) -> ServerMsg {
    let code;
    let payload;
    let retry;
    match err {
        Error::NotFound => {
            code = OobCode::NotFound;
            payload = None;
            retry = util::Retry::None;
        }
        Error::HashMismatch(_) => {
            code = OobCode::HashMismatch;
            payload = None;
            retry = util::Retry::None;
        }
        Error::Io(e) => {
            alarm!("i/o error: {:?}", e);
            return util::oob_server_err();
        }
        Error::CorruptedStorage(e) => {
            alarm!("corrupted storage error: {:?}", e);
            code = OobCode::ServerError;
            payload = None;
            retry = util::Retry::None;
        }
        Error::InvalidInput(e) => {
            code = OobCode::InvalidInput;
            payload = util::oob_msg(&format!("invalid data: {:?}", e));
            retry = util::Retry::None;
        }
        Error::InvalidContent(e) => {
            code = OobCode::InvalidContent;
            payload = util::oob_msg(&format!("the data stored is invalid for the task: {:?}", e));
            retry = util::Retry::None;
        }
    }
    ServerMsg::Oob {
        channel: channel.unwrap_or_else(util::default_channel),
        close_connection: None,
        code,
        retry_delay_ms: retry.as_msg_ms(),
        payload,
        encoding: None,
    }
}
