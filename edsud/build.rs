use cc;

fn main() {
    // Compile aux.c
    cc::Build::new()
        .file("c/aux.c")
        .include("/usr/local/include")
        .compile("aux");

    println!("cargo:rustc-link-lib=bearssl");
}
