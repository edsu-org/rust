#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <pwd.h>

#include "bearssl.h"

/* NOTES

TODO: Circular buffers (the way this is currently used potentially triggers copies like crazy)

*/

/////////////////////////
// Not synced with Rust

#define NUM_SERVER_SOCKS 2
#define SSL_BUFFER_SIZE BR_SSL_BUFSIZE_BIDI
#define MAX_CERT_TABLE_GENERATIONS 256

struct net_sock_ssl {
    bool active;
    br_ssl_server_context sc;
    uint8_t buffer[SSL_BUFFER_SIZE];
};

// Globals
static uint32_t NumSocks = 0;
static int Kq = -1;
static struct kevent *KEvents = NULL;
static uint32_t KEventsSize = 0;
static struct net_sock *Socks = NULL;
static struct net_sock_status *Statuses = NULL;
static struct net_sock_ssl *Ssls = NULL;
static uint8_t *BufsIn = NULL;
static uint8_t *BufsOut = NULL;
static int SSockEdsu = -1;
static int SSockWs = -1;
static struct net_cert_item *CertTables[MAX_CERT_TABLE_GENERATIONS] = { 0 };
static uint8_t *CertHeaps[MAX_CERT_TABLE_GENERATIONS] = { 0 };
static uint32_t CurCertTableLen = 0;
static uint32_t CurCertTableGeneration = 0;
static br_ssl_session_cache_lru SessionCache;


/////////////////////////
// Synced with Rust

#define NET_OK                      0
#define NET_BUF_WOULD_OFLOW         1
#define NET_ERR                     -1
#define NET_FATAL                   -2
#define NET_EV_NEW_SOCK             0b00000001
#define NET_EV_SOCK_HANDSHAKE_DONE  0b00000010
#define NET_EV_NEW_BYTES            0b00000100
#define NET_EV_RUST_CLOSE           0b00001000
#define NET_CALL_RECV               0b00000001
#define NET_CALL_SEND               0b00000010
#define NET_CALL_RECV_SSL           0b00000100
#define NET_CALL_SEND_SSL           0b00001000
#define NET_SOCK_TYPE_CLOSED        0x00
#define NET_SOCK_TYPE_EDSU          0x01
#define NET_SOCK_TYPE_WS            0x02

#define NET_MAX_COMMON_NAME_LEN  256
#define NET_CERT_DECODE_STRUCT_LEN 2560
#define NET_CERT_MAX_CHAIN_LEN 4
#define NET_BUF_SIZE_IN 65536
#define NET_BUF_SIZE_OUT 65536

struct net_sock {
    uint32_t fd;
    bool     sock_4_not_6;
    uint8_t  sock_ip[16];
    uint16_t buf_in_live_bytes;
    uint16_t buf_out_live_bytes;
    uint64_t last_send;
    uint64_t last_recv;
    uint64_t cert_generation;
};

struct net_sock_status {
    uint8_t stype;
    uint8_t event;
    uint8_t call;
};

struct net_cert_item {
    uint32_t key_idx;
    uint32_t key_len;
    uint32_t num_certs;
    bool valid;
    char common_name[NET_MAX_COMMON_NAME_LEN];
    uint64_t chain_array[NET_CERT_MAX_CHAIN_LEN * 2];
    uint8_t decode_struct_mem[NET_CERT_DECODE_STRUCT_LEN];
};


int32_t net_init(uint32_t num_socks, uint32_t session_cache_size, char *ip_edsu,
                 char *port_edsu, char *ip_ws, char *port_ws, char *unix_user,
                 char *unix_group, struct net_sock *socks, struct net_sock_status *statuses,
                 uint8_t *bufs_in, uint8_t *bufs_out);
int32_t net_select(uint32_t tick_us);
int32_t net_write(uint32_t sock_id, uint8_t *bytes, uint16_t num_bytes);
int32_t net_report_consumption(uint32_t sock_id, uint16_t num_bytes);
void net_fini();
int32_t net_set_cert_table(struct net_cert_item *cert_table, uint32_t cert_table_len,
                           uint8_t *cert_heap, uint32_t cert_heap_len);


/////////////////////////

// Moved this code out for clarity

#include "ssl.c"

/////////////////////////

uint8_t *buf_in(uint32_t sock_id) {
    return &(BufsIn[sock_id * NET_BUF_SIZE_IN]);
}


uint8_t *buf_out(uint32_t sock_id) {
    return &BufsOut[sock_id * NET_BUF_SIZE_OUT];
}


void sock_close(uint32_t sock_id) {
    struct net_sock *sock = &(Socks[sock_id]);
    if (sock->fd > 0) {
        close(sock->fd);
        alog(LOG_DEBUG, 0, "closing sock: %d", sock_id);
    }
    bzero(sock, sizeof(struct net_sock));
    bzero(&(Statuses[sock_id]), sizeof(struct net_sock_status));
    bzero(&(Ssls[sock_id]), sizeof(struct net_sock_ssl));
    // Paranoia
    bzero(buf_in(sock_id), NET_BUF_SIZE_IN);
    bzero(buf_out(sock_id), NET_BUF_SIZE_OUT);
}


uint16_t send_to_socket(uint32_t sock_id, struct net_sock* sock, struct net_sock_status* status,
                        uint8_t *buf, uint16_t buf_len, bool *stop)
{
    *stop = false;

    if (buf_len == 0)
        return 0;

    for (int try_num = 0; try_num < 2; try_num++) {
        int bytes_sent = send(sock->fd, buf, buf_len, 0);
        if (bytes_sent < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                status->call &= ~NET_CALL_SEND;
                return 0;
            } else if (errno == ENOBUFS) {
                return 0;
            } else {
                alog(LOG_INFO, NET_ERR, "send(): %s", strerror(errno));
                sock_close(sock_id);
                *stop = true;
                return 0;
            }
        } else {
            return bytes_sent;
        }
    }
    return 0;
}


void send_from_net_sock(uint32_t sock_id, struct net_sock* sock, struct net_sock_status *status) {
    if (status->stype == NET_SOCK_TYPE_CLOSED) 
        return;

    // Figure out how to address the sock buffer
    uint8_t *sock_buf = buf_out(sock_id);
    uint16_t sock_buf_len = sock->buf_out_live_bytes;
    uint16_t sock_buf_consumed = 0;

    struct net_sock_ssl *ssl = &(Ssls[sock_id]);
    if (ssl->active) {
        // SSL
        // If there's anything in the sock buffer, try to send it to the SSL engine and flush
        if (sock_buf_len > 0 && br_ssl_engine_current_state(&ssl->sc.eng) & BR_SSL_SENDAPP) {
            size_t clear_buf_len = 0;
            uint8_t *clear_buf = br_ssl_engine_sendapp_buf(&ssl->sc.eng, &clear_buf_len);
            size_t to_copy = (sock_buf_len < clear_buf_len) ? sock_buf_len : clear_buf_len;
            memcpy(clear_buf, sock_buf, to_copy);
            br_ssl_engine_sendapp_ack(&ssl->sc.eng, to_copy);
            br_ssl_engine_flush(&ssl->sc.eng, 0);
            sock_buf_consumed = to_copy;
        }

        // Check to see if there's any ciphertext to write to the socket
        unsigned state = br_ssl_engine_current_state(&ssl->sc.eng);
        // - First, see if what we wrote messed up the SSL engine
        if (state & BR_SSL_CLOSED) {
            sock_close(sock_id);
            alog(LOG_DEBUG, 0, "SSL closed, last err: %d",
                 br_ssl_engine_last_error(&ssl->sc.eng));
            return;
        }

        // Send any ciphertext to the socket (maybe from the write above, but also maybe because
        // of the engine doing handshakes, etc.
        if (state & BR_SSL_SENDREC) {
            size_t cipher_buf_len = 0;
            bool stop = false;
            uint8_t *cipher_buf = br_ssl_engine_sendrec_buf(&ssl->sc.eng, &cipher_buf_len);
            size_t bytes_sent = 0;

            if (cipher_buf_len > 0) {
                bytes_sent = send_to_socket(sock_id, sock, status, cipher_buf, cipher_buf_len,
                                            &stop);
                if (stop)
                    return;
                if (bytes_sent > 0)
                    sock->last_send = time(NULL);

                // If there's no bytes left in the SSL engine, clear the related flag
                if (cipher_buf_len == bytes_sent)
                    status->call &= ~NET_CALL_SEND_SSL;
            }
            br_ssl_engine_sendrec_ack(&ssl->sc.eng, bytes_sent);
        }
    } else {
        // Plain socket
        bool stop = false;
        sock_buf_consumed = send_to_socket(sock_id, sock, status, sock_buf, sock_buf_len, &stop);
        if (stop)
            return;
    }

    if (sock_buf_consumed > 0) {
        // Shift remaining bytes down
        sock->buf_out_live_bytes -= sock_buf_consumed;
        memmove(buf_out(sock_id), buf_out(sock_id) + sock_buf_consumed, sock->buf_out_live_bytes);
    }

    // If there's nothing left in the sock buffer, clear the call flag
    if (sock->buf_out_live_bytes == 0)
        status->call &= ~NET_CALL_SEND;
}


size_t recv_from_socket(uint32_t sock_id, struct net_sock* sock, struct net_sock_status* status,
                          uint8_t *buf, size_t buf_len, bool *stop)
{
    *stop = false;
    
    if (buf_len == 0)
        return 0;

    for (int try_num = 0; try_num < 2; try_num++) {
        int bytes_recvd = recv(sock->fd, buf, buf_len, MSG_DONTWAIT);
        if (bytes_recvd < 0) {
            if (errno == EINTR) {
                continue;
            } else if (errno == EAGAIN) {
                status->call &= ~NET_CALL_RECV;
                return 0;
            } else {
                alog(LOG_INFO, NET_ERR, "recv(): %s", strerror(errno));
                sock_close(sock_id);
                *stop = true;
                return 0;
            }
        } else if (bytes_recvd == 0) {
            // 0 bytes (and buf_len != 0) means the client disconnected
            sock_close(sock_id);
            alog(LOG_DEBUG, 0, "recv'd 0 bytes: closing");
            *stop = true;
            return 0;
        } else {
            return bytes_recvd;
        }
    }
    return 0;
}


void recv_into_net_sock(uint32_t sock_id, struct net_sock* sock, struct net_sock_status *status) {
    if (status->stype == NET_SOCK_TYPE_CLOSED)
        return;

    // Figure out how to address the sock buffer
    uint8_t *sock_buf = buf_in(sock_id) + sock->buf_in_live_bytes;
    uint16_t sock_buf_left = NET_BUF_SIZE_IN - sock->buf_in_live_bytes;
    uint16_t sock_buf_written = 0;

    // Not much productive we can do here if the sock buffer is full
    if (sock_buf_left == 0)
        return;

    struct net_sock_ssl *ssl = &(Ssls[sock_id]);
    if (ssl->active) {
        // SSL
        // Get bytes from the socket (if kqueue says we can)
        if (status->call & NET_CALL_RECV) {
            size_t buf_len = 0;
            uint8_t *buf = br_ssl_engine_recvrec_buf(&ssl->sc.eng, &buf_len);
            bool stop = false;
            size_t bytes_recvd = recv_from_socket(sock_id, sock, status, buf, buf_len, &stop);
            br_ssl_engine_recvrec_ack(&ssl->sc.eng, bytes_recvd);
            if (stop)
                return;
        }
        
        // See if there's any plaintext for us
        unsigned state = br_ssl_engine_current_state(&ssl->sc.eng);
        // - First, see if the above recvrec caused any errors with the SSL engine
        if (state & BR_SSL_CLOSED) {
            sock_close(sock_id);
            alog(LOG_DEBUG, 0, "SSL closed, last err: %d",
                 br_ssl_engine_last_error(&ssl->sc.eng));
            return;
        }

        // - Copy the cleartext buffer into the net_sock buffer (if there is some)
        if (state & BR_SSL_RECVAPP) {
            size_t clear_buf_len = 0;
            uint8_t *clear_buf = br_ssl_engine_recvapp_buf(&ssl->sc.eng, &clear_buf_len);
            size_t copy_len = (sock_buf_left < clear_buf_len) ? sock_buf_left : clear_buf_len;
            memcpy(sock_buf, clear_buf, copy_len);
            br_ssl_engine_recvapp_ack(&ssl->sc.eng, copy_len);

            // Set the flag if we didn't get everything, or clear it if we did
            if (sock_buf_left < clear_buf_len)
                status->call |= NET_CALL_RECV_SSL;
            else
                status->call &= ~NET_CALL_RECV_SSL;

            // Note that we've written bytes into the sock buffer
            sock_buf_written = copy_len;
        } else {
            // If there's nothing, clear the flag that says there is
            status->call &= ~NET_CALL_RECV_SSL;
        }

        // - Finally, the engine might have created some bytes it needs to send out - set status
        if (br_ssl_engine_current_state(&ssl->sc.eng) & BR_SSL_SENDREC)
            status->call |= NET_CALL_SEND_SSL;
    } else {
        // Plain socket
        bool stop = false;
        sock_buf_written = recv_from_socket(sock_id, sock, status, sock_buf, sock_buf_left, &stop);
        if (stop)
            return;
    }

    // If there's new bytes in the sock buffer, do some housekeeping
    if (sock_buf_written > 0) {
        sock->buf_in_live_bytes += sock_buf_written;
        status->event |= NET_EV_NEW_BYTES;
        sock->last_recv = time(NULL);
    }
}


int open_server_sock(char *ip, char *port) {
    // Set up config
    struct addrinfo *addr;
    struct addrinfo hints;
    bzero(&hints, sizeof(hints));
    hints.ai_family = (strstr(ip, ":") == NULL) ? AF_INET : AF_INET6;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    // Do the socket() -> bind() dance
    int error = getaddrinfo(ip, port, &hints, &addr);
    if (error != 0)
        return alog(LOG_FATAL, NET_FATAL, "getaddrinfo(), port %s: %s", port, gai_strerror(error));

    int server_sock = -1;
    for (struct addrinfo *ai = addr; ai != NULL; ai = ai->ai_next) {
        server_sock = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        if (server_sock < 0)
            continue;
        int opt = 1;
        if (setsockopt(server_sock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt)) < 0)
            alog(LOG_ERR, 0, "setsockopt(), port %s: %s", port, strerror(errno));
        if (bind(server_sock, ai->ai_addr, ai->ai_addrlen) < 0)
            return alog(LOG_FATAL, NET_FATAL, "bind(), port %s: %s", port, strerror(errno));

    }
    if (server_sock < 0)
        return alog(LOG_FATAL, NET_FATAL, "socket(), port %s: %s", port, strerror(errno));

    // Free some memory
    freeaddrinfo(addr);

    // Set to non-block and listen()
    int existing_flags = fcntl(server_sock, F_GETFL, 0);
    if (existing_flags < 0)
        alog(LOG_ERR, 0, "fcntl(F_GETFL), port %s: %s", port, strerror(errno));
    if (fcntl(server_sock, F_SETFL, O_NONBLOCK | existing_flags) < 0)
        alog(LOG_ERR, 0, "fcntl(F_SETFL), port %s: %s", port, strerror(errno));

    if (listen(server_sock, SOMAXCONN) < 0)
        return alog(LOG_FATAL, NET_FATAL, "listen(), port %s: %s", port, strerror(errno));


    // Add to kqueue
    struct kevent kev = (struct kevent) { server_sock, EVFILT_READ, EV_ADD, 0, 0, NULL };
    kevent(Kq, &kev, 1, NULL, 0, NULL);

    return server_sock;
}


int32_t drop_privileges(char *unix_user, char *unix_group) {
    // Get the passwd struct
    struct passwd pw;
    struct passwd *pw_ptr;
    char pw_buf[4906];

    int getpwnam_res = getpwnam_r(unix_user, &pw, pw_buf, sizeof(pw_buf), &pw_ptr);
    if (getpwnam_res != 0 || pw_ptr != &pw)
        return alog(LOG_FATAL, NET_FATAL, "problem getting UID/GID for %s: %s", unix_user,
                    strerror(getpwnam_res));
    
    // Change our UID/GID (note: the ordering is critical)
    if (setgid(pw.pw_gid) == -1) {
        return alog(LOG_FATAL, NET_FATAL, "could not setgid() to %s(%d): %s", unix_group, pw.pw_gid,
                    strerror(errno));
    }
    if (setuid(pw.pw_uid) == -1) {
        return alog(LOG_FATAL, NET_FATAL, "could not setuid() to %s(%d): %s", unix_user, pw.pw_uid,
                    strerror(errno));
    }

    return alog(LOG_INFO, NET_OK, "switched to user %s(%d), group %s(%d)", unix_user, pw.pw_uid,
                unix_group, pw.pw_gid);
}

void sigint_handler() {
    printf("\n");
    net_fini();
    exit(0);
}


int32_t net_init(uint32_t num_socks, uint32_t session_cache_size, char *ip_edsu,
                 char *port_edsu, char *ip_ws, char *port_ws, char *unix_user, char *unix_group,
                 struct net_sock *socks, struct net_sock_status *statuses,
                 uint8_t *bufs_in, uint8_t *bufs_out)
{
    NumSocks = num_socks;
    Socks = socks;
    Statuses = statuses;
    BufsIn = bufs_in;
    BufsOut = bufs_out;

    // Sanity checks
    if (sizeof(br_skey_decoder_context) > NET_CERT_DECODE_STRUCT_LEN)
        return alog(LOG_FATAL, NET_FATAL, "failed net_init sanity check 0");
    if (sizeof(br_x509_certificate) != sizeof(uint64_t) * 2)
        return alog(LOG_FATAL, NET_FATAL, "failed net_init sanity check 1");

    // Disable the signal for when we accidentally write to a dropped tcp/ip connection
    signal(SIGPIPE, SIG_IGN);

    // Catch ctrl-c to make sure we release the sockets if we're coding
    signal(SIGINT, sigint_handler);

    // Init kqueue stuff
    Kq = kqueue();
    if (Kq < 0)
        return alog(LOG_FATAL, NET_FATAL, "kqueue(): %s", strerror(errno));
    
    // Start listening for sockets
    SSockEdsu = open_server_sock(ip_edsu, port_edsu);
    if (SSockEdsu < 0)
        return SSockEdsu;
    SSockWs = open_server_sock(ip_ws, port_ws);
    if (SSockWs < 0)
        return SSockWs;

    // We don't need root to bind to low ports anymore
    int32_t drop_priv_res = drop_privileges(unix_user, unix_group);
    if (drop_priv_res != NET_OK) {
        return drop_priv_res;
    }

    // Set up the rest of the heap (these should be the only non-stack allocations in net.c, aside
    // from the heap allocations when setting new certs (i.e. not client accessible))
    Ssls = (struct net_sock_ssl *)calloc(NumSocks, sizeof(struct net_sock_ssl));
    KEventsSize = NumSocks * 2 + NUM_SERVER_SOCKS;
    KEvents = (struct kevent *)calloc(KEventsSize, sizeof(struct kevent));
    uint8_t *session_cache_mem = calloc(1, session_cache_size);
    br_ssl_session_cache_lru_init(&SessionCache, session_cache_mem, session_cache_size);

    return alog(LOG_INFO, NET_OK, "net init successful");
}



int32_t net_set_cert_table(struct net_cert_item *cert_table, uint32_t cert_table_len,
                           uint8_t *cert_heap, uint32_t cert_heap_len)
{
    // The callbacks from BearSSL to get the certs and the private key are a tricky situation -
    // if the cert table and heap are freed, there there's a concern when the pointers to them
    // will be at re-allocated memory, and given that certs are sent straight over the wire
    // (making the conservative assumption that they won't always be validated), it's a worry.
    // True, they should only be used within a certain time period (/if/ re-negotation is
    // disabled), but then security is dependant on timing.  So we keep the memory around in
    // generations till there are no more socks using it

    // Find a free slot in the generation table - this wastes the first slot in the interests
    // of simplicity (sock structs get initialized with memset())
    uint32_t table_gen = 0;
    for (uint32_t gen = 1; gen < MAX_CERT_TABLE_GENERATIONS; gen++) {
        if (CertTables[gen] == NULL) {
            // Never been used
            table_gen = gen;
            break;
        }
        // Has been used - is it still in use?
        bool found = false;
        for (uint32_t sock_id = 0; sock_id < NumSocks; sock_id++) {
            if (Socks[sock_id].cert_generation == gen) {
                found = true;
                break;
            }
        }
        if (!found) {
            table_gen = gen;
            break;
        }
    }

    // If there are no unused slots left, that's extremely unlikely - restart the server by dying
    if (table_gen == 0)
        return alog(LOG_FATAL, NET_FATAL, "ran out of cert table generations");

    // We got a slot - free the memory of the old one (if used)
    if (CertTables[table_gen] != NULL)
        free(CertTables[table_gen]);
    if (CertHeaps[table_gen] != NULL)
        free(CertHeaps[table_gen]);
 
    // Allocate and copy the new table/heap
    CurCertTableLen = cert_table_len;
    size_t table_size = sizeof(struct net_cert_item) * cert_table_len;
    CertTables[table_gen] = (struct net_cert_item *)malloc(table_size);
    memcpy(CertTables[table_gen], cert_table, table_size);
    
    CertHeaps[table_gen] = malloc(cert_heap_len);
    memcpy(CertHeaps[table_gen], cert_heap, cert_heap_len);

    // Set the global for the current generation
    CurCertTableGeneration = table_gen;

    // Alloc and copy in the new ones
    // Set up some data structures
    ssl_init_cert_table();
    return NET_OK;
}



int32_t net_report_consumption(uint32_t sock_id, uint16_t num_bytes) {
    // Paranoia
    if (sock_id >= NumSocks || Statuses[sock_id].stype == NET_SOCK_TYPE_CLOSED)
        return alog(LOG_FATAL, NET_FATAL,
                    "net_report_consumption(): bad sock ID: %d", sock_id);

    struct net_sock *sock = &(Socks[sock_id]);
    if (num_bytes > sock->buf_in_live_bytes)
        return alog(LOG_FATAL, NET_FATAL, "tried to consume non-existant bytes");

    // Shift all the bytes down by num_bytes
    sock->buf_in_live_bytes -= num_bytes;
    memmove(buf_in(sock_id), buf_in(sock_id) + num_bytes, sock->buf_in_live_bytes);
    return NET_OK;
}


int32_t net_write(uint32_t sock_id, uint8_t *bytes, uint16_t num_bytes) {
    // Paranoia
    if (sock_id >= NumSocks || Statuses[sock_id].stype == NET_SOCK_TYPE_CLOSED)
        return alog(LOG_FATAL, NET_FATAL, "net_write(): bad sock ID: %d", sock_id);

    struct net_sock *sock = &(Socks[sock_id]);

    bool was_empty = (sock->buf_out_live_bytes == 0);
    if (NET_BUF_SIZE_OUT - sock->buf_out_live_bytes < num_bytes)
        return NET_BUF_WOULD_OFLOW;

    // Copy in
    memcpy(buf_out(sock_id) + sock->buf_out_live_bytes, bytes, num_bytes);
    sock->buf_out_live_bytes += num_bytes;

    // If the buffer was empty, it probably wasn't already waiting for a write event, so
    // kick it off
    if (was_empty)
        Statuses[sock_id].call |= NET_CALL_SEND;
    return NET_OK;
}

uint64_t now_in_us() {
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME_FAST, &ts);
    return ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
}


int32_t net_select(uint32_t tick_us) {
    // Reset the event state of the socks, and honour any close requests
    for (uint32_t i = 0; i < NumSocks; i++) {
        if ((Statuses[i].event & NET_EV_RUST_CLOSE) != 0) {
            alog(LOG_DEBUG, 0, "closing sock as requested by Rust side: %d", i);
            sock_close(i);
        }
        Statuses[i].event = 0;
    }

    uint64_t done_time_us = now_in_us() + tick_us;   

    // Loop until we've gone a full tick
    for (uint64_t now = now_in_us(); now < done_time_us; now = now_in_us()) {
        // Get the new kevents
        struct timespec ts = { .tv_sec = 0, .tv_nsec = (done_time_us - now) * 1000 };
        int num_kevents = kevent(Kq, NULL, 0, KEvents, KEventsSize, &ts);
        if (num_kevents < 0) {
            // Got an error
            if (errno == EINTR)
                return alog(LOG_INFO, NET_OK, "EINTR during kevent() select, will try again");
            else 
                return alog(LOG_FATAL, NET_FATAL, "kevent() select: %s", strerror(errno));
        }

        // Loop through kevents, updating the socks
        for (int kev_num = 0; kev_num < num_kevents; kev_num++) {
            struct kevent *kev = &(KEvents[kev_num]);

            if ((int)kev->ident == SSockEdsu || (int)kev->ident == SSockWs) {
                // Server socket
                if (kev->flags & EV_EOF) {
                    return alog(LOG_FATAL, NET_FATAL, "server socket died: %s",
                                strerror(errno));
                } else if (kev->filter == EVFILT_READ) {
                    // New socket
                    // Find a free sock
                    struct net_sock *sock = NULL;
                    struct net_sock_status *status = NULL;
                    size_t sock_id = NumSocks;
                    for (uint32_t i = 0; i < NumSocks; i++) {
                        if (Statuses[i].stype == NET_SOCK_TYPE_CLOSED) {
                            sock_id = i;
                            sock = &(Socks[sock_id]);
                            status = &(Statuses[sock_id]);
                            break;
                        }
                    }
                    if (sock == NULL) {
                        //alog(LOG_INFO, 0, "no room for socket - backlogging");
                        // TODO: Fills the log with noise - going to change this behaviour
                        continue;
                    }

                    // Accept the client socket
                    struct sockaddr sock_addr;
                    socklen_t sock_len = sizeof(sock_addr);
                    for (;;) {
                        int fd = accept(kev->ident, &sock_addr, &sock_len);
                        if (fd < 0) {
                            if (errno == EINTR) {
                                continue;
                            } if (errno == EWOULDBLOCK || errno == EAGAIN) {
                                break;
                            } else {
                                alog(LOG_ERR, 0, "failed to accept() socket: %s", strerror(errno));
                                break;
                            }
                        }

                        // Init the sock
                        sock->fd = (uint32_t)fd;
                        status->event = NET_EV_NEW_SOCK;
                        status->stype = ((int)kev->ident == SSockEdsu) ? NET_SOCK_TYPE_EDSU :
                                                                         NET_SOCK_TYPE_WS;
                        if (sock_addr.sa_family == AF_INET) {
                            sock->sock_4_not_6 = true;
                            uint32_t ip = ((struct sockaddr_in *)(&sock_addr))->sin_addr.s_addr;
                            sock->sock_ip[0] = ip >> 24;
                            sock->sock_ip[1] = (ip >> 16) & 0xff;
                            sock->sock_ip[2] = (ip >> 8) & 0xff;
                            sock->sock_ip[3] = ip & 0xff;
                        } else if (sock_addr.sa_family == AF_INET6) {
                            sock->sock_4_not_6 = false;
                            uint8_t *ip = ((struct sockaddr_in6 *)(&sock_addr))->sin6_addr.s6_addr;
                            memcpy(sock->sock_ip, ip, 16);
                        } else {
                            alog(LOG_ERR, 0, "sock not IPv4 or IPv6, closing");
                            sock_close(sock_id);
                        }
                        ssl_init(sock_id);

                        // Add to kqueue (adding 1 to sock_id to distinguish from NULL)
                        // Calling close() on the sock will automatically remove these
                        struct kevent kev_rd = { .ident = fd, .filter = EVFILT_READ,
                                                 .flags = EV_ADD | EV_CLEAR, .fflags = 0,
                                                 .data = 0, .udata = (void *)(sock_id+1) };
                        kevent(Kq, &kev_rd, 1, NULL, 0, NULL);
                        struct kevent kev_wr = { .ident = fd, .filter = EVFILT_WRITE,
                                                 .flags = EV_ADD | EV_CLEAR, .fflags = 0,
                                                 .data = 0, .udata = (void *)(sock_id+1) };
                        kevent(Kq, &kev_wr, 1, NULL, 0, NULL);
                        alog(LOG_DEBUG, 0, "Socket accepted: %d", sock_id);
                    }
                } else {
                    alog(LOG_ERR, 0, "unknown kevent on ssock: %d %d", kev->filter, kev->flags);
                }
            } else if (kev->udata != NULL) {
                // A sock's kevent
                uint32_t sock_id = ((size_t)kev->udata)-1; 
                struct net_sock_status *status = &(Statuses[sock_id]);
                if (kev->flags & EV_EOF) {
                    alog(LOG_DEBUG, 0, "sock closed from kevent: %d", sock_id);
                    // Client socket closed
                    sock_close(sock_id);
                } else if (kev->filter == EVFILT_READ) {
                    //alog(LOG_DEBUG, 0, "sock read event");
                    status->call |= NET_CALL_RECV;
                } else if (kev->filter == EVFILT_WRITE) {
                    //alog(LOG_DEBUG, 0, "sock write event");
                    status->call |= NET_CALL_SEND;
                } else {
                    alog(LOG_ERR, 0, "unknown sock kevent!");
                }
            } else if (kev->flags & EV_ERROR) {
                // Kqueue can put errors into the queue
                if (kev->data == EINTR)
                    alog(LOG_INFO, 0, "caught EINTR during kevent() select, will try again");
                else 
                    return alog(LOG_FATAL, NET_FATAL, "kevent() select (kev->data): %s",
                                strerror(kev->data));
            } else {
                alog(LOG_ERR, 0, "unknown kevent: %d %d", kev->filter, kev->flags);
            }
        }

        // Working on the model that the only reliable way to tell if a non-blocking socket can't
        // r/w is to get an EAGAIN, I'm using kqueue in edge-triggered mode and keeping positive
        // state until I get one
        for (uint32_t sock_id = 0; sock_id < NumSocks; sock_id++) {
            struct net_sock *sock = &(Socks[sock_id]);
            struct net_sock_status *status = &(Statuses[sock_id]);
            if (status->call & NET_CALL_SEND || status->call & NET_CALL_SEND_SSL)
                send_from_net_sock(sock_id, sock, status);
            if (status->call & NET_CALL_RECV || status->call & NET_CALL_RECV_SSL)
                recv_into_net_sock(sock_id, sock, status);
        }
    }

    return NET_OK;
}

// Distinct from Rust's notion of a hard close
void sock_close_hard(int sock_num) {
    struct linger so_linger;
    so_linger.l_onoff = 1;
    so_linger.l_linger = 0;
    setsockopt(sock_num, SOL_SOCKET, SO_LINGER, &so_linger, sizeof(struct linger));
    close(sock_num);
}

void net_fini() {
    for (uint32_t sock_num = 0; sock_num < NumSocks; sock_num++) {
        if (Socks[sock_num].fd > 0)
            sock_close_hard(sock_num);
    }

    if (SSockEdsu >= 0) {
        sock_close_hard(SSockEdsu);
        SSockEdsu = -1;
    }
    if (SSockWs >= 0) {
        sock_close_hard(SSockWs);
        SSockWs = -1;
    }
    printf("sockets closed\n");
}
