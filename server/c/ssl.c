#include <assert.h>


struct net_cert_item *cert_search(struct net_cert_item *table, uint32_t table_len, const char* common_name) {
    if (table_len == 0)
        return NULL;
    
    uint32_t index = table_len >> 1;
    struct net_cert_item *item = &table[index];
    int32_t ord = strncmp(item->common_name, common_name, NET_MAX_COMMON_NAME_LEN);
    if (ord == 0) {
        return item;
    } else if (ord > 0) {
        return cert_search(table, index, common_name);
    } else {
        return cert_search(&table[index+1], table_len - index - 1, common_name);
    }
}

struct net_cert_item *cert_exhaustive_search(const char* common_name) {
    // - Lowercase the common name, and while doing that find the first dot
    char to_search[NET_MAX_COMMON_NAME_LEN];
    int32_t first_dot = 0;
    for (uint32_t i = 0; i < NET_MAX_COMMON_NAME_LEN; i++) {
        char ch = common_name[i];
        if (ch >= 'A' && ch <= 'Z') {
            to_search[i] = ch + 0x20;
        } else {
            to_search[i] = ch;
            if (ch == '.' && first_dot == 0)
                first_dot = i;
        }
        if (ch == '\0')
            break;
    }
    // Paranoia
    to_search[NET_MAX_COMMON_NAME_LEN-1] = '\0';

    // Try the whole common name first, then try as a wildcard
    struct net_cert_item *table = CertTables[CurCertTableGeneration];
    uint32_t table_len = CurCertTableLen;

    struct net_cert_item *item = cert_search(table, table_len, to_search);
    if (item == NULL)
        item = cert_search(table, table_len, &(to_search[first_dot]));
    return item;
}


void ssl_init_cert_table() {
    for (uint32_t item_num = 0; item_num < CurCertTableLen; item_num++) {
        struct net_cert_item *table = CertTables[CurCertTableGeneration];
        uint8_t *heap = CertHeaps[CurCertTableGeneration];
        struct net_cert_item *item = &(table[item_num]);
        item->valid = false;

        // Parse the DER-encoded private key
        br_skey_decoder_context *decoder = (br_skey_decoder_context*)(item->decode_struct_mem);
        br_skey_decoder_init(decoder);
        br_skey_decoder_push(decoder, &(heap[item->key_idx]), item->key_len);

        int decode_result = br_skey_decoder_last_error(decoder);
        if (decode_result != 0) {
            alog(LOG_ERR, 0, "decoding key for '%s'", item->common_name);
            continue;
        }

        if (br_skey_decoder_key_type(decoder) != BR_KEYTYPE_RSA) {
            alog(LOG_ERR, 0, "only RSA keys currently supported: '%s'", item->common_name);
            continue;
        }

        // Fix up some pointers to DER-encoded certs
        for (uint32_t cert_num = 0; cert_num < item->num_certs; cert_num++) {
            br_x509_certificate *cert = (br_x509_certificate *)&(item->chain_array[cert_num*2]);
            // Convert an index into the heap into a pointer into the heap
            cert->data = &(heap[(uint64_t)cert->data]);
        }
        item->valid = true;
    }
}


int ssl_choose(const br_ssl_server_policy_class **pctx, const br_ssl_server_context *sc,
               br_ssl_server_choices *choices)
{
    // Based on ssl/ssl_scert_single_rsa.c in the BearSSL source, 2018-04-23

    // Get the sock ID et al (see ssl_init_profile() about this pc->chain_len abuse)
    br_ssl_server_policy_rsa_context *pc = (br_ssl_server_policy_rsa_context *)pctx;
    uint32_t sock_id = pc->chain_len;
    struct net_sock_status *status = &(Statuses[sock_id]);

    // Set the key and chain based on the SNI common name
    const char *common_name = br_ssl_engine_get_server_name(&sc->eng);
    if (common_name == NULL)
        return alog(LOG_INFO, 0, "client connected with empty SNI common name");

    struct net_cert_item *cert_item = cert_exhaustive_search(common_name);
    if (cert_item == NULL)
        return alog(LOG_INFO, 0, "unknown common name: %s", common_name);

    // Make sure it's valid (i.e. it parsed OK)
    if (!cert_item->valid)
        return alog(LOG_INFO, 0, "invalid cert: %s", common_name);

    // Let the Rust side know it can proceed
    status->event |= NET_EV_SOCK_HANDSHAKE_DONE;

    // Set the cert generation, so we don't ever point to reallocated memory
    Socks[sock_id].cert_generation = CurCertTableGeneration;
    
    // Set the chain
    choices->chain = (br_x509_certificate *)cert_item->chain_array;
    choices->chain_len = cert_item->num_certs;

    // Set the key
    pc->sk = br_skey_decoder_get_rsa((br_skey_decoder_context*)(cert_item->decode_struct_mem));

    // Choose hash
    static const uint8_t pref[] = { br_sha256_ID, br_sha384_ID, br_sha512_ID, br_sha224_ID };
    uint32_t hash_id = 0;
    uint32_t client_hashes = br_ssl_server_get_client_hashes(sc) >> 8;
    for (uint32_t i = 0; i < sizeof pref; i++) {
        uint32_t hash_candidate = pref[i];
        if ((client_hashes >> hash_candidate) & 1) {
            hash_id = hash_candidate;
            break;
        }
    }

    // Chose cipher (and algo in the case of ECDHE_RSA)
    size_t suites_len;
    const br_suite_translated *suites = br_ssl_server_get_client_suites(sc, &suites_len);
    for (size_t i = 0; i < suites_len; i++) {
        switch (suites[i][1] >> 12) {
            case BR_SSLKEYX_RSA:
                choices->cipher_suite = suites[i][0];
                return 1;
            case BR_SSLKEYX_ECDHE_RSA:
                if (hash_id != 0) {
                    choices->cipher_suite = suites[i][0];
                    choices->algo_id = hash_id + 0xFF00;
                    return 1;
                }
        }
    }

    // Didn't find a match between us and the client
    return 0;
}

uint32_t ssl_do_keyx(const br_ssl_server_policy_class **pctx, unsigned char *data, size_t *len) {
    // Based on ssl/ssl_scert_single_rsa.c in the BearSSL source, 2018-04-23
    br_ssl_server_policy_rsa_context *pc = (br_ssl_server_policy_rsa_context *)pctx;
    return br_rsa_ssl_decrypt(pc->irsacore, pc->sk, data, *len);
}


static const unsigned char HASH_OID_SHA256[] = {
    0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01
};
static const unsigned char HASH_OID_SHA384[] = {
    0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x02
};
static const unsigned char HASH_OID_SHA512[] = {
    0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x03
};
static const unsigned char *HASH_OID[] = {
    NULL, // HASH_OID_SHA1,
    NULL, // HASH_OID_SHA224,
    HASH_OID_SHA256,
    HASH_OID_SHA384,
    HASH_OID_SHA512
};

size_t ssl_do_sign(const br_ssl_server_policy_class **pctx, unsigned algo_id, unsigned char *data,
                   size_t hv_len, size_t len)
{
    // Based on ssl/ssl_scert_single_rsa.c in the BearSSL source, 2018-04-23
    unsigned char hv[64];
    size_t sig_len;
    const unsigned char *hash_oid;

    br_ssl_server_policy_rsa_context *pc = (br_ssl_server_policy_rsa_context *)pctx;
    memcpy(hv, data, hv_len);
    algo_id &= 0xFF;
    if (algo_id == 0) {
        hash_oid = NULL;
    } else if (algo_id >= 2 && algo_id <= 6) {
        hash_oid = HASH_OID[algo_id - 2];
    } else {
        return 0;
    }
    sig_len = (pc->sk->n_bitlen + 7) >> 3;
    if (len < sig_len) {
        return 0;
    }
    return pc->irsasign(hash_oid, hv, hv_len, pc->sk, data) ? sig_len : 0;
}

static const br_ssl_server_policy_class ssl_policy_vtable = {
    sizeof(br_ssl_server_policy_rsa_context),
    ssl_choose,
    ssl_do_keyx,
    ssl_do_sign
};


void ssl_init_profile(br_ssl_server_context *sc, uint32_t sock_id) {
    // Reset the server context
	br_ssl_server_zero(sc);

    // Which versions of SSL we support (just TLS 1.2)
	br_ssl_engine_set_versions(&sc->eng, BR_TLS12, BR_TLS12);

    // Cipher suites (in order of preference)
	static const uint16_t suites[] = {
        BR_TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
        BR_TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
        BR_TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
        BR_TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,
        BR_TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,
        BR_TLS_RSA_WITH_AES_128_GCM_SHA256,
        BR_TLS_RSA_WITH_AES_256_GCM_SHA384,
        BR_TLS_RSA_WITH_AES_128_CBC_SHA256,
        BR_TLS_RSA_WITH_AES_256_CBC_SHA256,
	};
	br_ssl_engine_set_suites(&sc->eng, suites, (sizeof suites) / (sizeof suites[0]));

    // Set the EC implementation (for ECDHE; we use the built-in)
	br_ssl_engine_set_default_ec(&sc->eng);

    // Server policy - borrowing the single_ec struct to store some things, but not actually
    // using that handler
    sc->chain_handler.single_rsa.vtable = &ssl_policy_vtable;
    sc->chain_handler.single_rsa.irsacore = br_rsa_private_get_default();
    sc->chain_handler.single_rsa.irsasign = br_rsa_pkcs1_sign_get_default();
    sc->policy_vtable = &sc->chain_handler.single_rsa.vtable;

    // There doesn't appear to be a way to create a custom chain_handler type, so I'm abusing
    // the RSA one
    sc->chain_handler.single_rsa.chain_len = sock_id;

    // Supported hashes
	static const br_hash_class *hashes[] = {
		NULL, // &br_md5_vtable,
		NULL, // &br_sha1_vtable,
		NULL, // &br_sha224_vtable,
		&br_sha256_vtable,
		&br_sha384_vtable,
		&br_sha512_vtable
	};
	for (int id = br_md5_ID; id <= br_sha512_ID; id ++) {
		const br_hash_class *hc;
		hc = hashes[id - 1];
		br_ssl_engine_set_hash(&sc->eng, id, hc);
	}

    // PRF implementations
    br_ssl_engine_set_prf10(&sc->eng, &br_tls10_prf);
	br_ssl_engine_set_prf_sha256(&sc->eng, &br_tls12_sha256_prf);
	br_ssl_engine_set_prf_sha384(&sc->eng, &br_tls12_sha384_prf);

	// Supported symmetric ciphers
	br_ssl_engine_set_default_aes_cbc(&sc->eng);
	br_ssl_engine_set_default_aes_gcm(&sc->eng);
	br_ssl_engine_set_default_des_cbc(&sc->eng);
	br_ssl_engine_set_default_chapol(&sc->eng);
}

void ssl_init(uint32_t sock_id) {
    struct net_sock_ssl *ssl = &(Ssls[sock_id]);
    ssl_init_profile(&ssl->sc, sock_id);
    br_ssl_engine_set_buffer(&ssl->sc.eng, &ssl->buffer, SSL_BUFFER_SIZE, 1);
    br_ssl_server_reset(&ssl->sc);
    br_ssl_server_set_cache(&ssl->sc, &SessionCache.vtable);
    ssl->active = true;
}

