#include <stdint.h>
#include <sys/file.h>
#include <unistd.h>
// Globals

int32_t alog(int level, int32_t ret, char *format, ...);

#define AUX_RET_OK               0
#define AUX_RET_ERR             -1
#define AUX_RET_FATAL           -2

int32_t lock_file(const char *path) {
    int fd = open(path, O_RDWR|O_CREAT, 0600);
    if (fd < 0)
        return AUX_RET_FATAL;
    if (flock(fd, LOCK_EX | LOCK_NB) < 0) {
        close(fd);
        return AUX_RET_FATAL;
    }
    return AUX_RET_OK;
}


// Bring in broken-out code
#include "logging.c"
#include "net.c"

