#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/*

This is in C because:

* Logging is needed by both sides
* I haven't been able to figure out a thread-safe way to share a mutable
  global (e.g. a channel Send) between threads.  The way that the logging 
  crate does is not safe, IMO - there's a difference between two threads
  using clone()ed results of one Mutex the same time and two threads using
  the *same* Mutex to clone itself, and anything you put in a static mut
  is going to be a variation on the latter (as best I can figure)

*/


/////////////////////////
// Synced with Rust

#define LOG_RET_OK               0
#define LOG_RET_FATAL           -2
#define LOG_DEBUG               0x01
#define LOG_INFO                0x02
#define LOG_ERR                 0x03
#define LOG_ALERT               0x04
#define LOG_ALARM               0x05
#define LOG_FATAL               0x06
int32_t put_log(uint32_t level, uint8_t *text);
int32_t get_log(uint8_t *buffer, uint32_t buffer_len);

/////////////////////////

#define LOG_MAX_LEN (4096-64)  // Leaving room to fit in page with struct

static struct Log *Head = NULL;
static struct Log *Tail = NULL;
static pthread_mutex_t LogLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t LogCond = PTHREAD_COND_INITIALIZER;

struct Log {
    struct Log *next;
    char log[LOG_MAX_LEN];
};


int32_t put_log(uint32_t level, uint8_t *text) {
    // Alloc a Log
    struct Log* log = (struct Log*)malloc(sizeof(struct Log));
    if (log == NULL)
        return LOG_RET_FATAL;
    log->next = NULL;

    // Copy everything in
    // - Write time
    time_t t = time(NULL);
    struct tm* tm_info = localtime(&t);
    size_t t_len = strftime(log->log, LOG_MAX_LEN-1, "%Y-%m-%d %H:%M:%S %Z", tm_info);
    
    // - Convert level to text
    char *level_text = NULL;
    switch (level) {
        case LOG_DEBUG: level_text = "Debug"; break;
        case LOG_INFO: level_text = "Info"; break;
        case LOG_ERR: level_text = "Err"; break;
        case LOG_ALERT: level_text = "Alert"; break;
        case LOG_ALARM: level_text = "Alarm"; break;
        case LOG_FATAL: level_text = "Fatal"; break;
    }
    if (level_text == NULL)
        return LOG_RET_FATAL;

    // - Copy level and text in (note: snprintf always null-terminates)
    snprintf(log->log + t_len, LOG_MAX_LEN - t_len, " [%s] %s", level_text, text);

    // Add to linked list
    if (pthread_mutex_lock(&LogLock) != 0) return LOG_RET_FATAL;
        if (Head == NULL) {
            // Empty list
            Head = log;
            Tail = log;
        } else {
            if (Tail == NULL) {
                // This shouldn't happen
                snprintf(log->log, LOG_MAX_LEN, "!!! ERROR: logging has been corrupted");
                Head = log;
                Tail = log;
            } else {
                Tail->next = log;
                Tail = log;
            }
        }

        // If a get_log() is waiting, wake it up
        pthread_cond_signal(&LogCond);
    if (pthread_mutex_unlock(&LogLock) != 0) return LOG_RET_FATAL;

    return LOG_RET_OK;
}


int32_t get_log(uint8_t *buffer, uint32_t buffer_len) {
    // Lock and try to grab a Log off the linked list
    struct Log *log = NULL;
    if (pthread_mutex_lock(&LogLock) != 0) return LOG_RET_FATAL;
        // If there isn't anything to log, wait
        if (Head == NULL && pthread_cond_wait(&LogCond, &LogLock) != 0) {
            pthread_mutex_unlock(&LogLock);
            return LOG_RET_FATAL;
        }

        // If the signal indicates a Log (not a guarentee) grab it off the LL
        if (Head != NULL) {
            log = Head;
            Head = log->next;
        }
    if (pthread_mutex_unlock(&LogLock) != 0) return LOG_RET_FATAL;

    // Either zero the buffer to indicate nothing yet (recursing might blow the stack)
    // or copy the current Log in
    if (log == NULL) {
        if (buffer_len > 0) // Paranoia
            buffer[0] = '\0';
    } else {
        // OK, we've got a log, write it into the buffer provided
        size_t log_len = strnlen(log->log, LOG_MAX_LEN);
        int copy_len = (log_len < buffer_len-1) ? log_len : buffer_len-1;
        strncpy((char *)buffer, log->log, copy_len);
        // - strncpy doesn't always terminate :(
        buffer[copy_len] = '\0';

        // Deallocate Log
        free(log);
    }
    return LOG_RET_OK;
}



int32_t alog(int level, int32_t ret, char *format, ...) {
    char buffer[LOG_MAX_LEN];

    int used = snprintf(buffer, LOG_MAX_LEN, "aux.c - ");
    
    va_list args;
    va_start(args, format);
    vsnprintf(buffer+used, LOG_MAX_LEN-used, format, args);
    va_end(args);

    put_log(level, (uint8_t*)buffer);

    return ret;
}

