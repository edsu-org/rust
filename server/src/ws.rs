use crate::logging;
use base64;
use ring;
use std::mem;

const MAGIC_GUID: &[u8] = b"258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
const KEY_HTTP_KEY: &[u8] = b"Sec-WebSocket-Key:";
const KEY_HTTP_KEY_LC: &[u8] = b"sec-websocket-key:";
const VERS_HTTP_KEY: &[u8] = b"Sec-WebSocket-Version:";
const VERS_HTTP_KEY_LC: &[u8] = b"sec-websocket-version:";
const HTTP_RESP_HEAD: &[u8] = b"HTTP/1.1 101 Switching Protocols\r\n\
                                        Connection: Upgrade\r\n\
                                        Upgrade: websocket\r\n\
                                        Sec-WebSocket-Accept: ";
const HTTP_VERS_RESP: &[u8] = b"HTTP/1.1 400 Bad Request\r\n\
                                        Sec-WebSocket-Version: 13\r\n\
                                        Connection: close\r\n\
                                        Content-Length: 0\r\n";
const HTTP_BAD_REQ_RESP: &[u8] = b"HTTP/1.1 400 Bad Request\r\n\
                                           Connection: close\r\n\
                                           Content-Length: 0\r\n";
const HTTP_PAGE_NOT_FOUND_RESP: &[u8] = b"HTTP/1.1 404 Not Found\r\n\
                                                  Connection: close\r\n\
                                                  Content-Length: 15\r\n\r\n\
                                                  Page not found!";
const MIN_HEADER_BYTES: usize = 6; // First two bytes + mask
const OPCODE_BIN: u8 = 0x2;
const OPCODE_CLOSE: u8 = 0x8;
const OPCODE_PING: u8 = 0x9;
const OPCODE_PONG: u8 = 0xa;

// TODO: If/when I move to split buffers, this needs to be changed to having iter's (instead of
//       slices) as inputs

#[derive(Debug)]
pub enum Error {
    Http(&'static [u8]),
    HttpWithClose(&'static [u8]),
}

#[derive(Clone)]
pub enum ParserState {
    Header(usize),
    Payload(u64, [u8; 4], usize, bool),
}

impl Default for ParserState {
    fn default() -> Self {
        ParserState::Header(MIN_HEADER_BYTES)
    }
}

pub enum ParserResp {
    Close,
    Ping(Vec<u8>),
}

pub struct ParserIter<'a> {
    bytes: &'a [u8],
    state: ParserState,
    consumed: usize,
    resp: Option<ParserResp>,
}

impl<'a> ParserIter<'a> {
    pub fn new(state: ParserState, bytes: &'a [u8]) -> Self {
        ParserIter {
            bytes,
            state,
            consumed: 0,
            resp: None,
        }
    }
    pub fn decompose(self) -> (usize, Option<ParserResp>, ParserState) {
        (self.consumed, self.resp, self.state)
    }
}

impl<'a> Iterator for ParserIter<'a> {
    type Item = u8;
    fn next(&mut self) -> Option<u8> {
        // Return immediately if closed (it might have a payload which we don't care about)
        if let Some(ParserResp::Close) = self.resp {
            return None;
        }

        loop {
            match self.state.clone() {
                ParserState::Header(num_bytes_needed) => {
                    let header_bytes = &self.bytes[self.consumed..];
                    if header_bytes.len() < num_bytes_needed {
                        return None;
                    }

                    if header_bytes[1] & 0b1000_0000 == 0 {
                        err!("got an unmasked WS packet: 0b{:b}", header_bytes[1]);
                    }

                    // Parse header
                    let len_byte = header_bytes[1] & 0b0111_1111;
                    let mut ping = false;

                    // First, check to see if it's a ping or a close
                    let opcode = header_bytes[0] & 0b0000_1111;
                    if opcode == OPCODE_CLOSE {
                        self.resp = Some(ParserResp::Close);
                        return None;
                    } else if opcode == OPCODE_PING {
                        if len_byte > 125 {
                            self.resp = Some(ParserResp::Close);
                            return None;
                        } else {
                            let v = Vec::with_capacity(len_byte as usize);
                            self.resp = Some(ParserResp::Ping(v));
                            ping = true;
                        }
                    }

                    // Get the length
                    if len_byte == 127 {
                        // 8 byte length
                        if header_bytes.len() < 14 {
                            self.state = ParserState::Header(14);
                            return None;
                        } else {
                            self.consumed += 14;
                            let mut len: u64 = (u64::from(header_bytes[2])) << 56;
                            len |= (u64::from(header_bytes[3])) << 48;
                            len |= (u64::from(header_bytes[4])) << 40;
                            len |= (u64::from(header_bytes[5])) << 32;
                            len |= (u64::from(header_bytes[6])) << 24;
                            len |= (u64::from(header_bytes[7])) << 16;
                            len |= (u64::from(header_bytes[8])) << 8;
                            len |= u64::from(header_bytes[9]);
                            if len == 0 {
                                self.state = ParserState::Header(MIN_HEADER_BYTES);
                            } else {
                                let mask = [
                                    header_bytes[10],
                                    header_bytes[11],
                                    header_bytes[12],
                                    header_bytes[13],
                                ];
                                self.state = ParserState::Payload(len, mask, 0, ping);
                            }
                        }
                    } else if len_byte == 126 {
                        // 2 byte length
                        if header_bytes.len() < 8 {
                            self.state = ParserState::Header(8);
                        } else {
                            self.consumed += 8;
                            let mut len: u64 = (u64::from(header_bytes[2])) << 8;
                            len |= u64::from(header_bytes[3]);
                            if len == 0 {
                                self.state = ParserState::Header(MIN_HEADER_BYTES);
                            } else {
                                let mask = [
                                    header_bytes[4],
                                    header_bytes[5],
                                    header_bytes[6],
                                    header_bytes[7],
                                ];
                                self.state = ParserState::Payload(len, mask, 0, ping);
                            }
                        }
                    } else {
                        // 7-bit length
                        self.consumed += 6;
                        if len_byte == 0 {
                            self.state = ParserState::Header(MIN_HEADER_BYTES);
                        } else {
                            let mask = [
                                header_bytes[2],
                                header_bytes[3],
                                header_bytes[4],
                                header_bytes[5],
                            ];
                            self.state = ParserState::Payload(u64::from(len_byte), mask, 0, ping);
                        }
                    }
                }
                ParserState::Payload(num_bytes_left, mask, mask_place, ping) => {
                    assert!(num_bytes_left != 0);

                    // Grab the next byte
                    let byte = if let Some(b) = self.bytes.get(self.consumed) {
                        *b
                    } else {
                        return None;
                    };
                    self.consumed += 1;

                    // Unmask it
                    let unmasked = byte ^ mask[mask_place];

                    // Set up state for next time
                    self.state = if num_bytes_left == 1 {
                        ParserState::Header(MIN_HEADER_BYTES)
                    } else {
                        ParserState::Payload(
                            num_bytes_left - 1,
                            mask,
                            (mask_place + 1) & 0b11,
                            ping,
                        )
                    };

                    // Set it as the result, or put it in the ping buffer
                    if ping {
                        if let Some(ParserResp::Ping(ref mut v)) = self.resp {
                            v.push(unmasked);
                            if num_bytes_left == 1 {
                                // We've got the ping payload - convert it to a pong packet
                                let mut enc = Vec::with_capacity(v.len() + 2);
                                enc.push(OPCODE_PONG | 0b1000_0000);
                                enc.push(v.len() as u8);
                                enc.extend(v.iter());
                                mem::swap(&mut enc, v);
                            }
                            continue;
                        } else {
                            unreachable!();
                        }
                    } else {
                        return Some(unmasked);
                    }
                }
            }
        }
    }
}

pub fn handshake(bytes: &[u8]) -> Result<Option<(Vec<u8>, bool)>, Error> {
    // Do handshake
    if let Some(end_idx) = bytes.windows(4).position(|window| window == b"\r\n\r\n") {
        // If we got more bytes after the end, that's an error
        if end_idx != bytes.len() - 4 {
            return Err(Error::HttpWithClose(HTTP_BAD_REQ_RESP));
        }

        // Make sure it's the right URL
        if &bytes[..12] != b"GET /edsu/0.2/ws" {
            return Ok(Some((HTTP_PAGE_NOT_FOUND_RESP.to_vec(), false)));
        }

        // Make sure it's the one WS version we understand
        if http_header_val(bytes, VERS_HTTP_KEY, VERS_HTTP_KEY_LC) != Ok(b"13") {
            return Err(Error::Http(HTTP_VERS_RESP));
        }

        // Grab the WS key
        let ws_key = http_header_val(bytes, KEY_HTTP_KEY, KEY_HTTP_KEY_LC)
            .map_err(|_| Error::HttpWithClose(HTTP_BAD_REQ_RESP))?;

        // Construct the response key
        let mut hasher = ring::digest::Context::new(&ring::digest::SHA1_FOR_LEGACY_USE_ONLY);
        hasher.update(ws_key);
        hasher.update(MAGIC_GUID);
        let resp_key = base64::encode(hasher.finish().as_ref());

        // Return our HTTP response
        let mut http_resp = Vec::new();
        http_resp.extend(HTTP_RESP_HEAD.iter());
        http_resp.extend(resp_key.as_bytes().iter());
        http_resp.extend(b"\r\n\r\n".iter());

        Ok(Some((http_resp, true)))
    } else {
        Ok(None)
    }
}

fn http_header_val<'a>(bytes: &'a [u8], key: &[u8], key_lc: &[u8]) -> Result<&'a [u8], ()> {
    let key_idx = bytes
        .windows(key.len())
        .position(|window| window == key || window == key_lc)
        .ok_or(())?;

    // Find the start of the key
    let start = bytes[key_idx + key.len()..]
        .iter()
        .position(|ch| *ch != b' ')
        .ok_or(())?
        + key_idx
        + key.len();

    // Find the end of the key
    let end = bytes[start..]
        .iter()
        .position(|ch| *ch == b'\r')
        .ok_or(())?
        + start;

    Ok(&bytes[start..end])
}

pub fn encapsulate(bytes: &[u8]) -> Vec<u8> {
    // TODO: If I make Vec<Vec<u8>> the type that net.write() gets, wouldn't have to copy
    let mut enc = Vec::with_capacity(bytes.len() + 4);
    enc.push(OPCODE_BIN | 0b1000_0000);
    if bytes.len() > 125 {
        enc.push(126);
        enc.push((bytes.len() >> 8) as u8);
        enc.push(bytes.len() as u8);
    } else {
        enc.push(bytes.len() as u8)
    }
    enc.extend(bytes.iter());
    enc
}
