use crate::email::EmailAlarm;
use std::fs::{File, OpenOptions};
use std::io::{self, Write};
use std::path::PathBuf;
use std::{borrow, mem, process, thread};

#[derive(Debug)]
pub enum LogLevel {
    Debug,
    Info,
    Error,
    Alert,
    Alarm,
    Fatal,
}

// ////////////////////////////
// Synced with C

const LOG_RET_OK: i32 = 0;
//const LOG_RET_ERR: i32 = -1;
//const LOG_RET_FATAL: i32 = -2;
const LOG_DEBUG: u32 = 0x01;
const LOG_INFO: u32 = 0x02; // Something that doesn't need fixing
const LOG_ERR: u32 = 0x03; // Something that /does/ need fixing
const LOG_ALERT: u32 = 0x04; // Demands immediate attention
const LOG_ALARM: u32 = 0x05; // And error important enough to interrupt the admin
const LOG_FATAL: u32 = 0x06; // This kills the server
fn map_level(x: &LogLevel) -> u32 {
    match x {
        LogLevel::Debug => LOG_DEBUG,
        LogLevel::Info => LOG_INFO,
        LogLevel::Error => LOG_ERR,
        LogLevel::Alert => LOG_ALERT,
        LogLevel::Alarm => LOG_ALARM,
        LogLevel::Fatal => LOG_FATAL,
    }
}

extern "C" {
    fn put_log(level: u32, text: *const u8) -> i32;
    fn get_log(buffer: *const u8, buffer_len: u32) -> i32;
}

// ////////////////////////////

const LOG_BUFFER_SIZE: u32 = 4096;

#[macro_export]
macro_rules! debug {
    ($txt:expr) => (logging::log(&logging::LogLevel::Debug, $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (logging::log(&logging::LogLevel::Debug, &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! info {
    ($txt:expr) => (logging::log(&logging::LogLevel::Info, $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (logging::log(&logging::LogLevel::Info, &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! err {
    ($txt:expr) => (logging::log(&logging::LogLevel::Error, $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (logging::log(&logging::LogLevel::Error, &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! alert {
    ($txt:expr) => (logging::log(&logging::LogLevel::Alert, $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (logging::log(&logging::LogLevel::Alert, &format!($fmt, $($arg)*),
                                              file!(), line!()));
}
#[macro_export]
macro_rules! alarm {
    ($txt:expr) => (logging::log(&logging::LogLevel::Alarm, $txt, file!(), line!()));
    ($fmt:expr, $($arg:tt)*) => (logging::log(&logging::LogLevel::Alarm, &format!($fmt, $($arg)*),
                                              file!(), line!()));
}

fn open_log_file(path: &PathBuf) -> io::Result<File> {
    OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(path)
}

fn write_with_newline(file: &mut File, bytes: &[u8]) -> io::Result<()> {
    file.write_all(bytes)?;
    file.write_all(&[b'\n'])?;
    file.flush()
}

pub fn log(level: &LogLevel, text: &str, file: &str, line: u32) {
    // Convert to C string (CString was really annoying)
    let formatted = format!("{}:{} - {}", file, line, text);
    let mut vec = Vec::from(formatted);
    vec.push(b'\0');
    let result = unsafe { put_log(map_level(level), vec.as_ptr()) };
    if result != LOG_RET_OK {
        panic!("fatal error from put_log()");
    }
}

pub fn init_logging(path: PathBuf, mut alarm_maybe: Option<EmailAlarm>) -> io::Result<()> {
    let mut file = open_log_file(&path)?;
    let buffer = [0; LOG_BUFFER_SIZE as usize];

    // Start logging thread
    thread::spawn(move || {
        loop {
            // Fill buffer from C code
            let result = unsafe { get_log(buffer.as_ptr(), LOG_BUFFER_SIZE) };
            if result != LOG_RET_OK {
                println!("fatal error from get_log() - exiting...");
                process::exit(8);
            }

            // Interpret it as a str
            let mut log;
            if let Some(null_idx) = buffer.iter().position(|c| *c == b'\0') {
                log = String::from_utf8_lossy(&buffer[..null_idx]);
            } else {
                log = borrow::Cow::Borrowed(
                    "!!! [Alarm]: could not find null byte in get_log() buffer",
                );
            }

            // There isn't a great spot for this, but here's easiest for now
            if let Some(alarm) = alarm_maybe.as_mut() {
                if let Some(loc) = log.find("[Alarm]") {
                    alarm.send(&log.to_mut()[loc..]);
                }
            }

            match write_with_newline(&mut file, log.as_bytes()) {
                Ok(_) => continue,
                Err(_) => match open_log_file(&path) {
                    Ok(mut new_file) => match write_with_newline(&mut file, log.as_bytes()) {
                        Ok(_) => {
                            mem::swap(&mut file, &mut new_file);
                            continue;
                        }
                        Err(e) => {
                            println!("fatal: logger can't write file: {:?}", e);
                            process::exit(8);
                        }
                    },
                    Err(e) => {
                        println!("fatal: logger can't open file: {:?}", e);
                        process::exit(8);
                    }
                },
            }
        }
    });
    Ok(())
}
