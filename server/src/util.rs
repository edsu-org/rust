use std::{
    io::{self, Read, Write},
    os::unix::fs::PermissionsExt,
    path::{self, Path, PathBuf},
    {env, fs},
};

// ////////////////////////

// Synced with aux.c

//const AUX_OK: i32 = 0;
//const AUX_ERR: i32 = -1;
const AUX_FATAL: i32 = -2;

extern "C" {
    fn lock_file(path: *const u8) -> i32;
}

// ////////////////////////

pub fn acquire_lock(path: &PathBuf) {
    // It's easier to make sure the file exists from Rust
    let parent_path = path.parent().unwrap();
    if !parent_path.is_dir() {
        fs::DirBuilder::new()
            .recursive(true)
            .create(&parent_path)
            .unwrap();
    }
    unsafe {
        if lock_file(path.to_str().unwrap().as_bytes().as_ptr()) == AUX_FATAL {
            panic!("failed to acquire file lock");
        }
    }
}

/// Atomically create a new file (errors if it exists) and puts the given bytes into it
pub fn bytes_to_new_file(path: &path::Path, bytes: &[u8], mode: Option<u32>) -> io::Result<()> {
    let mut file = fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(path)?;
    if let Some(m) = mode {
        let metadata = file.metadata()?;
        let mut perms = metadata.permissions();
        perms.set_mode(m);
    }
    file.write_all(bytes)
}

/// Create a new file (silently overwrites if it exists) and puts the given bytes into it
pub fn bytes_to_file(path: &path::Path, bytes: &[u8], mode: Option<u32>) -> io::Result<()> {
    fs::remove_file(&path).ok();
    bytes_to_new_file(path, bytes, mode)
}

/// Get the contents of a file
pub fn file_to_bytes(path: &path::Path) -> io::Result<Vec<u8>> {
    let mut bytes = Vec::new();
    fs::File::open(path)?.read_to_end(&mut bytes)?;
    Ok(bytes)
}

/// Attempts to expand ~ into $HOME
pub fn expand_tilde(path: &Path) -> PathBuf {
    if let Ok(suffix) = path.strip_prefix("~") {
        if let Ok(home) = env::var("HOME") {
            let mut path_build = PathBuf::new();
            path_build.set_file_name(home);
            path_build.push(suffix);
            path_build
        } else {
            path.to_path_buf()
        }
    } else {
        path.to_path_buf()
    }
}
