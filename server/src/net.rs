use crate::logging;
use crate::ssl::SslCert;
use bs58;
use std::{
    cmp::Ordering,
    collections::HashMap,
    net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr},
};

// //////////////////////////////////////
// Synced with C

const NET_OK: i32 = 0;
const NET_BUF_WOULD_OFLOW: i32 = 1;
const NET_ERR: i32 = -1;
const NET_FATAL: i32 = -2;
const NET_EV_NEW_SOCK: u8 = 0b0000_0001;
const NET_EV_SOCK_HANDSHAKE_DONE: u8 = 0b0000_0010;
const NET_EV_NEW_BYTES: u8 = 0b0000_0100;
const NET_EV_RUST_CLOSE: u8 = 0b0000_1000;
const NET_SOCK_TYPE_CLOSED: u8 = 0x00;
const NET_SOCK_TYPE_EDSU: u8 = 0x01;
const NET_SOCK_TYPE_WS: u8 = 0x02;

const NET_MAX_COMMON_NAME_LEN: usize = 256;
const NET_CERT_DECODE_STRUCT_LEN: usize = 2560;
pub const NET_CERT_MAX_CHAIN_LEN: usize = 4;
const NET_BUF_SIZE_IN: usize = 65536;
const NET_BUF_SIZE_OUT: usize = 65536;

#[derive(Clone)]
#[repr(C)]
struct NetSock {
    fd: u32,
    sock_4_not_6: bool,
    sock_ip: [u8; 16],
    buf_in_live_bytes: u16,
    buf_out_live_bytes: u16,
    last_send: u64,
    last_recv: u64,
    cert_generation: u64,
}

#[derive(Clone)]
#[repr(C)]
struct NetSockStatus {
    stype: u8,
    event: u8,
    call: u8,
}

#[repr(C)]
struct NetCertItem {
    key_idx: u32,
    key_len: u32,
    num_certs: u32,
    valid: bool,
    common_name: [u8; NET_MAX_COMMON_NAME_LEN],
    chain_array: [u64; NET_CERT_MAX_CHAIN_LEN * 2],
    decode_struct_mem: [u8; NET_CERT_DECODE_STRUCT_LEN],
}

extern "C" {
    fn net_init(
        num_socks: u32,
        session_cache_size: u32,
        ip_edsu: *const u8,
        port_edsu: *const u8,
        ip_ws: *const u8,
        port_ws: *const u8,
        unix_user: *const u8,
        unix_group: *const u8,
        socks: *const NetSock,
        statuses: *const NetSockStatus,
        bufs_in: *const u8,
        bufs_out: *const u8,
    ) -> i32;
    fn net_select(tick_us: u32) -> i32;
    fn net_write(sock_id: u32, bytes: *const u8, num_bytes: u16) -> i32;
    fn net_report_consumption(sock_id: u32, num_bytes: u16) -> i32;
    fn net_fini();
    fn net_set_cert_table(
        cert_table: *const NetCertItem,
        cert_table_len: u32,
        cert_heap: *const u8,
        cert_heap_len: u32,
    ) -> i32;
}

// //////////////////////////////////////

// Ordering of NetCertItems
impl Ord for NetCertItem {
    fn cmp(&self, other: &NetCertItem) -> Ordering {
        self.common_name.cmp(&other.common_name)
    }
}
impl PartialOrd for NetCertItem {
    fn partial_cmp(&self, other: &NetCertItem) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl PartialEq for NetCertItem {
    fn eq(&self, other: &NetCertItem) -> bool {
        self.common_name
            .iter()
            .zip(other.common_name.iter())
            .all(|(a, b)| a == b)
    }
}
impl Eq for NetCertItem {}

#[derive(Debug)]
pub enum Error {
    WouldOflow,
    AuxError,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct SockId(u32);

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SockType {
    Closed,
    Edsu,
    Ws,
}

pub enum NetEvent {
    SockClosed(SockId),
    SockNew(SockId),
    SockNewBytes(SockId),
}

#[derive(Debug)]
pub struct NetConfig {
    pub ssl_session_cache_size: u32,
    pub num_socks: u32,
    pub edsu_addr: SocketAddr,
    pub ws_addr: SocketAddr,
    pub unix_user: String,
    pub unix_group: String,
}

pub struct Net {
    net_socks: Box<[NetSock]>,
    net_statuses: Box<[NetSockStatus]>,
    bufs_in: Box<[u8]>,
}

impl Net {
    pub fn new(config: &NetConfig) -> Self {
        let num_socks = config.num_socks as usize;

        // Allocate the four big shared buffers
        let net_socks = vec![
            NetSock {
                fd: 0,
                sock_4_not_6: false,
                sock_ip: [0; 16],
                buf_in_live_bytes: 0,
                buf_out_live_bytes: 0,
                last_send: 0,
                last_recv: 0,
                cert_generation: 0,
            };
            num_socks
        ]
        .into_boxed_slice();
        let net_statuses = vec![
            NetSockStatus {
                stype: 0,
                event: 0,
                call: 0,
            };
            num_socks
        ]
        .into_boxed_slice();
        let bufs_in = vec![0u8; num_socks * NET_BUF_SIZE_IN].into_boxed_slice();
        let bufs_out = vec![0u8; num_socks * NET_BUF_SIZE_OUT].into_boxed_slice();

        // Init the C side
        let result = unsafe {
            net_init(
                config.num_socks,
                config.ssl_session_cache_size,
                format!("{}\0", config.edsu_addr.ip()).as_ptr(),
                format!("{}\0", config.edsu_addr.port()).as_ptr(),
                format!("{}\0", config.ws_addr.ip()).as_ptr(),
                format!("{}\0", config.ws_addr.port()).as_ptr(),
                format!("{}\0", config.unix_user).as_ptr(),
                format!("{}\0", config.unix_group).as_ptr(),
                net_socks.as_ptr(),
                net_statuses.as_ptr(),
                bufs_in.as_ptr(),
                bufs_out.as_ptr(),
            )
        };
        if result == NET_FATAL {
            panic!("got fatal error from net during init");
        }

        // Set up our state
        Net {
            net_socks,
            net_statuses,
            bufs_in,
        }
    }
    pub fn select<T>(
        &mut self,
        sock_map: &HashMap<SockId, T>,
        events: &mut Vec<NetEvent>,
        tick_us: u32,
    ) {
        // Call the aux lib's select()
        let result = unsafe { net_select(tick_us) };
        if result == NET_FATAL {
            panic!("got fatal error from net during net_select()");
        }

        // Synchronize our socket state with the C side's
        for (sock_id, net_status) in self.net_statuses.iter_mut().enumerate() {
            let sock_id = SockId(sock_id as u32);

            if net_status.stype == NET_SOCK_TYPE_CLOSED {
                // If it's closed, make sure any associated conn is closed too
                if sock_map.contains_key(&sock_id) {
                    events.push(NetEvent::SockClosed(sock_id));
                }
            } else {
                if net_status.event & NET_EV_NEW_SOCK != 0 {
                    info!("new socket: {:?}", sock_id);
                    // New socket, but still need to do handshake before we can create the Conn
                    // If there was an old socket attached to a conn, that was closed, evidently
                    if sock_map.contains_key(&sock_id) {
                        events.push(NetEvent::SockClosed(sock_id));
                    }
                }
                if net_status.event & NET_EV_SOCK_HANDSHAKE_DONE != 0 {
                    // SSL handshake done, create the Conn
                    info!("SSL handshake complete: {:?}", sock_id);
                    events.push(NetEvent::SockNew(sock_id));
                }
                if net_status.event & NET_EV_NEW_BYTES != 0 {
                    // New bytes on a socket
                    events.push(NetEvent::SockNewBytes(sock_id));
                }
            }
        }
    }
    pub fn set_certs(&mut self, ssl_certs: Vec<SslCert>) {
        // Put into a format that is easy for the C side to work with
        let mut heap: Vec<u8> = Vec::new();
        let mut items = Vec::new();

        'next_eson: for cert in ssl_certs {
            // Decode the ESON
            let SslCert {
                common_names,
                key: key_b58,
                certs: certs_b58,
            } = cert;
            if certs_b58.len() > NET_CERT_MAX_CHAIN_LEN {
                err!("bad cert ESON - too many certs: {:?}", common_names);
                continue;
            }

            // Decode the b58
            // - Key
            let key = if let Ok(x) = bs58::decode(key_b58.as_bytes()).into_vec() {
                x
            } else {
                err!("bad cert ESON - bad b58 encoding for key: {:?}", common_names);
                continue;
            };
            // - Certs  TODO: redo if iterator_try_fold becomes ungated
            let mut certs = Vec::with_capacity(certs_b58.len());
            for cb58 in certs_b58 {
                if let Ok(x) = bs58::decode(cb58.as_bytes()).into_vec() {
                    certs.push(x);
                } else {
                    err!("bad cert ESON - bad b58 encoding for key: {:?}", common_names);
                    continue 'next_eson;
                };
            }

            // Put into the heap
            let key_idx = heap.len() as u32;
            heap.extend(key.iter());

            let mut cert_idxs = Vec::with_capacity(certs.len());
            for crt in certs.iter() {
                cert_idxs.push(heap.len());
                heap.extend(crt.iter());
            }

            // Create chain array (index of the cert followed by its length)
            let mut chain_array = [0; NET_CERT_MAX_CHAIN_LEN * 2];
            for (i, crt) in certs.iter().enumerate() {
                chain_array[i * 2] = cert_idxs[i] as u64;
                chain_array[i * 2 + 1] = crt.len() as u64;
            }

            // Create an item for each common name
            for common_name in common_names {
                if common_name.as_bytes().len() >= NET_MAX_COMMON_NAME_LEN {
                    err!("bad cert ESON - common name too long: {:?}", common_name);
                    continue 'next_eson;
                }

                let mut item = NetCertItem {
                    key_idx,
                    key_len: key.len() as u32,
                    num_certs: certs.len() as u32,
                    valid: false,
                    common_name: [0; NET_MAX_COMMON_NAME_LEN],
                    chain_array,
                    decode_struct_mem: [0; NET_CERT_DECODE_STRUCT_LEN],
                };
                // Fill in common name
                // - Knock off the * if it's a wildcard
                let common_name_bytes = if common_name.as_str().starts_with('*') {
                    &common_name.as_bytes()[1..]
                } else {
                    common_name.as_bytes()
                };

                for (dst, src) in item.common_name.iter_mut().zip(common_name_bytes.iter()) {
                    *dst = *src;
                }
                items.push(item);
            }
        }

        // Sort the items (they're going to be looked up using a binary search)
        items.sort_unstable();

        // Call the C code
        let result = unsafe {
            net_set_cert_table(
                items.as_ptr(),
                items.len() as u32,
                heap.as_ptr(),
                heap.len() as u32,
            )
        };
        match result {
            NET_FATAL => panic!("fatal error doing net_set_cert_table()"),
            NET_ERR => alarm!("net_set_cert_table() returned NET_ERR"),
            NET_OK => (),
            _ => panic!(),
        };
    }
    pub fn get_in_buf(&self, sock_id: SockId) -> &[u8] {
        let sock_id = sock_id.0 as usize;
        let start = NET_BUF_SIZE_IN * sock_id;
        let end = start + self.net_socks[sock_id].buf_in_live_bytes as usize;
        &self.bufs_in[start..end]
    }
    pub fn get_ip(&self, sock_id: SockId) -> IpAddr {
        let sock = &self.net_socks[sock_id.0 as usize];
        if sock.sock_4_not_6 {
            let ip = sock.sock_ip;
            IpAddr::V4(Ipv4Addr::new(ip[3], ip[2], ip[1], ip[0]))
        } else {
            let ip = sock.sock_ip;
            IpAddr::V6(Ipv6Addr::new(
                ((ip[0] as u16) << 8) + ip[1] as u16,
                ((ip[2] as u16) << 8) + ip[3] as u16,
                ((ip[4] as u16) << 8) + ip[5] as u16,
                ((ip[6] as u16) << 8) + ip[7] as u16,
                ((ip[8] as u16) << 8) + ip[9] as u16,
                ((ip[10] as u16) << 8) + ip[11] as u16,
                ((ip[12] as u16) << 8) + ip[13] as u16,
                ((ip[14] as u16) << 8) + ip[15] as u16,
            ))
        }
    }
    pub fn report_consumption(&self, sock_id: SockId, num_bytes: u16) -> Result<(), Error> {
        let result = unsafe { net_report_consumption(sock_id.0, num_bytes) };
        match result {
            NET_FATAL => panic!("fatal error doing net_report_consumption()"),
            NET_ERR => Err(Error::AuxError),
            NET_OK => Ok(()),
            _ => panic!(),
        }
    }
    pub fn write(&self, sock_id: SockId, bytes: &[u8]) -> Result<(), Error> {
        if bytes.len() > 0xffff {
            panic!()
        };
        let result = unsafe { net_write(sock_id.0, bytes.as_ptr(), bytes.len() as u16) };
        match result {
            NET_FATAL => panic!("fatal error doing net_write()"),
            NET_BUF_WOULD_OFLOW => Err(Error::WouldOflow),
            NET_ERR => Err(Error::AuxError),
            NET_OK => Ok(()),
            _ => panic!(),
        }
    }
    pub fn close_hard(&mut self, sock_id: SockId) {
        debug!("requesting a hard close: {:?}", sock_id);
        self.net_statuses[sock_id.0 as usize].event |= NET_EV_RUST_CLOSE;
    }
    pub fn sock_type(&self, sock_id: SockId) -> SockType {
        let net_status = &self.net_statuses[sock_id.0 as usize];
        match net_status.stype {
            NET_SOCK_TYPE_CLOSED => SockType::Closed,
            NET_SOCK_TYPE_EDSU => SockType::Edsu,
            NET_SOCK_TYPE_WS => SockType::Ws,
            _ => panic!(),
        }
    }
}

impl Drop for Net {
    fn drop(&mut self) {
        unsafe { net_fini() }
    }
}
