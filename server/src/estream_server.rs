#![allow(unused_imports, dead_code)]
use crate::logging;
use crate::net::{self, Net, NetConfig, NetEvent, SockId, SockType};
use crate::ssl::SslCert;
use crate::ws;
use edsu_common::eson::{self, Eson};
use std::{collections::HashMap, fmt, net::IpAddr};

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct EstreamId {
    sock_id: SockId,
    unique_id: u64,
}

impl fmt::Debug for EstreamId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "EstreamId({}, {:?})", self.unique_id, self.sock_id)
    }
}

pub enum EstreamEvent {
    SockClosed(EstreamId),     // Net reports that the socket is closed
    SockClosedSoft(EstreamId), // Writing has been disabled by us
    SockNew(EstreamId, IpAddr),
    EsonsNew(EstreamId, Vec<Eson>, usize),
    EsonParseError(EstreamId, eson::Error),
}

enum EsonReader {
    Edsu(eson::Parser),
    Ws((Option<ws::ParserState>, eson::Parser)),
}

impl EsonReader {
    pub fn new(stype: SockType) -> Self {
        match stype {
            SockType::Closed => unreachable!(),
            SockType::Edsu => EsonReader::Edsu(eson::Parser::new()),
            SockType::Ws => EsonReader::Ws((None, eson::Parser::new())),
        }
    }
}

#[derive(PartialEq, Eq)]
pub enum ReadMode {
    Normal,
    Paused,
    SoftClosed,
}

pub enum Writable {
    Eson(Eson),
    Esons(Vec<Eson>),
    PreConverted(Vec<u8>),
}

struct SockState {
    unique_id: u64,
    is_ws: bool,
    reader: EsonReader,
    read_mode: ReadMode,
}

impl SockState {
    fn new(unique_id: u64, is_ws: bool, reader: EsonReader) -> Self {
        SockState {
            unique_id,
            is_ws,
            reader,
            read_mode: ReadMode::Normal,
        }
    }
}

pub enum WriteResult {
    BytesSent(usize),
    WouldOverflow(Writable),
    SockDisappeared,
    NetError(net::Error),
}

pub struct EstreamServer {
    net: Net,
    sock_states: HashMap<SockId, SockState>,
    net_events: Vec<NetEvent>,
    cur_unique_id: u64,
}

impl EstreamServer {
    pub fn new(config: &NetConfig) -> Self {
        let net = Net::new(config);
        EstreamServer {
            net,
            sock_states: HashMap::new(),
            net_events: Vec::new(),
            cur_unique_id: 0,
        }
    }
    pub fn select(&mut self, sock_events: &mut Vec<EstreamEvent>, tick_us: u32) {
        self.net
            .select(&self.sock_states, &mut self.net_events, tick_us);

        for nev in self.net_events.drain(..) {
            match nev {
                NetEvent::SockClosed(sock_id) => {
                    // Remove our sock state, then pass the event along
                    let sock_state = self.sock_states.remove(&sock_id).unwrap();
                    sock_events.push(EstreamEvent::SockClosed(EstreamId {
                        sock_id,
                        unique_id: sock_state.unique_id,
                    }));
                }
                NetEvent::SockNew(sock_id) => {
                    // Set up sock state, then pass the event along
                    let reader = EsonReader::new(self.net.sock_type(sock_id));
                    let is_ws = self.net.sock_type(sock_id) == SockType::Ws;
                    self.cur_unique_id += 1;
                    let unique_id = self.cur_unique_id;
                    self.sock_states
                        .insert(sock_id, SockState::new(unique_id, is_ws, reader));
                    let estream_id = EstreamId { sock_id, unique_id };
                    sock_events.push(EstreamEvent::SockNew(estream_id, self.net.get_ip(sock_id)));
                }
                NetEvent::SockNewBytes(sock_id) => {
                    handle_new_bytes(&mut self.net, &mut self.sock_states, sock_events, sock_id);
                }
            }
        }
    }
    pub fn write(&mut self, estream_id: EstreamId, to_write: Writable) -> WriteResult {
        let (is_ws, sock_id) = match self.sock_states.get(&estream_id.sock_id) {
            None => return WriteResult::SockDisappeared,
            Some(sock_state) => {
                if sock_state.unique_id == estream_id.unique_id {
                    (sock_state.is_ws, estream_id.sock_id)
                } else {
                    return WriteResult::SockDisappeared;
                }
            }
        };
        let mut bytes = Vec::new();
        let mut suppress_encapsulation = false;
        match to_write {
            Writable::Eson(eson) => eson.write_into(&mut bytes).unwrap(),
            Writable::Esons(esons) => {
                for eson in esons {
                    eson.write_into(&mut bytes).unwrap();
                }
            }
            Writable::PreConverted(b) => {
                bytes = b;
                suppress_encapsulation = true;
            }
        }

        // Potentially ecapsulate in WS
        if !suppress_encapsulation && is_ws {
            bytes = ws::encapsulate(&bytes);
        }

        match self.net.write(sock_id, &bytes) {
            Ok(_) => WriteResult::BytesSent(bytes.len()),
            Err(net::Error::WouldOflow) => {
                WriteResult::WouldOverflow(Writable::PreConverted(bytes))
            }
            Err(e) => WriteResult::NetError(e),
        }
    }
    pub fn close_hard(&mut self, estream_id: EstreamId) {
        let sock_id = estream_id.sock_id;
        if let Some(sock_state) = self.sock_states.get(&sock_id) {
            if sock_state.unique_id == estream_id.unique_id {
                self.net.close_hard(sock_id);
            }
        }
    }
    // Passthroughs to Net
    pub fn set_certs(&mut self, ssl_certs: Vec<SslCert>) {
        self.net.set_certs(ssl_certs);
    }
}

fn handle_new_bytes(
    net: &mut Net,
    sock_states: &mut HashMap<SockId, SockState>,
    sock_events: &mut Vec<EstreamEvent>,
    sock_id: SockId,
) {
    // Grab the sock_state, the in buffer, and set up some state
    let sock_state = sock_states.get_mut(&sock_id).unwrap();
    let estream_id = EstreamId {
        sock_id,
        unique_id: sock_state.unique_id,
    };
    let in_buf = net.get_in_buf(sock_id);

    // Exit early if it's a false positive, or we're paused
    if in_buf.is_empty() || sock_state.read_mode == ReadMode::Paused {
        return;
    }

    // If the read has closed (i.e. a soft close) discard the bytes (can't just do a
    // shutdown() because it doesn't play well with SSL
    if sock_state.read_mode == ReadMode::SoftClosed {
        if net
            .report_consumption(sock_id, in_buf.len() as u16)
            .is_err()
        {
            err!(
                "report_consumption() failed for {:?}, hard closing",
                sock_id
            );
            net.close_hard(sock_id);
        }
        return;
    }

    let mut consumed = 0;
    let mut esons_new = Vec::new();
    match sock_state.reader {
        EsonReader::Edsu(ref mut parser) => {
            let mut iter = in_buf.iter().cloned();
            match parser.add_bytes(&mut iter) {
                Ok(None) => (),
                Ok(Some(eson)) => esons_new.push(eson),
                Err(e) => {
                    sock_state.read_mode = ReadMode::SoftClosed;
                    sock_events.push(EstreamEvent::EsonParseError(estream_id, e));
                }
            }
            consumed = in_buf.len() - iter.count();
        }
        EsonReader::Ws((ref mut maybe_parser_state, ref mut edsu_parser)) => {
            if maybe_parser_state.is_none() {
                // Need to handshake first
                match ws::handshake(in_buf) {
                    Ok(None) => (), // Need more bytes - do nothing until then
                    Ok(Some((http_resp, upgraded))) => {
                        // Parsed OK, respond with either the HTTP upgrade or the auth page
                        if let Err(e) = net.write(sock_id, &http_resp) {
                            debug!("error writing WS handshake: {:?}", e);
                            sock_state.read_mode = ReadMode::SoftClosed;
                            sock_events.push(EstreamEvent::SockClosedSoft(estream_id));
                        }
                        consumed = in_buf.len();

                        if upgraded {
                            // Set state for the next time around
                            *maybe_parser_state = Some(ws::ParserState::default());
                        } else {
                            // "Soft close" the conn - we've sent the HTTP data so we're done
                            sock_state.read_mode = ReadMode::SoftClosed;
                            sock_events.push(EstreamEvent::SockClosedSoft(estream_id));
                        }
                    }
                    Err(ws::Error::Http(http_resp)) => {
                        if let Err(e) = net.write(sock_id, &http_resp) {
                            debug!("error writing WS handshake error: {:?}", e);
                            sock_state.read_mode = ReadMode::SoftClosed;
                            sock_events.push(EstreamEvent::SockClosedSoft(estream_id));
                        }
                        consumed = in_buf.len();
                    }
                    Err(ws::Error::HttpWithClose(http_resp)) => {
                        if let Err(e) = net.write(sock_id, &http_resp) {
                            debug!("error writing WS handshake error while closing: {:?}", e);
                        }
                        net.close_hard(sock_id);
                        return;
                    }
                }
            } else {
                // Handshake is done, start or continue parsing
                let parser_state = maybe_parser_state.take().unwrap();

                // Create iter and feed it to the Edsu parser
                let mut iter = ws::ParserIter::new(parser_state, in_buf);
                match edsu_parser.add_bytes(&mut iter) {
                    Ok(None) => (),
                    Ok(Some(eson)) => esons_new.push(eson),
                    Err(e) => {
                        sock_state.read_mode = ReadMode::SoftClosed;
                        sock_events.push(EstreamEvent::EsonParseError(estream_id, e));
                    }
                }

                // Decompose the iter into the parts we'll need
                let (iter_consumed, resp, new_parser_state) = iter.decompose();

                // Handle response (if any)
                match resp {
                    None => (),
                    Some(ws::ParserResp::Close) => {
                        // Either the client or the iter requested a close connection
                        net.close_hard(sock_id);
                        return;
                    }
                    Some(ws::ParserResp::Ping(pong_bytes)) => {
                        // WS ping, respond with a pong
                        if let Err(e) = net.write(sock_id, &pong_bytes) {
                            debug!("error writing WS pong: {:?}", e);
                            sock_state.read_mode = ReadMode::SoftClosed;
                            sock_events.push(EstreamEvent::SockClosedSoft(estream_id));
                        }
                    }
                }

                // Set state for the next time around
                consumed = iter_consumed;
                *maybe_parser_state = Some(new_parser_state);
            }
        }
    }

    if !esons_new.is_empty() {
        sock_events.push(EstreamEvent::EsonsNew(estream_id, esons_new, consumed));
    }

    // Report how many bytes where consumed (and so can be freed)
    assert!(consumed <= 0xffff);
    net.report_consumption(sock_id, consumed as u16).unwrap();
}
