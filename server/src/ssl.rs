use bs58;
use edsu_common::edsu::Value;
use openssl::{nid::Nid, pkey::PKey, x509::X509};
use std::{collections::HashSet, fmt};

#[derive(Debug)]
pub enum Error {
    InvalidInput(String),
}

pub struct SslCert {
    pub common_names: Vec<Value>,
    pub key: Value,
    pub certs: Vec<Value>,
}

impl SslCert {
    pub fn from_pems(key_bytes: &[u8], chain_bytes: &[u8]) -> Result<Self, Error> {
        // Convert to DER
        // - Private key
        let key = PKey::private_key_from_pem(&key_bytes)
            .map_err(|e| Error::InvalidInput(format!("while reading PEM private key: {:?}", e)))?;
        let key_der = key
            .private_key_to_der()
            .map_err(|_| Error::InvalidInput("DER serializing PEM private key".to_owned()))?;

        // - Cert chain
        let chain = X509::stack_from_pem(&chain_bytes)
            .map_err(|e| Error::InvalidInput(format!("while reading PEM cert chain: {:?}", e)))?;
        let chain_ders = chain
            .iter()
            .filter_map(|x| x.to_der().ok())
            .collect::<Vec<Vec<u8>>>();
        if chain_ders.len() != chain.len() {
            return Err(Error::InvalidInput(
                "DER serializing one or more PEM certs".to_owned(),
            ));
        }

        // Construct from DER
        Self::from_ders(&key_der, &chain_ders)
    }
    pub fn from_ders(key_der: &[u8], chain_ders: &[Vec<u8>]) -> Result<Self, Error> {
        // Extract common names from first cert in chain
        if chain_ders.is_empty() {
            return Err(Error::InvalidInput("empty cert chain".to_owned()));
        }
        let cert0 = X509::from_der(&chain_ders[0])
            .map_err(|e| Error::InvalidInput(format!("while converting DER to cert: {:?}", e)))?;
        let mut common_names = HashSet::new();
        let subject = cert0
            .subject_name()
            .entries_by_nid(Nid::COMMONNAME)
            .next()
            .and_then(|x| x.data().as_utf8().ok());
        if let Some(x) = subject {
            common_names.insert((&x).to_lowercase());
        }
        if let Some(names) = cert0.subject_alt_names() {
            for alt in names {
                if let Some(x) = alt.dnsname() {
                    common_names.insert(x.to_lowercase());
                }
            }
        }

        // Encode everything
        let mut common_name_vals = Vec::with_capacity(common_names.len());
        for common_name in common_names {
            if let Ok(x) = Value::from_bytes(common_name.as_bytes()) {
                common_name_vals.push(x);
            } else {
                return Err(Error::InvalidInput(format!(
                    "common names has illegal chars: {:?}",
                    common_name
                )));
            }
        }
        let key_b58 = Value::from_bytes(bs58::encode(key_der).into_string().as_bytes()).unwrap();
        let chain_b58 = chain_ders
            .iter()
            .map(|x| Value::from_bytes(bs58::encode(x).into_string().as_bytes()).unwrap())
            .collect();

        Ok(SslCert {
            common_names: common_name_vals,
            key: key_b58,
            certs: chain_b58,
        })
    }
}

impl fmt::Debug for SslCert {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SslCert(")?;
        for hn in self.common_names.iter() {
            write!(f, "{}", hn.as_str())?;
        }
        write!(f, ")")?;
        Ok(())
    }
}
