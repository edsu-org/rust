use crate::prelude::*;
use openssl::ssl::{SslConnector, SslMethod};
use std::{
    collections::HashMap,
    convert,
    hash::BuildHasher,
    io::{self, Read, Write},
    net::{TcpStream, ToSocketAddrs},
    time::{self, Duration, Instant},
};

#[derive(Debug)]
pub enum Error {
    NotConfigured,
    Io(io::Error),
    Ssl(String),
    ResponseHttp,
    Service(String),
}

impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

fn escape_json(raw_text: &str) -> String {
    let mut text = String::with_capacity(raw_text.len() * 2);
    for ch in raw_text.chars() {
        match ch {
            '"' => text.push_str(r#"\""#),
            '\\' => text.push_str(r#"\\"#),
            '\n' => text.push_str(r#"\n"#),
            '\t' => text.push_str(r#"\t"#),
            c if c.is_control() => (),
            c => text.push(c),
        }
    }
    text
}

pub fn send_email<S: BuildHasher>(
    email_service: &EmailService,
    template: &EmailTemplate,
    to: &str,
    args: &HashMap<String, String, S>,
) -> Result<(), Error> {
    // Some common values
    let connector_builder = SslConnector::builder(SslMethod::tls()).unwrap();
    let connector = connector_builder.build();

    match email_service {
        // Postmark
        EmailService::Postmark { secret } => {
            // Encode the HTTP request
            let mut args_enc = "{".to_owned();
            for (i, (k, v)) in args.iter().enumerate() {
                if i != 0 {
                    args_enc.push(',')
                }
                args_enc.push_str(&format!(r#""{}":"{}""#, escape_json(k), escape_json(v)));
            }
            args_enc.push_str("}");
            let template_id = template.id.parse::<u32>().map_err(|_| {
                Error::Service(format!(
                    "Postmark template IDs are numeric: {:?}",
                    template.id
                ))
            })?;
            let json = format!(
                r#"{{"From":"{}","To":"{}","TemplateId":{},
                                   "TemplateModel": {}}}"#,
                &escape_json(&template.from_addr),
                &escape_json(to),
                template_id,
                args_enc
            );
            let http = format!(
                "POST /email/withTemplate/ HTTP/1.1\r\n\
                 Host: api.postmarkapp.com\r\n\
                 Accept: application/json\r\n\
                 Connection: close\r\n\
                 Content-Type: application/json\r\n\
                 X-Postmark-Server-Token: {}\r\n\
                 Content-Length: {}\r\n\r\n",
                secret,
                json.len()
            );

            // Send it
            let timeout = time::Duration::new(EMAIL_SEND_TIMEOUT_S, 0);
            let api_addr = "api.postmarkapp.com:443"
                .to_socket_addrs()
                .unwrap()
                .next()
                .unwrap();
            let stream = TcpStream::connect_timeout(&api_addr, timeout)?;
            stream.set_read_timeout(Some(timeout))?;
            stream.set_write_timeout(Some(timeout))?;
            let mut ssl = connector
                .connect("api.postmarkapp.com", stream)
                .map_err(|e| Error::Ssl(format!("SSL error: {:?}", e)))?;
            ssl.write_all(http.as_bytes())?;
            ssl.write_all(json.as_bytes())?;

            // Interpret the response
            let mut resp = String::new();
            ssl.read_to_string(&mut resp)?;
            let payload = resp.split("\r\n\r\n").nth(1).ok_or(Error::ResponseHttp)?;
            if resp.find("200 OK").is_some() && payload.find(r#""ErrorCode":0"#).is_some() {
                Ok(())
            } else {
                Err(Error::Service(payload.to_owned()))
            }
        }
    }
}

#[derive(Clone)]
pub struct EmailAlarm {
    pub service: EmailService,
    pub template: EmailTemplate,
    pub addr: String,
    pub limit_s: u64,
    pub last_sent: Instant,
}

impl EmailAlarm {
    pub fn send(&mut self, txt: &str) {
        // Limit how many emails get sent
        if Instant::now() - Duration::new(self.limit_s, 0) < self.last_sent {
            return;
        }

        let mut args = HashMap::new();
        args.insert("log".to_owned(), txt.to_owned());
        send_email(&self.service, &self.template, &self.addr, &args).ok();
    }
}

#[derive(Clone)]
pub enum EmailService {
    Postmark { secret: String },
}

#[derive(Clone)]
pub struct EmailTemplate {
    pub id: String,
    pub from_addr: String,
    pub from_name: String,
    pub subject: String,
}
